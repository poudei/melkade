
/* 2014.07.01 */

CREATE TABLE adv_estates
(
  id bigint NOT NULL,
  type integer,
  request integer,
  area numeric(7,2),
  metrage numeric(14,2),
  beds integer,
  ex_price numeric(14,2),
  state bigint,
  city bigint,
  street character varying(200),
  longitude double precision,
  latitude double precision,
  phones character varying(100),
  cellphone character varying(11),
  is_agent boolean,
  creation_date timestamp without time zone NOT NULL,
  modified_date timestamp without time zone,
  last_modified_by bigint,
  deleted boolean NOT NULL,
  status integer,
  user_id bigint,
  neighbor_id bigint,
  price numeric(16,2),
  year_built integer,
  CONSTRAINT dev_estates_id_pkey PRIMARY KEY (id )
)

CREATE TABLE subscriptions (
    id bigint NOT NULL,
    user_id bigint,
    start_date date,
    end_date date,
    bill_date date NOT NULL,
    bill_amount integer NOT NULL,
    ads_number integer,
    used_ads integer,
    plan integer,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    transaction_no character varying(15),
    deleted boolean NOT NULL
);

/* 2014.07.20 */

--
-- Name: oauth_client; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_client (
    client_id VARCHAR(80) NOT NULL,
    client_secret VARCHAR(80) NOT NULL,
    name varchar(50) NULL,
    redirect_uri VARCHAR(2000) NOT NULL
    );


ALTER TABLE public.oauth_client OWNER TO postgres;

--
-- Name: oauth_access_token ; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_access_token (
    id serial NOT NULL,
    token VARCHAR(40) NOT NULL,
    token_secret VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );

ALTER TABLE public.oauth_access_token OWNER TO postgres;

--
-- Name: oauth_authorization_code; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_authorization_code (
    authorization_code VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    redirect_uri VARCHAR(2000),
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );

ALTER TABLE public.oauth_authorization_code OWNER TO postgres;

--
-- Name: oauth_refresh_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_refresh_token (
    refresh_token VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );

ALTER TABLE public.oauth_refresh_token OWNER TO postgres;

