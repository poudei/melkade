--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agencies; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE agencies (
    id bigint NOT NULL,
    "name" character varying(200) NOT NULL,
    phones character varying(100) NOT NULL,
    featured integer,
    longitude double precision,
    latitude double precision,
    "state" bigint,
    city bigint,
    street character varying(100),
    bystreet character varying(100),
    lane character varying(100),
    "number" integer,
    postal_code character(10),
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    deleted boolean NOT NULL,
    last_modified_by bigint,
    manager_id bigint,
    manager_name character varying(200),
    origin_id character varying(200),
    origin_address character varying(200),
    neighbor_id bigint
);


ALTER TABLE public.agencies OWNER TO postgres;

--
-- Name: agents; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE agents (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    occupation_type integer,
    agency_id bigint,
    agency_branch integer,
    start_date date,
    end_date date,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL,
    manager_id bigint,
    featured integer,
    status numeric(4,0),
    agency_confirm numeric(1,0)
);


ALTER TABLE public.agents OWNER TO postgres;

--
-- Name: bills; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bills (
    id bigint NOT NULL,
    amount integer NOT NULL,
    bill_date date NOT NULL,
    payer_id bigint NOT NULL,
    transaction_no character varying(15),
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL
);


ALTER TABLE public.bills OWNER TO postgres;

--
-- Name: bookmarks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bookmarks (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    estate_id bigint NOT NULL,
    creation_date timestamp without time zone,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean
);


ALTER TABLE public.bookmarks OWNER TO postgres;

--
-- Name: deals; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE deals (
    id bigint NOT NULL,
    agency_id bigint NOT NULL,
    agent_id bigint NOT NULL,
    manager_id bigint,
    estate_id bigint NOT NULL,
    price integer NOT NULL,
    unit_price integer NOT NULL,
    rent integer NOT NULL,
    deposit integer NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL
);


ALTER TABLE public.deals OWNER TO postgres;

--
-- Name: adv_estates; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE adv_estates (
    id bigint NOT NULL,
    "type" integer,
    request integer,
    area numeric(7,2),
    metrage numeric(14,2),
    beds integer,
    ex_price numeric(14,2),
    "state" bigint,
    city bigint,
    street character varying(200),
    longitude double precision,
    latitude double precision,
    phones character varying(100),
    cellphone character varying(11),
    is_agent boolean,
    status  integer,
    user_id bigint,
    neighbor_id bigint,
    price numeric(16,2),
    year_built integer,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL,
);


ALTER TABLE public.adv_estates OWNER TO postgres;


--
-- Name: devices; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devices (
    device_id character varying(225),
    version integer,
    "type" character varying(225),
    brand character varying(225),
    model character varying(225),
    manufacturer character varying(225),
    register_date timestamp without time zone
);


ALTER TABLE public.devices OWNER TO postgres;

--
-- Name: estate_histories; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estate_histories (
    id bigint NOT NULL,
    estate_id bigint NOT NULL,
    user_id bigint NOT NULL,
    modified_date timestamp without time zone
);


ALTER TABLE public.estate_histories OWNER TO postgres;


--
-- Name: estate_photos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estate_photos (
    id bigint NOT NULL,
    estate_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    deleted boolean NOT NULL,
    last_modified_by bigint,
    image character varying(250) NOT NULL,
    "type" integer
);


ALTER TABLE public.estate_photos OWNER TO postgres;

--
-- Name: estates; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estates (
    id bigint NOT NULL,
    operator_id bigint NOT NULL,
    street character varying(100) NOT NULL,
    bystreet character varying(100),
    stories integer,
    units integer,
    unit integer,
    direction integer[],
    toilets integer,
    ir_toilets integer,
    kitchen integer,
    cabinet integer[],
    floor integer[],
    parking integer,
    frontage integer[],
    owner_residance integer,
    plan integer,
    document integer,
    description character varying(1000),
    request_date date NOT NULL,
    expire_date date,
    creation_date timestamp without time zone,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean,
    owner_user_id bigint,
    "state" bigint NOT NULL,
    city bigint NOT NULL,
    postal_code character(10),
    year_built integer,
    baths integer,
    phones integer,
    deal integer[],
    permisson integer DEFAULT 0,
    sides integer,
    due_date date,
    beds integer,
    address_visible integer NOT NULL,
    location_visible integer NOT NULL,
    access integer[],
    building integer,
    age integer,
    quality integer[],
    storey integer,
    kitchen_furnishment integer,
    living_furnished integer,
    beds_furnished integer,
    airconditioner integer[],
    facilities integer[],
    sport_facilities integer[],
    equipments integer[],
    securities integer[],
    longitude double precision,
    latitude double precision,
    "number" integer,
    request integer,
    "type" integer,
    visit_description character varying(1000),
    featured integer,
    lane character varying(100),
    metrage numeric(14,2),
    balcony numeric(5,2),
    storage numeric(5,2),
    loan numeric(12,2),
    price numeric(16,2),
    ex_price numeric(14,2),
    "length" numeric(5,2),
    area numeric(7,2),
    width numeric(5,2),
    refinement numeric(5,2),
    neighbor_id bigint,
    status integer NOT NULL,
    advertised_by integer NOT NULL,
    agent_user_id bigint,
    agency_id bigint,
    wall integer[],
    ceiling_height numeric(6,2),
    code character(10)
);


ALTER TABLE public.estates OWNER TO postgres;

--
-- Name: neighbors; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE neighbors (
    id bigint NOT NULL,
    title character varying(255) NOT NULL,
    parent_id bigint DEFAULT 0,
    "path" character varying(100),
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL,
    description text,
    boundary_text text,
    center_text text,
    center_geo geometry(Point,4326),
    boundary_geo geometry(MultiPolygon,4326)
);


ALTER TABLE public.neighbors OWNER TO postgres;

--
-- Name: plan_users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plan_users (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    plan_id bigint NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    creation_date timestamp without time zone NOT NULL,
    deleted boolean NOT NULL
);


ALTER TABLE public.plan_users OWNER TO postgres;

--
-- Name: plans; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE plans (
    id bigint NOT NULL,
    bill_id bigint,
    "type" integer,
    start_date date,
    end_date date,
    months integer,
    users integer,
    regions integer[],
    amount integer,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    agency_id bigint,
    deleted boolean NOT NULL
);


ALTER TABLE public.plans OWNER TO postgres;

--
-- Name: queue_default_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE queue_default_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queue_default_id_seq OWNER TO postgres;

--
-- Name: queue_default; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE queue_default (
    id integer DEFAULT nextval('queue_default_id_seq'::regclass) NOT NULL,
    queue character varying(64) NOT NULL,
    "data" text NOT NULL,
    status smallint NOT NULL,
    created timestamp without time zone NOT NULL,
    scheduled timestamp without time zone NOT NULL,
    executed timestamp without time zone,
    finished timestamp without time zone,
    message character varying(256),
    trace text
);


ALTER TABLE public.queue_default OWNER TO postgres;

--
-- Name: requests; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE requests (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    district_id bigint,
    estate_type character varying(9) NOT NULL,
    request_type character varying(4) NOT NULL,
    storey integer,
    bedrooms integer,
    marital character varying(7) NOT NULL,
    metrage integer,
    parking integer,
    persons integer,
    rent integer NOT NULL,
    deposit integer NOT NULL,
    budget integer NOT NULL,
    comments character varying(1000) NOT NULL,
    request_date date,
    expire_date date,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL
);


ALTER TABLE public.requests OWNER TO postgres;


--
-- Name: session; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE session (
    id character varying NOT NULL,
    "name" character varying,
    modified bigint,
    lifetime integer,
    "data" text,
    user_id bigint
);


ALTER TABLE public.session OWNER TO postgres;


--
-- Name: user_clicks; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_clicks (
    id bigint NOT NULL,
    user_id bigint NOT NULL,
    agency_id bigint,
    estate_id bigint NOT NULL,
    view_date date,
    "value" numeric(14,2),
    deleted boolean NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint
);


ALTER TABLE public.user_clicks OWNER TO postgres;


--
-- Name: subscriptions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE subscriptions (
    id bigint NOT NULL,
    user_id bigint,
    start_date date,
    end_date date,
    bill_date date NOT NULL,
    bill_amount integer NOT NULL,
    ads_number integer,
    used_ads integer,
    plan integer,
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    transaction_no character varying(15),
    deleted boolean NOT NULL
);


ALTER TABLE public.subscriptions OWNER TO postgres;


--
-- Name: user_remember_me; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_remember_me (
    sid character varying NOT NULL,
    token character varying NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.user_remember_me OWNER TO postgres;

--
-- Name: oauth_client; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_client (
    client_id VARCHAR(80) NOT NULL,
    client_secret VARCHAR(80) NOT NULL,
    name varchar(50) NULL,
    redirect_uri VARCHAR(2000) NOT NULL
    );


ALTER TABLE public.oauth_client OWNER TO postgres;

--
-- Name: oauth_access_token ; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_access_token (
    id serial NOT NULL,
    token VARCHAR(40) NOT NULL,
    token_secret VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );

ALTER TABLE public.oauth_access_token OWNER TO postgres;

--
-- Name: oauth_authorization_code; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_authorization_code (
    authorization_code VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    redirect_uri VARCHAR(2000),
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );

ALTER TABLE public.oauth_authorization_code OWNER TO postgres;

--
-- Name: oauth_refresh_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oauth_refresh_token (
    refresh_token VARCHAR(40) NOT NULL,
    client_id bigint NOT NULL,
    user_id bigint NOT NULL,
    expires TIMESTAMP NOT NULL,
    scope VARCHAR(2000)
    );

ALTER TABLE public.oauth_refresh_token OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id bigint NOT NULL,
    firstname character varying(100),
    lastname character varying(100) NOT NULL,
    national_id character varying(10),
    cellphone character varying(11) NOT NULL,
    email character varying(40),
    username character varying(25),
    password character varying(128) NOT NULL,
    createdon date,
    status numeric(4,0),
    creation_date timestamp without time zone NOT NULL,
    modified_date timestamp without time zone,
    last_modified_by bigint,
    deleted boolean NOT NULL,
    amount integer,
    "type" numeric(4,0) DEFAULT 2501,
    gender numeric(4,0),
    phones character varying[],
    created_by_id bigint NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;



--
-- Name: agencies_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY agencies
    ADD CONSTRAINT agencies_id_pkey PRIMARY KEY (id);

--
-- Name: agents_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY agents
    ADD CONSTRAINT agents_id_pkey PRIMARY KEY (id);


--
-- Name: bills_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bills
    ADD CONSTRAINT bills_id_pkey PRIMARY KEY (id);


--
-- Name: bookmarks_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bookmarks
    ADD CONSTRAINT bookmarks_id_pkey PRIMARY KEY (id);



--
-- Name: cellphone; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT cellphone UNIQUE (cellphone);


--
-- Name: deals_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY deals
    ADD CONSTRAINT deals_id_pkey PRIMARY KEY (id);

--
-- Name: adv_estates_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY adv_estates
    ADD CONSTRAINT adv_estates_id_pkey PRIMARY KEY (id);

--
-- Name: device_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT device_id_pkey PRIMARY KEY (device_id);

--
-- Name: estate_histories_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estate_histories
    ADD CONSTRAINT estate_histories_id_pkey PRIMARY KEY (id);

--
-- Name: estate_photos_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estate_photos
    ADD CONSTRAINT estate_photos_id_pkey PRIMARY KEY (id);


--
-- Name: estates_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estates
    ADD CONSTRAINT estates_id_pkey PRIMARY KEY (id);


--
-- Name: neighbors_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY neighbors
    ADD CONSTRAINT neighbors_id_pkey PRIMARY KEY (id);


--
-- Name: plans_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plans
    ADD CONSTRAINT plans_id_pkey PRIMARY KEY (id);


--
-- Name: queue_default_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY queue_default
    ADD CONSTRAINT queue_default_id_pkey PRIMARY KEY (id);

--
-- Name: requests_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY requests
    ADD CONSTRAINT requests_id_pkey PRIMARY KEY (id);


--
-- Name: session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY session
    ADD CONSTRAINT session_pkey PRIMARY KEY (id);

--
-- Name: user_clicks_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_clicks
    ADD CONSTRAINT user_clicks_id_pkey PRIMARY KEY (id);


--
-- Name: user_plans_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY plan_users
    ADD CONSTRAINT user_plans_id_pkey PRIMARY KEY (id);

--
-- Name: subscriptions_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY subscriptions
    ADD CONSTRAINT subscriptions_id_pkey PRIMARY KEY (id);


--
-- Name: user_remember_me_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_remember_me
    ADD CONSTRAINT user_remember_me_pkey PRIMARY KEY (sid, token, user_id);

--
-- Name: oauth_client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_client
    ADD CONSTRAINT oauth_client_pkey PRIMARY KEY (client_id);

--
-- Name: oauth_access_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_access_token
    ADD CONSTRAINT oauth_access_token_pkey PRIMARY KEY (id);

--
-- Name: oauth_authorization_code_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_authorization_code
    ADD CONSTRAINT oauth_authorization_code_pkey PRIMARY KEY (authorization_code);

--
-- Name: oauth_refresh_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oauth_refresh_token
    ADD CONSTRAINT oauth_refresh_token_pkey PRIMARY KEY (refresh_token);


--
-- Name: users_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_id_pkey PRIMARY KEY (id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
