---
---     PostGIS settings
---

SELECT AddGeometryColumn('public','estates','coordinate',4326,'POINT',2);
SELECT AddGeometryColumn('public','neighbors','center_geo',4326,'POINT',2);
SELECT AddGeometryColumn('public','neighbors','boundary_geo',4326,'MULTIPOLYGON',2);

SELECT AddGeometryColumn('public','neighbors_22','center_geo',4326,'POINT',2);
SELECT AddGeometryColumn('public','neighbors_22','boundary_geo',4326,'MULTIPOLYGON',2);

CREATE INDEX idx_boundary_neighbor ON neighbors
USING GIST (boundary_geo);
CREATE INDEX idx_center_neighbor ON neighbors
USING GIST (center_geo);

CREATE INDEX idx_coordinate_estate ON estates
USING GIST (coordinate);

CREATE OR REPLACE FUNCTION estate_insert() RETURNS trigger AS $$
BEGIN
       NEW.coordinate = ST_GeomFromText('POINT (' || NEW.longitude || ' ' || NEW.latitude || ')', 4326);
       RETURN NEW;
END;$$
LANGUAGE plpgsql;

CREATE TRIGGER estate_insert BEFORE INSERT OR UPDATE ON estates FOR EACH ROW EXECUTE PROCEDURE estate_insert();


---
---	Run this after adding tehran data to ur DB
---



INSERT INTO neighbors(id, title, parent_id, center_text, boundary_text, center_geo, boundary_geo, creation_date, deleted, description)
SELECT mahallat_id, name,(region_id+6006000), center_geom, the_geom, ST_GeomFromText(center_geom, 4326),ST_GeomFromText( the_geom, 4326), date_trunc('second', current_timestamp), FALSE, history FROM map_tehran_neighbors
WHERE center_geom != 'POINT ()';


INSERT INTO neighbors(id, title, parent_id, center_text, boundary_text, center_geo, boundary_geo, creation_date, deleted, description)
SELECT id, name,null, center_geom, the_geom, null, null, date_trunc('second', current_timestamp), FALSE, '' FROM neighbors_22
WHERE center_geom != 'POINT ()';