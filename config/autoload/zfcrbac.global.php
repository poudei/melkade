<?php
/**
 * ZfcRbac Configuration
 *
 * If you have a ./config/autoload/ directory set up for your project, you can
 * drop this config file in it and change the values as you wish.
 */
$settings = array(
    /**
     * The default role that is used if no role is found from the
     * role provider.
     */
    'anonymousRole' => 'guest',

    /**
     * Flag: enable or disable the routing firewall.
     */
    'firewallRoute' => true,

    /**
     * Flag: enable or disable the controller firewall.
     */
    'firewallController' => true,

    /**
     * Set the view template to use on a 403 error.
     */
    'template' => 'error/403',

    /**
     * flag: enable or disable the use of lazy-loading providers.
     */
    'enableLazyProviders' => true,

    'firewalls' => array(
        'ZfcRbac\Firewall\Controller' => array(
        ),
        'ZfcRbac\Firewall\Route' => array(
            array('route' => 'user/login',   'roles' => 'guest'),
            array('route' => 'api/user_register', 'roles' => 'guest'),
            array('route' => 'api/neighbor', 'roles' => 'guest'),
            array('route' => 'api/estate', 'roles' => 'guest'),
            array('route' => 'api/*',   'roles' => 'member'),
            array('route' => 'admin/*', 'roles' => 'admin')
        ),
    ),

    'providers' => array(
        'ZfcRbac\Provider\Generic\Role\InMemory' => array(
            'roles' => array(
                'admin',
                'member' => array('admin'),
                'guest'  => array('member'),
            ),
        ),

        'ZfcRbac\Provider\Generic\Permission\InMemory' => array(
            'permissions' => array(
                'admin' => array('admin'),
            )
        ),
    ),

    /**
     * Set the identity provider to use. The identity provider must be retrievable from the
     * service locator and must implement \ZfcRbac\Identity\IdentityInterface.
     */
    'identity_provider' => 'zfcuser_auth_service'
);

/**
 * You do not need to edit below this line
 */
return array(
    'zfcrbac' => $settings
);
