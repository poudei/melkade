<?php
/**
 * Configuration file generated by ZFTool
 * The previous configuration file is stored in application.config.old
 *
 * @see https://github.com/zendframework/ZFTool
 */

return array(
    'modules' => array(
        'DoctrineModule',
        'DoctrineORMModule',
        'Session',
        'ZfcBase',
        'ZfcUser',
        'ZfcUserDoctrineORM',
        'GoalioRememberMe',
        'GoalioRememberMeDoctrineORM',
        //'GoalioMailService',
        //'GoalioForgotPassword',
        //'GoalioForgotPasswordDoctrineORM',
        'ZfcAdmin',
        'ZfcRbac',
        'Application',
        'WebinoImageThumb',
        'RealEstate',
        'SlmQueue',
        'SlmQueueDoctrine'
    ),

    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor'
        ),
        'config_glob_paths' => array('config/autoload/{,*.}{global,local}.php')
    ),

    'service_manager' => array(
        'aliases' => array(
            'Zend\Authentication\AuthenticationService' => 'zfcuser_auth_service',
            'doctrine.entitymanager.orm_default' => 'Doctrine\ORM\EntityManager',
            'Thumbnailer' => 'WebinoImageThumb'
        ),

        'factories' => array(
            'Hydrator' => function ($sm) {
                $hydrator = new RealEstate\Stdlib\Hydrator\DoctrineObject($sm->get('Doctrine\ORM\EntityManager'));
                $hydrator->addStrategy('creationDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
                $hydrator->addStrategy('modifiedDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
                $hydrator->addStrategy('startDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
                $hydrator->addStrategy('endDate', new \Application\Stdlib\Hydrator\Strategy\DateTimeStrategy());
                return $hydrator;
            },
        )
    ),
);
