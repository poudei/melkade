<?php

return array(
    'router' => array(
        'routes' => array(
            'api' => array(
                'child_routes' => array(
                    'agencies' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/agencies[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AgencyApi',
                            ),
                        ),
                    ),
                    'agency_agents' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/agencies/[:agency_id]/agents[/[:id]]',
                            'constraints' => array(
                                'agency_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AgencyAgent',
                            )
                        ),
                    ),
                    'agents' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/agents',
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\Agent',
                            ),
                        ),
                    ),
                    'deals' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/deals',
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\Deal',
                            ),
                        ),
                    ),
                    'agency_deals' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/agencies/[:agency_id]/deals[/[:id]]',
                            'constraints' => array(
                                'agency_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AgencyDeal',
                            ),
                        ),
                    ),
                    'estate-apis' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9,]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstateApi'
                            ),
                        ),
                    ),
                    'estates_nearby' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates/nearby',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstateApi',
                                'action' => 'nearby'
                            ),
                        ),
                    ),
                    'estate_photos' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates/[:estate_id]/photos[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'estate_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstatePhoto',
                            ),
                        ),
                    ),
                    
                    'estate_similars' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates/[:estate_id]/similars',
                            'constraints' => array(
                                'estate_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstateSimilars',
                            ),
                        ),
                    ),
                    'operatr' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/operators-statistic',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\OperatorEstates',
                            ),
                        ),
                    ),
                    'check_duplicate' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estate/check-duplicate',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\CheckDuplicate',
                                'action' => 'index'
                            ),
                        ),
                    ),
                    'requests' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users/[:user_id]/requests[/[:id]]',
                            'constraints' => array(
                                'user_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\Request',
                            ),
                        ),
                    ),
                    'user_request' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/requests[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\UserRequest',
                            ),
                        ),
                    ),
                    'neighbors' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/neighbors[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\Neighbor',
                            ),
                        ),
                    ),
                    'bookmarks' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/bookmarks[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\Bookmark',
                            ),
                        ),
                    ),
                    'estate_clicks' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates/[:estate_id]/clicks',
                            'constraints' => array(
                                'estate_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstateClicks',
                            ),
                        ),
                    ),
                    'user_clicks' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users/[:user_id]/clicks[/[:id]]',
                            'constraints' => array(
                                'user_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\UserClick',
                            )
                        ),
                    ),
                    'agency_clicks' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/agencies/[:agency_id]/clicks[/[:id]]',
                            'constraints' => array(
                                'agency_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AgencyClick',
                            )
                        ),
                    ),
                    'user_plans' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users/[:user_id]/plans[/[:id]]',
                            'constraints' => array(
                                'user_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\UserPlan',
                            )
                        ),
                    ),
                    'current_user_plan' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/plans[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\CurrentUserPlan',
                            )
                        ),
                    ),
                    'user_bill' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users/[:user_id]/bills[/[:id]]',
                            'constraints' => array(
                                'user_id' => '[0-9]+',
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\Bill',
                            )
                        ),
                    ),
                    
                    'estate_history' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates/[:estate_id]/history',
                            'constraints' => array(
                                'estate_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstateHistory',
                            ),
                        ),
                    ),
                    
                   /* 'test' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/test',
                            'constraints' => array(   
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\TestAgency',
                                'action' => 'test'
                            ),
                        ),
                    ),
                    */
               /*     'test_agency' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/test-agency',
                            'constraints' => array(
                                
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\TestAgency',
                                'action' => 'index'
                            ),
                        ),
                    ),
                */
                   /* 'convert_estate' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/convert-estate',
                            'constraints' => array(
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\TestAgency',
                                'action' => 'convert'
                            ),
                        ),
                    ),
                    */
                    'adv_estates' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/adv-estate[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9,]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AdvEstateApi'
                            ),
                        ),
                    ),
                    
                    'adv_photos' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/adv/[:adv_id]/photos[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                                'adv_id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AdvPhoto',
                            ),
                        ),
                    ),
                    
                    'my-estates' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/my-estates[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9,]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\MyEstate'
                            ),
                        ),
                    ),
                    
                    'agency_mobil' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/agencies-mobile[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\AgencyMobile',
                            )
                        ),
                    ),
                    'estate_mobil' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/estates-mobile[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'RealEstate\Controller\EstateMobile',
                            )
                        ),
                    ),
                ),
            ),
            
            'plan_buy' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/plans/buy',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\PlanBuy',
                        'action' => 'buy'
                    ),
                ),
            ),
            'checkout' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/plans/checkout',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\PlanBuy',
                        'action' => 'checkout'
                    ),
                ),
            ),
            'payment' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/plans/payment',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\PlanBuy',
                        'action' => 'payment'
                    ),
                ),
            ),
            'callback' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/plans/callback',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\PlanBuy',
                        'action' => 'callback'
                    ),
                ),
            ),
          
          
            
            'form' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin/pla/form',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\RegisterPlan',
                        'action' => 'form'
                    ),
                ),
            ),
            'save' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin/pla/save',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\RegisterPlan',
                        'action' => 'save'
                    ),
                ),
            ),
           
            
            'estate-index' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Estate',
                        'action' => 'index'
                    ),
                ),
            ),
            'estate-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/estates/[:id]/view[/[:title]]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                        'title' => '.*'
                    ),
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Estate',
                        'action' => 'view'
                    ),
                ),
            ),
            
            'agent-index' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/agents',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\AgentUser',
                        'action' => 'index'
                    ),
                ),
            ),
            
               'agent-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/agents/[:id]/estates',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\AgentUser',
                        'action' => 'view'
                    ),
                ),
            ),
            
            'agency-index' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/agencies',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Agency',
                        'action' => 'index'
                    ),
                ),
            ),
            
            'agency-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/agency/[:id]/estates',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Agency',
                        'action' => 'view'
                    ),
                ),
            ),
            
             'sitemap' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/sitemap.xml',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Sitemap',
                        'action' => 'index'
                    ),
                ),
            ),
            
            'rss' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/rss.xml',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Rss',
                        'action' => 'index'
                    ),
                ),
            ),
            
            'page' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/about',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Page',
                        'action' => 'about'
                    ),
                ),
            ),
            
            'com' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/a',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Page',
                        'action' => 'com'
                    ),
                ),
            ),
            
          /*  'chart-view' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/chart-view',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Chart',
                        'action' => 'view'
                    ),
                ),
            ),
            
            'chart-bilt' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/chart-bilt',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Chart',
                        'action' => 'bilt'
                    ),
                ),
            ),
            */
            'chart-php' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/charts',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Chart',
                        'action' => 'index'
                    ),
                ),
            ),
            
            'form-subscription' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin/plans/form',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Subscription',
                        'action' => 'form'
                    ),
                ),
            ),
            'save-subscription' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin/plans/save',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Subscription',
                        'action' => 'save'
                    ),
                ),
            ),
            
             'adv-index' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/advs',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\AdvEstate',
                        'action' => 'index'
                    ),
                ),
            ),
            'adv-view' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/advs/[:id]/view',
                    'constraints' => array(
                        'id' => '[0-9]+'
                    ),
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\AdvEstate',
                        'action' => 'view'
                    ),
                ),
            ),
                   
            'form-admin-subscription' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin/plan/form',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\AdminSubscription',
                        'action' => 'form'
                    ),
                ),
            ),
            'search-admin-subscription' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/admin/plan/search',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\AdminSubscription',
                        'action' => 'serach'
                    ),
                ),
            ),
            
            'form-user-subscription' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/plan',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\UserSubscription',
                        'action' => 'index'
                    ),
                ),
            ),
        ),
    ),
    
    
    'doctrine' => array(
        'driver' => array(
            'realestate_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/RealEstate/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'RealEstate\Entity' => 'realestate_entities',
                )
            ),
        ),
    ),
    
    'view_manager' => array(
        'template_map' => array(
            'realestate/email/notification'         => __DIR__ . '/../view/real-estate/email/notification.phtml',
            'real-estate/plan-buy/buy'              => __DIR__ . '/../view/real-estate/plan-buy/buy.phtml',
            'real-estate/plan-buy/checkout'         => __DIR__ . '/../view/real-estate/plan-buy/checkout.phtml',
            'real-estate/plan-buy/payment'          => __DIR__ . '/../view/real-estate/plan-buy/payment.phtml',
            'real-estate/plan-buy/callback'         => __DIR__ . '/../view/real-estate/plan-buy/callback.phtml',
            'real-estate/plan-buy/error'            => __DIR__ . '/../view/real-estate/plan-buy/error.phtml',
            'real-estate/plan-buy/success'          => __DIR__ . '/../view/real-estate/plan-buy/success.phtml',
            'real-estate/register-plan/form'        => __DIR__ . '/../view/real-estate/register-plan/form.phtml',
            'real-estate/register-plan/save'        => __DIR__ . '/../view/real-estate/register-plan/save.phtml',
            'real-estate/estate/index'              => __DIR__ . '/../view/real-estate/view-estate/index.phtml',
            'real-estate/estate/view'               => __DIR__ . '/../view/real-estate/view-estate/view.phtml',
            'real-estate/estate/apartment-view'     => __DIR__ . '/../view/real-estate/view-estate/apartment-view.phtml',
            'real-estate/estate/land-view'          => __DIR__ . '/../view/real-estate/view-estate/land-view.phtml',
            'real-estate/estate/office-view'        => __DIR__ . '/../view/real-estate/view-estate/office-view.phtml',
            'real-estate/estate/shop-view'          => __DIR__ . '/../view/real-estate/view-estate/shop-view.phtml',
            'real-estate/estate/villa-view'         => __DIR__ . '/../view/real-estate/view-estate/villa-view.phtml',
            'real-estate/estate/property-view'      => __DIR__ . '/../view/real-estate/view-estate/property-view.phtml',
            'real-estate/sitemap/index'             => __DIR__ . '/../view/real-estate/sitemap/index.phtml',
            'real-estate/page/about'                => __DIR__ . '/../view/real-estate/page/about.phtml',
            'real-estate/rss/index'                 => __DIR__ . '/../view/real-estate/rss/index.phtml',
            'real-estate/agent-user/index'          => __DIR__ . '/../view/real-estate/view-agent/index.phtml',
            'real-estate/agency/index'              => __DIR__ . '/../view/real-estate/view-agency/index.phtml',
            'real-estate/agent-user/view'           => __DIR__ . '/../view/real-estate/view-agent/view.phtml',
            'real-estate/agency/view'               => __DIR__ . '/../view/real-estate/view-agency/view.phtml',
            'real-estate/chart/index'               => __DIR__ . '/../view/real-estate/view-chart/index.phtml',
            'real-estate/chart/view'                => __DIR__ . '/../view/real-estate/view-chart/view.phtml',
            'real-estate/chart/bilt'                => __DIR__ . '/../view/real-estate/view-chart/bilt.phtml',
            'real-estate/view-chart/error'          => __DIR__ . '/../view/real-estate/view-chart/error.phtml',
            'real-estate/subscription/form'         => __DIR__ . '/../view/real-estate/subscription/form.phtml',
            'real-estate/subscription/save'         => __DIR__ . '/../view/real-estate/subscription/save.phtml',
            'real-estate/subscription/error'        => __DIR__ . '/../view/real-estate/subscription/error.phtml',
            'real-estate/estate/error'              => __DIR__ . '/../view/real-estate/view-estate/error.phtml',
            'real-estate/adv-estate/view'           => __DIR__ . '/../view/real-estate/view-adv/view.phtml',
            'real-estate/adv-estate/error'          => __DIR__ . '/../view/real-estate/view-adv/error.phtml',
            'real-estate/adv-estate/index'          =>  __DIR__ . '/../view/real-estate/view-adv/index.phtml',
            'real-estate/admin-subscription/form'   => __DIR__ . '/../view/real-estate/admin-subscription/form.phtml',
            'real-estate/admin-subscription/serach' => __DIR__ . '/../view/real-estate/admin-subscription/search.phtml',
            'real-estate/user-subscription/index'   => __DIR__ . '/../view/real-estate/user-subscription/index.phtml',
            'real-estate/page/com'                  => __DIR__ . '/../view/real-estate/page/com.phtml',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'RealEstate\Controller\EstateSimilars'  => 'RealEstate\Controller\EstateSimilarsController',
            'RealEstate\Controller\AgencyAgent'     => 'RealEstate\Controller\AgencyAgentController',
            'RealEstate\Controller\UserRequest'     => 'RealEstate\Controller\UserRequestController',
            'RealEstate\Controller\EstatePhoto'     => 'RealEstate\Controller\EstatePhotoController',
            'RealEstate\Controller\Bill'            => 'RealEstate\Controller\BillController',
            'RealEstate\Controller\AgencyDeal'      => 'RealEstate\Controller\AgencyDealController',
            'RealEstate\Controller\Bookmark'        => 'RealEstate\Controller\BookmarkController',
            'RealEstate\Controller\Neighbor'        => 'RealEstate\Controller\NeighborController',
            'RealEstate\Controller\Request'         => 'RealEstate\Controller\RequestController',
            'RealEstate\Controller\CurrentUserPlan' => 'RealEstate\Controller\CurrentUserPlanController',
            'RealEstate\Controller\AgencyApi'       => 'RealEstate\Controller\AgencyApiController',
            'RealEstate\Controller\PlanBuy'         => 'RealEstate\Controller\PlanBuyController',
            'RealEstate\Controller\EstateApi'       => 'RealEstate\Controller\EstateApiController',
            'RealEstate\Controller\Agent'           => 'RealEstate\Controller\AgentController',
            'RealEstate\Controller\Deal'            => 'RealEstate\Controller\DealController',
            'RealEstate\Controller\UserPlan'        => 'RealEstate\Controller\UserPlanController',
            'RealEstate\Controller\RegisterPlan'    => 'RealEstate\Controller\RegisterPlanController',
            'RealEstate\Controller\UserClick'       => 'RealEstate\Controller\UserClickController',
            'RealEstate\Controller\AgencyClick'     => 'RealEstate\Controller\AgencyClickController',
            'RealEstate\Controller\EstateClicks'    => 'RealEstate\Controller\EstateClicksController',
            'RealEstate\Controller\OperatorEstates' => 'RealEstate\Controller\OperatorEstatesController',
            'RealEstate\Controller\CheckDuplicate'  => 'RealEstate\Controller\CheckDuplicateController',
            'RealEstate\Controller\Estate'          => 'RealEstate\Controller\EstateController',
            'RealEstate\Controller\Sitemap'         => 'RealEstate\Controller\SitemapController',
            'RealEstate\Controller\Page'            => 'RealEstate\Controller\PageController',
            'RealEstate\Controller\Rss'             => 'RealEstate\Controller\RssController',
            'RealEstate\Controller\EstateHistory'   => 'RealEstate\Controller\EstateHistoryController',
            'RealEstate\Controller\AgentUser'       => 'RealEstate\Controller\AgentUserController',
            'RealEstate\Controller\Agency'          => 'RealEstate\Controller\AgencyController',
            'RealEstate\Controller\TestAgency'      => 'RealEstate\Controller\TestAgencyController',
            'RealEstate\Controller\AdvEstateApi'    => 'RealEstate\Controller\AdvEstateApiController',
            'RealEstate\Controller\Chart'           => 'RealEstate\Controller\ChartController',
            'RealEstate\Controller\Subscription'    => 'RealEstate\Controller\SubscriptionController',
            'RealEstate\Controller\MyEstate'        => 'RealEstate\Controller\MyEstateController',
            'RealEstate\Controller\AdvPhoto'        => 'RealEstate\Controller\AdvPhotoController',
            'RealEstate\Controller\AgencyMobile'    => 'RealEstate\Controller\AgencyMobileController',
            'RealEstate\Controller\AdvEstate'       => 'RealEstate\Controller\AdvEstateController',
            'RealEstate\Controller\EstateMobile'    => 'RealEstate\Controller\EstateMobileController',
            'RealEstate\Controller\AdminSubscription'=> 'RealEstate\Controller\AdminSubscriptionController',
            'RealEstate\Controller\UserSubscription' => 'RealEstate\Controller\UserSubscriptionController',
        ),
    ),
    'slm_queue' => array(
        'job_manager' => array(
            'factories' => array(
                'RealEstate\Job\PublishEstate' => function ($locator) {
                    return new RealEstate\Job\PublishEstate($locator->getServicelocator());
                }
            )
        ),
        'queue_manager'  => array(
            'factories'  => array(
                'estate' => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory'
            )
        )
    ),
);
