<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Bill
 *
 * @ORM\Table(name="bills")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Bill
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $amount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bill_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(true)
     */
    private $billDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="payer_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(true)
     */
    private $payerId;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_no", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":15}})
     */
    private $transactionNo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return Bill
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set billDate
     *
     * @param \DateTime $billDate
     * @return Bill
     */
    public function setBillDate($billDate)
    {
        $this->billDate = $billDate;

        return $this;
    }

    /**
     * Get billDate
     *
     * @return \DateTime 
     */
    public function getBillDate()
    {
        return $this->billDate;
    }

    /**
     * Set payerId
     *
     * @param integer $payerId
     * @return Bill
     */
    public function setPayerId($payerId)
    {
        $this->payerId = $payerId;

        return $this;
    }

    /**
     * Get payerId
     *
     * @return integer 
     */
    public function getPayerId()
    {
        return $this->payerId;
    }

    /**
     * Set transactionNo
     *
     * @param string $transactionNo
     * @return Bill
     */
    public function setTransactionNo($transactionNo)
    {
        $this->transactionNo = $transactionNo;

        return $this;
    }

    /**
     * Get transactionNo
     *
     * @return string 
     */
    public function getTransactionNo()
    {
        return $this->transactionNo;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Bill
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Bill
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Bill
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Bill
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

}
