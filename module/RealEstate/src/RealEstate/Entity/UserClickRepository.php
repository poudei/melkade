<?php
namespace RealEstate\Entity;

use Doctrine\ORM\EntityRepository;

class UserClickRepository extends EntityRepository
{
    public function sumClickAmount($userId, $agencyId)
    {
        $em = $this->getEntityManager();

        if ($userId == null) {
            $query = $em->createQuery("SELECT SUM(a.value) FROM RealEstate\Entity\UserClick a
                 WHERE a.agencyId = :agency_id");

            $query->setParameters(
                    array(
                        'agency_id' => $agencyId
                    )
            );
        }

        if ($agencyId == null) {
            $query = $em->createQuery("SELECT SUM(a.value) FROM RealEstate\Entity\UserClick a
                 WHERE a.userId = :user_id");

            $query->setParameters(
                    array(
                        'user_id' => $userId
                    )
            );
        }

        return (double) $query->getSingleScalarResult();
    }

}
