<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity
 *
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class User implements \ZfcUser\Entity\UserInterface, \ZfcRbac\Identity\IdentityInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(false)
     * @Form\Options({"label":"Firstname :"})
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(true)
     * @Form\Options({"label":"Lastname :"})
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="national_id", type="string", length=10, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     * @Form\Options({"label":"National id :"})
     */
    private $nationalId;

    /**
     * @var string
     *
     * @ORM\Column(name="cellphone", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^0[0-9]{10}$/"}})
     * @Form\Options({"label":"Cellphone :"})
     */
    private $cellphone;

    /**
     * @var integer
     * 
     * @ORM\Column(name="phones", type="intarray", precision=0, scale=0,  nullable=true , unique=false)
     * @Form\Required(false)
     * 
     */
    private $phones;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=25, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(false)
     * @Form\Validator({"name":"EmailAddress"})
     * @Form\Options({"label":"Email :"})
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=25, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":25}})
     * @Form\Exclude()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=25, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":25}})
     * @Form\Required(false)
     * @Form\Exclude()
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdon", type="date", precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Exclude()
     */
    private $createdon;

    /**
     *  @var float
     *
     * @ORM\Column(name="status",  type="decimal", nullable=true , precision=0, scale=0, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Exclude()
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer",precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Validator({"name":"Digits"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Required(false)
     * @Form\Exclude()
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="`type`", type="decimal", nullable=true , precision=0, scale=0, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     * @Form\Options({"label":"Type :"})
     * @Form\Exclude()
     */
    private $type = 2501;

    /**
     * @var float
     *
     * @ORM\Column(name="gender", type="decimal", nullable=true , precision=0, scale=0, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     * @Form\Options({"label":"Gender :"})
     */
    private $gender;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_by_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Exclude()
     */
    private $createdById;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set nationalId
     *
     * @param string $nationalId
     * @return User
     */
    public function setNationalId($nationalId)
    {
        $this->nationalId = $nationalId;

        return $this;
    }

    /**
     * Get nationalId
     *
     * @return string
     */
    public function getNationalId()
    {
        return $this->nationalId;
    }

    /**
     * Set cellphone
     *
     * @param string $cellphone
     * @return User
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    /**
     * Get cellphone
     *
     * @return string
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * Set phones
     *
     * @param intarray $phones
     * @return User
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return intarray 
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set createdon
     *
     * @param \DateTime $createdon
     * @return User
     */
    public function setCreatedon($createdon)
    {
        $this->createdon = $createdon;

        return $this;
    }

    /**
     * Get createdon
     *
     * @return \DateTime
     */
    public function getCreatedon()
    {
        return $this->createdon;
    }

    /**
     * Set status
     *
     * @param float $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return float 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return User
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set type
     *
     * @param float $type
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return float 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set gender
     *
     * @param float $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return float 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set createdById
     *
     * @param integer $createdById
     * @return User
     */
    public function setCreatedById($createdById)
    {
        $this->createdById = $createdById;

        return $this;
    }

    /**
     * Get createdById
     *
     * @return integer 
     */
    public function getCreatedById()
    {
        return $this->createdById;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return User
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return User
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return User
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return User
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Get displayName.
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->firstname;
    }

    /**
     * Set displayName.
     *
     * @param string $displayName
     * @return UserInterface
     */
    public function setDisplayName($displayName)
    {
        $this->firstname = $displayName;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->status;
    }

    /**
     * Set state.
     *
     * @param int $state
     * @return UserInterface
     */
    public function setState($state)
    {
        $this->status = $state;

        return $this;
    }

    public function getRoles()
    {
        return array(
            'member'
        );
    }

}
