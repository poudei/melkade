<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * District
 *
 * @ORM\Table(name="neighbors")
 * @ORM\Entity
 * 
 *  Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Neighbor
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="title", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":255}})
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="parent_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     */
    private $parentId = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="`path`", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(false)
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="center_text", type="text", nullable=true)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(false)
     */
    private $centerText;

    /**
     * @var string
     *
     * @ORM\Column(name="boundary_text", type="text", nullable=true)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(false)
     */
    private $boundaryText;

    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param integer $title
     * @return Neighbor
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return integer 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Neighbor
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Neighbor
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Neighbor
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
    
       /**
     * Set centerText
     *
     * @param string $centerText
     * @return Neighbor
     */
    public function setCenterText($centerText)
    {
        $this->centerText = $centerText;
    
        return $this;
    }

    /**
     * Get centerText
     *
     * @return string 
     */
    public function getCenterText()
    {
        return $this->centerText;
    }
    
    /**
     * Set boundaryText
     *
     * @param string $boundaryText
     * @return Neighbor
     */
    public function setBoundaryText($boundaryText)
    {
        $this->boundaryText = $boundaryText;
    
        return $this;
    }

    /**
     * Get boundaryText
     *
     * @return string 
     */
    public function getBoundaryText()
    {
        return $this->boundaryText;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Neighbor
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Neighbor
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Neighbor
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Neighbor
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

}
