<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Bookmark
 *
 * @ORM\Table(name="bookmarks")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="RealEstate\Entity\BookmarkRepository")
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 */
class Bookmark
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint",  precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="estate_id", type="bigint",precision=0, scale=0, nullable=false, unique=false)
     */
    private $estateId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime",precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude() 
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Bookmark
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set estateId
     *
     * @param integer $estateId
     * @return Bookmark
     */
    public function setEstateId($estateId)
    {
        $this->estateId = $estateId;

        return $this;
    }

    /**
     * Get estateId
     *
     * @return integer 
     */
    public function getEstateId()
    {
        return $this->estateId;
    }

    /**
     * Set markDate
     *
     * @param \DateTime $markDate
     * @return Bookmark
     */
    public function setMarkDate($markDate)
    {
        $this->markDate = $markDate;

        return $this;
    }

    /**
     * Get markDate
     *
     * @return \DateTime 
     */
    public function getMarkDate()
    {
        return $this->markDate;
    }
    
     /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Bookmark
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Bookmark
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Bookmark
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;
    
        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Bookmark
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

}
