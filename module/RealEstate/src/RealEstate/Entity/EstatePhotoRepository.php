<?php

namespace RealEstate\Entity;

use Doctrine\ORM\EntityRepository;

class EstatePhotoRepository extends EntityRepository
{
    public function estatePh($estateId)
    {
        $sql ='SELECT a FROM RealEstate\Entity\EstatePhoto a ' .
                'where  a.estateId = :estate_id AND a.type = 1';

        $params = array(
                    'estate_id' => $estateId
        );
        
        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
        
        $estatePhotos = $query->getResult();
        $array = array();

        foreach ($estatePhotos as $estatePhoto) {
            //$array['photos'][] = $hydrator->extract($estatePhoto);
            $array[] = $estatePhoto->getImage();
        }
        
        return ($array);
    }
    
    public function advPhoto($advEstateId)
    {
        $sql ='SELECT a FROM RealEstate\Entity\EstatePhoto a ' .
                'where  a.estateId = :estate_id  AND a.type = 0';

        $params = array(
                    'estate_id' => $advEstateId
        );
        
        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
        
        $estatePhotos = $query->getResult();
        $array = array();

        foreach ($estatePhotos as $estatePhoto) {
            //$array['photos'][] = $hydrator->extract($estatePhoto);
            $array[] = $estatePhoto->getImage();
        }
        
        return ($array);
    }

}
