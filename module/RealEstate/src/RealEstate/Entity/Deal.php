<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Deal
 *
 * @ORM\Table(name="deals")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Deal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="agency_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $agencyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="agent_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $agentId;

    /**
     * @var integer
     *
     * @ORM\Column(name="manager_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     */
    private $managerId;

    /**
     * @var integer
     *
     * @ORM\Column(name="estate_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Required(true)
     */
    private $estateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="price", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Required(true)
     * @Form\Validator({"name":"Digits"})
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit_price", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Required(true)
     * @Form\Validator({"name":"Digits"})
     */
    private $unitPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="rent", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $rent;

    /**
     * @var integer
     *
     * @ORM\Column(name="deposit", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Required(true)
     * @Form\Validator({"name":"Digits"})
     */
    private $deposit;
    

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $deleted;
    
    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agencyId
     *
     * @param integer $agencyId
     * @return Deal
     */
    public function setAgencyId($agencyId)
    {
        $this->agencyId = $agencyId;
    
        return $this;
    }

    /**
     * Get agencyId
     *
     * @return integer 
     */
    public function getAgencyId()
    {
        return $this->agencyId;
    }

    /**
     * Set agentId
     *
     * @param integer $agentId
     * @return Deal
     */
    public function setAgentId($agentId)
    {
        $this->agentId = $agentId;
    
        return $this;
    }

    /**
     * Get agentId
     *
     * @return integer 
     */
    public function getAgentId()
    {
        return $this->agentId;
    }

    /**
     * Set managerId
     *
     * @param integer $managerId
     * @return Deal
     */
    public function setManagerId($managerId)
    {
        $this->managerId = $managerId;
    
        return $this;
    }

    /**
     * Get managerId
     *
     * @return integer 
     */
    public function getManagerId()
    {
        return $this->managerId;
    }

    /**
     * Set estateId
     *
     * @param integer $estateId
     * @return Deal
     */
    public function setEstateId($estateId)
    {
        $this->estateId = $estateId;
    
        return $this;
    }

    /**
     * Get estateId
     *
     * @return integer 
     */
    public function getEstateId()
    {
        return $this->estateId;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Deal
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set unitPrice
     *
     * @param integer $unitPrice
     * @return Deal
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    
        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return integer 
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set rent
     *
     * @param integer $rent
     * @return Deal
     */
    public function setRent($rent)
    {
        $this->rent = $rent;
    
        return $this;
    }

    /**
     * Get rent
     *
     * @return integer 
     */
    public function getRent()
    {
        return $this->rent;
    }

    /**
     * Set deposit
     *
     * @param integer $deposit
     * @return Deal
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    
        return $this;
    }

    /**
     * Get deposit
     *
     * @return integer 
     */
    public function getDeposit()
    {
        return $this->deposit;
    }
    
    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Deal
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Deal
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Deal
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;
    
        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Deal
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}