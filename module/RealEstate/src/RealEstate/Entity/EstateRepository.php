<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;
use Doctrine\ORM\EntityRepository;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;
use RealEstate\Entity\Estate;
use RealEstate\Entity\User;

class EstateRepository extends EntityRepository
{

    public function getNearbyPoints($long = 0, $lat = 0, $raduis = 0)
    {
        //TODO : DO it by Doctrine !!!

        $conn = pg_pconnect("host=localhost port=5432 dbname=chardivari user=mohammad password=mohammad");

        $result = pg_query($conn, "
                            SELECT * FROM estate 
                            WHERE ST_DWithin(coordinate, 'POINT(" . floatval(51.3884) . "." . floatval(35.6945) . ")',500000)");
        while ($row = pg_fetch_assoc($result)) {
            $results[] = array(
                'id' => $row['id']
            );
        }
        empty($results) ? $results = array() : '';
        return $results;
    }

    public function getOperatorEstate($date = "now")
    {
        $date = new \DateTime($date);
        $date1 = $date->format('Y-m-d') . ' 0:0:0';
        $date2 = $date->format('Y-m-d') . ' 23:59:59';

        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT s FROM RealEstate\Entity\User s
                 WHERE s.type = 2503 ");

        $query->setParameters(array());

        $user = $query->getResult();

        $nUsers = array();
        foreach ($user as $use) {
            if (!empty($use)) {
                
            }
            $result = array(
                'name' => $use->getFirstname() . " " . $use->getLastname(),
                'id' => $use->getId(),
            );

            $id = $use->getId();

            $vilaCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.type = 202 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $vila = $vilaCount->getSingleScalarResult();

            $result['202'] = $vila;


            $apartmentCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.type = 201 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $apartment = $apartmentCount->getSingleScalarResult();

            $result['201'] = $apartment;


            $landCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.type = 203 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $land = $landCount->getSingleScalarResult();

            $result['203'] = $land;


            $officeCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.type = 204 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $office = $officeCount->getSingleScalarResult();

            $result['204'] = $office;


            $shopCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.type = 205 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $shop = $shopCount->getSingleScalarResult();

            $result['205'] = $shop;


            $propertyCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.type = 206 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $property = $propertyCount->getSingleScalarResult();

            $result['206'] = $property;


            $saleCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.request = 301 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $sale = $saleCount->getSingleScalarResult();

            $result['301'] = $sale;


            $salesCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.request = 302 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $sales = $salesCount->getSingleScalarResult();

            $result['302'] = $sales;


            $rentCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.request = 303 AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $rent = $rentCount->getSingleScalarResult();

            $result['303'] = $rent;

            $estatesCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.operatorId = $id AND a.creationDate BETWEEN '$date1' AND '$date2'");

            $estates = $estatesCount->getSingleScalarResult();

            $result['estateCount'] = $estates;

            $nUsers[$use->getId()] = $result;
        }

        return $nUsers;
    }

    public function getEstateCount($date = "now")
    {
        $date = new \DateTime($date);
        $date1 = $date->format('Y-m-d') . ' 0:0:0';
        $date2 = $date->format('Y-m-d') . ' 23:59:59';

        $em = $this->getEntityManager();

        $estateeCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Estate a
                 WHERE a.creationDate BETWEEN '$date1' AND '$date2'");

        $estatee = $estateeCount->getSingleScalarResult();

        return $estatee;
    }
    
    public function getSimilarEstates($estate)
    {
        $em = $this->getEntityManager();

        $estateId = $estate->getId();

        $request    = $estate->getRequest();
        $type       = $estate->getType();
        $neighborId = $estate->getNeighborId();

        $price = $estate->getPrice();
        if (!empty($price)) {
            $threshold = (int) $price / 5;
            $min       = $price - $threshold;
            $max       = $price + $threshold;
        }

        $conditions = array (
           "a.request    = $request",
           "a.type       = $type",
           "a.neighborId = $neighborId",
           "a.price BETWEEN '$min' AND '$max'"
        );
        
        do {
            $similarEstates = $em->createQuery("SELECT a FROM RealEstate\Entity\Estate a
                 WHERE a.id != $estateId AND " . join(" AND ", $conditions));

            $similarEstates->setMaxResults(10);

            $result = $similarEstates->getResult();
            
            array_pop($conditions);
        } while(count($result) <= 0 );

        return $result;
    }
    
    public function getSubscription(&$data) 
    {
        $em    = $this->getEntityManager();
        $advId = $data['advId'];

        $advEstate = $em->find('RealEstate\Entity\AdvEstate', $advId);

        if (!$advEstate instanceof AdvEstate) {
            return array('error' => 'Adv Not Found', 'code' => 404);
        }

        $advUser = $advEstate->getUserId();

        $now = new \DateTime('now');
        $end = $now->modify('-1 months')->format('Y-m-d');

        $estateCount = $em->createQuery("SELECT count(e) FROM RealEstate\Entity\Estate e
                WHERE e.deleted = false AND (e.ownerUserId ='$advUser' OR e.agencyId ='$advUser' OR 
                e.agentUserId = '$advUser') AND e.creationDate >= '$end' ");

        $estate = $estateCount->getSingleScalarResult();
        
        if ($estate >= 1) {
         
            $subscription = $em->createQuery("SELECT s FROM RealEstate\Entity\Subscription s
               WHERE s.userId ='$advUser' AND ( s.plan =3001 OR s.plan = 3002 OR
                   s.plan = 3003 OR s.plan = 3004 ) AND s.deleted = false 
                   AND s.startDate >= '$end' ORDER BY s.startDate DESC");

            $subscriptions = $subscription->getResult();
            
            if (empty($subscriptions)) {
                return array('error' => 'پلن خریداری نشده', 'code' => 404);
            }

            $usedAds = $subscriptions[0]->getUsedAds();
            $adsNumber = $subscriptions[0]->getAdsNumber();

            if ($usedAds >= $adsNumber) {
                return array('error' => 'تعداد آگهی های مجاز کاربر به پایان رسیده است', 'code' => 404);
            }

            $querys = $em->createQuery("UPDATE RealEstate\Entity\AdvEstate a
                    SET a.status = '1' 
                    WHERE a.userId = :user_id AND a.id = :adv_id AND a.status = '0'");

            $querys->setParameters(
                    array(
                        'user_id' => $advUser,
                        'adv_id' => $advId
                    )
            );

            $querye = $querys->execute();

            $plans = $em->createQuery("UPDATE RealEstate\Entity\Subscription s
                    SET s.usedAds = $usedAds+1
                    WHERE s.userId ='$advUser' AND ( s.plan =3001 OR s.plan = 3002 OR
                    s.plan = 3003 OR s.plan = 3004 ) AND  s.deleted = false 
                    AND s.startDate >= '$end' ");

            $plan = $plans->execute();

            $data['paid'] = 1;
            $data['requested'] = 1;
        } else {
            $querys = $em->createQuery("UPDATE RealEstate\Entity\AdvEstate a
                    SET a.status = '1' 
                    WHERE a.userId = :user_id AND a.id = :adv_id AND a.status = '0'");

            $querys->setParameters(
                    array(
                        'user_id' => $advUser,
                        'adv_id' => $advId
                    )
            );

            $querye = $querys->execute();
            
            $data['paid'] = 0;
            $data['requested'] = 0;
        }
    }

    public function getMobile( $agencyId, $agentUserId, $ownerUserId,  $neighborId)
    {
        $em = $this->getEntityManager();

        if (isset($agencyId) && !empty($agencyId)) {
            $agency = $em->getRepository('RealEstate\Entity\Agency')
                    ->find($agencyId);
            
            if ($agency != null) {
                $estateArray['agency'] = array(
                    'name' => $agency->getName(),
                    'phone' => $agency->getPhones()
                );
            }
        } 

        if (isset($agentUserId) && !empty($agentUserId)) {
            $agentUser = $em->getRepository('RealEstate\Entity\User')
                    ->find($agentUserId);

            if ($agentUser != null) {
                $estateArray['agentUser'] = array(
                    'id' => $agentUser->getId(),
                    'lastname'    => $agentUser->getLastname(),
                    'cellphone'   => $agentUser->getCellphone(),
                    'agentPhones' => $agentUser->getPhones()
                );
            }
        }
      
        if (isset($ownerUserId) && !empty($ownerUserId)) {         
            $ownerUser = $em->getRepository('RealEstate\Entity\User')
                    ->find($ownerUserId);

            if ($ownerUser != null) {
                $estateArray['ownerUser'] = array(
                    'lastname'  => $ownerUser->getLastname(),
                    'cellphone' => $ownerUser->getCellphone(),
                    'phone'     => $ownerUser->getPhones()
                );
            }
        }
       
        if (isset($neighborId) && !empty($neighborId)) {
            $neighbor = $em->getRepository('RealEstate\Entity\Neighbor')
                    ->find($neighborId);

            if ($neighbor != null) {
                $estateArray['neighbor'] = array(
                    'id'       => $neighbor->getId(),
                    'title'    => $neighbor->getTitle(),
                    'parentId' => $neighbor->getParentId()
                );
            }
        }
        
        return ($estateArray);
    }

}

