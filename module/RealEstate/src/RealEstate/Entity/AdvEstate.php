<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * AdvEstate
 *
 * @ORM\Table(name="adv_estates")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class AdvEstate
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="request",type="integer", precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $request;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="metrage", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $metrage;

    /**
     * @var integer
     *
     * @ORM\Column(name="beds", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $beds;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $price;
    
    /**
     * @var float
     *
     * @ORM\Column(name="ex_price", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $exPrice;
    
      /**
     * @var integer
     *
     * @ORM\Column(name="year_built", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $yearBuilt;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="bigint",  precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":2, "max":255}})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="city", type="bigint",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":2, "max":255}})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":200}})
     * @Form\Required(false)
     */
    private $street;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="decimal",precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="phones", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^0[0-9]{10}$/"}}) in alamto gereft gheyr faal
     * @Form\Required(false)
     */
    private $phones;

    /**
     * @var string
     *
     * @ORM\Column(name="cellphone", type="string", length=40, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^0[0-9]{10}$/"}})
     * @Form\Options({"label":"Cellphone :"})
     * @Form\Required(false)
     */
    private $cellphone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_agent", type="boolean", precision=0, scale=0, nullable=true, unique=false)
     */
    private $isAgent;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $status;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $userId;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="neighbor_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $neighborId; 
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return AdvEstate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set request
     *
     * @param integer $request
     * @return AdvEstate
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return integer 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set area
     *
     * @param string $area
     * @return AdvEstate
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return string 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set metrage
     *
     * @param string $metrage
     * @return AdvEstate
     */
    public function setMetrage($metrage)
    {
        $this->metrage = $metrage;

        return $this;
    }

    /**
     * Get metrage
     *
     * @return string 
     */
    public function getMetrage()
    {
        return $this->metrage;
    }

    /**
     * Set beds
     *
     * @param integer $beds
     * @return AdvEstate
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;

        return $this;
    }

    /**
     * Get beds
     *
     * @return integer 
     */
    public function getBeds()
    {
        return $this->beds;
    }
    
     /**
     * Set price
     *
     * @param float $price
     * @return AdvEstate
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }


    /**
     * Set exPrice
     *
     * @param string $exPrice
     * @return AdvEstate
     */
    public function setExPrice($exPrice)
    {
        $this->exPrice = $exPrice;

        return $this;
    }

    /**
     * Get exPrice
     *
     * @return string 
     */
    public function getExPrice()
    {
        return $this->exPrice;
    }
    
     /**
     * Set yearBuilt
     *
     * @param integer $yearBuilt
     * @return AdvEstate
     */
    public function setYearBuilt($yearBuilt)
    {
        $this->yearBuilt = $yearBuilt;

        return $this;
    }

    /**
     * Get yearBuilt
     *
     * @return integer 
     */
    public function getYearBuilt()
    {
        return $this->yearBuilt;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return AdvEstate
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param integer $city
     * @return AdvEstate
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return integer 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return AdvEstate
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return AdvEstate
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return AdvEstate
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set phones
     *
     * @param string $phones
     * @return AdvEstate
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return string 
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set cellphone
     *
     * @param string $cellphone
     * @return AdvEstate
     */
    public function setCellphone($cellphone)
    {
        $this->cellphone = $cellphone;

        return $this;
    }

    /**
     * Get cellphone
     *
     * @return string 
     */
    public function getCellphone()
    {
        return $this->cellphone;
    }

    /**
     * Set isAgent
     *
     * @param boolean $isAgent
     * @return AdvEstate
     */
    public function setIsAgent($isAgent)
    {
        $this->isAgent = $isAgent;

        return $this;
    }

    /**
     * Get isAgent
     *
     * @return boolean 
     */
    public function getIsAgent()
    {
        return $this->isAgent;
    }
    
     /**
     * Set status
     *
     * @param integer $status
     * @return AdvEstate
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
     /**
     * Set userId
     *
     * @param integer $userId
     * @return AdvEstate
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }
    
     /**
     * Set neighborId
     *
     * @param integer $neighborId
     * @return AdvEstate
     */
    public function setNeighborId($neighborId)
    {
        if ($neighborId == "") {
            $neighborId = null;
        }
        
        $this->neighborId = $neighborId;

        return $this;
    }

    /**
     * Get neighborId
     *
     * @return integer 
     */
    public function getNeighborId()
    {
        return $this->neighborId;
    }
    
    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return AdvEstate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return AdvEstate
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return AdvEstate
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return AdvEstate
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }


}
