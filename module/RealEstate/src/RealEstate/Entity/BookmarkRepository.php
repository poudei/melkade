<?php

namespace RealEstate\Entity;

use Doctrine\ORM\EntityRepository;

class BookmarkRepository extends EntityRepository
{
    public function hasBookmark($userId, $estateId)
    {
        $sql = 'SELECT s FROM RealEstate\Entity\Bookmark s ' .
                'WHERE s.userId = :user_id AND s.estateId = :estate_id AND s.deleted = false';
      
        $params = array(
            'estate_id' => $estateId,
            'user_id'   => $userId
        );

        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
        $query->setMaxResults(1);
        
        $result = $query->getOneOrNullResult();
        
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

}
