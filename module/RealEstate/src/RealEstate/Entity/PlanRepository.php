<?php

namespace RealEstate\Entity;

use Doctrine\ORM\EntityRepository;

class PlanRepository extends EntityRepository
{
    public function hasPlan($userId)
    {
        $sql = 'SELECT a FROM RealEstate\Entity\Plan a
            JOIN  RealEstate\Entity\PlanserU p 
            WITH a.planId = p.plan_id  '.
           'WHERE a.userId = :userId AND a.billId != null';
        
        $params = array(
            'user_id' => $userId
        );

        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);

        $result = $query->getResult();

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getUnlimitedPlans($userId)
    {
        $sql = 'SELECT a FROM RealEstate\Entity\Plan a
            JOIN  RealEstate\Entity\PlanUser p 
            WITH a.id = p.planId
            WHERE p.userId = :user_id AND a.billId IS NOT NULL
            AND a.type = 2801 AND a.endDate >= :now
            ORDER BY a.creationDate DESC ';
         
        $params = array(
            'user_id' => $userId,
            'now'     =>  new \DateTime('now')
        );

        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
               
        $result = $query->getResult();
        
        return $result;
    }
    
    public function getTotalCredit($userId)
    {
         $sql = 'SELECT SUM(a.amount) FROM RealEstate\Entity\Plan a
            JOIN  RealEstate\Entity\PlanUser p 
            WITH a.id = p.planId
            WHERE p.userId = :user_id AND a.billId IS NOT NULL
            AND a.type = 2802';
         
        $params = array(
            'user_id' => $userId
        );

        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
               
        return $query->getSingleScalarResult();
    }
}