<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * EstateHistorie
 *
 * @ORM\Table(name="estate_histories")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="estateId", column="id")
 */
class EstateHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
     private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="estate_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $estateId;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;


    /**
     * Set id
     *
     * @param integer $id
     * @return Folder
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estateId
     *
     * @param integer $estateId
     * @return EstateHistory
     */
    public function setEstateId($estateId)
    {
        $this->estateId = $estateId;

        return $this;
    }

    /**
     * Get estateId
     *
     * @return integer 
     */
    public function getEstateId()
    {
        return $this->estateId;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return EstateHistory
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return EstateHistory
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }
}
