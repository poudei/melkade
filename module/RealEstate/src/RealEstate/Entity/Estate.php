<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Estate
 *
 * @ORM\Table(name="estates")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="RealEstate\Entity\EstateRepository")
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Estate
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="operator_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Exclude()
     */
    private $operatorId;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_user_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $ownerUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="neighbor_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $neighborId;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="lane", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(false)
     */
    private $lane;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="request",type="integer", precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $request;

    /**
     * @var integer
     *
     * @ORM\Column(name="stories", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $stories;

    /**
     * @var integer
     *
     * @ORM\Column(name="storey", type="integer", precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $storey;

    /**
     * @var integer
     *
     * @ORM\Column(name="units", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $units;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $unit;

    /**
     * @var integer
     *
     * @ORM\Column(name="direction", type="intarray", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"}) 
     * @Form\Required(false)
     */
    private $direction = array();

    /**
     * @var float
     *
     * @ORM\Column(name="metrage", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $metrage;

    /**
     * @var integer
     *
     * @ORM\Column(name="beds", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $beds;

    /**
     * @var integer
     *
     * @ORM\Column(name="toilets", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $toilets;

    /**
     * @var integer
     *
     * @ORM\Column(name="ir_toilets", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $irToilets;

    /**
     * 
     * @var float
     *
     * @ORM\Column(name="balcony", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Float"})
     * @Form\Required(false)
     */
    private $balcony;

    /**
     * @var integer
     *
     * @ORM\Column(name="kitchen", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $kitchen;

    /**
     * @var integer
     *
     * @ORM\Column(name="cabinet", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $cabinet = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="floor", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $floor = array();

    /**
     * @var float
     *
     * @ORM\Column(name="storage", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $storage;

    /**
     * @var integer
     *
     * @ORM\Column(name="parking", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"}) 
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $parking;

    /**
     * @var integer
     *
     * @ORM\Column(name="frontage", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $frontage = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $age;

    /**
     * @var integer
     *
     * @ORM\Column(name="owner_residance", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $ownerResidance;

    /**
     * @var float
     *
     * @ORM\Column(name="loan", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $loan;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="ex_price", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $exPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="length", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $length;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $width;

    /**
     * @var float
     *
     * @ORM\Column(name="refinement", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $refinement;

    /**
     * @var integer
     *
     * @ORM\Column(name="plan", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $plan;

    /**
     * @var integer
     *
     * @ORM\Column(name="permisson", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $permisson;

    /**
     * @var integer
     *
     * @ORM\Column(name="document", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $document;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":1000}})
     * @Form\Required(false)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="decimal",precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $latitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(true)
     */
    private $requestDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Validator({"name":"Date"}) 
     * @Form\Required(false)
     */
    private $expireDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="advertised_by", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $advertisedBy;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="bigint",  precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":2, "max":255}})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="city", type="bigint",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":2, "max":255}})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]{10})$/"}})
     * @Form\Required(false)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $code;

    /**
     * @var integer
     *
     * @ORM\Column(name="address_visible", type="integer", precision=0, scale=0,  nullable=true , unique=false)
     *
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $addressVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="location_visible", type="integer", precision=0, scale=0, nullable=true , unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $locationVisible;

    /**
     * @var integer
     *
     * @ORM\Column(name="access", type="intarray", precision=0, scale=0, nullable=true , unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $access = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="building", type="integer",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $building;

    /**
     * @var integer
     * 
     * @ORM\Column(name="quality", type="intarray", precision=0, scale=0,  nullable=true , unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $quality = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="year_built", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $yearBuilt;

    /**
     * @var integer
     *
     * @ORM\Column(name="baths", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $baths;

    /**
     * @var integer
     *
     * @ORM\Column(name="kitchen_furnishment", type="integer", precision=0, scale=0, nullable=true , unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $kitchenFurnishment;

    /**
     * @var integer
     *
     * @ORM\Column(name="living_furnished", type="integer", precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $livingFurnished;

    /**
     * @var integer
     *
     * @ORM\Column(name="beds_furnished", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $bedsFurnished;

    /**
     * @var integer
     *
     * @ORM\Column(name="airconditioner", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $airconditioner = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="phones", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false) 
     */
    private $phones;

    /**
     * @var integer
     *
     * @ORM\Column(name="facilities", type="intarray",  precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $facilities = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="sport_facilities", type="intarray",  precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $sportFacilities = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="securities", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $securities = array();

    /**
     * @var integer
     *
     * @ORM\Column(name="due_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(false)
     */
    private $dueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="sides", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $sides;

    /**
     * @var integer
     *
     * @ORM\Column(name="deal", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"}) 
     * @Form\Required(false)
     */
    private $deal = array();

    /**
     * @var string
     *
     * @ORM\Column(name="visit_description", type="string", length=1000, nullable=true)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":1000}})
     * @Form\Required(false)
     */
    private $visitDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="equipments", type="intarray", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $equipments = array();

    /**
     * @var string
     *
     * @ORM\Column(name="bystreet", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(false)
     */
    private $bystreet;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="featured", type="integer",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $featured = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="agent_user_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $agentUserId;

    /**
     * @var integer
     *
     * @ORM\Column(name="agency_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $agencyId;

    /**
     * @var float
     *
     * @ORM\Column(name="ceiling_height", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $ceilingHeight;

    /**
     * @var integer
     *
     * @ORM\Column(name="wall", type="intarray", precision=0, scale=0,  nullable=true , unique=false)
     * @Form\Filter({"name":"Application\Filter\ArrayFilter"})
     * @Form\Required(false)
     */
    private $wall = array();
    
    /**
     * @var integer
     *
     * @ORM\Column(name="requested", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $requested;

    /**
     * @var integer
     *
     * @ORM\Column(name="paid", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $paid;

    /**
     * @var integer
     *
     * @ORM\Column(name="add_photo", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $addPhoto = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     *
     * @Form\Exclude()  
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * Set id
     * 
     * @param integer $id 
     * @return Folder
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set operatorId
     *
     * @param integer $operatorId
     * @return Estate
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;

        return $this;
    }

    /**
     * Get operatorId
     *
     * @return integer 
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * Set ownerUserId
     *
     * @param integer $ownerUserId
     * @return Estate
     */
    public function setOwnerUserId($ownerUserId)
    {
        $this->ownerUserId = $ownerUserId;

        return $this;
    }

    /**
     * Get ownerUserId
     *
     * @return integer 
     */
    public function getOwnerUserId()
    {
        return $this->ownerUserId;
    }

    /**
     * Set neighborId
     *
     * @param integer $neighborId
     * @return Estate
     */
    public function setNeighborId($neighborId)
    {
        $this->neighborId = $neighborId;

        return $this;
    }

    /**
     * Get neighborId
     *
     * @return integer 
     */
    public function getNeighborId()
    {
        return $this->neighborId;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Estate
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set bystreet
     *
     * @param string $bystreet
     * @return Estate
     */
    public function setBystreet($bystreet)
    {
        $this->bystreet = $bystreet;

        return $this;
    }

    /**
     * Get bystreet
     *
     * @return string 
     */
    public function getBystreet()
    {
        return $this->bystreet;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Estate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set request
     *
     * @param float $request
     * @return Estate
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return float 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set stories
     *
     * @param integer $stories
     * @return Estate
     */
    public function setStories($stories)
    {
        $this->stories = $stories;

        return $this;
    }

    /**
     * Get stories
     *
     * @return integer 
     */
    public function getStories()
    {
        return $this->stories;
    }

    /**
     * Set storey
     *
     * @param integer $storey
     * @return Estate
     */
    public function setStorey($storey)
    {
        $this->storey = $storey;

        return $this;
    }

    /**
     * Get storey
     *
     * @return integer 
     */
    public function getStorey()
    {
        return $this->storey;
    }

    /**
     *
     * @param integer $units
     * @return Estate
     */
    public function setUnits($units)
    {
        $this->units = $units;

        return $this;
    }

    /**
     * Get units
     *
     * @return integer 
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * Set unit
     *
     * @param integer $unit
     * @return Estate
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return integer 
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set direction
     *
     * @param intarray $direction
     * @return Estate
     */
    public function setDirection($direction)
    {
        $this->direction = (array) $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return intarray
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set metrage
     *
     * @param float $metrage
     * @return Estate
     */
    public function setMetrage($metrage)
    {
        $this->metrage = $metrage;

        return $this;
    }

    /**
     * Get metrage
     *
     * @return float 
     */
    public function getMetrage()
    {
        return $this->metrage;
    }

    /**
     * Set beds
     *
     * @param integer $beds
     * @return Estate
     */
    public function setBeds($beds)
    {
        $this->beds = $beds;

        return $this;
    }

    /**
     * Get beds
     *
     * @return integer 
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * Set toilets
     *
     * @param integer $toilets
     * @return Estate
     */
    public function setToilets($toilets)
    {
        $this->toilets = $toilets;

        return $this;
    }

    /**
     * Get toilets
     *
     * @return integer 
     */
    public function getToilets()
    {
        return $this->toilets;
    }

    /**
     * Set irToilets
     *
     * @param integer $irToilets
     * @return Estate
     */
    public function setIrToilets($irToilets)
    {
        $this->irToilets = $irToilets;

        return $this;
    }

    /**
     * Get irToilets
     *
     * @return integer 
     */
    public function getIrToilets()
    {
        return $this->irToilets;
    }

    /**
     * Set balcony
     *
     * @param float $balcony
     * @return Estate
     */
    public function setBalcony($balcony)
    {
        $this->balcony = $balcony;

        return $this;
    }

    /**
     * Get balcony
     *
     * @return float 
     */
    public function getBalcony()
    {
        return $this->balcony;
    }

    /**
     * Set kitchen
     *
     * @param string $kitchen
     * @return Estate
     */
    public function setKitchen($kitchen)
    {
        $this->kitchen = $kitchen;

        return $this;
    }

    /**
     * Get kitchen
     *
     * @return string 
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * Set cabinet
     *
     * @param intarray $cabinet
     * @return Estate
     */
    public function setCabinet($cabinet)
    {
        $this->cabinet = (array) $cabinet;

        return $this;
    }

    /**
     * Get cabinet
     *
     * @return intarray
     */
    public function getCabinet()
    {
        return $this->cabinet;
    }

    /**
     * Set floor
     *
     * @param intarray $floor
     * @return Estate
     */
    public function setFloor($floor)
    {
        $this->floor = (array) $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return intarray
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set storage
     *
     * @param float $storage
     * @return Estate
     */
    public function setStorage($storage)
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * Get storage
     *
     * @return float 
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * Set parking
     *
     * @param integer $parking
     * @return Estate
     */
    public function setParking($parking)
    {
        $this->parking = $parking;

        return $this;
    }

    /**
     * Get parking
     *
     * @return integer 
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set frontage
     *
     * @param intarray $frontage
     * @return Estate
     */
    public function setFrontage($frontage)
    {
        $this->frontage = (array) $frontage;

        return $this;
    }

    /**
     * Get frontage
     *
     * @return intarray 
     */
    public function getFrontage()
    {
        return $this->frontage;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return Estate
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set ownerResidance
     *
     * @param string $ownerResidance
     * @return Estate
     */
    public function setOwnerResidance($ownerResidance)
    {
        $this->ownerResidance = $ownerResidance;

        return $this;
    }

    /**
     * Get ownerResidance
     *
     * @return string 
     */
    public function getOwnerResidance()
    {
        return $this->ownerResidance;
    }

    /**
     * Set loan
     *
     * @param float $loan
     * @return Estate
     */
    public function setLoan($loan)
    {
        $this->loan = $loan;

        return $this;
    }

    /**
     * Get loan
     *
     * @return float 
     */
    public function getLoan()
    {
        return $this->loan;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Estate
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set exPrice
     *
     * @param float $exPrice
     * @return Estate
     */
    public function setExPrice($exPrice)
    {
        $this->exPrice = $exPrice;

        return $this;
    }

    /**
     * Get exPrice
     *
     * @return float 
     */
    public function getExPrice()
    {
        return $this->exPrice;
    }

    /**
     * Set area
     *
     * @param float $area
     * @return Estate
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return float 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set length
     *
     * @param float $length
     * @return Estate
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return float 
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set width
     *
     * @param float $width
     * @return Estate
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return float 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set refinement
     *
     * @param float $refinement
     * @return Estate
     */
    public function setRefinement($refinement)
    {
        $this->refinement = $refinement;

        return $this;
    }

    /**
     * Get refinement
     *
     * @return float 
     */
    public function getRefinement()
    {
        return $this->refinement;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Estate
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set permisson
     *
     * @param integer $permisson
     * @return Estate
     */
    public function setPermisson($permisson)
    {
        $this->permisson = $permisson;

        return $this;
    }

    /**
     * Get permisson
     *
     * @return integer 
     */
    public function getPermisson()
    {
        return $this->permisson;
    }

    /**
     * Set document
     *
     * @param integer $document
     * @return Estate
     */
    public function setDocument($document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     *
     * @return integer 
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Estate
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Agency
     */
    public function setLongitude($longitude)
    {
        $this->longitude = (double) $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return (double) $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Agency
     */
    public function setLatitude($latitude)
    {
        $this->latitude = (double) $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return (double) $this->latitude;
    }

    /**
     * Set requestDate
     *
     * @param \DateTime $requestDate
     * @return Estate
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * Get requestDate
     *
     * @return \DateTime 
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     * @return Estate
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;

        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime 
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * Set plan
     *
     * @param integer $plan
     * @return Estate
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return integer 
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set advertisedBy
     *
     * @param integer $advertisedBy
     * @return Estate
     */
    public function setAdvertisedBy($advertisedBy)
    {
        $this->advertisedBy = $advertisedBy;

        return $this;
    }

    /**
     * Get advertisedBy
     *
     * @return integer 
     */
    public function getAdvertisedBy()
    {
        return $this->advertisedBy;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Estate
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param integer $city
     * @return Estate
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return integer 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Estate
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Estate
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set addressVisible
     *
     * @param integer $addressVisible
     * @return Estate
     */
    public function setAddressVisible($addressVisible)
    {
        $this->addressVisible = $addressVisible;

        return $this;
    }

    /**
     * Get addressVisible
     *
     * @return integer 
     */
    public function getAddressVisible()
    {
        return $this->addressVisible;
    }

    /**
     * Set locationVisible
     *
     * @param integer $locationVisible
     * @return Estate
     */
    public function setLocationVisible($locationVisible)
    {
        $this->locationVisible = $locationVisible;

        return $this;
    }

    /**
     * Get locationVisible
     *
     * @return integer 
     */
    public function getLocationVisible()
    {
        return $this->locationVisible;
    }

    /**
     * Set access
     *
     * @param intarray $access
     * @return Estate
     */
    public function setAccess($access)
    {
        $this->access = (array) $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return intarray 
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * Set building
     *
     * @param integer $building
     * @return Estate
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return integer 
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set quality
     *
     * @param intarray $quality
     * @return Estate
     */
    public function setQuality($quality)
    {
        $this->quality = (array) $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return intarray 
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set yearBuilt
     *
     * @param integer $yearBuilt
     * @return Estate
     */
    public function setYearBuilt($yearBuilt)
    {
        $this->yearBuilt = $yearBuilt;

        return $this;
    }

    /**
     * Get yearBuilt
     *
     * @return integer 
     */
    public function getYearBuilt()
    {
        return $this->yearBuilt;
    }

    /**
     * Set baths
     *
     * @param integer $baths
     * @return Estate
     */
    public function setBaths($baths)
    {
        $this->baths = $baths;

        return $this;
    }

    /**
     * Get baths
     *
     * @return integer 
     */
    public function getBaths()
    {
        return $this->baths;
    }

    /**
     * Set kitchenFurnishment
     *
     * @param integer $kitchenFurnishment
     * @return Estate
     */
    public function setKitchenFurnishment($kitchenFurnishment)
    {
        $this->kitchenFurnishment = $kitchenFurnishment;

        return $this;
    }

    /**
     * Get kitchenFurnishment
     *
     * @return integer 
     */
    public function getKitchenFurnishment()
    {
        return $this->kitchenFurnishment;
    }

    /**
     * Set livingFurnished
     *
     * @param integer $livingFurnished
     * @return Estate
     */
    public function setLivingFurnished($livingFurnished)
    {
        $this->livingFurnished = (bool) $livingFurnished;

        return $this;
    }

    /**
     * Get livingFurnished
     *
     * @return integer 
     */
    public function getLivingFurnished()
    {
        return $this->livingFurnished;
    }

    /**
     * Set bedsFurnished
     *
     * @param integer $bedsFurnished
     * @return Estate
     */
    public function setBedsFurnished($bedsFurnished)
    {
        $this->bedsFurnished = (bool) $bedsFurnished;

        return $this;
    }

    /**
     * Get bedsFurnished
     *
     * @return integer 
     */
    public function getBedsFurnished()
    {
        return $this->bedsFurnished;
    }

    /**
     * Set airconditioner
     *
     * @param intarray $airconditioner
     * @return Estate
     */
    public function setAirconditioner($airconditioner)
    {
        $this->airconditioner = (array) $airconditioner;

        return $this;
    }

    /**
     * Get airconditioner
     *
     * @return intarray 
     */
    public function getAirconditioner()
    {
        return $this->airconditioner;
    }

    /**
     * Set phones
     *
     * @param integer $phones
     * @return Estate
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return integer 
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set facilities
     *
     * @param intarray $facilities
     * @return Estate
     */
    public function setFacilities($facilities)
    {
        $this->facilities = (array) $facilities;

        return $this;
    }

    /**
     * Get facilities
     *
     * @return intarray 
     */
    public function getFacilities()
    {
        return $this->facilities;
    }

    /**
     * Set sportFacilities
     *
     * @param intarray $sportFacilities
     * @return Estate
     */
    public function setSportFacilities($sportFacilities)
    {
        $this->sportFacilities = (array) $sportFacilities;

        return $this;
    }

    /**
     * Get sportFacilities
     *
     * @return intarray 
     */
    public function getSportFacilities()
    {
        return $this->sportFacilities;
    }

    /**
     * Set securities
     *
     * @param intarray $securities
     * @return Estate
     */
    public function setSecurities($securities)
    {
        $this->securities = (array) $securities;

        return $this;
    }

    /**
     * Get securities
     *
     * @return intarray 
     */
    public function getSecurities()
    {
        return $this->securities;
    }

    /**
     * Set dueDate
     *
     * @param \DateTime $dueDate
     * @return Estate
     */
    public function setDueDate($dueDate)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate
     *
     * @return \DateTime 
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set sides
     *
     * @param integer $sides
     * @return Estate
     */
    public function setSides($sides)
    {
        $this->sides = $sides;

        return $this;
    }

    /**
     * Get sides
     *
     * @return integer 
     */
    public function getSides()
    {
        return $this->sides;
    }

    /**
     * Set deal
     *
     * @param intarray $deal
     * @return Estate
     */
    public function setDeal($deal)
    {
        $this->deal = (array) $deal;

        return $this;
    }

    /**
     * Get deal
     *
     * @return intarray 
     */
    public function getDeal()
    {
        return $this->deal;
    }

    /**
     * Set visitDescription
     *
     * @param string $visitDescription
     * @return Estate
     */
    public function setVisitDescription($visitDescription)
    {
        $this->visitDescription = $visitDescription;

        return $this;
    }

    /**
     * Get visitDescription
     *
     * @return string 
     */
    public function getVisitDescription()
    {
        return $this->visitDescription;
    }

    /**
     * Set equipments
     *
     * @param intarray $equipments
     * @return Estate
     */
    public function setEquipments($equipments)
    {
        $this->equipments = (array) $equipments;

        return $this;
    }

    /**
     * Get equipments
     *
     * @return intarray 
     */
    public function getEquipments()
    {
        return $this->equipments;
    }

    /**
     * Set lane
     *
     * @param string $lane
     * @return Estate
     */
    public function setLane($lane)
    {
        $this->lane = $lane;

        return $this;
    }

    /**
     * Get lane
     *
     * @return string 
     */
    public function getLane()
    {
        return $this->lane;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Estate
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Estate
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set featured
     *
     * @param integer $featured
     * @return Estate
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return integer 
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set agentUserId
     *
     * @param integer $agentUserId
     * @return Estate
     */
    public function setAgentUserId($agentUserId)
    {
        $this->agentUserId = $agentUserId;

        return $this;
    }

    /**
     * Get agentUserId
     *
     * @return integer 
     */
    public function getAgentUserId()
    {
        return $this->agentUserId;
    }

    /**
     * Set agencyId
     *
     * @param integer $agencyId
     * @return Estate
     */
    public function setAgencyId($agencyId)
    {
        $this->agencyId = $agencyId;

        return $this;
    }

    /**
     * Get agencyId
     *
     * @return integer 
     */
    public function getAgencyId()
    {
        return $this->agencyId;
    }

    /**
     * Set ceilingHeight
     *
     * @param float $ceilingHeight
     * @return Estate
     */
    public function setCeilingHeight($ceilingHeight)
    {
        $this->ceilingHeight = $ceilingHeight;

        return $this;
    }

    /**
     * Get ceilingHeight
     *
     * @return float 
     */
    public function getCeilingHeight()
    {
        return $this->ceilingHeight;
    }

    /**
     * Set wall
     *
     * @param intarray $wall
     * @return Estate
     */
    public function setWall($wall)
    {
        $this->wall = (array) $wall;

        return $this;
    }

    /**
     * Get wall
     *
     * @return intarray 
     */
    public function getWall()
    {
        return $this->wall;
    }
    
     /**
     * Set requested
     *
     * @param integer $requested
     * @return Estate
     */
    public function setRequested($requested)
    {
        $this->requested = $requested;

        return $this;
    }

    /**
     * Get requested
     *
     * @return integer 
     */
    public function getRequested()
    {
        return $this->requested;
    }

    /**
     * Set paid
     *
     * @param integer $paid
     * @return Estate
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return integer 
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set addPhoto
     *
     * @param integer $addPhoto
     * @return Estate
     */
    public function setAddPhoto($addPhoto)
    {
        $this->addPhoto = $addPhoto;

        return $this;
    }

    /**
     * Get addPhoto
     *
     * @return integer 
     */
    public function getAddPhoto()
    {
        return $this->addPhoto;
    }


    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Estate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Estate
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Estate
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Estate
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

}
