<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * UserClick
 *
 * @ORM\Table(name="user_clicks")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="RealEstate\Entity\UserClickRepository")
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class UserClick
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(true)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="agency_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(true)
     */
    private $agencyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="estate_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(true)
     */
    private $estateId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="view_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(true)
     */
    private $viewDate;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name" : "Float"})
     * @Form\Required(false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserClick
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set agencyId
     *
     * @param integer $agencyId
     * @return UserClick
     */
    public function setAgencyId($agencyId)
    {
        $this->agencyId = $agencyId;

        return $this;
    }

    /**
     * Get agencyId
     *
     * @return integer 
     */
    public function getAgencyId()
    {
        return $this->agencyId;
    }

    /**
     * Set estateId
     *
     * @param integer $estateId
     * @return UserClick
     */
    public function setEstateId($estateId)
    {
        $this->estateId = $estateId;

        return $this;
    }

    /**
     * Get estateId
     *
     * @return integer 
     */
    public function getEstateId()
    {
        return $this->estateId;
    }

    /**
     * Set viewDate
     *
     * @param \DateTime $viewDate
     * @return UserClick
     */
    public function setViewDate($viewDate)
    {
        $this->viewDate = $viewDate;

        return $this;
    }

    /**
     * Get viewDate
     *
     * @return \DateTime 
     */
    public function getViewDate()
    {
        return $this->viewDate;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return UserClick
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return UserClick
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return UserClick
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return UserClick
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return UserClick
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

}
