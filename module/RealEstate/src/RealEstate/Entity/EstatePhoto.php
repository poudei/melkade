<?php
namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * EstatePhoto
 *
 * @ORM\Table(name="estate_photos")
 * @ORM\Entity
 * 
 * @ORM\Entity(repositoryClass="RealEstate\Entity\EstatePhotoRepository")
 * 
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="estateId", column="id")
 */
class EstatePhoto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="estate_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $estateId;

    
    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=250, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":250}})
     */
    private $image;
    
     /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $type;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;
    
    /**
     * Set id
     * 
     * @param integer $id 
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estateId
     *
     * @param integer $estateId
     * @return EstatePhoto
     */
    public function setEstateId($estateId)
    {
        $this->estateId = $estateId;
    
        return $this;
    }

    /**
     * Get estateId
     *
     * @return integer 
     */
    public function getEstateId()
    {
        return $this->estateId;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return EstatePhoto
     */
    public function setImage($image)
    {
        $this->image = $image;
    
        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }
    
     /**
     * Set type
     *
     * @param integer $type
     * @return Estate
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return EstatePhoto
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return EstatePhoto
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return EstatePhoto
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return EstatePhoto
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;
    
        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

}
