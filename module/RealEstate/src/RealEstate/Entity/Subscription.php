<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Subscription
 *
 * @ORM\Table(name="subscriptions")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Subscription
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $userId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(false)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(false)
     */
    private $endDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="bill_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(true)
     */
    private $billDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="bill_amount", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     */
    private $billAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="ads_number", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $adsNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="used_ads", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $usedAds;

    /**
     * @var integer
     *
     * @ORM\Column(name="plan", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $plan;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_no", type="string", length=15, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":15}})
     * @Form\Options({"label":"transaction_no :"})
     * @Form\Required(true)
     */
    private $transactionNo;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Subscription
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     * @return Subscription
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime 
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     * @return Subscription
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set billDate
     *
     * @param \DateTime $billDate
     * @return Subscription
     */
    public function setBillDate($billDate)
    {
        $this->billDate = $billDate;

        return $this;
    }

    /**
     * Get billDate
     *
     * @return \DateTime 
     */
    public function getBillDate()
    {
        return $this->billDate;
    }

    /**
     * Set billAmount
     *
     * @param integer $billAmount
     * @return Subscription
     */
    public function setBillAmount($billAmount)
    {
        $this->billAmount = $billAmount;

        return $this;
    }

    /**
     * Get billAmount
     *
     * @return integer 
     */
    public function getBillAmount()
    {
        return $this->billAmount;
    }

    /**
     * Set adsNumber
     *
     * @param integer $adsNumber
     * @return Subscription
     */
    public function setAdsNumber($adsNumber)
    {
        $this->adsNumber = $adsNumber;

        return $this;
    }

    /**
     * Get adsNumber
     *
     * @return integer 
     */
    public function getAdsNumber()
    {
        return $this->adsNumber;
    }

    /**
     * Set usedAds
     *
     * @param integer $usedAds
     * @return Subscription
     */
    public function setUsedAds($usedAds)
    {
        $this->usedAds = $usedAds;

        return $this;
    }

    /**
     * Get usedAds
     *
     * @return integer 
     */
    public function getUsedAds()
    {
        return $this->usedAds;
    }

    /**
     * Set plan
     *
     * @param integer $plan
     * @return Subscription
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return integer 
     */
    public function getPlan()
    {
        return $this->plan;
    }
    
     /**
     * Set transactionNo
     *
     * @param string $transactionNo
     * @return Subscription
     */
    public function setTransactionNo($transactionNo)
    {
        $this->transactionNo = $transactionNo;

        return $this;
    }

    /**
     * Get transactionNo
     *
     * @return string 
     */
    public function getTransactionNo()
    {
        return $this->transactionNo;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Subscription
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Subscription
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Subscription
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Subscription
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}
