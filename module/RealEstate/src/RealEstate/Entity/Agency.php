<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Agency
 *
 * @ORM\Table(name="agencies")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Agency
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`name`", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":200}})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phones", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^0[0-9]{10}$/"}}) in alamto gereft gheyr faal
     */
    private $phones;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="decimal", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $latitude;

    /**
     * @var integer
     *
     * @ORM\Column(name="featured", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $featured;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="bigint",  precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":2, "max":255}})
     * @Form\Required(false)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="city", type="bigint",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":2, "max":255}})
     * @Form\Required(false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=100, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(false)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="bystreet", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}})
     * @Form\Required(false)
     */
    private $bystreet;

    /**
     * @var string
     *
     * @ORM\Column(name="lane", type="string", length=100, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":100}}) 
     * @Form\Required(false)
     */
    private $lane;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="postal_code", type="string",  precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name" : "Regex", "options" : {"pattern" : "/^([0-9]+)(\.[0-9]+)?$/"}})
     * @Form\Required(false)
     */
    private $postalCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="manager_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Int"})
     * @Form\Required(false)
     * @Form\Validator({"name":"Digits"})
     */
    private $managerId;
    
     /**
     * @var string
     *
     * @ORM\Column(name="manager_name", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":200}})
     * @Form\Required(false)
     */
    private $managerName;
    
     /**
     * @var string
     *
     * @ORM\Column(name="origin_id", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":50}})
     * @Form\Required(false)
     */
    private $originId;
    
     /**
     * @var string
     *
     * @ORM\Column(name="origin_address", type="string", length=200, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":200}})
     * @Form\Required(false)
     */
    private $originAddress;

     /**
     * @var integer
     *
     * @ORM\Column(name="neighbor_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name":"Digits"})
     * @Form\Required(false)
     */
    private $neighborId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Agency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phones
     *
     * @param string $phones
     * @return Agency
     */
    public function setPhones($phones)
    {
        $this->phones = $phones;

        return $this;
    }

    /**
     * Get phones
     *
     * @return string 
     */
    public function getPhones()
    {
        return $this->phones;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return Agency
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return Agency
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set featured
     *
     * @param integer $featured
     * @return Agency
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Get featured
     *
     * @return integer 
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return Agency
     */
    public function setState($state)
    {
        $this->state = (int) $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set city
     *
     * @param integer $city
     * @return Agency
     */
    public function setCity($city)
    {
        $this->city = (int) $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return integer 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Agency
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set bystreet
     *
     * @param string $bystreet
     * @return Agency
     */
    public function setBystreet($bystreet)
    {
        $this->bystreet = $bystreet;

        return $this;
    }

    /**
     * Get bystreet
     *
     * @return string 
     */
    public function getBystreet()
    {
        return $this->bystreet;
    }

    /**
     * Set lane
     *
     * @param string $lane
     * @return Agency
     */
    public function setLane($lane)
    {
        $this->lane = $lane;

        return $this;
    }

    /**
     * Get lane
     *
     * @return string 
     */
    public function getLane()
    {
        return $this->lane;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Agency
     */
    public function setNumber($number)
    {
        $this->number = (int) $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     * @return Agency
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string 
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set managerId
     *
     * @param integer $managerId
     * @return Agency
     */
    public function setManagerId($managerId)
    {
        $this->managerId = $managerId;

        return $this;
    }

    /**
     * Get managerId
     *
     * @return integer 
     */
    public function getManagerId()
    {
        return $this->managerId;
    }
    
    /**
     * Set originId
     *
     * @param string $originId
     * @return Agency
     */
    public function setOriginId($originId)
    {
        $this->originId = $originId;

        return $this;
    }

    /**
     * Get originId
     *
     * @return string 
     */
    public function getOriginId()
    {
        return $this->originId;
    }
    
    /**
     * Set originAddress
     *
     * @param string $originAddress
     * @return Agency
     */
    public function setOriginAddres($originAddress)
    {
        $this->originAddress = $originAddress;

        return $this;
    }

    /**
     * Get originAddress
     *
     * @return string 
     */
    public function getOriginAddress()
    {
        return $this->originAddress;
    }
    
    /**
     * Set neighborId
     *
     * @param integer $neighborId
     * @return Agency
     */
    public function setNeighborId($neighborId)
    {
        $this->neighborId = $neighborId;

        return $this;
    }

    /**
     * Get neighborId
     *
     * @return integer 
     */
    public function getNeighborId()
    {
        return $this->neighborId;
    }
    
     /**
     * Set managerName
     *
     * @param string $managerName
     * @return Agency
     */
    public function setManagerName($managerName)
    {
        $this->managerName = $managerName;

        return $this;
    }

    /**
     * Get managerName
     *
     * @return string 
     */
    public function getManagerNames()
    {
        return $this->managerName;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Agency
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Agency
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;

        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Agency
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Agency
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;

        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

}
