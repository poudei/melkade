<?php

namespace RealEstate\Entity;

use Doctrine\ORM\EntityRepository;

class AgentRepository extends EntityRepository
{

    public function isUserAgent($userId)
    {
        $sql = 'SELECT count(a) FROM RealEstate\Entity\Agent a ' .
                'WHERE a.userId =:user_id';

        $params = array(
            'user_id' => $userId
        );

        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);

        $result = $query->getSingleScalarResult();

        if ($result > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getUserAgent($userId)
    {
        $sql = ' SELECT a FROM RealEstate\Entity\Agent a ' .
                'WHERE a.userId =:user_id AND a.status = 2901';

        $params = array(
            'user_id' => $userId
        );

        $query = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
        $query->setMaxResults(1);

        $result = $query->getResult();

        if (count($result) > 0 ) {
            return $result[0];
        } else {
            return null;    
        }
    }

}

