<?php

namespace RealEstate\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Request
 *
 * @ORM\Table(name="requests")
 * @ORM\Entity
 * 
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Request
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @Form\Exclude()
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="district_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * 
     */
    private $districtId;

    /**
     * @var string
     *
     * @ORM\Column(name="estate_type", type="string", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name" : "InArray", "options" : {"haystack" : {"apartment", "villa", "old", "land", "office", "shop"}}})
     */
    private $estateType;

    /**
     * @var string
     *
     * @ORM\Column(name="request_type", type="string", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name" : "InArray", "options" : {"haystack" : {"buy", "rent"}}})
     */
    private $requestType;

    /**
     * @var integer
     *
     * @ORM\Column(name="storey", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     * 
     */
    private $storey;

    /**
     * @var integer
     *
     * @ORM\Column(name="bedrooms", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $bedrooms;

    /**
     * @var string
     *
     * @ORM\Column(name="marital", type="string", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Required(true)
     * @Form\Validator({"name" : "InArray", "options" : {"haystack" : {"single", "married"}}})
     */
    private $marital;

    /**
     * @var integer
     *
     * @ORM\Column(name="metrage", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     * 
     */
    private $metrage;

    /**
     * @var integer
     *
     * @ORM\Column(name="parking", type="integer", precision=0, scale=0, nullable=true, unique=false)
     *
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     * 
     */
    private $parking;

    /**
     * @var integer
     *
     * @ORM\Column(name="persons", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $persons;

    /**
     * @var integer
     *
     * @ORM\Column(name="rent", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     *
     */
    private $rent;

    /**
     * @var integer
     *
     * @ORM\Column(name="deposit", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     * 
     */
    private $deposit;

    /**
     * @var integer
     *
     * @ORM\Column(name="budget", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(true)
     * 
     */
    private $budget;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=1000, precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":1000}})
     * @Form\Required(true)
     */
    private $comments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="request_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(false)
     */
    private $requestDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_date", type="date", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"Date"})
     * @Form\Required(false)
     */
    private $expireDate;

    
        /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $modifiedDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_modified_by", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $lastModifiedBy;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", precision=0, scale=0, nullable=false, unique=false)
     * @Form\Exclude()
     */
    private $deleted;
    
    
    /**
     * Set  id
     *
     * @param integer $id
     * @return Folder 
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Request
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set districtId
     *
     * @param integer $districtId
     * @return Request
     */
    public function setDistrictId($districtId)
    {
        $this->districtId = $districtId;
    
        return $this;
    }

    /**
     * Get districtId
     *
     * @return integer 
     */
    public function getDistrictId()
    {
        return $this->districtId;
    }

    /**
     * Set estateType
     *
     * @param string $estateType
     * @return Request
     */
    public function setEstateType($estateType)
    {
        $this->estateType = strtoupper($estateType);
    
        return $this;
    }

    /**
     * Get estateType
     *
     * @return string 
     */
    public function getEstateType()
    {
        return $this->estateType;
    }

    /**
     * Set requestType
     *
     * @param string $requestType
     * @return Request
     */
    public function setRequestType($requestType)
    {
        $this->requestType = strtoupper($requestType);
    
        return $this;
    }

    /**
     * Get requestType
     *
     * @return string 
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * Set storey
     *
     * @param integer $storey
     * @return Request
     */
    public function setStorey($storey)
    {
        $this->storey = $storey;
    
        return $this;
    }

    /**
     * Get storey
     *
     * @return integer 
     */
    public function getStorey()
    {
        return $this->storey;
    }

    /**
     * Set bedrooms
     *
     * @param integer $bedrooms
     * @return Request
     */
    public function setBedrooms($bedrooms)
    {
        $this->bedrooms = $bedrooms;
    
        return $this;
    }

    /**
     * Get bedrooms
     *
     * @return integer 
     */
    public function getBedrooms()
    {
        return $this->bedrooms;
    }

    /**
     * Set marital
     *
     * @param string $marital
     * @return Request
     */
    public function setMarital($marital)
    {
        $this->marital = strtoupper($marital);
    
        return $this;
    }

    /**
     * Get marital
     *
     * @return string 
     */
    public function getMarital()
    {
        return $this->marital;
    }

    /**
     * Set metrage
     *
     * @param integer $metrage
     * @return Request
     */
    public function setMetrage($metrage)
    {
        $this->metrage = $metrage;
    
        return $this;
    }

    /**
     * Get metrage
     *
     * @return integer 
     */
    public function getMetrage()
    {
        return $this->metrage;
    }

    /**
     * Set parking
     *
     * @param integer $parking
     * @return Request
     */
    public function setParking($parking)
    {
        $this->parking = $parking;
    
        return $this;
    }

    /**
     * Get parking
     *
     * @return integer 
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set persons
     *
     * @param integer $persons
     * @return Request
     */
    public function setPersons($persons)
    {
        $this->persons = $persons;
    
        return $this;
    }

    /**
     * Get persons
     *
     * @return integer 
     */
    public function getPersons()
    {
        return $this->persons;
    }

    /**
     * Set rent
     *
     * @param integer $rent
     * @return Request
     */
    public function setRent($rent)
    {
        $this->rent = $rent;
    
        return $this;
    }

    /**
     * Get rent
     *
     * @return integer 
     */
    public function getRent()
    {
        return $this->rent;
    }

    /**
     * Set deposit
     *
     * @param integer $deposit
     * @return Request
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;
    
        return $this;
    }

    /**
     * Get deposit
     *
     * @return integer 
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     * @return Request
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    
        return $this;
    }

    /**
     * Get budget
     *
     * @return integer 
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return Request
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    
        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set requestDate
     *
     * @param \DateTime $requestDate
     * @return Request
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;
    
        return $this;
    }

    /**
     * Get requestDate
     *
     * @return \DateTime 
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set expireDate
     *
     * @param \DateTime $expireDate
     * @return Request
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    
        return $this;
    }

    /**
     * Get expireDate
     *
     * @return \DateTime 
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Request
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modifiedDate
     *
     * @param \DateTime $modifiedDate
     * @return Request
     */
    public function setModifiedDate($modifiedDate)
    {
        $this->modifiedDate = $modifiedDate;
    
        return $this;
    }

    /**
     * Get modifiedDate
     *
     * @return \DateTime 
     */
    public function getModifiedDate()
    {
        return $this->modifiedDate;
    }

    /**
     * Set lastModifiedBy
     *
     * @param integer $lastModifiedBy
     * @return Request
     */
    public function setLastModifiedBy($lastModifiedBy)
    {
        $this->lastModifiedBy = $lastModifiedBy;
    
        return $this;
    }

    /**
     * Get lastModifiedBy
     *
     * @return integer 
     */
    public function getLastModifiedBy()
    {
        return $this->lastModifiedBy;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Request
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    
        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean 
     */
    public function getDeleted()
    {
        return $this->deleted;
    }
}