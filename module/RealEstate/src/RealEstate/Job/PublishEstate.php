<?php
namespace RealEstate\Job;

use SlmQueue\Job\AbstractJob;
use Zend\ServiceManager\ServiceManager;

class PublishEstate extends AbstractJob
{
    protected $serviceLocator;
    
    public function __construct(ServiceManager $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }
    
    public function execute()
    {
       //TODO change requerst
       return;
       $content = $this->getContent();
       $estate  = $content['estate'];
       
       $query   = "SELECT r FROM RealEstate\Entity\Request r 
           WHERE r.requestType = :request_type AND r.estateType = :estate_type";
       
       $em    = $this->serviceLocator->get('Doctrine\ORM\EntityManager');
       $query = $em->createQuery($query);

       $reqTyp = ($estate['requestType'] == 'RENTAL') ? 'RENT' : 'BUY';
       
       $query->setParameters(array(
            'request_type' => $reqTyp,
            'estate_type'  => $estate['estateType']
        ));

        $items = $query->getResult();
        
        $jobManager   = $this->serviceLocator->get('SlmQueue\Job\JobPluginManager');
        $queueManager = $this->serviceLocator->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('email');
        foreach ($items as $request) {
            $user = $em->find('RealEstate\Entity\User', $request->getUserId());
            
            if ($user) {
                $job = $jobManager->get('Application\Job\SendEmail');

                $job->setContent(array(
                    'template' => 'realestate/email/notification',
                    'subject'  => 'Notification',
                    'to_email' => $user->getEmail(),
                    'to_name'  => $user->getFirstName() . ' ' . $user->getLastName(),
                    'data' => array(
                        'estate' => $estate
                    )
                ));

                $queue->push($job);
            }
        }
    }
}