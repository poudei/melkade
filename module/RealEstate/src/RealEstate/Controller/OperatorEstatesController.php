<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\User;

class OperatorEstatesController extends AbstractRestfulController
{
    public function getList()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $this->identity();
        $date = $this->params()->fromQuery("date", null);
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        if ($user && $user->getType() == 2503 || $user->getType() == 2504) {

            $userCount = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\User a
                 WHERE a.type = 2503 ");

            $users = $userCount->getSingleScalarResult();

            $getOperatorEstates = $em->getRepository('\RealEstate\Entity\Estate')
                    ->getOperatorEstate($date);
            
            $getEstateCount = $em->getRepository('\RealEstate\Entity\Estate')
                    ->getEstateCount($date); 
            
            $return = array(
                'estatesCount'   => $getEstateCount,
                'operatorsCount' => $users,
                'items'          => array(),
            );
            
            foreach ($getOperatorEstates as $getOperatorEstate) {
                $return['items'][] = $getOperatorEstate;
            }
            return new JsonModel($return);
        }

        $this->getResponse()->setStatusCode(403);
        return new JsonModel(array('error' => 'Forbidden'));
    }

}
