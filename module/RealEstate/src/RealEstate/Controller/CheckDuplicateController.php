<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\User;

class CheckDuplicateController extends AbstractRestfulController
{
    public function indexAction()
    {
        return new JsonModel(array('items' => null));
        
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();

        $conditions = array();
        
        switch ((int) $data['type']) {
            case 201:

                if (!empty($data['street'])) {
                    $conditions['street'] = $data['street'];
                }
                if (!empty($data['lane'])) {
                    $conditions['lane'] = $data['lane'];
                }

                if (!empty($data['metrage'])) {
                    $conditions['metrage'] = $data['metrage'];
                }

                if (!empty($data['beds'])) {
                    $conditions['beds'] = $data['beds'];
                }

                if (!empty($data['storey'])) {
                    $conditions['storey'] = $data['storey'];
                }

                if (!empty($data['unit'])) {
                    $conditions['unit'] = $data['unit'];
                }

                break;

            case 202:
                
                if (!empty($data['street'])) {
                    $conditions['street'] = $data['street'];
                }

                if (!empty($data['bystreet'])) {
                    $conditions['bystreet'] = $data['bystreet'];
                }

                if (!empty($data['lane'])) {
                    $conditions['lane'] = $data['lane'];
                }

                if (!empty($data['area'])) {
                    $conditions['area'] = $data['area'];
                }

                if (!empty($data['number'])) {
                    $conditions['number'] = $data['number'];
                }
                
                if (!empty($data['units'])) {
                    $conditions['units'] = $data['units'];
                }
                
                if (!empty($data['stories'])) {
                    $conditions['stories'] = $data['stories'];
                }

            case 203:
                
                if (!empty($data['street'])) {
                    $conditions['street'] = $data['street'];
                }

                if (!empty($data['bystreet'])) {
                    $conditions['bystreet'] = $data['bystreet'];
                }

                if (!empty($data['lane'])) {
                    $conditions['lane'] = $data['lane'];
                }

                if (!empty($data['area'])) {
                    $conditions['area'] = $data['area'];
                }
               
                break;
            
             case 204:
                 
                if (!empty($data['street'])) {
                    $conditions['street'] = $data['street'];
                }

                if (!empty($data['bystreet'])) {
                    $conditions['bystreet'] = $data['bystreet'];
                }

                if (!empty($data['lane'])) {
                    $conditions['lane'] = $data['lane'];
                }

                if (!empty($data['unit'])) {
                    $conditions['unit'] = $data['unit'];
                }

                if (!empty($data['number'])) {
                    $conditions['number'] = $data['number'];
                }

                if (!empty($data['metrage'])) {
                    $conditions['metrage'] = $data['metrage'];
                }

                if (!empty($data['storey'])) {
                    $conditions['storey'] = $data['storey'];
                }
                
                 if (!empty($data['units'])) {
                    $conditions['units'] = $data['units'];
                }
                
                if (!empty($data['stories'])) {
                    $conditions['stories'] = $data['stories'];
                }
           
                break;
            
              case 205:
                  
                if (!empty($data['street'])) {
                    $conditions['street'] = $data['street'];
                }

                if (!empty($data['bystreet'])) {
                    $conditions['bystreet'] = $data['bystreet'];
                }

                if (!empty($data['lane'])) {
                    $conditions['lane'] = $data['lane'];
                }

                if (!empty($data['metrage'])) {
                    $conditions['metrage'] = $data['metrage'];
                }

                if (!empty($data['number'])) {
                    $conditions['number'] = $data['number'];
                }
                
                if (!empty($data['unit'])) {
                    $conditions['unit'] = $data['unit'];
                }
                
                if (!empty($data['storey'])) {
                    $conditions['storey'] = $data['storey'];
                }
               
                break;

            case 206:
                
                if (!empty($data['street'])) {
                    $conditions['street'] = $data['street'];
                }

                if (!empty($data['bystreet'])) {
                    $conditions['bystreet'] = $data['bystreet'];
                }

                if (!empty($data['lane'])) {
                    $conditions['lane'] = $data['lane'];
                }

                if (!empty($data['area'])) {
                    $conditions['area'] = $data['area'];
                }

                if (!empty($data['number'])) {
                    $conditions['number'] = $data['number'];
                }
              
               if (!empty($data['unit'])) {
                    $conditions['unit'] = $data['unit'];
                }
                
                if (!empty($data['storey'])) {
                    $conditions['storey'] = $data['storey'];
                }
                
                if (!empty($data['units'])) {
                    $conditions['units'] = $data['units'];
                }
              
                break;
        }
        
        $estates = $em->getRepository('RealEstate\Entity\Estate')
                ->findBy($conditions);

        $result = array();
        if (is_array($estates)) {
            foreach ($estates as $estate) {
                $estateArray = $this->extractAndFill($estate);
                $result[]    = $estateArray;
            }
        }
        
        if (count($result) > 0) {
            return new JsonModel(array('items' => $result));
        } else {
            return new JsonModel(array('items' => null));
        }
    }

}
