<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Deal;
//estefade nashode .. mamele ye agancy
class AgencyDealController extends AbstractRestfulController
{

    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $agencyId = $this->params()->fromRoute("agency_id");
        $filter   = $this->params()->fromQuery('filter', array());
        $user     = $this->identity();

        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $conditions = array('agencyId' => $agencyId);

        $query = $em->createQuery("SELECT a FROM RealEstate\Entity\Agent a
            where a.userId=:user_id AND a.agencyId=:agency_id");
        
        $query->setParameters(
              array(
                  'user_id' => $user->getId(),
                  'agency_id' => $agencyId
             )
        );

        $agents = $query->getResult();
        if (count($agents) < 1) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $agents[0];

        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Deal', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'price',
            'unitPrice',
            'rent',
            'deposit'                                
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        $query  = $qb->getQuery();
        $deals  = $query->getResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );
        
        foreach ($deals as $deal) {
            $dealArray         = $this->extractAndFill($deal);
            $return['items'][] = $dealArray;
        }
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $agencyId = $this->params()->fromRoute("agency_id", null);
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $query = $em->createQuery("SELECT a FROM RealEstate\Entity\Agent a
                where a.userId=:user_id AND a.agencyId=:agency_id");
      
        $query->setParameters(
             array(
                  'user_id' => $user->getId(),
                  'agency_id' => $agencyId
             )
        );

        $agents = $query->getResult();
        if (count($agents) < 1) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $agents[0];
        $deal  = $em->find('RealEstate\Entity\Deal', $id);

        if (!$deal instanceof Deal) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($deal->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        $array = $this->extractAndFill($deal);

        return new JsonModel($array);
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $query = $em->createQuery("SELECT a FROM RealEstate\Entity\Agent a
              where a.userId=:user_id AND a.agencyId=:agency_id");
        
        $query->setParameters(
               array(
                   'user_id' => $user->getId(),
                   'agency_id' => $agencyId
               )
        );

        $agents = $query->getResult();
        
        if (count($agents) < 1) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent   = $agents[0];
        $builder = new AnnotationBuilder();
        $deal    = new Deal();
        $form    = $builder->createForm($deal);

        $form->setHydrator($hydrator);
        $form->bind($deal);
        $form->setData($data);

        if ($form->isValid()) {
            $deal = $form->getData();

            $deal->setCreationDate(new \DateTime('now'));
            $deal->setModifiedDate(new \DateTime('now'));
            $deal->setDeleted(false);
            $deal->setAgencyId($agencyId);
            $deal->setAgentId($agent->getId());

            $em->persist($deal);
            $em->flush();

            return new JsonModel($this->extractAndFill($deal));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $query = $em->createQuery("SELECT a FROM RealEstate\Entity\Agent a
               where a.userId=:user_id AND a.agencyId=:agency_id");
        
        $query->setParameters(
                array(
                    'user_id' => $user->getId(),
                    'agency_id' => $agencyId
            )
        );
        
        $agents = $query->getResult();
        
        if (count($agents) < 1) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $agents[0];
        $deal  = $em->find('RealEstate\Entity\Deal', $id);

        if (!$deal instanceof Deal) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($deal->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $preData = $this->extract($deal);
        $data    = array_merge($preData, $data);
        
        $builde  = new AnnotationBuilder();
        $form    = $builde->createForm($deal);

        $form->setHydrator($hydrator);
        $form->bind($deal);
        $form->setData($data);

        if ($form->isValid()) {
            $deal = $form->getData();

            $deal->setModifiedDate(new \DateTime('now'));
            $deal->setLastModifiedBy($this->identity()->getId());
            $deal->setAgencyId($agencyId);

            $em->flush();

            return new JsonModel($this->extractAndFill($deal));
        } else {
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function delete($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $query = $em->createQuery("SELECT a FROM RealEstate\Entity\Agent a
               where a.userId=:user_id AND a.agencyId=:agency_id");
       
        $query->setParameters(
                array(
                    'user_id' => $user->getId(),
                    'agency_id' => $agencyId
             )
        );
        
        $agents = $query->getResult();
        
        if (count($agents) < 1) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $agents[0];
        $deal  = $em->find('RealEstate\Entity\Deal', $id);
        $deal->setAgencyId($agencyId);

        if (!$deal instanceof Deal) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($deal->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $deal->setDeleted(true);

        $em->flush();

        return new JsonModel(array('sucees' => true));
    }

}