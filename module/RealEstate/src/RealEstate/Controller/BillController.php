<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Bill;
// estefade nashode .. list pardakht
class BillController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServicelocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        $userId  = $this->params()->fromRoute("user_id");
        $user    = $em->find('RealEstate\Entity\User', $userId);
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }
        
        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Bill', 's');
      
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.payerId', $userId));
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'amount',
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);
        
        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        
        $query  = $qb->getQuery();
        $bills  = $query->getResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($bills as $bill) {
            $billArray         = $this->extractAndFill($bill);
            $return['items'][] = $billArray;
        }
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em     = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $userId = $this->params()->fromRoute("user_id");
        $user   = $em->find('RealEstate\Entity\User', $userId);
        
        if ($user== null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        } 
        
        $bill    = $em->find('RealEstate\Entity\Bill', $id);

        if (!$bill instanceof Bill) {
            $this->getResponce()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($bill->getPayerId() != $userId) {
            $this->getResponce()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }
        
        if ($bill->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $array = $this->extractAndFill($bill);

        return new JsonModel($array);
    }
}
