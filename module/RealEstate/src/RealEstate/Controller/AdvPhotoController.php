<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\EstatePhoto;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Subscription;
use RealEstate\Entity\AdvEstate;

class AdvPhotoController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $order   = array($orderBy => $orderDy);
        $id      = $this->params()->fromRoute("adv_id");
        $adv     = $em->find('RealEstate\Entity\AdvEstate', $id);

        if (!$adv instanceof AdvEstate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $photos = $em->getRepository('RealEstate\Entity\EstatePhoto')
           ->findBy(
               array(
                    "estateId" => $id,
                    "deleted"  => false,
                    "type"     => 0
                  ),
                  $order,
                  $limit,
                  $start
             );

        $query = $em->createQuery("SELECT  count(s) FROM RealEstate\Entity\EstatePhoto s 
              WHERE s.estateId = :estate_id AND s.deleted = false AND s.type = '0'");
        
        $query->setParameters(
             array(
                 'estate_id' => $id
             )
        );
        
        $count  = $query->getSingleScalarResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($photos as $photo) {
            $return['items'][] = $this->extractAndFill($photo);
        }

        return new JsonModel($return);
    }

    public function create($data)
    {
    
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $advId = $this->params()->fromRoute("adv_id");
        $adv   = $em->find('RealEstate\Entity\AdvEstate', $advId);
        $user  = $this->identity();
        
        if (!$adv instanceof AdvEstate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }
        
        if (!isset($_FILES)) {
           return new JsonModel(array('status' => 'error'));
        }
        
         foreach ($_FILES as $file) {
            if ($file['error']) {
                continue;
            }
                $md5    = md5(microtime());
                $random = substr($md5, 0, 10);
             
                $name = $random . '-' . $file['name'];
                $path = ROOT_PATH . "/public/files/images/" . $name;
               
                copy($file['tmp_name'], $path);
                $path = "/files/images/" . $name;

                $photo = new EstatePhoto();
                $photo->setImage($path);
                $photo->setEstateId($advId);
                $photo->setType(0);
                $photo->setCreationDate(new \DateTime('now'));
                $photo->setModifiedDate(new \DateTime('now'));
                $photo->setDeleted(false);

                $em->persist($photo);
            }    
        $em->flush();

       if (isset($photo)) {
        return new JsonModel($this->extractAndFill($photo));
       } else {
           $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
       }
    }
}