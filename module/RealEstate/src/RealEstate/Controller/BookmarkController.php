<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Bookmark;

class BookmarkController extends AbstractRestfulController
{
    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $order    = array($orderBy => $orderDy);
        $user     = $this->identity();

        $bookmarks = $em->getRepository('RealEstate\Entity\Bookmark')
            ->findBy(
                array(
                    "userId" => $user->getId(),
                    "deleted" => false
                   ),
                   $order,
                   $limit,
                   $start
           );

        $query = $em->createQuery("SELECT  count(s) FROM RealEstate\Entity\Bookmark s 
              WHERE s.userId = :user_id and s.deleted = false");
      
        $query->setParameters(
             array(
                 'user_id' => $user->getId()
             )
        );
        
        $count  = $query->getSingleScalarResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($bookmarks as $bookmark) {
            $bookmarkArray     = $this->extractAndFill($bookmark);
            $return['items'][] = $bookmarkArray;
        }
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user     = $this->identity();
        $bookmark = $em->find('RealEstate\Entity\Bookmark', $id);
       
        if (!$bookmark instanceof Bookmark) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($bookmark->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($bookmark->getUserId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'permission denied'));
        }

        $array = $this->extractAndFill($bookmark);

        return new JsonModel($array);
    }

    public function create($data)
    {  
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if (!isset($data['estateId']) || empty($data['estateId'])) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $estateId = $em->getRepository('RealEstate\Entity\Estate')
                ->find($data['estateId']);

        $builder  = new AnnotationBuilder;
        $bookmark = new Bookmark();
        $form     = $builder->createForm($bookmark);

        $form->setHydrator($hydrator);
        $form->bind($bookmark);
        $form->setData($data);

        $user = $this->identity();

        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $hasBookmark = $em->getRepository('RealEstate\Entity\Bookmark')
                ->hasBookmark($user->getId(), $estateId->getId());

        if ($hasBookmark) {
            $this->getResponse()->setStatusCode(200);
            return new JsonModel(array('id' => $hasBookmark->getId()));
        }

        if ($form->isValid()) {
            $bookmark = $form->getData();
            $user     = $this->identity();

            $bookmark->setCreationDate(new \DateTime('now'));
            $bookmark->setModifiedDate(new \DateTime('now'));
            $bookmark->setDeleted(false);
            $bookmark->setUserId($user->getId());

            $em->persist($bookmark);
            $em->flush();

            return new JsonModel($this->extractAndFill($bookmark));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $user     = $this->identity();
        $bookmark = $em->find('RealEstate\Entity\Bookmark', $id);
       
        if (!$bookmark instanceof Bookmark) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($bookmark->getUserId() != $user->getId()) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        if ($bookmark->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $preDate = $this->extract($bookmark);
        $data    = array_merge($preDate, $data);
        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($bookmark);

        $form->setHydrator($hydrator);
        $form->bind($bookmark);
        $form->setData($data);

        if ($form->isvalid()) {
            $bookmark = $form->getData();

            $bookmark->setModifiedDate(new \DateTime('now'));
            $bookmark->setLastModifiedBy($this->identity()->getId());
            $bookmark->setUserId($user->getId());

            $em->flush();

            return new JsonModel($this->extractAndFill($bookmark));
        } else {
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function delete($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $bookmark = $em->find('RealEstate\Entity\Bookmark', $id);
        $user     = $this->identity();

        if (!$bookmark instanceof Bookmark) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($bookmark->getUserId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        if ($bookmark->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $bookmark->setDeleted(true);
        $em->flush();

        return new JsonModel(array('sucees' => true));
    }
}