<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Neighbor;
use RealEstate\Entity\User;
use Doctrine\ORM\Query\ResultSetMapping;
use JpGraph\JpGraph;
use Application\Service\DateService;

class ChartController extends AbstractActionController
{
    public function indexAction()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $request  = $this->getRequest()->getPost('request');
        $metrage  = $this->getRequest()->getPost('metrage');
        $neighbor = $this->getRequest()->getPost('neighbor');

        //   $creation_date     = new DateTime('-1 year');
        //   $creation_date_condition = "AND creation_date >=".$creation_date ;

        if (isset($neighbor) && $neighbor != "") {
            $neighbor_condition = "AND neighbor_id =" . $neighbor;
        } else {
            $neighbor_condition = "";
        }

        if (isset($metrage) && $metrage != "") {
            $metrage_condition = "AND metrage <=" . $metrage;
        } else {
            $metrage_condition = "";
        }

        if (isset($request) && $request != "") {
            $request_condition = "AND request in (" . $request . ")";
        } else {
            $request_condition = "";
        }
        
        $data1 = new \DateTime('-1 months');
        $data = $data1->format('Y-m-d');

        // content="text/plain; charset=utf-8"
        require_once (ROOT_PATH . '/vendor/jpgraph/jpgraph/lib/JpGraph/src/jpgraph.php');
        require_once (ROOT_PATH . '/vendor/jpgraph/jpgraph/lib/JpGraph/src/jpgraph_bar.php');

        $bilts = $em->getConnection()->executeQuery("SELECT
            creation_date::TIMESTAMP::DATE,
            
            MIN(ex_price), MAX(ex_price), ROUND(AVG(ex_price)) AS average_price,
            
            COUNT(*) AS Sample_Size FROM estates  WHERE type=201 " . $request_condition . " 
            
           " . $neighbor_condition . "  " . $metrage_condition . " AND creation_date>='$data' 
               
            AND year_built>EXTRACT(YEAR FROM NOW())-621-3

            GROUP BY 1 HAVING COUNT(*)>2 ORDER BY 1 ");

        $results = $bilts->fetchAll();
      
        if (!isset($results) || empty($results) || count($results)<=1) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/view-chart/error');
            return $view;
        }

        $days = array();

        foreach ($results as $row) {

            $date   = DateService::convertGregorianToJalali($row['creation_date']);
            $parts  = explode('/', $date);
            $year   = $parts[0];
            $day    = $parts[1] . '/' . $parts[2];
            $days[] = $day;

            $averge[$day] = $row['average_price'] / 1000000;
        }

        $estate = $em->getConnection()->executeQuery("SELECT
            creation_date::TIMESTAMP::DATE,
            
            MIN(ex_price), MAX(ex_price), ROUND(AVG(ex_price)) AS average_price,
            
            COUNT(*) AS Sample_Size FROM estates  WHERE type=201 " . $request_condition . " 
            
           " . $neighbor_condition . "  " . $metrage_condition . " AND creation_date>='$data' 
           
            GROUP BY 1 HAVING COUNT(*)>2 ORDER BY 1 ");

        $estates = $estate->fetchAll();

        if (!isset($estates) || empty($estates) || count($estates)<=1) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/view-chart/error');
            return $view;
        }

        foreach ($estates as $row) {

            $date   = DateService::convertGregorianToJalali($row['creation_date']);
            $parts  = explode('/', $date);
            $year   = $parts[0];
            $day    = $parts[1] . '/' . $parts[2];
            $days[] = $day;

            $averge1[$day] = $row['average_price'] / 1000000;
        }
        
        $datay1 = array();
        $datay2 = array();
        $days   = array_unique($days);
        sort($days);
        
        foreach ($days as $day) {
            
            $datay1[] = !isset($averge[$day]) ? 0 : $averge[$day];
            $datay2[] = !isset($averge1[$day]) ? 0 : $averge1[$day];
        }

        $graph = new \Graph(1300, 600, 'auto');
        $graph->SetScale("textlin");

        $theme_class = new \UniversalTheme;
        $graph->SetTheme($theme_class);

        $graph->SetBox(false);

        $graph->ygrid->SetFill(false);
        $graph->xaxis->SetTickLabels($days);
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

//        $graph->xaxis->title->Set("tarikh");
//        $graph->yaxis->title->Set("gheymat");
 
        $b1plot = new \BarPlot($datay1);
        $b2plot = new \BarPlot($datay2);

        $gbplot = new \GroupBarPlot(array($b1plot, $b2plot));
        $graph->Add($gbplot);

        if ($request == 303) {
            $b1plot->SetColor("white");
            $b1plot->SetFillColor("#8737D7 ");

            $b2plot->SetColor("white");
            $b2plot->SetFillColor("#0fcc89");
        } elseif ($request == 301 || $request == 302) {
            $b1plot->SetColor("white");
            $b1plot->SetFillColor("#DA494A ");

            $b2plot->SetColor("white");
            $b2plot->SetFillColor("#0fcc89");
        } elseif ($request == "") {
            $b1plot->SetColor("white");
            $b1plot->SetFillColor("#E9967A");

            $b2plot->SetColor("white");
            $b2plot->SetFillColor("#0fcc89");
        }

        $graph->title->set("melkade.com");
        $graph->legend->SetFrameWeight(1);

        /* $img = 'chart/images/' .
          substr(
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 1
          ) . substr(md5(microtime()), 1) . '.png';
         */

        if (!is_dir(ROOT_PATH . "/public/chart/images/$neighbor/")) {

            mkdir(ROOT_PATH . "/public/chart/images/$neighbor/", 0775, true);

            $img = "chart/images/$neighbor/" . ($neighbor . $request . $metrage ) . '.png';
        } else {
            $img = "chart/images/$neighbor/" . ($neighbor . $request . $metrage ) . '.png';
        }

        $return = $graph->Stroke(ROOT_PATH . '/public/' . $img);

        $view = new ViewModel(
                 array(
                     'result' => $img,
                     'year' => $year,
                     'request' => $request
               )
        );

        return $view;
        //$graph->Stroke();
    }

    public function viewAction()
    {

        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $request  = $this->getRequest()->getPost('request');
        $metrage  = $this->getRequest()->getPost('metrage');
        $neighbor = $this->getRequest()->getPost('neighbor');

        //   $creation_date     = new DateTime('-1 year');
        //   $creation_date_condition = "AND creation_date >=".$creation_date ;

        if (isset($neighbor) && $neighbor != "") {
            $neighbor_condition = "AND neighbor_id =" . $neighbor;
        } else {
            $neighbor_condition = "";
        }

        if (isset($metrage) && $metrage != "") {
            $metrage_condition = "AND metrage <=" . $metrage;
        } else {
            $metrage_condition = "";
        }

        if (isset($request) && $request != "") {
            $request_condition = "AND request in (" . $request . ")";
        } else {
            $request_condition = "";
        }

        // content="text/plain; charset=utf-8"
        require_once (ROOT_PATH . '/vendor/jpgraph/jpgraph/lib/JpGraph/src/jpgraph.php');
        require_once (ROOT_PATH . '/vendor/jpgraph/jpgraph/lib/JpGraph/src/jpgraph_line.php');

        $estates = $em->getConnection()->executeQuery("SELECT
            creation_date::TIMESTAMP::DATE, neighbor_id, neighbor_id%100 AS region,
            
            MIN(ex_price), MAX(ex_price), ROUND(AVG(ex_price)) AS average_price,
            
            COUNT(*) AS Sample_Size FROM estates  WHERE type=201 " . $request_condition . " 
            
           " . $neighbor_condition . "  " . $metrage_condition . " AND creation_date>='2014-04-15'
           
           GROUP BY 1,2 HAVING COUNT(*)>2 ORDER BY 1,2 ");

        $results = $estates->fetchAll();

        foreach ($results as $row) {

            $date  = DateService::convertGregorianToJalali($row['creation_date']);
            $parts = explode('/', $date);
            $year  = $parts[0];
            $day[] = $parts[1] . '/' . $parts[2];

            $averge1[] = $row['average_price'] / 1000000;
        }

        $datay1 = $averge1;

        $graph = new \Graph(1500, 500);
        $graph->SetScale("textlin");

        $theme_class = new \UniversalTheme;

        $graph->SetTheme($theme_class);
        $graph->img->SetAntiAliasing(false);
        //  $graph->title->Set('منطقه ');
        $graph->SetBox(false);

        $graph->img->SetAntiAliasing();

        $graph->yaxis->HideZeroLabel();
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        $graph->xgrid->Show();
        $graph->xgrid->SetLineStyle("solid");
        $graph->xaxis->SetTickLabels($day);
        $graph->xgrid->SetColor('#E3E3E3');

        $p1 = new \LinePlot($datay1);
        $graph->Add($p1);
        $p1->SetColor("#B22222");

        $graph->legend->SetFrameWeight(1);

        /* $img = 'chart/images/' .
          substr(
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 1
          ) . substr(md5(microtime()), 1) . '.png'; */

        if (!is_dir(ROOT_PATH . "/public/chart/images/$neighbor/")) {

            mkdir(ROOT_PATH . "/public/chart/images/$neighbor/", 0775, true);

            $img = "chart/images/$neighbor/" . ($neighbor . $request . $metrage ) . '.png';
        } else {
            $img = "chart/images/$neighbor/" . ($neighbor . $request . $metrage ) . '.png';
        }

        $return = $graph->Stroke(ROOT_PATH . '/public/' . $img);

        $view = new ViewModel(
                 array(
                    'result' => $img,
                     'year' => $year
              )
        );

        return $view;

        //$graph->Stroke();
    }

    public function biltAction()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $request  = $this->getRequest()->getPost('request');
        $metrage  = $this->getRequest()->getPost('metrage');
        $neighbor = $this->getRequest()->getPost('neighbor');

        //   $creation_date     = new DateTime('-1 year');
        //   $creation_date_condition = "AND creation_date >=".$creation_date ;

        if (isset($neighbor) && $neighbor != "") {
            $neighbor_condition = "AND neighbor_id =" . $neighbor;
        } else {
            $neighbor_condition = "";
        }

        if (isset($metrage) && $metrage != "") {
            $metrage_condition = "AND metrage <=" . $metrage;
        } else {
            $metrage_condition = "";
        }

        if (isset($request) && $request != "") {
            $request_condition = "AND request in (" . $request . ")";
        } else {
            $request_condition = "";
        }


        // content="text/plain; charset=utf-8"
        require_once (ROOT_PATH . '/vendor/jpgraph/jpgraph/lib/JpGraph/src/jpgraph.php');
        require_once (ROOT_PATH . '/vendor/jpgraph/jpgraph/lib/JpGraph/src/jpgraph_line.php');

        $estates = $em->getConnection()->executeQuery("SELECT
            creation_date::TIMESTAMP::DATE, neighbor_id, neighbor_id%100 AS region,
            
            MIN(ex_price), MAX(ex_price), ROUND(AVG(ex_price)) AS average_price,
            
            COUNT(*) AS Sample_Size FROM estates  WHERE type=201 " . $request_condition . " 
            
           " . $neighbor_condition . "  " . $metrage_condition . " AND
               
           year_built>EXTRACT(YEAR FROM NOW())-621-3 AND  creation_date>='2014-04-05' 
            
           GROUP BY 1,2 HAVING COUNT(*)>2 ORDER BY 1,2 ");

        $results = $estates->fetchAll();

        foreach ($results as $row) {

            $date  = DateService::convertGregorianToJalali($row['creation_date']);
            $parts = explode('/', $date);
            $year  = $parts[0];
            $day[] = $parts[1] . '/' . $parts[2];

            $averge[] = $row['average_price'] / 1000000;
        }

        $datay1 = $averge;

        $graph = new \Graph(1500, 500);
        $graph->SetScale("textlin");

        $theme_class = new \UniversalTheme;

        $graph->SetTheme($theme_class);
        $graph->img->SetAntiAliasing(false);
        $graph->SetBox(false);

        $graph->img->SetAntiAliasing();

        $graph->yaxis->HideZeroLabel();
        $graph->yaxis->HideLine(false);
        $graph->yaxis->HideTicks(false, false);

        $graph->xgrid->Show();
        $graph->xgrid->SetLineStyle("solid");
        $graph->xaxis->SetTickLabels($day);
        $graph->xgrid->SetColor('#E3E3E3');

        $p1 = new \LinePlot($datay1);
        $graph->Add($p1);
        $p1->SetColor("#6495ED");
        $graph->legend->SetFrameWeight(1);

        /* $img = 'chart/images/' .
          substr(
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", mt_rand(0, 50), 1
          ) . substr(md5(microtime()), 1) . '.png'; */

        if (!is_dir(ROOT_PATH . "/public/chart/images/$neighbor/")) {

            mkdir(ROOT_PATH . "/public/chart/images/$neighbor/", 0775, true);

            $img = "chart/images/$neighbor/" . ($neighbor . $request . $metrage ) . '.png';
        } else {
            $img = "chart/images/$neighbor/" . ($neighbor . $request . $metrage ) . '.png';
        }

        $return = $graph->Stroke(ROOT_PATH . '/public/' . $img);

        $view = new ViewModel(
                 array(
                     'result' => $img,
                      'year' => $year
             )
        );

        return $view;
    }

}