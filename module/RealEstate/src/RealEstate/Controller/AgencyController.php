<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Agency;
use RealEstate\Entity\Estate;

class AgencyController extends AbstractActionController
{

    public function indexAction()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 20);
        $orderBy = $this->params()->fromQuery('orderBy', 'name');
        $orderDy = $this->params()->fromQuery('orderDy', 'ASC');
        $order   = array($orderBy => $orderDy);
        $name    = $this->getRequest()->getPost('search_query');
        $user    = $this->identity();

        $agencies = $em->createQuery("SELECT a FROM RealEstate\Entity\Agency a
                WHERE a.deleted=false AND (a.name LIKE '%$name%' OR a.street LIKE '%$name%')
                ORDER BY a.$orderBy $orderDy");

        $agencies->setFirstResult($start);
        $agencies->setMaxResults($limit);

        $result = $agencies->getResult();

        $query = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Agency a
                 WHERE a.name LIKE '%$name%'");

        $query->setParameters(array());

        $count = $query->getSingleScalarResult();
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($result as $agencie) {
            $agencieArray = $this->extractAndFill($agencie);

            $agencyId = $agencie->getId();
            $estate = $em->createQuery("SELECT e FROM RealEstate\Entity\Estate e  
                     WHERE e.agencyId = '$agencyId'");
            
            $estatess = $estate->getResult();
            
            foreach ($estatess as $estatee){
                $agencieArray['estate'] = $this->extract($estatee);
            }
            
            $estates = $em->createQuery("SELECT count(e) FROM RealEstate\Entity\Estate e  
                     WHERE e.agencyId = :agency_id");

            $estates->setParameters(
                    array(
                        'agency_id' => $agencie->getId()
                    )
            );
            
            $agencieArray['countEstate'] = $estates->getSingleScalarResult(); 
            $return['items'][]           = $agencieArray;
        }

        $view = new ViewModel(
                 array(
                      'result' => $return,
                       'start' => $start,
                       'limit' => $limit,
                       'count' => $count,
                       'user'  => $user
           )
        );

        return $view;
    }

    public function viewAction()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $order   = array($orderBy => $orderDy);
        $user    = $this->identity();

        $agencyId = $this->params()->fromRoute("id");
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);
        
        $estates  = $em->createQuery("SELECT e FROM RealEstate\Entity\Agency u
                   JOIN RealEstate\Entity\Estate e
                   WITH u.id = e.agencyId
                   WHERE u.id = :agency_id");

        $estates->setFirstResult($start);
        $estates->setMaxResults($limit);
        
        $estates->setParameters(
                    array(
                        'agency_id' => $agency->getId(),
                    )
            ); 

        $result = $estates->getResult();

        $query = $em->createQuery("SELECT count(e) FROM RealEstate\Entity\Agency u
              JOIN RealEstate\Entity\Estate e
              WITH u.id = e.agencyId
              WHERE u.id = :agency_id");

        $query->setParameters(
                    array(
                        'agency_id' => $agency->getId(),
                    )
            );

        $count = $query->getSingleScalarResult();

        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($result as $estate) {

            $estateArray = $this->extractAndFill($estate);

            if ($user != null) {
                $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                        ->hasBookmark($user->getId(), $estate->getId());

                if ($hasBookmark) {
                    $estateArray['bookmarkId'] = $hasBookmark->getId();
                } else {
                    $estateArray['bookmarkId'] = -1;
                }
            } else {
                $estateArray['bookmarkId'] = -1;
            }

            $return['items'][] = $estateArray;
        }

        $view = new ViewModel(
                  array(
                       'result' => $return,
                       'start'  => $start,
                       'limit'  => $limit,
                       'count'  => $count,
              )
        );

        return $view;
    }

}