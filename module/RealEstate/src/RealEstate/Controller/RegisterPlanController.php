<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use RealEstate\Entity\Bill;
use RealEstate\Entity\Plan;
use RealEstate\Entity\PlanUser;
use RealEstate\Entity\User;
// estefade nashode
class RegisterPlanController extends AbstractActionController
{
    public function formAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $query     = $em->createQuery("SELECT a FROM RealEstate\Entity\Neighbor a");
        $neighbors = $query->getResult();
        
        $querys = $em->createQuery("SELECT a FROM RealEstate\Entity\User a");
        $userss = $querys->getResult();

        return new ViewModel(
                array(
                    'neighbors' => $neighbors,
                    'userss'    => $userss
             )
        );
    }

    public function saveAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();

        $payerr = $em->getRepository('RealEstate\Entity\User')
                ->findOneBy(
                 array(
                      'cellphone' => $data['payerId']
             )
        );

        if ($payerr == null) {
            $view = new ViewModel(array());

            $view->setTemplate('real-estate/plan-buy/error');
            return $view;
        }

        $payerrId = $payerr->getId();

        if ($data['type'] == 2802) {
            $plan = new Plan();
            $plan->setMonths((int) $data['months']);
            $plan->setUsers($data['usersCount']);
            $plan->setAmount($data['amount']);

            if (!empty($data['agencyId'])) {
                $agency = $em->getRepository('RealEstate\Entity\Agency')
                        ->findOneBy(
                          array(
                              'phones' => $data['agencyId']
                    )
                );

                if ($agency == null) {
                    $view = new ViewModel(array());

                    $view->setTemplate('real-estate/plan-buy/error');
                    return $view;
                }

                $agencyId = $agency->getId();
                $plan->setAgencyId($agencyId);
            }

            $plan->setType(2802);
            $plan->setCreationDate(new \DateTime('now'));
            $plan->setDeleted(false);

            $bill = new Bill();
            $bill->setAmount($data['amount']);
            $bill->setPayerId($payerrId);
            $bill->setTransactionNo($data['transactionNo']);
            $bill->setBillDate(new \DateTime($data['billDate']));
            $bill->setCreationDate(new \DateTime('now'));
            $bill->setDeleted(false);
        }

        if ($data['type'] == 2801) {
            $plan = new Plan();
            $plan->setMonths((int) $data['months']);
            $plan->setUsers($data['usersCount']);
            $plan->setStartDate(new \DateTime('now'));
            $plan->setEndDate(new \DateTime((int) $data['months'] . ' months'));
            $plan->setRegions($data['regions']);
            $plan->setAmount($data['amount']);

            if (!empty($data['agencyId'])) {
                $agency = $em->getRepository('RealEstate\Entity\Agency')
                        ->findOneBy(
                         array(
                             'phones' => $data['agencyId']
                    )
                );

                if ($agency == null) {
                    $view = new ViewModel(array());
                    
                    $view->setTemplate('real-estate/plan-buy/error');
                    return $view;
                }

                $agencyId = $agency->getId();
                $plan->setAgencyId($agencyId);
            }

            $plan->setType(2801);
            $plan->setCreationDate(new \DateTime('now'));
            $plan->setDeleted(false);

            $bill = new Bill();
            $bill->setAmount($data['amount']);
            $bill->setPayerId($payerrId);
            $bill->setTransactionNo($data['transactionNo']);
            $bill->setBillDate(new \DateTime($data['billDate']));
            $bill->setCreationDate(new \DateTime('now'));
            $bill->setDeleted(false);
        }
        
        $em->persist($bill);
        $plan->setBillId($bill->getId());
 
        $em->persist($plan);

        $planUser = new PlanUser();
        $planUser->setUserId($bill->getPayerId());
        $planUser->setPlanId($plan->getId());
        $planUser->setCreationDate(new \DateTime('now'));
        $planUser->setDeleted(false);
        $em->persist($planUser);

        $use = $data['users'];
        $nUsers = array();
        foreach ($use as $u) {
            if (!empty($u)) {
                $nUsers[] = "'" . $u . "'";
            }
        }

        $us = implode(", ", $nUsers);
        if (!empty($us)) {
            $users = $em->createQuery("SELECT a FROM RealEstate\Entity\User a
                 WHERE a.cellphone IN ($us)");

            $users->setParameters(array());

            $userss = $users->getResult();

            foreach ($userss as $uid) {
                if (!empty($uid)) {
                    $planUser = new PlanUser();
                    $planUser->setUserId($uid->getId());
                    $planUser->setPlanId($plan->getId());
                    $planUser->setCreationDate(new \DateTime('now'));
                    $planUser->setDeleted(false);
                    $em->persist($planUser);
                }
            }
        }
        
        $em->flush();        
               
        $regions = array();

        if (is_array($data['regions'])) {
            foreach ($data['regions'] as $regionId) {
                $regions[] = $em->find('RealEstate\Entity\Neighbor', $regionId);
            }
        } 
          
        return new ViewModel(
                  array(
                      'regions'  => $regions,
                      'bill'     => $bill,
                      'plan'     => $plan,
                      'planUser' => $planUser
              )
        );
    }
}
