<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;

class SitemapController extends AbstractActionController 
{
    public function indexAction()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = 0;
        $limit   = 49900;
        
        $sitemap = $em->createQuery("SELECT a FROM RealEstate\Entity\Estate a
                 WHERE a.deleted = false  ORDER BY a.creationDate DESC");
        
        $sitemap->setParameters(array());
 
        $sitemap->setFirstResult($start);
        $sitemap->setMaxResults($limit);
       
        $sites = $sitemap->getResult();
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'items' => array(),
        );
        
        foreach ($sites as $site) {
            $siteArray         = $this->extractAndFill($site);
            $return['items'][] = $siteArray;
        }  
        
        $headers = $this->getResponse()->getHeaders();
        $headers->addHeaderLine('content-type', 'text/xml');
        
         $view = new ViewModel(
                 array(
                     'result'   => $return,
            )
        );
         $view->setTerminal(true);
        
        return $view;        
       
    }
}
