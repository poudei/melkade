<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Agency;

class AgencyMobileController extends AbstractRestfulController
{

    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $order    = array($orderBy => $orderDy);

        $agencies = $em->getRepository('RealEstate\Entity\Agency')
               ->findBy(
                   array(
                       "featured" => 12
                     ),
                     $order,
                     $limit, 
                     $start
               ); 

        $query = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Agency a
               WHERE a.featured = '12' AND  a.deleted = false");

        $count = $query->getSingleScalarResult();
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($agencies as $agency) {
            $agencyArray      = $this->extractAndFill($agency);
            $return['items'][] = $agencyArray;
        }
        return new JsonModel($return);
    }

}
