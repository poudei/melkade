<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\view\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;

class RssController extends AbstractActionController
{
    public function indexAction()
    {
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start = 0;
        $limit = 50;

        $rss = $em->createQuery("SELECT a FROM RealEstate\Entity\Estate a
                 WHERE a.deleted = false  ORDER BY a.creationDate DESC");

        $rss->setParameters(array());

        $rss->setFirstResult($start);
        $rss->setMaxResults($limit);

        $rsss = $rss->getResult();

        $return = array(
            'start' => $start,
            'limit' => $limit,
            'items' => array(),
        );

        foreach ($rsss as $rss) {
            $rssArray          = $this->extractAndFill($rss);
            $return['items'][] = $rssArray;
        }

        $headers = $this->getResponse()->getHeaders();
        $headers->addHeaderLine('content-type', 'text/xml');

        $view = new ViewModel(
                array(
                    'result' => $return,
            )
        );

        $view->setTerminal(true);

        return $view;
    }

}
