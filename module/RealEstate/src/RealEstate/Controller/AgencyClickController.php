<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\UserClick;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Agency;
// estefade nashode
class AgencyClickController extends AbstractRestfulController
{

    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $order    = array($orderBy => $orderDy);
        $agencyId = $this->params()->fromRoute("agency_id");
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);
        
        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }
   
        $agencyClicks = $em->getRepository('RealEstate\Entity\UserClick')
               ->findBy(
                   array(
                       "agencyId" => $agency->getId(),
                       "deleted" => false
                     ),
                     $order,
                     $limit, 
                     $start
               ); 

        $query = $em->createQuery("SELECT count(s) FROM RealEstate\Entity\UserClick s
               WHERE s.agencyId = :agency_id AND s.deleted = false");

        $query->setParameters(
              array(
                   'agency_id' => $agency->getId()
             )
        );

        $count = $query->getSingleScalarResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($agencyClicks  as $agencyClick) {
            $agencyClickArray    = $this->extractAndFill($agencyClick);
            
            $estate = $em->find('RealEstate\Entity\Estate', $agencyClick->getEstateId());
            $agencyClickArray['estate'] = $this->extract($estate);
            
            $return['items'][] = $agencyClickArray;
        }
        return new JsonModel($return);
    }

}
