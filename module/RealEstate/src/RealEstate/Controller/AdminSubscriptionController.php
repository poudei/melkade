<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use RealEstate\Entity\Subscription;
use Application\Service\DateService;
use RealEstate\Entity\User;

class AdminSubscriptionController extends AbstractActionController
{
    public function formAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
       
        $querys = $em->createQuery("SELECT a FROM RealEstate\Entity\User a");
        $user   = $querys->getResult();

        return new ViewModel(
                array(
                    'user' => $user
             )
        );
    }  
    
    public function serachAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();
        
        $users = $em->getRepository('RealEstate\Entity\User')
                ->findOneBy(
                array(
                    'cellphone' => $data['userId']
             )
        );
        
        if ($users == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "کاربر وجود ندارد "));
            return $view;
        }
        
        $userId   = $users->getId();
        $cellphone = $users->getCellphone();

        $subscriptions = $em->getRepository('RealEstate\Entity\Subscription')
                ->findBy(
                array(
                    'userId' => $userId
             )
        );
        
         if ($subscriptions == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "در حال حاضر پلنی برای کاربر $cellphone ثبت  نشده است. "));
            return $view;
        }
        
        $billDate  = array();
        $startDate = array();
        $endDate   = array();
        
        foreach ($subscriptions as $subscription) {
            //ba extract tabdil be array , key value :
            $billDate[$subscription->getId()]  = DateService::convertGregorianToJalali($subscription->getBillDate()->format('Y-m-d'));
            $startDate[$subscription->getId()] = DateService::convertGregorianToJalali($subscription->getStartDate()->format('Y-m-d'));
            $endDate[$subscription->getId()]   = DateService::convertGregorianToJalali($subscription->getEndDate()->format('Y-m-d'));
        }
        
        $return = array(
            'items' => $subscriptions
        );
        
        return new ViewModel(
                array(
                    'subscription'=> $return,
                    'user'        => $users,
                    'billDate'    => $billDate,
                    'startDate'   => $startDate,
                    'endDate'     => $endDate
             )
        );
    }
}
