<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\EstatePhoto;
use RealEstate\Entity\Bookmark;
use RealEstate\Entity\Neighbor;
use RealEstate\Entity\Subscription;
use Application\Service\DateService;

class EstateController extends AbstractActionController
{

    public function indexAction()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 100);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        $user    = $this->identity();

        $neighborId = $this->params()->fromQuery('neighborId');

        if (!empty($neighborId)) {
            $neighbor = $em->find('RealEstate\Entity\Neighbor', $neighborId);

            if ($neighbor->getParentId() == 0) {

                $neighbors = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->findBy(
                        array(
                            "parentId" => $neighborId
                        )
                );

                $neighb = array();
                $ids = array($neighborId);

                foreach ($neighbors as $neighb) {
                    $ids[] = $neighb->getId();
                }

                $filter[] = "neighborId|in|" . join(',', $ids);
            } else {
                $filter[] = "neighborId|eq|" . $neighborId;
            }
        }

        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Estate', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'direction',
            'neighborId',
            'cabinet',
            'floor',
            'frontage',
            'deal',
            'access',
            'quality',
            'airconditioner',
            'facilities ',
            'sport_facilities',
            'equipments',
            'securities',
            'type',
            'beds',
            'stories',
            'request',
            'metrage',
            'price',
            'ex_price',
            'area',
            'loan',
            'status',
            'ownerUserId',
            'agentUserId',
            'agencyId'
        );

        $hasWhere = false;
        $limitResult = true;
        foreach ($filter as $row) {
            $data = explode('|', $row);

            if ($data[0] == 'date') {

                if (strtotime($data[2]) > strtotime('-15 days')) {
                    $date1m = new \DateTime('-15 days');
                    $data[2] = $date1m->format('Y-m-d');
                }

                $data[2] = "'" . $data[2] . "'";

                $or = $qb->expr()->orX();
                $hasOr = false;

                switch ($data[1]) {
                    case 'gt':
                    case 'gte';
                        $op = $data[1];
                        $or->add($qb->expr()->$op('s.creationDate', $data[2]));
                        $or->add($qb->expr()->$op('s.modifiedDate', $data[2]));
                        $hasOr = true;
                        $limitResult = false;
                        break;
                }

                if ($hasOr) {
                    $and->add($or);
                    $hasWhere = true;
                }
            }

            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;

                    case 'in':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->in('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'between':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->between('s.' . $col, $val[0], $val[1]));
                        $hasWhere = true;
                        break;

                    case 'ora':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->orArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'anda':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->andArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'like':
                        $val = '%' . $data[2] . '%';
                        $and->add($qb->expr()->like('s.' . $data[0], $qb->expr()->literal($val)));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s');
        $qb->orderBy('s.' . $orderBy, $orderDy);

        if ($limitResult) {
            $qb->setFirstResult($start)
                    ->setMaxResults($limit);
        }

        $query   = $qb->getQuery();
        $estates = $query->getResult();

        if ($limitResult) {
            $return = array(
                'start' => $start,
                'limit' => $limit,
                'count' => $count,
                'items' => array(),
            );
        } else {
            $return = array(
                'count' => $count,
                'items' => array(),
            );
        }

        foreach ($estates as $estate) {

            $estateArray = $this->extractAndFill($estate);

            $estateArray['requestDate']   = DateService::convertGregorianToJalali($estate->getRequestDate()->format('Y-m-d'));

            $estateArray['expireDate']    = DateService::convertGregorianToJalali($estate->getExpireDate()->format('Y-m-d'));
            
            $estateArray['creationDate']  = DateService::convertGregorianToJalali($estate->getCreationDate()->format('Y-m-d'));

            if ($user != null) {

                if ($user->getType() == 2503) {

                    $operator = $em->find('RealEstate\Entity\User', $estate->getOperatorId());

                    $estateArray['operatorName'] = $operator->getLastname();
                    
                    $estateArray['creationDate'] = $estateArray['creationDate'];
                }
                $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                        ->hasBookmark($user->getId(), $estate->getId());

                if ($hasBookmark) {
                    $estateArray['bookmarkId'] = $hasBookmark->getId();
                } else {
                    $estateArray['bookmarkId'] = -1;
                }
            } else {
                $estateArray['bookmarkId'] = -1;
            }
            
             $estateArray['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                ->estatePh($estate->getId());

            $return['items'][] = $estateArray;
        }

        $view = new ViewModel(
                 array(
                     'result' => $return,                  
                     'start'  => $start,
                     'limit'  => $limit,
                     'count'  => $count,
            )
        );
        
        return $view;
    }

    public function viewAction()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $estateId = $this->params()->fromRoute("id", null);       
        $estate   = $em->find('RealEstate\Entity\Estate', $estateId);
        $user     = $this->identity();

        if (!$estate instanceof Estate) {
            $view = new ViewModel(array());

            $view->setTemplate('real-estate/estate/error');
            return $view;
        }

        if ($estate->getDeleted()) {
            $view = new ViewModel(array());

            $view->setTemplate('real-estate/estate/error');
            return $view;
        }
        $array = $this->extractAndFill($estate);

         $array['requestDate']  = DateService::convertGregorianToJalali($estate->getRequestDate()->format('Y-m-d'));
         
         $array['expireDate']   = DateService::convertGregorianToJalali($estate->getExpireDate()->format('Y-m-d'));
         
         $array['creationDate'] = DateService::convertGregorianToJalali($estate->getCreationDate()->format('Y-m-d'));
         
        if ($user != null) {

            if ($user->getType() == 2503) {

                $operator = $em->find('RealEstate\Entity\User', $estate->getOperatorId());

                $array['operatorName'] = $operator->getLastname();
                $array['creationDate'] = $array['creationDate'];
            }

            $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                    ->hasBookmark($user->getId(), $estate->getId());

            if ($hasBookmark) {
                $array['bookmarkId'] = $hasBookmark->getId();
            } else {
                $array['bookmarkId'] = -1;
            }
        } else {
            $array['bookmarkId'] = -1;
        }

        $array['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                ->estatePh($estate->getId());
       
       $neighbor = $em->find('RealEstate\Entity\Neighbor', $estate->getNeighborId());  
       
       $request = '';
       switch ($estate->getRequest()) {
            case 301:
                $request .= 'فروش ';
                break;
            case 303:
                $request .= ' رهن و اجاره';
                break;
            case 302:
                $request .= 'پیش فروش ';
        }
       
       $pageTitle = '';
       switch ($estate->getType()) {
            case 201:
                $pageTitle   .= 'آپارتمان ';
                break;
            case 202 :
                  $pageTitle .= 'ویلایی ';
                break;
            case 203:
                  $pageTitle .= 'زمین ';
                break;
            case 204:
                  $pageTitle .= 'اداری ';
                break;
            case 205:
                $pageTitle   .= 'مغازه ';
                break;
            case 206:
                  $pageTitle .= 'مستغلات ';
                break;
        }
       $pageTitl = ' ' . $request . ' ' . $pageTitle . ' ' . (int)$estate->getMetrage() . ' متری  ' . ',' . $neighbor->getTitle() . ' ' . ' ,' . $estate->getStreet();
       
      $pageDescriptio = '';
         switch ($estate->getType()) {
            case 201:
                $pageDescriptio   .= 'آپارتمان ';
                break;
            case 202 :
                  $pageDescriptio .= 'ویلایی ';
                break;
            case 203:
                  $pageDescriptio .= 'زمین ';
                break;
            case 204:
                  $pageDescriptio .= 'اداری ';
                break;
            case 205:
                $pageDescriptio   .= 'مغازه ';
                break;
            case 206:
                  $pageDescriptio .= 'مستغلات ';
                break;
        }
        
         $pageDescription = ' ' . $request . ' ' . $pageDescriptio . ' ' . (int)$estate->getMetrage() . ' متری  ' . ',' . $neighbor->getTitle() . ' ' . ' ,' . $estate->getStreet();
 
         $SimilarEstates = $em->getRepository('\RealEstate\Entity\Estate')
                ->getSimilarEstates($estate);
         
        $return['items'] = array();
        
        foreach ($SimilarEstates as $SimilarEstate) {    
            $SimilarEstateArray = $this->extractAndFill($SimilarEstate);
            
            $return['items'][]  = $SimilarEstateArray;
        }
        
       $view = new ViewModel(
             array(
                  'request'         => $request,
                  'neighbor'        => $neighbor,
                  'estate'          => $array,
                  'pageTitle'       => $pageTitl,
                  'pageDescription' => $pageDescription,
                  'user'            => $user,
                  'result'          => $return
             )
        );

        if ($estate->getType() == 201) {
            $view->setTemplate('real-estate/estate/apartment-view');
        } elseif ($estate->getType() == 203) {
            $view->setTemplate('real-estate/estate/land-view');
        } elseif ($estate->getType() == 204) {
            $view->setTemplate('real-estate/estate/office-view');
        }  elseif ($estate->getType() == 205) {
            $view->setTemplate('real-estate/estate/shop-view');
        }  elseif ($estate->getType() == 202) {
            $view->setTemplate('real-estate/estate/villa-view');
        }  elseif ($estate->getType() == 206) {
            $view->setTemplate('real-estate/estate/property-view');
        }

        return $view;
    }
}