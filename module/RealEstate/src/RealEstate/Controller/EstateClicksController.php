<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\UserClick;
use RealEstate\Entity\Estate;
use RealEstate\Entity\User;
// estefade nashode
class EstateClicksController extends AbstractRestfulController
{
    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $order    = array($orderBy => $orderDy);
        $estateId = $this->params()->fromRoute("estate_id");
        $user     = $this->identity();

        $estate = $em->find('RealEstate\Entity\Estate', $estateId);

        if (!$estate instanceof Estate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $clicks = $em->getRepository('RealEstate\Entity\UserClick')
           ->findBy(
               array(
                   "estateId" => $estateId, 
                   "deleted" => false
                 ), 
                 $order,
                 $limit, 
                 $start
            );

        $query = $em->createQuery("SELECT count(s) FROM RealEstate\Entity\UserClick s
               WHERE s.estateId = :estate_id and s.deleted = false");

        $query->setParameters(
             array(
                  'estate_id' => $estateId
            )
        );

        $count  = $query->getSingleScalarResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($clicks as $click) {
            $userClickArray    = $this->extractAndFill($click);
            $return['items'][] = $userClickArray;
        }
        return new JsonModel($return);
    }

}
