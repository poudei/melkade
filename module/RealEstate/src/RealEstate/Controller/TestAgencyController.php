<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\UserClick;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Agency;
use Doctrine\ORM\Query\ResultSetMapping;
use Application\Service\DateService;

class TestAgencyController extends AbstractRestfulController
{
    public function testAction()
    {
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start = $this->params()->fromQuery('start', 0);
        $limit = $this->params()->fromQuery('limit', 10);

        // string mikone hanero 
        $conten = file_get_contents('/home/sahar/agencies.sql');

        $conn = @pg_pconnect("dbname=chardivari user=postgres password=postgres host=127.0.0.1 port=5432");

        $offset = 0;
        while ($str = strpos($conten, 'INSERT', $offset)) {
            $srtsimi = strpos($conten, ';', $str);

            $query  = substr($conten, $str, $srtsimi - $str + 1);

            $agency = strpos($query, 'agencies');

            $query  = substr_replace($query, 'tests', $agency, 8);

            $result = pg_query($conn, $query);

            $offset = $srtsimi;
        }

        return new JsonModel($result);
    }

    public function indexAction()
    {
        set_time_limit(0);
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start = $this->params()->fromQuery('start', 0);
        $limit = $this->params()->fromQuery('limit', 1000);

        $agencies = $em->getConnection()->executeQuery("SELECT a.name, a.phones, a.featured, a.longitude,
             a.latitude, a.state, a.city, a.street, a.bystreet , a.lane, a.number,
             a.postal_code, a.manager_id , a.manager_name, a.origin_id, a.origin_address FROM tests a
              LIMIT $limit OFFSET $start");

        $result = $agencies->fetchAll();
        
        foreach ($result as $row) {

            $agency = new Agency();
            $agency->setName($row['name']);
            $agency->setPhones($row['phones']);
            $agency->setFeatured($row['featured']);
            $agency->setLongitude($row['longitude']);
            $agency->setLatitude($row['latitude']);
            $agency->setState($row['state']);
            $agency->setCity($row['city']);
            $agency->setStreet($row['street']);
            $agency->setBystreet($row['bystreet']);
            $agency->setLane($row['lane']);
            $agency->setNumber($row['number']);
            $agency->setPostalCode($row['postal_code']);
            $agency->setManagerId($row['manager_id']);
            $agency->setOriginId($row['origin_address']);
            $agency->setOriginAddres($row['manager_name']);
            $agency->setManagerName($row['origin_id']);
            $agency->setCreationDate(new \DateTime('now'));
            $agency->setModifiedDate(new \DateTime('now'));
            $agency->setDeleted(false);
            
            $street = $agency->getStreet();
            if (empty($street)) {
                 $agency->setStreet($row['manager_name']);
            }
            $em->persist($agency);
            $em->flush();           
        }

        $return = array(
            'start' => $start,
            'limit' => $limit,
            'items' => $result
        ); 
        return new JsonModel($return);
    }
    
    public function convertAction() 
    {
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
     //   $start = $this->params()->fromQuery('start', 0);
      //  $limit = $this->params()->fromQuery('limit', 1000);

        $estateDates = $em->getRepository('RealEstate\Entity\Estate')
                ->findBy(array(),array());

        foreach ($estateDates as $estateDate) {
            $requestDate = DateService::convertJalaliToGregorian($estateDate->getRequestDate()->format('Y-m-d'));
            $expireDate  = DateService::convertJalaliToGregorian($estateDate->getExpireDate()->format('Y-m-d'));
            
            $estateDate->setRequestDate(new \DateTime($requestDate));
            $estateDate->setExpireDate(new \DateTime($expireDate));

        }
  
         $em->flush();

         return new JsonModel(array('success' => true));
    }

}
