<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\ApiEstate;
use RealEstate\Entity\AdvEstate;

class MyEstateController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $order   = array($orderBy => $orderDy);
        $user    = $this->identity();

        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $userId = $user->getId();

        $date1 = new \DateTime('-15 days');
        $data = $date1->format('Y-m-d');

        $estate = $em->createQuery("SELECT e FROM RealEstate\Entity\Estate e
               WHERE e.deleted = false AND (e.creationDate >= '$data' OR e.modifiedDate >= '$data') AND
               (e.ownerUserId = '$userId' OR  e.agentUserId = '$userId' OR e.agencyId = '$userId')
                ORDER BY e.$orderBy $orderDy");

        $estates = $estate->getResult();

        $advEstate = $em->createQuery("SELECT a FROM RealEstate\Entity\AdvEstate a
               WHERE a.userId = '$userId' AND (a.creationDate >= '$data' OR a.modifiedDate >= '$data')
                AND a.deleted = false ORDER BY a.$orderBy $orderDy");

        $advEstates = $advEstate->getResult();

        $return = array(
            'estate' => array(),
            'adv' => array()
        );

        foreach ($estates as $estate) {
            $estateArray = $this->extractAndFill($estate);
            $estateArray['isOwner'] = 1;

            $estateArray['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->estatePh($estate->getId());

            $return['estate'][] = $estateArray;
        }

        foreach ($advEstates as $advEstate) {
            $advEstateArray = $this->extractAndFill($advEstate);

            $advEstateArray['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->advPhoto($advEstate->getId());

            $return['adv'][] = $advEstateArray;
        }

        return new JsonModel($return);
    }

}