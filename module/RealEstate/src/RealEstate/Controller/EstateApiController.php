<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\UserClick;
use RealEstate\Entity\EstatePhoto;
use RealEstate\Entity\Bookmark;
use Zend\Crypt\Password\Bcrypt;
use RealEstate\Entity\EstateHistory;
use Application\Service\DateService;
use RealEstate\Entity\AdvEstate;
use RealEstate\Entity\Agency;

class EstateApiController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 100);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        $user    = $this->identity();

        $neighborId = $this->params()->fromQuery('neighborId');
        
        if (!empty($neighborId)) {
            $neighbor = $em->find('RealEstate\Entity\Neighbor', $neighborId);

            if ($neighbor->getParentId() == 0) {

                $neighbors = $em->getRepository('RealEstate\Entity\Neighbor')
                    ->findBy(
                        array(
                            "parentId" => $neighborId
                      )
                 ); 
                
                $neighb = array();
                $ids    = array($neighborId);

                foreach ($neighbors as $neighb) {
                    $ids[] = $neighb->getId();
                }

                $filter[] = "neighborId|in|" . join(',', $ids);      
            } else {
                $filter[] = "neighborId|eq|" . $neighborId;
            }
        }
        
        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Estate', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));
      
        $validNames = array(
            'direction',
            'neighborId',
            'cabinet',
            'floor',
            'frontage',
            'deal',
            'access',
            'quality',
            'airconditioner',
            'facilities ',
            'sport_facilities',
            'equipments',
            'securities',
            'type',
            'beds',
            'stories',
            'request',
            'metrage',
            'price',
            'exPrice',
            'area',
            'loan',
            'status',
            'ownerUserId',
            'agentUserId',
            'agencyId'
        );
        
        $hasWhere = false;
        $limitResult = true;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            
            if ($data[0] == 'date') {

                if (strtotime($data[2]) < strtotime('-14 days')) {
                    $date1m  = new \DateTime('-14 days');
                    $data[2] = $date1m->format('Y-m-d');
                }

                $data[2] = "'" . $data[2] . "'";

                $or    = $qb->expr()->orX();
                $hasOr = false;

                switch ($data[1]) {
                    case 'gt':
                    case 'gte':
                        $op = $data[1];
                        $or->add($qb->expr()->$op('s.creationDate', $data[2]));
                        $or->add($qb->expr()->$op('s.modifiedDate', $data[2]));
                        $hasOr = true;
                        $limitResult = false;
                        break;
                }

                if ($hasOr) {
                    $and->add($or);
                    $hasWhere = true;
                }
            }

            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                        $and->add($qb->expr()->eq('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                    
                    case 'in':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->in('s.' . $col, $val));
                        $hasWhere = true;
                        break;
                    
                    case 'between':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->between('s.' . $col, $val[0], $val[1]));
                        $hasWhere = true;
                        break;

                    case 'ora':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->orArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'anda':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->andArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;
                    
                    case 'like':
                        $val = '%' . $data[2] . '%';
                        $and->add($qb->expr()->like('s.' . $data[0], $qb->expr()->literal($val)));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);
        
        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s');
        $qb->orderBy('s.' . $orderBy, $orderDy);

        if ($limitResult) {
             $qb->setFirstResult($start)
                ->setMaxResults($limit);
        }

        $query   = $qb->getQuery();        
        $estates = $query->getResult();
       
        if ($limitResult) {
            $return  = array(
                'start' => $start,
                'limit' => $limit,
                'count' => $count,
                'items' => array(),
                'agencies'=> array()
            );
        } else {
            $return  = array(
                'count' => $count,
                'items' => array(),
                'agencies'=> array()
            );
        }

        foreach ($estates as $estate) { 
            $estateArray = $this->extractAndFill($estate);

            if ($user != null) {

                if ($user->getType() == 2503) {

                    $operator = $em->find('RealEstate\Entity\User', $estate->getOperatorId());

                    $estateArray['operatorName'] = $operator->getLastname();
                }

                $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                        ->hasBookmark($user->getId(), $estate->getId());

                if ($hasBookmark) {
                    $estateArray['bookmarkId'] = $hasBookmark->getId();
                } else {
                    $estateArray['bookmarkId'] = -1;
                }
            } else {
                $estateArray['bookmarkId'] = -1;
            }
            
             $estateArray['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                ->estatePh($estate->getId());
            
            $return['items'][] = $estateArray;            
        }
        
        $agencies = $em->getRepository('RealEstate\Entity\Agency')
               ->findBy(
                   array(
                       "featured" => 12
                     )
               ); 
        
         foreach ($agencies  as $agency) {
            $agencyArray    = $this->extractAndFill($agency);
            $return['agencies'][] = $agencyArray;
        } 
        
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        
        if (is_array($id)) {
            foreach ($id as $ids) {
                $estate = $em->find('RealEstate\Entity\Estate', $ids);
                $user   = $this->identity();

                if (!$estate instanceof Estate) {
                    $this->getResponse()->setStatusCode(404);
                    return new JsonModel(array('error' => 'Not Found'));
                }

                if ($estate->getDeleted()) {
                    $this->getResponse()->setStatusCode(404);
                    return new JsonModel(array('error' => 'Not Found'));
                }

                $array = $this->extractAndFill($estate);

                if ($user != null) {

                    if ($user->getType() == 2503) {

                        $operator = $em->find('RealEstate\Entity\User', $estate->getOperatorId());

                        $array['operatorName'] = $operator->getLastname();
                    }

                    $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                            ->hasBookmark($user->getId(), $estate->getId());

                    if ($hasBookmark) {
                        $array['bookmarkId'] = $hasBookmark->getId();
                    } else {
                        $array['bookmarkId'] = -1;
                    }
                } else {
                    $array['bookmarkId'] = -1;
                }
                
                $array['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                        ->estatePh($estate->getId());

                $this->insertUserClick($ids);
                
                $estateaArray[] = $array;
            }                 
               return new JsonModel($estateaArray);
        } else {

            $estate = $em->find('RealEstate\Entity\Estate', $id);
            $user   = $this->identity();

            if (!$estate instanceof Estate) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            if ($estate->getDeleted()) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            $array = $this->extractAndFill($estate);

            if ($user != null) {

                if ($user->getType() == 2503) {

                    $operator = $em->find('RealEstate\Entity\User', $estate->getOperatorId());

                    $array['operatorName'] = $operator->getLastname();
                }

                $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                        ->hasBookmark($user->getId(), $estate->getId());

                if ($hasBookmark) {
                    $array['bookmarkId'] = $hasBookmark->getId();
                } else {
                    $array['bookmarkId'] = -1;
                }
            } else {
                $array['bookmarkId'] = -1;
            }

            $array['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->estatePh($estate->getId());

            $this->insertUserClick($id);

            return new JsonModel($array);
        }
    }
    
    public function insertUserClick($estateId)
    { 
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
   /*     $user = $this->identity();
        
        if ($user == null) {
            return;
        }
        
        $userId = $user->getId();
        $agent  = $em->getRepository('\RealEstate\Entity\Agent')
                ->getUserAgent($user->getId());
        
        if ($agent) {
            $agencyId  = $agent->getAgencyId();
            $agentUser = $em->createQuery("SELECT s FROM RealEstate\Entity\UserClick s
                WHERE s.estateId = :estate_id AND s.agencyId = :agency_id");
            
            $agentUser->setParameters(
                    array(
                        'estate_id' => $estateId,
                        'agency_id' => $agencyId
                    )
            );
        } else {
            $agentUser = $em->createQuery("SELECT s FROM RealEstate\Entity\UserClick s
                WHERE s.userId = :user_id AND s.estateId = :estate_id");

            $agentUser->setParameters(
                   array(
                       'estate_id' => $estateId,
                       'user_id'   => $userId
                 )
            );
        }
        $userCl = $agentUser->getResult();

        if (!empty($userCl)) {
            return;
        }
        
        $value  = 0;
        $estate = $em->find('RealEstate\Entity\Estate', $estateId);
        
        if ($estate->getRequest() == 301) {
            $price = $estate->getPrice();
            $value = $price * pow(10, -6);
        } elseif ($estate->getRequest() == 303) {
            $price   = (float)$estate->getPrice();
            $exPrice = $estate->getExPrice();
            $ax      = ($price * 0.03) + $exPrice;
            $value   = $ax * pow(10, -4);
        } 
        
        $unlimitedPlans = $em->getRepository('\RealEstate\Entity\Plan')
                ->getUnlimitedPlans($userId);
        foreach ($unlimitedPlans as $plan) {
            if (in_array($estate->getNeighborId(), $plan->getRegions())) {
                $value = 0;
            }
        }
        
        $userClick = new UserClick();
        $userClick->setEstateId($estateId);
        $userClick->setCreationDate(new \DateTime('now'));
        $userClick->setDeleted(0);
        $userClick->setUserId($user->getId());
        $userClick->setViewDate(new \DateTime('now'));
        $userClick->setValue($value);
        if ($agent) {
            $userClick->setAgencyId($agent->getAgencyId());
        }
        
        $em->persist($userClick);
        $em->flush(); */
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        
        if (isset($data['advId']) && !empty($data['advId'])) {
            
            $subscription = $em->getRepository('\RealEstate\Entity\Estate')
                    ->getSubscription($data);

            if (is_array($subscription) && isset($subscription['error'])) {
                $this->getResponse()->setStatusCode($subscription['code']);
                return new JsonModel(array('error' => $subscription['error']));
            }
        } else {
            $data['paid'] = 0;
            $data['requested'] = 0;
        }
        $builder  = new AnnotationBuilder();
        $estate   = new Estate();
        $form     = $builder->createForm($estate);

        $form->setHydrator($hydrator);
        $form->bind($estate);

        if (empty($data['area']) || $data['area'] == 0) {
            unset($data['area']);
        }
        
        if (empty($data['length']) || $data['length'] == 0) {
            unset($data['length']);
        }
        
        if (empty($data['width']) || $data['width'] == 0) {
            unset($data['width']);
        }
        
        if (empty($data['refinement']) || $data['refinement'] == 0) {
            unset($data['refinement']);
        }

        if (empty($data['storage']) || $data['storage'] == 0) {
            unset($data['storage']);
        }

        if (empty($data['balcony']) || $data['balcony'] == 0) {
            unset($data['balcony']);
        }

        if (empty($data['loan']) || $data['loan'] == 0) {
            unset($data['loan']);
        }

        if (empty($data['number']) || $data['number'] == 0) {
            unset($data['number']);
        }

        if (empty($data['unit']) || $data['unit'] == 0) {
            unset($data['unit']);
        }
        
        if (empty($data['stories']) || $data['stories'] == 0) {
            unset($data['stories']);
        }
        
        if (empty($data['storey']) || $data['storey'] == 0) {
            unset($data['storey']);
        }
        
        if (empty($data['units']) || $data['units'] == 0) {
            unset($data['units']);
        }

        if (empty($data['ceilingHeight']) || $data['ceilingHeight'] == 0) {
            unset($data['ceilingHeight']);
        }
       
        if (isset($data['expireDate'])) {
            $data['expireDate'] = DateService::convertJalaliToGregorian($data['expireDate']);
        }

        if (isset($data['requestDate'])) {
            $data['requestDate'] = DateService::convertJalaliToGregorian($data['requestDate']);
        }
       
        $form->setData($data);
      
        $user = $this->identity();

        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }
        
         if (empty($data['price']) && empty($data['exPrice'])) {
             
             $this->getResponse()->setStatusCode(404);
              return new JsonModel(array('error' => 'Empty price or exPrice'));
         }
             
        if ($data['advertisedBy'] == 101) {
            $newOwner = false;
            
             if (empty($data['ownerCellphone']) || empty($data['ownerLastname'])) {

                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Empty cellphon or username'));
            }

            $filter = new \Application\Filter\ConvertFarsiNum();
            $data['ownerCellphone'] = $filter->filter($data['ownerCellphone']);

            $ownerUser = $em->getRepository('RealEstate\Entity\User')
                    ->findOneBy(
                            array(
                                'cellphone' => $data['ownerCellphone'],
                    )
            );
        
            if (!$ownerUser) {
                $ownerUser = new \RealEstate\Entity\User();
                $ownerUser->setUsername($data['ownerCellphone']);
                $ownerUser->setCellphone($data['ownerCellphone']);
                $ownerUser->setFirstname('');
                $ownerUser->setLastname($data['ownerLastname']);
                if (isset($data['ownerPhones'])) {
                    $ownerUser->setPhones($data['ownerPhones']);
                }
                $ownerUser->setType(2501);
                /*
                  $str      = "abcdefghijkmnpqrstuvwxyz23456789";
                  $str      = str_shuffle($str);
                  $pass     = substr($str, 0, 8);
                 */
                
                $pass        = $ownerUser->getCellphone();
                $usernameSms = $ownerUser->getCellphone();
                
                $passs   = "+98" . substr($pass, -10);              
                $message = "وقت به خیر،\n فایل شما در ملکده (melkade.com) ثبت شد،\n نام کاربری:$usernameSms\nکلمه عبور:$pass\n";
                $phone   = $passs;
                $this->sendSms($phone, $message);

                $bcrypt   = new Bcrypt();
                $password = $bcrypt->create($pass);

                $ownerUser->setPassword($password);
                $ownerUser->setCreationDate(new \DateTime('now'));
                $ownerUser->setModifiedDate(new \DateTime('now'));
                $ownerUser->setDeleted(false);
              
                $em->persist($ownerUser);
                
                $ownerUser->setCreatedById($user->getId());
                
                if ($ownerUser->getEmail() != null) {

                    $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
                    $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
                    $queue        = $queueManager->get('email');

                    $job = $jobManager->get('Application\Job\SendEmail');
                    $job->setContent(
                            array(
                               'template' => 'realestate/email/register',
                               'subject'  => 'Notification',
                               'to_email' => $ownerUser->getEmail(),
                               'to_name'  => $ownerUser->getFirstName() . ' ' . $ownerUser->getLastName(),
                               'data'     => array(
                                                 'password' => $pass,
                                                 'username' => $ownerUser->getCellphone()
                          )
                       )
                    );
                   
                    $queue->push($job);
                }

                $newOwner = true;
            }  else {

                $date  = new \DateTime('now');
                $date1 = $date->format('Y-m-d') . ' 0:0:0';
                $date2 = $date->format('Y-m-d') . ' 19:00:00';

                $ownerId = $ownerUser->getId();
                $query   = $em->createQuery("SELECT count(s) FROM RealEstate\Entity\Estate s
                  WHERE s.ownerUserId = $ownerId AND  s.creationDate BETWEEN '$date1' AND '$date2'");

                $result = $query->getSingleScalarResult();

                if ($result == 0) {
                    
                    $pass    = $ownerUser->getCellphone();
                    $passs   = "+98" . substr($pass, -10);               
                    $message = "وقت به خیر،\n فایل شما در ملکده (melkade.com) ثبت شد";
                    $phone   = $passs;
                    $this->sendEstateSms($phone, $message);
                }
  
                $em->flush($ownerUser);
            }
            
        } elseif ($data['advertisedBy'] == 102) {
            $newAgency = false;
            
            if (empty($data['agencyName']) || empty($data['agencyPhones']) ||
                    empty($data['agentCellphone']) || empty($data['agentLastname'])) {

                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Empty Phones or Name'));
            }
            
            $agency    = $em->getRepository('RealEstate\Entity\Agency')
                    ->findOneBy(
                      array(
                         'phones' => $data['agencyPhones'],
                 )
            );

            if (!$agency) {
                $agency = new \RealEstate\Entity\Agency();
                $agency->setPhones($data['agencyPhones']);
                $agency->setName($data['agencyName']);
                $agency->setCreationDate(new \DateTime('now'));
                $agency->setModifiedDate(new \DateTime('now'));
                $agency->setDeleted(false);

                $em->persist($agency);
                
                $newAgency = true;
            }

            $newAgent = false;
            
            $filter   = new \Application\Filter\ConvertFarsiNum();
            $data['agentCellphone'] = $filter->filter($data['agentCellphone']);

            $agentUser = $em->getRepository('RealEstate\Entity\User')
                    ->findOneBy(
                      array(
                         'cellphone' => $data['agentCellphone'],
                 )
            );

            if (!$agentUser) {
                $agentUser = new \RealEstate\Entity\User();
                $agentUser->setCellphone($data['agentCellphone']);
                $agentUser->setUsername($data['agentCellphone']);
                $agentUser->setLastname($data['agentLastname']);

                if (isset($data['agentPhones'])) {
                    $agentUser->setPhones($data['agentPhones']);
                }

                $agentUser->setType(2502);
                /*
                  $str      = "abcdefghijkmnpqrstuvwxyz23456789";
                  $str      = str_shuffle($str);
                  $pass     = substr($str, 0, 8);
                 */
                $pass        = $agentUser->getCellphone();
                $usernameSms = $agentUser->getCellphone();

                $passs   = "+98" . substr($pass, -10);
                $message = "وقت به خیر،\n فایل شما در ملکده (melkade.com) ثبت شد،\n نام کاربری:$usernameSms\nکلمه عبور:$pass\n";
                $phone   = $passs;
                $this->sendSms($phone, $message);


                $bcrypt   = new Bcrypt();
                $password = $bcrypt->create($pass);

                $agentUser->setPassword($password);
                $agentUser->setCreationDate(new \DateTime('now'));
                $agentUser->setModifiedDate(new \DateTime('now'));
                $agentUser->setDeleted(false);

                $em->persist($agentUser);
                
                $agentUser->setCreatedById($user->getId());

                if ($agentUser->getEmail() != null) {

                    $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
                    $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
                    $queue        = $queueManager->get('email');

                    $job = $jobManager->get('Application\Job\SendEmail');
                    $job->setContent(
                            array(
                               'template' => 'realestate/email/register',
                               'subject'  => 'Notification',
                               'to_email' => $agentUser->getEmail(),
                               'to_name'  => $agentUser->getFirstName() . ' ' . $agentUser->getLastName(),
                               'data'     => array(
                                                 'password' => $pass,
                                                 'username' => $agentUser->getCellphone()
                           )
                        )
                    );

                    
                    $queue->push($job);
                }
             
                $newAgent = true;
            } else {

                $date  = new \DateTime('now');
                $date1 = $date->format('Y-m-d') . ' 0:0:0';
                $date2 = $date->format('Y-m-d') . ' 19:00:00';

                $agentId = $agentUser->getId();
                $query   = $em->createQuery("SELECT count(s) FROM RealEstate\Entity\Estate s
                  WHERE s.agentUserId = $agentId AND  s.creationDate BETWEEN '$date1' AND '$date2'");

                $result = $query->getSingleScalarResult();
               
                if ($result == 0) {
                    
                    $pass = $agentUser->getCellphone();
                    
                    $passs   = "+98" . substr($pass, -10);
                    $message = "وقت به خیر،\n فایل شما در ملکده (melkade.com) ثبت شد";
                    $phone   = $passs;
                    $this->sendEstateSms($phone, $message);
                }
            }

            $agent = $em->getRepository('RealEstate\Entity\Agent')
                    ->findOneBy(
                    array(
                        'userId'   => $agentUser->getId(),
                        'agencyId' => $agency->getId()
                    )
              );
            if (!$agent) {
                $agent = new \RealEstate\Entity\Agent();
                $agent->setAgencyId($agency->getId());
                $agent->setUserId($agentUser->getId());
                $agent->setCreationDate(new \DateTime('now'));
                $agent->setModifiedDate(new \DateTime('now'));
                $agent->setDeleted(false);

                $em->persist($agent);
                //  $newAgent = true;
            }
        } else {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($data['type'] == 201) {
            $form->remove('access');
            $form->remove('building');
            $form->remove('age');
            $form->remove('sides');
            $form->remove('refinement');
            $form->remove('plan');
            $form->remove('permisson');
            $form->remove('deal');
            $form->remove('ceilingHeight');
            $form->remove('wall');
        } elseif ($data['type'] == 202) {
            $form->remove('access');
            $form->remove('building');
            $form->remove('storey');
            $form->remove('unit');
            $form->remove('deal');
            $form->remove('sides');
            $form->remove('ceilingHeight');
            $form->remove('wall');
        } elseif ($data['type'] == 203) {
            $form->remove('access');
            $form->remove('building');
            $form->remove('age');
            $form->remove('yearBuilt');
            $form->remove('storage');
            $form->remove('stories');
            $form->remove('storey');
            $form->remove('units');
            $form->remove('unit');
            $form->remove('quality');
            $form->remove('balcony');
            $form->remove('metrage');
            $form->remove('beds');
            $form->remove('baths');
            $form->remove('irToilets');
            $form->remove('toilets');
            $form->remove('parking');
            $form->remove('kitchen');
            $form->remove('cabinet');
            $form->remove('floor');
            $form->remove('kitchenFurnishment');
            $form->remove('livingFurnished');
            $form->remove('bedsFurnished');
            $form->remove('frontage');
            $form->remove('airconditioner');
            $form->remove('facilities');
            $form->remove('sportFacilities');
            $form->remove('equipments');
            $form->remove('securities');
            $form->remove('ownerResidance');
            $form->remove('length');
            $form->remove('deal');
            $form->remove('ceilingHeight');
            $form->remove('wall');
        } elseif ($data['type'] == 204) {
            $form->remove('access');
            $form->remove('building');
            $form->remove('age');
            $form->remove('baths');
            $form->remove('balcony');
            $form->remove('kitchen');
            $form->remove('cabinet');
            $form->remove('livingFurnished');
            $form->remove('bedsFurnished');
            $form->remove('facilities');
            $form->remove('sportFacilities');
            $form->remove('ownerResidance');
            $form->remove('length');
            $form->remove('sides');
            $form->remove('refinement');
            $form->remove('plan');
            $form->remove('permisson');
            $form->remove('ceilingHeight');
            $form->remove('wall');
        } elseif ($data['type'] == 205) {
            $form->remove('building');
            $form->remove('age');
            $form->remove('baths');
            $form->remove('kitchen');
            $form->remove('cabinet');
            $form->remove('ownerResidance');
            $form->remove('livingFurnished');
            $form->remove('sportFacilities');
            $form->remove('refinement');
            $form->remove('plan');
            $form->remove('permisson');
            $form->remove('beds');
            $form->remove('bedsFurnished');
            $form->remove('direction');
        } elseif ($data['type'] == 206) {
            $form->remove('baths');
            $form->remove('kitchen');
            $form->remove('cabinet');
            $form->remove('storey');
            $form->remove('unit');
            $form->remove('storage');
            $form->remove('irToilets');
            $form->remove('toilets');
            $form->remove('parking');
            $form->remove('beds');
            $form->remove('balcony');
            $form->remove('kitchenFurnishment');
            $form->remove('livingFurnished');
            $form->remove('bedsFurnished');
            $form->remove('sportFacilities');
            $form->remove('facilities');
            $form->remove('equipments');
            $form->remove('floor');
            $form->remove('ownerResidance');
            $form->remove('deal');
            $form->remove('ceilingHeight');
            $form->remove('wall');
        }
   
        if ($form->isValid()) {
            $estate = $form->getData();

            $birth = strtotime('2014-01-01');
            $today = strtotime('now');
            $dir   = $today - $birth;
            $y     = (int) ($dir / 60);
            $str   = "123456789";
            $str   = str_shuffle($str);
            $st    = substr($str, 0, 3);
            $dd    = $y . $st;

            $estate->setCode($dd);
            $estate->setCreationDate(new \DateTime('now'));
            $estate->setModifiedDate(new \DateTime('now'));
            $estate->setDeleted(false);
            $estate->setOperatorId($user->getId());
   
            $data['postalCode'] = trim($data['postalCode']);
            if (empty($data['postalCode'])) {
                $estate->setPostalCode(null);
            }

            if (isset($ownerUser)) {
                $estate->setOwnerUserId($ownerUser->getId());
            } else {
                $estate->setAgentUserId($agentUser->getId());
                $estate->setOwnerUserId($agentUser->getId());
                $estate->setAgencyId($agency->getId());
            }
         
            $em->persist($estate);
            
            if (isset($data['advId']) && !empty($data['advId'])) {

                $advId = $data['advId'];
                $advEstate = $em->find('RealEstate\Entity\AdvEstate', $advId);

                if (!$advEstate instanceof AdvEstate) {
                    $this->getResponse()->setStatusCode(404);
                    return new JsonModel(array('error' => 'Adv Not Found'));
                }
                
                $id = $estate->getId();
                
                $querys = $em->createQuery("UPDATE RealEstate\Entity\EstatePhoto a
                    SET a.estateId = '$id', a.type = '1'
                    WHERE a.estateId = '$advId' AND a.type = '0'");

                $querye = $querys->execute();
            }
/*
            foreach ($_FILES as $file) {
               if ($file['error']) {
                    continue;
               }
                 $md5    = md5(microtime());
                 $random = substr($md5, 0, 10);
                
                 $name = $random . '-' . $file['name'];
                 $path = ROOT_PATH . "/public/files/images/" . $name;

                 copy($file['tmp_name'], $path);
                 $path = "/files/images/" . $name;

                 $photo = new EstatePhoto();
                 $photo->setImage($path);
                 $photo->setType(1);
                 $photo->setEstateId($advEstate->getId());
                 $photo->setCreationDate(new \DateTime('now'));
                 $photo->setModifiedDate(new \DateTime('now'));
                 $photo->setDeleted(false);

                 $em->persist($photo);
            }
*/
            $em->flush();

            $job = $this->getServiceLocator()
                    ->get('SlmQueue\Job\JobPluginManager')
                    ->get('RealEstate\job\PublishEstate');

            $job->setContent(
                    array(
                        'estate' => $this->extractAndFill($estate)
                 )
            );

            $queue = $this->getServiceLocator()
                    ->get('SlmQueue\Queue\QueuePluginManager')
                    ->get('estate');
            $queue->push($job);

            $return = array('estate' => $this->extractAndFill($estate)
            );

            if (isset($ownerUser)) {
                $return['ownerUser'] = $this->extractAndFill($ownerUser);
                $return['newOwner']  = $newOwner;
            } else {
                $return['agency']    = $this->extractAndFill($agency);
                $return['newAgency'] = $newAgency;

                $return['agentUser'] = $this->extractAndFill($agentUser);
                $return['newAgent']  = $newAgent;

                $return['ownerUser'] = $this->extractAndFill($agentUser);
                $return['newAgent']  = $newAgent;
            }

            return new JsonModel($return);
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }
    
    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $estate   = $em->find('RealEstate\Entity\Estate', $id);
        $user     = $this->identity();
      
        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }
        
        if ($user->getType() == 2503) {

            if (!$estate instanceof Estate) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            if ($estate->getDeleted()) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            $preData = $this->extract($estate);
            $data    = array_merge($preData, $data);
            $builder = new AnnotationBuilder();
            $form    = $builder->createForm($estate);

            $form->setHydrator($hydrator);
            $form->bind($estate);

            if (empty($data['area']) || $data['area'] == 0) {
                unset($data['area']);
            }

            if (empty($data['length']) || $data['length'] == 0) {
                unset($data['length']);
            }

            if (empty($data['width']) || $data['width'] == 0) {
                unset($data['width']);
            }

            if (empty($data['refinement']) || $data['refinement'] == 0) {
                unset($data['refinement']);
            }

            if (empty($data['storage']) || $data['storage'] == 0) {
                unset($data['storage']);
            }

            if (empty($data['balcony']) || $data['balcony'] == 0) {
                unset($data['balcony']);
            }

            if (empty($data['loan']) || $data['loan'] == 0) {
                unset($data['loan']);
            }

            if (empty($data['number']) || $data['number'] == 0) {
                unset($data['number']);
            }

            if (empty($data['unit']) || $data['unit'] == 0) {
                unset($data['unit']);
            }

            if (empty($data['stories']) || $data['stories'] == 0) {
                unset($data['stories']);
            }

            if (empty($data['storey']) || $data['storey'] == 0) {
                unset($data['storey']);
            }

            if (empty($data['units']) || $data['units'] == 0) {
                unset($data['units']);
            }
            
            if (empty($data['ceilingHeight']) || $data['ceilingHeight'] == 0) {
                unset($data['ceilingHeight']);
            }

            if (isset($data['expireDate'])) {
                $data['expireDate'] = DateService::convertJalaliToGregorian($data['expireDate']);
            }

            if (isset($data['requestDate'])) {
                $data['requestDate'] = DateService::convertJalaliToGregorian($data['requestDate']);
            }

            $form->setData($data);
            
            if ($estate->getAdvertisedBy() == 101) {

                $ownerUser = $em->getRepository('RealEstate\Entity\User')
                        ->findOneBy(
                        array(
                            'id' => $estate->getOwnerUserId()
                        )
                  );
                if ($ownerUser) {

                    $ownerUser->setLastname($data['ownerLastname']);

                    if ($data['ownerCellphone'] != $ownerUser->getCellphone()) {
                        $ownerUser->setCellphone($data['ownerCellphone']);
                    }
                }

                $em->flush();
            }

            if ($estate->getAdvertisedBy() == 102) {
                $agency = $em->getRepository('RealEstate\Entity\Agency')
                        ->findOneBy(
                         array(
                             'id' => $estate->getAgencyId()
                        )
                  );
                
                if ($agency) {

                    $agency->setName($data['agencyName']);

                    if ($data['agencyPhones'] != $agency->getPhones()) {

                        $agency->setPhones($data['agencyPhones']);
                    }
                }

                $agentUser = $em->getRepository('RealEstate\Entity\User')
                        ->findOneBy(
                        array(
                            'id' => $estate->getAgentUserId()
                        )
                );
                
                if ($agentUser) {

                    $agentUser->setLastname($data['agentLastname']);

                    if ($data['agentCellphone'] != $agentUser->getCellphone()) {

                        $agentUser->setCellphone($data['agentCellphone']);
                    }
                }

                $em->flush();
            }

            if ($data['type'] == 201) {
                $form->remove('access');
                $form->remove('building');
                $form->remove('age');
                $form->remove('sides');
                $form->remove('refinement');
                $form->remove('plan');
                $form->remove('permisson');
                $form->remove('deal');
                $form->remove('ceilingHeight');
                $form->remove('wall');
            } elseif ($data['type'] == 202) {
                $form->remove('access');
                $form->remove('building');
                $form->remove('storey');
                $form->remove('unit');
                $form->remove('deal');
                $form->remove('sides');
                $form->remove('ceilingHeight');
                $form->remove('wall');
            } elseif ($data['type'] == 203) {
                $form->remove('access');
                $form->remove('building');
                $form->remove('age');
                $form->remove('yearBuilt');
                $form->remove('storage');
                $form->remove('stories');
                $form->remove('storey');
                $form->remove('units');
                $form->remove('unit');
                $form->remove('quality');
                $form->remove('balcony');
                $form->remove('metrage');
                $form->remove('beds');
                $form->remove('baths');
                $form->remove('irToilets');
                $form->remove('toilets');
                $form->remove('parking');
                $form->remove('kitchen');
                $form->remove('cabinet');
                $form->remove('floor');
                $form->remove('kitchenFurnishment');
                $form->remove('livingFurnished');
                $form->remove('bedsFurnished');
                $form->remove('frontage');
                $form->remove('airconditioner');
                $form->remove('facilities');
                $form->remove('sportFacilities');
                $form->remove('equipments');
                $form->remove('securities');
                $form->remove('ownerResidance');
                $form->remove('length');
                $form->remove('deal');
                $form->remove('ceilingHeight');
                $form->remove('wall');
            } elseif ($data['type'] == 204) {
                $form->remove('access');
                $form->remove('building');
                $form->remove('age');
                $form->remove('baths');
                $form->remove('balcony');
                $form->remove('kitchen');
                $form->remove('cabinet');
                $form->remove('livingFurnished');
                $form->remove('bedsFurnished');
                $form->remove('facilities');
                $form->remove('sportFacilities');
                $form->remove('ownerResidance');
                $form->remove('length');
                $form->remove('sides');
                $form->remove('refinement');
                $form->remove('plan');
                $form->remove('permisson');
                $form->remove('ceilingHeight');
                $form->remove('wall');
            } elseif ($data['type'] == 205) {
                $form->remove('building');
                $form->remove('age');
                $form->remove('baths');
                $form->remove('kitchen');
                $form->remove('cabinet');
                $form->remove('ownerResidance');
                $form->remove('livingFurnished');
                $form->remove('sportFacilities');
                $form->remove('refinement');
                $form->remove('plan');
                $form->remove('permisson');
                $form->remove('beds');
                $form->remove('bedsFurnished');
                $form->remove('direction');
            } elseif ($data['type'] == 206) {
                $form->remove('baths');
                $form->remove('kitchen');
                $form->remove('cabinet');
                $form->remove('storey');
                $form->remove('unit');
                $form->remove('storage');
                $form->remove('irToilets');
                $form->remove('toilets');
                $form->remove('parking');
                $form->remove('beds');
                $form->remove('balcony');
                $form->remove('kitchenFurnishment');
                $form->remove('livingFurnished');
                $form->remove('bedsFurnished');
                $form->remove('sportFacilities');
                $form->remove('facilities');
                $form->remove('equipments');
                $form->remove('floor');
                $form->remove('ownerResidance');
                $form->remove('deal');
                $form->remove('ceilingHeight');
                $form->remove('wall');
            }

            if ($form->isValid()) {
                $estate = $form->getData();

                $estate->setModifiedDate(new \DateTime('now'));
                $estate->setLastModifiedBy($this->identity()->getId());

                 
                $data['postalCode'] = trim($data['postalCode']);
                if (empty($data['postalCode'])) {
                    $estate->setPostalCode(null);
                }
                
                $history = new EstateHistory();
                $history->setUserId($user->getId());
                $history->setEstateId($estate->getId());
                $history->setModifiedDate(new \DateTime('now'));
                $em->persist($history);
                $em->flush();

                return new JsonModel($this->extractAndFill($estate));
            } else {
                return new JsonModel(array('error' => $form->getMessages()));
            }
        }
        $this->getResponse()->setStatusCode(403);
        return new JsonModel(array('error' => 'Forbidden'));
    }

    public function delete($id)
    {
        $em     = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $estate = $em->find('RealEstate\Entity\Estate', $id);
        $user   = $this->identity();

        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        if ($user->getType() == 2503) {

            if (!$estate instanceof Estate) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            if ($estate->getDeleted()) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            $estate->setDeleted(true);
            $em->flush();

            return new JsonModel(array('sucees' => true));
        }
        $this->getResponse()->setStatusCode(403);
        return new JsonModel(array('error' => 'Forbidden'));
    }

    public function nearbyAction()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $estates  = $em->getRepository('RealEstate\Entity\Estate')
                ->getNearbyPoints();

        $return = array();

        foreach ($estates as $estate) {
            $return[] = $estate;
        }

        $em->flush();
        return new JsonModel($return);
    }
}
