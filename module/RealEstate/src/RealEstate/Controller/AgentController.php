<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Agent;

class AgentController extends AbstractRestfulController
{

    public function getList()
    {
        $em         = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start      = $this->params()->fromQuery('start', 0);
        $limit      = $this->params()->fromQuery('limit', 10);
        $orderBy    = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy    = $this->params()->fromQuery('orderDy', 'DESC');
        $filter     = $this->params()->fromQuery('filter', array());
        $agencyId   = $this->params()->fromQuery("agency_id", null);
        $conditions = array();

        if ($agencyId != null) {
            $agency = $em->find('RealEstate\Entity\Agency', $agencyId);

            if (!$agency instanceof Agent) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            $conditions = array('agencyId' => $agencyId);
        }

        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Agent', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'occupationType',
            'agencyBranch',
            'startDate',
            'agencyId',
            'endDate',
            'userId'
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        $query  = $qb->getQuery();
        $agents = $query->getResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );
        
        foreach ($agents as $agent) {
            $agentArray        = $this->extractAndFill($agent);
            $return['items'][] = $agentArray;
        }

        return new JsonModel($return);
    }
}
