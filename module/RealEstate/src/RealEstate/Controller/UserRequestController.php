<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Request;
// estefade nashode .. request haye  user login e
class UserRequestController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        $user    = $this->identity();

        $qb = $em->createQueryBuilder();
        $qb->select('s')
                ->from('RealEstate\Entity\Request', 's')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.userId', $user->getId()));
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'estateType',
            'requestType',
            'storey',
            'bedrooms',
            'marital',
            'metrage',
            'parking',
            'persons',
            'rent',
            'deposit',
            'budget',
        );

        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $query    = $qb->getQuery();
        $requests = $query->getResult();
        $return   = array();

        foreach ($requests as $request) {
            $requestArray = $this->extractAndFill($request);
            $return[]     = $requestArray;
        }
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user    = $this->identity();
        $request = $em->find('RealEstate\Entity\Request', $id);

        if (!$request instanceof Request) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($request->getUserId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        if ($request->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $array = $this->extractAndFill($request);

        return new JsonModel($array);
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder;
        $request  = new Request();
        $form     = $builder->createForm($request);

        $form->setHydrator($hydrator);
        $form->bind($request);
        $form->setData($data);

        if ($form->isValid()) {
            $request = $form->getData();
            $user    = $this->identity();

            $request->setCreationDate(new \DateTime('now'));
            $request->setModifiedDate(new \DateTime('now'));
            $request->setDeleted(false);
            $request->setUserId($user->getId());

            $em->persist($request);
            $em->flush();

            return new JsonModel($this->extractAndFill($request));
        } else {
            $this->getResponse()->setStatusCode(400);

            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $user     = $this->identity();
        $request  = $em->find('RealEstate\Entity\Request', $id);

        if (!$request instanceof Request) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($request->getUserId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        if ($request->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $preDate = $this->extract($request);
        $data    = array_merge($preDate, $data);

        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($request);

        $form->setHydrator($hydrator);
        $form->bind($request);
        $form->setData($data);

        if ($form->isValid()) {
            $request = $form->getData();

            $request->setModifiedDate(new \DateTime('now'));
            $request->setLastModifiedBy($this->identity()->getId());
            $request->setUserId($user->getId());

            $em->flush();

            return new JsonModel($this->extractAndFill($request));
        } else {
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function delete($id)
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user    = $this->identity();
        $request = $em->find('RealEstate\Entity\Request', $id);

        if (!$request instanceof Request) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($request->getUserId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        if ($request->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $request->setDeleted(true);
        $em->flush();

        return new JsonModel(array('sucees' => true));
    }

}