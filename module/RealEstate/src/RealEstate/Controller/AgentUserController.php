<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Bookmark;
use RealEstate\Entity\Neighbor;
use RealEstate\Entity\User;
use RealEstate\Entity\Agency;
use RealEstate\Entity\Agent;
use Doctrine\ORM\Query\ResultSetMapping;

class AgentUserController extends AbstractActionController
{

    public function indexAction()
    {
        $em        = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start     = $this->params()->fromQuery('start', 0);
        $limit     = $this->params()->fromQuery('limit', 10);
        $orderBy   = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy   = $this->params()->fromQuery('orderDy', 'DESC');
        $order     = array($orderBy => $orderDy);
        $lastname  = $this->getRequest()->getPost('search_query');
        $cellphone = $this->getRequest()->getPost('search_query');
        $name      = $this->getRequest()->getPost('search_query');

        $agentUser = $em->getConnection()->executeQuery("SELECT
            a.id, a.lastname, a.firstname, a.cellphone, s.name, s.id as agency FROM Users a
            INNER JOIN Agents p ON a.id = p.user_id
            INNER JOIN Agencies s ON p.agency_id = s.id
            WHERE a.type = 2502 AND (concat(a.firstname, a.lastname) LIKE '%$lastname%' 
            OR a.cellphone LIKE '%$cellphone%')
            LIMIT $limit OFFSET $start" );
        
        $results = $agentUser->fetchAll();
        
        $query   = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\User a
                JOIN RealEstate\Entity\Agent p
                WITH a.id = p.userId 
                JOIN RealEstate\Entity\Agency s
                WITH p.agencyId = s.id
                WHERE a.type = 2502 AND (concat(a.firstname, a.lastname) LIKE '%$lastname%'
                OR a.cellphone LIKE '%$cellphone%') ");

        $query->setParameters(array());
        
        $count = $query->getSingleScalarResult();
       
        foreach ($results as &$row) {
            $estates = $em->createQuery("SELECT count(e) FROM RealEstate\Entity\Estate e  
                     WHERE e.agentUserId = :agent_user_id");

            $estates->setParameters(
                    array(
                        'agent_user_id' => $row['id']
                    )
            );

            $row['count'] = $estates->getSingleScalarResult();
        }
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => $results
        );
        
        
        $view = new ViewModel(
                 array(
                     'result' => $return,
                     'start'  => $start,
                     'limit'  => $limit,
                     'count'  => $count,
             )
        );

        return $view;
    }
    
     public function viewAction()
     {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $order   = array($orderBy => $orderDy);
        $user    = $this->identity();
        
        $agentId = $this->params()->fromRoute("id");
        $agent   = $em->find('RealEstate\Entity\User', $agentId);

        $estates = $em->createQuery("SELECT e FROM RealEstate\Entity\User u
                   JOIN RealEstate\Entity\Estate e
                   WITH u.id = e.agentUserId
                   WHERE u.type= 2502  AND u.id = :user_id");

        $estates->setFirstResult($start);
        $estates->setMaxResults($limit);
        
        $estates->setParameters(
                array(
                   'user_id' => $agent->getId(),
               )
        );

        $result = $estates->getResult();

        $query = $em->createQuery("SELECT count(e) FROM RealEstate\Entity\User u
             JOIN RealEstate\Entity\Estate e
              WITH u.id = e.agentUserId
               WHERE u.type= 2502  AND u.id = :user_id");

        $query->setParameters(
                array(
                    'user_id' => $agent->getId(),
                )
        );

        $count = $query->getSingleScalarResult();

        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($result as $estate) {
            $estateArray = $this->extractAndFill($estate);
            
             $neighbor   = $em->find('RealEstate\Entity\Neighbor', $estate->getNeighborId());  

            if ($user != null) {
                $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                        ->hasBookmark($user->getId(), $estate->getId());

                if ($hasBookmark) {
                    $estateArray['bookmarkId'] = $hasBookmark->getId();
                } else {
                    $estateArray['bookmarkId'] = -1;
                }
            } else {
                $estateArray['bookmarkId'] = -1;
            }

            $return['items'][] = $estateArray;
        }

        $view = new ViewModel(
                 array(
                      'result'   => $return,
                      'neighbor' => $neighbor,
                      'start'    => $start,
                      'limit'    => $limit,
                      'count'    => $count,
             )
        );

        return $view;
    }

}