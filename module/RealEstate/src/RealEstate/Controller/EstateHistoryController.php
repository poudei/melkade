<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\UserClick;
use RealEstate\Entity\Estate;
use RealEstate\Entity\User;
use RealEstate\Entity\EstateHistory;

class EstateHistoryController extends AbstractRestfulController
{
    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $order    = array($orderBy => $orderDy);
        $estateId = $this->params()->fromRoute("estate_id");
        
        $estate = $em->find('RealEstate\Entity\Estate', $estateId);
        
        if (!$estate instanceof Estate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $estateHistorys = $em->getRepository('RealEstate\Entity\EstateHistory')
             ->findBy(
                 array(
                      "estateId" => $estateId
                    ), 
                    $order,
                    $limit, 
                    $start
               );

        $query = $em->createQuery("SELECT count(s) FROM RealEstate\Entity\EstateHistory s
                       WHERE s.estateId = :estate_id ");

        $query->setParameters(
                array(
                    'estate_id' => $estateId
             )
        );

        $count = $query->getSingleScalarResult();
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($estateHistorys as $estateHistory) {
            $estateHistoryArray = $this->extractAndFill($estateHistory);

            $users = $em->find('RealEstate\Entity\User', $estateHistory->getUserId());
            $estateHistoryArray['Lastname'] =  $users->getLastname();
                        
            $return['items'][] = $estateHistoryArray;
        }
        return new JsonModel($return);
    }

}
