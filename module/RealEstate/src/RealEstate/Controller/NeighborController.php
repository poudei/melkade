<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Neighbor;

class NeighborController extends AbstractRestfulController
{
    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $parentId = $this->params()->fromQuery('parentId', null);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $filter   = $this->params()->fromQuery('filter' , array());
        
        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Neighbor', 's');
        
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        if ($parentId !== null) {
            $and->add($qb->expr()->eq('s.parentId' , $parentId));
        }
        
        $validNames = array(
            'title'
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                case 'eq':
                    $op = $data[1];
                    $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                    $hasWhere = true;
                    break;
              
                case 'like':
                    $val = '%' . $data[2] . '%';
                    $and->add($qb->expr()->like('s.' . $data[0], $qb->expr()->literal($val)));
                    $hasWhere = true;
                    break;
                }
                
            }
        }
        $qb->where($and);
    
        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();
        
        $qb->select('s') 
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);
        
        $query     = $qb->getQuery();
        $neighbors = $query->getResult();
        $return    = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );
        
        foreach ($neighbors as $neighbor) {
            $neighborArray = $this->extractAndFill($neighbor);

             if ($neighborArray['parentId'] > 0) {
                $parent = $em->find('RealEstate\Entity\Neighbor',
                        $neighborArray['parentId']);

                if ($parent) {
                    $neighborArray['title'] = $parent->getTitle() . " / "
                            . $neighborArray['title'];
                }
            }

            if (count($filter) > 0) {
                unset($neighborArray['lastModifiedBy']);
                unset($neighborArray['creationDate']);
                unset($neighborArray['modifiedDate']);
                unset($neighborArray['creationDate']);
                unset($neighborArray['modifiedDate']);
                unset($neighborArray['description']);
                unset($neighborArray['parentId']);
                unset($neighborArray['deleted']);
                unset($neighborArray['path']);
            }

            $return['items'][] = $neighborArray;
        }
        return new JsonModel($return);
    }
    
    public function get($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $neighbor = $em->find('RealEstate\Entity\Neighbor', $id);

        if (!$neighbor instanceof Neighbor) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($neighbor->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $array = $this->extractAndFill($neighbor);

        if ($array['parentId'] > 0) {
            $parent = $em->find('RealEstate\Entity\Neighbor', $array['parentId']);

            if ($parent) {
                $array['title'] = $parent->getTitle() . " / " . $array['title'];
            }
        }

        return new JsonModel($array);
    }

    /*
    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();
        $neighbor = new Neighbor();
        $form     = $builder->createForm($neighbor);

        $form->setHydrator($hydrator);
        $form->bind($neighbor);
        $form->setData($data);

        if ($form->isValid()) {
            $neighbor = $form->getData();
            $neighbor->setCreationDate(new \DateTime('now'));
            $neighbor->setModifiedDate(new \DateTime('now'));
            $neighbor->setDeleted(false);

            $em->persist($neighbor);
            $em->flush();

            return new JsonModel($this->extractAndFill($neighbor));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }
     */
}
