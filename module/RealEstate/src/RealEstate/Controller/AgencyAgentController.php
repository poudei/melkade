<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Agent;

class AgencyAgentController extends AbstractRestfulController
{

    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $filter   = $this->params()->fromQuery('filter ', array());
        $agencyId = $this->params()->fromRoute("agency_id", null);
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getManagerId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Agent', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));
        $and->add($qb->expr()->eq('s.agencyId', $agencyId));

        $validNames = array(
            'occupationType',
            'agencyBranch',
            'startDate',
            'agencyId',
            'endDate',
            'userId'
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                }
            }
        }

        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        $query  = $qb->getQuery();
        $agents = $query->getResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($agents as $agent) {
            $agentArray        = $this->extractAndFill($agent);
            $return['items'][] = $agentArray;
        }
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getManagerId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $em->find('RealEstate\Entity\Agent', $id);

        if (!$agent instanceof Agent) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agent->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        $array = $this->extractAndFill($agent);

        return new JsonModel($array);
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getManagerId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $builder = new AnnotationBuilder;
        $agent   = new Agent();
        $form    = $builder->createForm($agent);

        $form->setHydrator($hydrator);
        $form->bind($agent);
        $form->setData($data);

        if ($form->isValid()) {
            $agent = $form->getData();
            $user  = $this->identity();
    
            $query = $em->createQuery("SELECT s FROM RealEstate\Entity\Agent s
                WHERE s.userId = :user_id AND s.status = '2901'");
             
            $query->setParameters(
                  array(
                      'user_id' => $user->getId()
                 )
            );
            
            $queryStatus = $query->getResult();
            
            if (!empty($queryStatus)) {
                $querys = $em->createQuery("UPDATE RealEstate\Entity\Agent s
                    SET s.status = '2902' 
                    WHERE s.userId = :user_id AND s.status = '2901'");
                
                $querys->setParameters(
                       array(
                           'user_id' => $user->getId()
                     )
                );
                
                $querye = $querys->execute(); 
            }

            $agent->setCreationDate(new \DateTime('now'));
            $agent->setModifiedDate(new \DateTime('now'));
            $agent->setDeleted(false);
            $agent->setUserId($user->getId());
            $agent->setAgencyId($agencyId);
            $agent->setStatus(2901);

            $em->persist($agent);
            $em->flush();

            return new JsonModel($this->extractAndFill($agent));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getmanagerId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $em->find('RealEstate\Entity\Agent', $id);

        if (!$agent instanceof Agent) {
            $this->getResponse()->setStatuseCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agent->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $preDate = $this->extract($agent);
        $data    = array_merge($preDate, $data);
        $builder = new AnnotationBuilder;
        $form    = $builder->createForm($agent);

        $form->setHydrator($hydrator);
        $form->bind($agent);
        $form->setData($data);

        if ($form->isValid()) {
            $agent = $form->getData();

            $agent->setModifiedDate(new \DateTime('now'));
            $agent->setLastModifiedBy($this->identity()->getId());
            $agent->setAgencyId($agencyId);

            $em->flush();

            return new JsonModel($this->extractAndFill($agent));
        } else {
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function delete($id)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $agencyId = $this->params()->fromRoute("agency_id");
        $user     = $this->identity();
        $agency   = $em->find('RealEstate\Entity\Agency', $agencyId);

        if ($agency == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getManagerId() != $user->getId()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Permission denied'));
        }

        $agent = $em->find('RealEstate\Entity\Agent', $id);

        if (!$agent instanceof Agent) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $agent->setUserId($user->getId());
        $agent->setAgencyId($agencyId);

        if ($agent->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $agent->setDeleted(true);
        $em->flush();

        return new JsonModel(array('sucees' => true));
    }

}

