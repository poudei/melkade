<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Agency;
use RealEstate\Entity\Neighbor;

class AgencyApiController extends AbstractRestfulController
{

    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());

        $neighborId = $this->params()->fromQuery('neighborId');

        if (!empty($neighborId)) {
            $neighbor = $em->find('RealEstate\Entity\Neighbor', $neighborId);

            if ($neighbor->getParentId() == 0) {

                $neighbors = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->findBy(
                        array(
                            "parentId" => $neighborId
                        )
                );

                $neighb = array();
                $ids    = array($neighborId);

                foreach ($neighbors as $neighb) {
                    $ids[] = $neighb->getId();
                }

                $filter[] = "neighborId|in|" . join(',', $ids);
            } else {
                $filter[] = "neighborId|eq|" . $neighborId;
            }
        }
        
        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Agency', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'name',
            'neighborId'
        );
        
        $hasWhere = false;
        $hasLike  = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                    
                    case 'like':
                        $val = '%' . $data[2] . '%';
                        $and->add($qb->expr()->like('s.' . $data[0], $qb->expr()->literal($val)));
                        $hasWhere = true;
                        $hasLike  = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        $query    = $qb->getQuery();
        $agencies = $query->getResult();
        $return   = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($agencies as $agencie) {
            $agencieArray      = $this->extractAndFill($agencie);
            $return['items'][] = $agencieArray;
        }
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em     = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $agency = $em->find('RealEstate\Entity\Agency', $id);

        if (!$agency instanceof Agency) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $array = $this->extractAndFill($agency);

        return new JsonModel($array);
    }

    public function create($data)
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator= $this->getServiceLocator()->get('Hydrator');
        $builder = new AnnotationBuilder();
        $agency  = new Agency();
        $form    = $builder->createForm($agency);

        $form->setHydrator($hydrator);
        $form->bind($agency);
        $form->setData($data);

        if ($form->isValid()) {
            $agency = $form->getData();

            $agency->setCreationDate(new \DateTime('now'));
            $agency->setModifiedDate(new \DateTime('now'));
            $agency->setDeleted(false);

            $em->persist($agency);
            $em->flush();

            return new JsonModel($this->extractAndFill($agency));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $agency   = $em->find('RealEstate\Entity\Agency', $id);

        if (!$agency instanceof Agency) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $preData = $this->extract($agency);
        $data    = array_merge($preData, $data);
        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($agency);

        $form->setHydrator($hydrator);
        $form->setData($data);
        $form->bind($agency);

        if ($form->isValid()) {
            $agency = $form->getData();

            $agency->setModifiedDate(new \DateTime('now'));
            $agency->setLastModifiedBy($this->identity()->getId());

            $em->flush();

            return new JsonModel($this->extractAndFill($agency));
        } else {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function delete($id)
    {
        $em     = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $agency = $em->find('RealEstate\Entity\Agency', $id);

        if (!$agency instanceof Agency) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($agency->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $agency->setDeleted(true);      
        $em->flush();

        return new JsonModel(array('sucees' => true));
    }

}
