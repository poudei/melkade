<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Bookmark;
use RealEstate\Entity\EstatePhoto;
use Application\Service\DateService;
use RealEstate\Entity\Neighbor;
use RealEstate\Entity\User;
use RealEstate\Entity\Agency;
use RealEstate\Entity\Agent;

class EstateMobileController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 100);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        $user    = $this->identity();

        $neighborId = $this->params()->fromQuery('neighborId');

        if (!empty($neighborId)) {
            $neighbor = $em->find('RealEstate\Entity\Neighbor', $neighborId);

            if ($neighbor->getParentId() == 0) {

                $neighbors = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->findBy(
                        array(
                            "parentId" => $neighborId
                     )
                );

                $neighb = array();
                $ids    = array($neighborId);

                foreach ($neighbors as $neighb) {
                    $ids[] = $neighb->getId();
                }

                $filter[] = "neighborId|in|" . join(',', $ids);
            } else {
                $filter[] = "neighborId|eq|" . $neighborId;
            }
        }

        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Estate', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'neighborId',
            'beds',
            'stories',
            'request',
            'metrage',
            'price',
            'exPrice',
            'area',
            'status',
            'street',
            'yearBuilt',
            'type',
            'advertisedBy',
            'storey',
            'bystreet',
            'code',
            'creationDate',
            'longitude',
            'latitude',
            'ownerUserId',
            'agentUserId',
            'agencyId'
        );

        $hasWhere = false;
        $limitResult = true;
        foreach ($filter as $row) {
            $data = explode('|', $row);

            if ($data[0] == 'date') {

                if (strtotime($data[2]) < strtotime('-14 days')) {
                    $date1m = new \DateTime('-14 days');
                    $data[2] = $date1m->format('Y-m-d');
                }

                $data[2] = "'" . $data[2] . "'";

                $or = $qb->expr()->orX();
                $hasOr = false;

                switch ($data[1]) {
                    case 'gt':
                    case 'gte':
                        $op = $data[1];
                        $or->add($qb->expr()->$op('s.creationDate', $data[2]));
                        $or->add($qb->expr()->$op('s.modifiedDate', $data[2]));
                        $hasOr = true;
                        $limitResult = false;
                        break;
                }

                if ($hasOr) {
                    $and->add($or);
                    $hasWhere = true;
                }
            }

            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                        $and->add($qb->expr()->eq('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;

                    case 'in':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->in('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'between':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->between('s.' . $col, $val[0], $val[1]));
                        $hasWhere = true;
                        break;

                    case 'ora':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->orArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'anda':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->andArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'like':
                        $val = '%' . $data[2] . '%';
                        $and->add($qb->expr()->like('s.' . $data[0], $qb->expr()->literal($val)));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count = $cQuery->getSingleScalarResult();
//estate array 
        $qb->select(" s.id ,
            s.neighborId ,
            s.beds,
            s.stories ,
            s.request ,
            s.metrage ,
            s.price ,
            s.exPrice ,
            s.area ,
            s.status ,
            s.street,
            s.yearBuilt ,
            s.type ,
            s.advertisedBy ,
            s.storey ,
            s.bystreet,
            s.code,
            s.creationDate ,
            s.longitude ,
            s.latitude,
            s.ownerUserId , 
            s.agentUserId ,
            s.agencyId");
        $qb->orderBy('s.' . $orderBy, $orderDy);

        if ($limitResult) {
            $qb->setFirstResult($start)
                    ->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        $estates = $query->getResult();
      
        if ($limitResult) {
            $return = array(
                'start' => $start,
                'limit' => $limit,
                'count' => $count,
                'items' => array()
            );
        } else {
            $return = array(
                'count' => $count,
                'items' => array()
            );
        }

        foreach ($estates as $estate) {
           
            $estateArray = $em->getRepository('\RealEstate\Entity\Estate')
                    ->getMobile($estate['agencyId'], $estate['agentUserId'],
                            $estate['ownerUserId'], $estate['neighborId']);
           
            $estate['creationDate'] = $estate['creationDate']->format('Y-m-d H:i:s');
            
            $estateArray = array_merge($estateArray, $estate);

            if ($user != null) {
                $hasBookmark = $em->getRepository('\RealEstate\Entity\Bookmark')
                        ->hasBookmark($user->getId(), $estate['id']);

                if ($hasBookmark) {
                    $estateArray['bookmarkId'] = $hasBookmark->getId();
                } else {
                    $estateArray['bookmarkId'] = -1;
                }
            } else {
                $estateArray['bookmarkId'] = -1;
            }

            $estateArray['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->estatePh($estate['id']);

            $return['items'][] = $estateArray;
        }

        return new JsonModel($return);
    }

}