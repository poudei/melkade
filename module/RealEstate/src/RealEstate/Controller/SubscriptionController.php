<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use RealEstate\Entity\Subscription;
use Application\Service\DateService;
use RealEstate\Entity\User;

class SubscriptionController extends AbstractActionController 
{
    public function formAction()
    {
       $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
       
        $querys = $em->createQuery("SELECT a FROM RealEstate\Entity\User a");
        $user   = $querys->getResult();

        return new ViewModel(
                array(
                    'user' => $user
             )
        );
    }
    
    public function saveAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();
        
        $users = $em->getRepository('RealEstate\Entity\User')
                ->findOneBy(
                array(
                    'cellphone' => $data['userId']
             )
        );
        
        if ($users == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "کاربر وجود ندارد "));
            return $view;
        }
        
        $userId = $users->getId();
        
        $billDate  = DateService::convertJalaliToGregorian((new \DateTime($data['billDate']))->format('Y-m-d'));   
        $startDate = DateService::convertJalaliToGregorian((new \DateTime($data['startDate']))->format('Y-m-d'));
         
        $subscription = new Subscription();
        
        if ($data['plan'] == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "نوع اشتراک وارد نشده"));
            return $view;
        }
        
        $subscription->setPlan($data['plan']);
        
        if ($data['plan'] == 3001) {
            $amount    = 230000;
            $adsNumber = 200;
        } elseif ($data['plan'] == 3002) {
            $amount    = 190000;
            $adsNumber = 150;
        } elseif ($data['paln'] == 3003) {
            $amount    = 140000;
            $adsNumber = 100;
        } elseif ($data['plan'] == 3004) {
            $amount    = 80000;
            $adsNumber = 50;
        }
        
        if ($data['billAmount'] < $amount) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "مبلغ با نوع پلن همخوانی ندارد"));
            return $view;
        }
        
        $subscription->setBillAmount($amount);    
        $subscription->setAdsNumber($adsNumber);   
        $subscription->setUserId($userId);  
        $subscription->setBillDate(new \DateTime($billDate));
        $subscription->setStartDate(new \DateTime($startDate));
       
        $start = new \DateTime($data['startDate']);
        $endd  = $start->modify('+1 months'); 
        $end   = DateService::convertJalaliToGregorian($endd->format('Y-m-d')); 
        
        $subscription->setEndDate(new \DateTime($end));      
        $subscription->setUsedAds(0);
        
       if ($data['transactionNo'] == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "شماره تراکنش وارد نشده است."));
            return $view;
        }

        if (strlen($data['transactionNo']) < 15) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "شماره تراکنش اشتباه است."));
            return $view;
        }


        $transactionNo = $em->getRepository('RealEstate\Entity\Subscription')
                ->findOneBy(
                array(
                    'transactionNo' => $data['transactionNo']
              )
        );

        if ($transactionNo) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "شماره تراکنش تکراری است."));
            return $view;
        }
        
        $subscription->setTransactionNo($data['transactionNo']);
        $subscription->setCreationDate(new \DateTime('now'));
        $subscription->setDeleted(false);
        
        $em->persist($subscription);      
        $em->flush();

        $dateBill  = DateService::convertGregorianToJalali($subscription->getBillDate()->format('Y-m-d'));
        $dateStart = DateService::convertGregorianToJalali($subscription->getStartDate()->format('Y-m-d'));
        $dateEnd   = DateService::convertGregorianToJalali($subscription->getEndDate()->format('Y-m-d'));

        return new ViewModel(
                  array(
                      'subscription'=> $subscription,
                      'user'        => $users,
                      'data'        => $data,
                      'dateBill'    => $dateBill,
                      'dateStart'   => $dateStart,
                      'dateEnd'     => $dateEnd
                      
              )
        );
    }
}