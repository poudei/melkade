<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Form\Annotation\AnnotationBuilder;
use RealEstate\Entity\Plan;
use RealEstate\Entity\Bill;
use RealEstate\Entity\PlanUser;
use RealEstate\Entity\UserClick;
// estefade nashode
class CurrentUserPlanController extends AbstractRestfulController
{
   public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'a.modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $user    = $this->identity();
        $userId  = $user->getId();
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }
        
        $plan = $em->createQuery("SELECT a FROM RealEstate\Entity\Plan a
            JOIN  RealEstate\Entity\PlanUser p 
            WITH a.id = p.planId
            WHERE p.userId = :user_id AND a.billId IS NOT NULL
            ORDER BY $orderBy $orderDy");
        
        $plan->setParameters(
               array(
                    'user_id' => $userId
              )
        );

        $plan->setMaxResults($limit)
              ->setFirstResult($start);
        
        $plans = $plan->getResult();

        $query = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\Plan a
            JOIN  RealEstate\Entity\PlanUser p 
            WITH a.id = p.planId
            WHERE p.userId = :user_id AND a.deleted = false");
        
        $query->setParameters(
             array(
                  'user_id' => $userId
             ) 
         );
        
        $count = $query->getSingleScalarResult();


        $totalClickAmount = 0;
        $agent  = $em->getRepository('\RealEstate\Entity\Agent')
            ->getUserAgent($user->getId());
        
        if ($agent) {
            $totalClickAmount = $em->getRepository('RealEstate\Entity\UserClick')
                    ->sumClickAmount(null, $agent->getAgencyId());
        } else {
            $totalClickAmount = $em->getRepository('RealEstate\Entity\UserClick')
                    ->sumClickAmount($user->getId(), null);
        }
        
        $totalCredit =  $em->getRepository('RealEstate\Entity\Plan')
                ->getTotalCredit($user->getId());
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
            
            'usedValue'      => $totalClickAmount,
            'totalCredit'    => $totalCredit,
            'remainingValue' => $totalCredit - $totalClickAmount, 
        );

        foreach ($plans as $plan) {
            $planArray = $this->extractAndFill($plan);

            $planArray['bill'] = null;
            if ($plan->getBillId() != null) {
                $bill = $em->find('RealEstate\Entity\Bill', $plan->getBillId());
                if ($bill) {
                    $planArray['bill'] = $this->extract($bill);
                }
            }
            
            $return['items'][] = $planArray;
        }
        
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em     = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user   = $this->identity();
        $userId = $user->getId();
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        } 
        
        $plan = $em->find('RealEstate\Entity\Plan', $id);

        if (!$plan instanceof Plan) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $planUsers = $em->getRepository('RealEstate\Entity\PlanUser')
              ->findOneBy(
                  array(
                      "userId" => $userId, 
                      "planId" => $id
                 )
           );

        if (!$planUsers instanceof PlanUser) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        $array = $this->extractAndFill($plan);

        if ($plan->getBillId() == null) {
            $array['bill'] = null;
        } else {
            $bill = $em->find('RealEstate\Entity\Bill', $plan->getBillId());
            $array['bill'] = $this->extract($bill);
        }

        $users = $em->getRepository('RealEstate\Entity\PlanUser')
            ->findBy(
                 array(
                     "planId" => $id
              ) 
         );

        foreach ($users as $userPlan) {
            $array['planUsers'][] = $this->extract($userPlan);
        }
        
        return new Jsonmodel($array);
    }
}
