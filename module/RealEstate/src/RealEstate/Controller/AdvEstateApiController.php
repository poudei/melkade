<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\EstatePhoto;
use RealEstate\Entity\Bookmark;
use Zend\Crypt\Password\Bcrypt;
use RealEstate\Entity\EstateHistory;
use RealEstate\Entity\AdvEstate;
use RealEstate\Entity\User;

class AdvEstateApiController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 30);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        
        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\AdvEstate', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));
        $and->add($qb->expr()->eq('s.status', 0));
      
        $validNames = array(
            'direction',
            'type',
            'beds',
            'request',
            'metrage',
            'price',
            'exPrice',
            'area',
            'loan',
        );
        
        $hasWhere = false;
        $limitResult = true;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            
            if ($data[0] == 'date') {

                if (strtotime($data[2]) < strtotime('-1 months')) {
                    $date1m  = new \DateTime('-1 months');
                    $data[2] = $date1m->format('Y-m-d');
                }

                $data[2] = "'" . $data[2] . "'";

                $or    = $qb->expr()->orX();
                $hasOr = false;

                switch ($data[1]) {
                    case 'gt':
                    case 'gte':
                        $op = $data[1];
                        $or->add($qb->expr()->$op('s.creationDate', $data[2]));
                        $or->add($qb->expr()->$op('s.modifiedDate', $data[2]));
                        $hasOr = true;
                        $limitResult = false;
                        break;
                }

                if ($hasOr) {
                    $and->add($or);
                    $hasWhere = true;
                }
            }

            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                        $and->add($qb->expr()->eq('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                    
                    case 'in':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->in('s.' . $col, $val));
                        $hasWhere = true;
                        break;
                    
                    case 'between':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->between('s.' . $col, $val[0], $val[1]));
                        $hasWhere = true;
                        break;

                    case 'ora':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->orArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'anda':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->andArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;
                    
                    case 'like':
                        $val = '%' . $data[2] . '%';
                        $and->add($qb->expr()->like('s.' . $data[0], $qb->expr()->literal($val)));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);
        
        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s');
        $qb->orderBy('s.' . $orderBy, $orderDy);

        if ($limitResult) {
             $qb->setFirstResult($start)
                ->setMaxResults($limit);
        }

        $query      = $qb->getQuery();        
        $advEstates = $query->getResult();

        if ($limitResult) {
            $return  = array(
                'start' => $start,
                'limit' => $limit,
                'count' => $count,
                'items' => array(),
            );
        } else {
            $return  = array(
                'count' => $count,
                'items' => array(),
            );
        }
        
        foreach ($advEstates as $advEstate) {
            $estateArray       = $this->extractAndFill($advEstate);
            
             $estateArray['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->advPhoto($advEstate->getId());
             
            $return['items'][] = $estateArray;
        }
        
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        if (is_array($id)) {
            foreach ($id as $ids) {
                $advEstate = $em->find('RealEstate\Entity\AdvEstate', $ids);

                if (!$advEstate instanceof AdvEstate) {
                    $this->getResponse()->setStatusCode(404);
                    return new JsonModel(array('error' => 'Not Found'));
                }

                if ($advEstate->getDeleted()) {
                    $this->getResponse()->setStatusCode(404);
                    return new JsonModel(array('error' => 'Not Found'));
                }

                $array = $this->extractAndFill($advEstate);
                
                $array['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->advPhoto($advEstate->getId());
                
                $advEstateaArray[] = $array;
            }
            return new JsonModel($advEstateaArray);
        } else {

            $advEstate = $em->find('RealEstate\Entity\AdvEstate', $id);

            if (!$advEstate instanceof AdvEstate) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            if ($advEstate->getDeleted()) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            $array = $this->extractAndFill($advEstate);

             $array['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                    ->advPhoto($advEstate->getId());
            
            return new JsonModel($array);
        }
    }

    public function create($data)
    {
        $em        = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');
        $builder   = new AnnotationBuilder();
        $advEstate = new AdvEstate();
        $form      = $builder->createForm($advEstate);
       
        $form->setHydrator($hydrator);
        $form->bind($advEstate);
   
        $form->setData($data);
       
        $user = $this->identity();
       
        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        $currentUser = $em->find('RealEstate\Entity\User', $user->getId());
      
        if (!$currentUser instanceof User) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $type = $currentUser->getType();

        if ($type == 2501) {
            $isAgent = false;
        } elseif ($type == 2502) {
            $isAgent = true;
        } elseif ($type == 2503) {
            $isAgent = null;
        }
       
        if ($form->isValid()) {
            $advEstate = $form->getData();

            $advEstate->setStatus(0);
            $advEstate->setCity(87);
            $advEstate->setState(87);
            $advEstate->setIsAgent($isAgent);
            $advEstate->setCreationDate(new \DateTime('now'));
            $advEstate->setModifiedDate(new \DateTime('now'));
            $advEstate->setDeleted(false);
            $advEstate->setUserId($user->getId());
            
            $em->persist($advEstate);
      /*     
            foreach ($_FILES as $file) {
               if ($file['error']) {
                    continue;
               }
                 $md5    = md5(microtime());
                 $random = substr($md5, 0, 10);
                
                 $name = $random . '-' . $file['name'];
                 $path = ROOT_PATH . "/public/files/images/" . $name;

                 copy($file['tmp_name'], $path);
                 $path = "/files/images/" . $name;

                 $photo = new EstatePhoto();
                 $photo->setImage($path);
                 $photo->setType(0);
                 $photo->setEstateId($advEstate->getId());
                 $photo->setCreationDate(new \DateTime('now'));
                 $photo->setModifiedDate(new \DateTime('now'));
                 $photo->setDeleted(false);

                 $em->persist($photo);
            }
       */
            $em->flush();

            return new JsonModel($this->extractAndFill($advEstate));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }
    
    public function update($id, $data)
    {
        $em        = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator  = $this->getServiceLocator()->get('Hydrator');
        $advEstate = $em->find('RealEstate\Entity\AdvEstate', $id);

        if (!$advEstate instanceof AdvEstate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if ($advEstate->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $preData = $this->extract($advEstate);
        $data    = array_merge($preData, $data);
        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($advEstate);

        $form->setHydrator($hydrator);
        $form->bind($advEstate);

        $form->setData($data);

        if ($form->isValid()) {
            $advEstate = $form->getData();

            $advEstate->setModifiedDate(new \DateTime('now'));
            $advEstate->setLastModifiedBy($this->identity()->getId());

            $em->flush();

            return new JsonModel($this->extractAndFill($advEstate));
        } else {
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function delete($id)
    {
        $em        = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $advEstate = $em->find('RealEstate\Entity\AdvEstate', $id);
        $user      = $this->identity();

        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }

        if ($user->getType() == 2503) {

            if (!$advEstate instanceof DevEstate) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            if ($advEstate->getDeleted()) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Not Found'));
            }

            $advEstate->setDeleted(true);

            // $em->remove($estate);
            $em->flush();

            return new JsonModel(array('sucees' => true));
        }
        $this->getResponse()->setStatusCode(403);
        return new JsonModel(array('error' => 'Forbidden'));
    }

    public function nearbyAction()
    {
        $em          = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator    = $this->getServiceLocator()->get('Hydrator');
        $advEstates  = $em->getRepository('RealEstate\Entity\AdvEstates')
                  ->getNearbyPoints();
        
        $return = array();

        foreach ($advEstates as $advEstate) {
            $return[] = $advEstate;
        }
        
        $em->flush();
        return new JsonModel($return);
    }
}
