<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\EstatePhoto;
use RealEstate\Entity\Neighbor;
use RealEstate\Entity\AdvEstate;
use Application\Service\DateService;

class AdvEstateController extends AbstractActionController
{

    public function indexAction()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 30);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', 'DESC');
        $order    = array($orderBy => $orderDy);
        $request  = $this->getRequest()->getPost('request');
        $neighbor = $this->getRequest()->getPost('neighbor');
        
        if (isset($neighbor) && $neighbor != "") {
            $neighbor_condition = "AND a.neighborId =" . $neighbor ;
        } else {
            $neighbor_condition = "";
        }

        if (isset($request) && $request != "") {
            $request_condition = "AND a.request =" . $request;
        } else {
            $request_condition = "";
        }

        $advs = $em->createQuery("SELECT a FROM RealEstate\Entity\AdvEstate a
                WHERE a.status = '0' AND a.deleted=false " . $request_condition . "  " . $neighbor_condition . " ");
      
        $advs->setFirstResult($start);
        $advs->setMaxResults($limit);

        $result = $advs->getResult();

        $query = $em->createQuery("SELECT count(a) FROM RealEstate\Entity\AdvEstate a
                 WHERE a.status = '0' AND a.deleted=false " . $request_condition . "  " . $neighbor_condition . "");

        $query->setParameters(array());

        $count = $query->getSingleScalarResult();
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($result as $adv) {
            $advArray = $this->extractAndFill($adv);
            
            $advArray['creationDate']  = DateService::convertGregorianToJalali($adv->getCreationDate()->format('Y-m-d'));
           
            $return['items'][]  = $advArray;
        }

        $view = new ViewModel(
                 array(
                      'result' => $return,
                       'start' => $start,
                       'limit' => $limit,
                       'count' => $count
           )
        );

        return $view;
    }

    public function viewAction()
    { 
        $em     = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $advId  = $this->params()->fromRoute("id", null);       
        $adv    = $em->find('RealEstate\Entity\AdvEstate', $advId);

        if (!$adv instanceof AdvEstate) {
            $view = new ViewModel(array());

            $view->setTemplate('real-estate/adv-estate/error');
            return $view;
        }

        if ($adv->getDeleted()) {
            $view = new ViewModel(array());

            $view->setTemplate('real-estate/adv-estate/error');
            return $view;
        }
        $array = $this->extractAndFill($adv);

         $array['creationDate'] = DateService::convertGregorianToJalali($adv->getCreationDate()->format('Y-m-d'));

        $array['photos'] = $em->getRepository('\RealEstate\Entity\EstatePhoto')
                ->advPhoto($adv->getId());
        
        $neighbors = $adv->getNeighborId();
        
        if (isset($neighbors) && !empty($neighbors)) {
            $neighbor = $em->find('RealEstate\Entity\Neighbor', $neighbors);
            $neighbor = $neighbor->getTitle();
        } else {
            $neighbor = null;
        }
        
        $request = '';
       switch ($adv->getRequest()) {
            case 301:
                $request .= 'فروش ';
                break;
            case 303:
                $request .= ' رهن و اجاره';
                break;
            case 302:
                $request .= 'پیش فروش ';
        }
       
       $pageTitle = '';
       switch ($adv->getType()) {
            case 201:
                $pageTitle   .= 'آپارتمان ';
                break;
            case 202 :
                  $pageTitle .= 'ویلایی ';
                break;
            case 203:
                  $pageTitle .= 'زمین ';
                break;
            case 204:
                  $pageTitle .= 'اداری ';
                break;
            case 205:
                $pageTitle   .= 'مغازه ';
                break;
            case 206:
                  $pageTitle .= 'مستغلات ';
                break;
        }
        
         $pageTitl = ' ' . $request . ' ' . $pageTitle . ' ' . (int)$adv->getMetrage() . ' متری  ' . ',' . $neighbor. ' ' . ' ,' . $adv->getStreet();

        $view = new ViewModel(
                  array(
                       'neighbor'  => $neighbor,
                       'adv'    => $array,
                       'pageTitle' => $pageTitl
                  )
           );
        return $view;
    }

}