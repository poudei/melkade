<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Bookmark;
use RealEstate\Entity\Neighbor;

class PageController extends AbstractActionController
{
    public function aboutAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $view = new ViewModel(
                    array(
                 )
            );
        return $view;
    }

     public function comAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $view = new ViewModel(
                   array(
                  )
             );
        
         $view->setTerminal(true);
        return $view;
    }
}
