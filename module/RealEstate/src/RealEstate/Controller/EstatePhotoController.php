<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\EstatePhoto;
use RealEstate\Entity\Estate;
use RealEstate\Entity\Subscription;

class EstatePhotoController extends AbstractRestfulController
{

    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $order   = array($orderBy => $orderDy);
        $id      = $this->params()->fromRoute("estate_id");
        $estate  = $em->find('RealEstate\Entity\Estate', $id);

        if (!$estate instanceof Estate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $photos = $em->getRepository('RealEstate\Entity\EstatePhoto')
           ->findBy(
               array(
                    "estateId" => $id,
                    "deleted"  => false,
                    "type"     => 1
                  ),
                  $order,
                  $limit,
                  $start
             );

        $query = $em->createQuery("SELECT  count(s) FROM RealEstate\Entity\EstatePhoto s 
              WHERE s.estateId = :estate_id AND s.deleted = false AND s.type = '1'");
        
        $query->setParameters(
             array(
                 'estate_id' => $id
             )
        );
        
        $count  = $query->getSingleScalarResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($photos as $photo) {
            $return['items'][] = $this->extractAndFill($photo);
        }

        return new JsonModel($return);
    }

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $estateId = $this->params()->fromRoute("estate_id");
        $estate   = $em->find('RealEstate\Entity\Estate', $estateId);
        $user     = $this->identity();

        if (!$estate instanceof Estate) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Unauthorized'));
        }
 /*       
        $paid = $estate->getPaid();
        
        if ($paid == 0) {
            $owner  = (int) $estate->getOwnerUserId();
            $agent  = (int) $estate->getAgentUserId();
            $agency = (int) $estate->getAgencyId();

            $now = new \DateTime('now');
            $end = $now->modify('-1 months')->format('Y-m-d');

            $subscription = $em->createQuery("SELECT s FROM RealEstate\Entity\Subscription s
               WHERE (s.userId ='$owner' OR s.userId ='$agency' OR 
               s.userId = '$agent') AND s.plan = 3005 AND s.deleted = false 
                   AND s.startDate >= '$end' ORDER BY s.startDate DESC");

            $subscriptions = $subscription->getResult();

            if (empty($subscriptions)) {
                $this->getResponse()->setStatusCode(404);
                return new JsonModel(array('error' => 'Empty Plan '));
            }
            
            $querys = $em->createQuery("UPDATE RealEstate\Entity\Estate e
                    SET e.addPhoto = '1' 
                    WHERE e.id = $estateId AND (e.ownerUserId ='$owner' OR e.agencyId ='$agency' OR 
                    e.agentUserId = '$agent') AND e.paid = '0' AND e.addPhoto = '0'");
            
            $querye = $querys->execute();
            
        }
    */    
        if (!isset($_FILES)) {
            return new JsonModel(array('status' => 'error'));
        }

        foreach ($_FILES as $file) {

            if ($file['error']) {
                continue;
            }

            $md5 = md5(microtime());
            $random = substr($md5, 0, 10);

            $name = $random . '-' . $file['name'];
            $path = ROOT_PATH . "/public/files/images/" . $name;

            copy($file['tmp_name'], $path);
            $path = "/files/images/" . $name;

            $photo = new EstatePhoto();
            $photo->setImage($path);
            $photo->setEstateId($estateId);
            $photo->setType(1);
            $photo->setCreationDate(new \DateTime('now'));
            $photo->setModifiedDate(new \DateTime('now'));
            $photo->setDeleted(false);

            $em->persist($photo);
        }


        $em->flush();

        if (isset($photo)) {
            return new JsonModel($this->extractAndFill($photo));
        } else {
             $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
    }
}