<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Request;
// estefade nashode .. barye admin user che requesti zade
class RequestController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $filter  = $this->params()->fromQuery('filter', array());
        $userId  = $this->params()->fromRoute('userId', null);
        $user    = $em->find('RealEstate\Entity\User', $userId);
        
        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }

        $qb = $em->createQueryBuilder();
        $qb ->from('RealEstate\Entity\Request', 's');
        
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        if (!empty($userId)) {
            $and->add($qb->expr()->eq('s.userId', $userId));
        }

        $validNames = array(
            'requestDate',
            'requestType',
            'estateType',
            'districtId',
            'expireDate',
            'comments',
            'bedrooms',
            'marital',
            'metrage',
            'parking',
            'persons',
            'deposit',
            'budget',
            'userId',
            'storey',
            'rent'
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
                }
            }
        }

        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();
        
        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);
        
        $query    = $qb->getQuery();
        $requests = $query->getResult();
        $return   = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );
        
        foreach ($requests as $request) {
            $return['items'][] = $this->extractAndFill($request);
        }
        
        return new JsonModel($return);
    }

    public function get($id)
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $request = $em->find('RealEstate\Entity\Request', $id);
        
        if (!$request instanceof Request) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        if ($request->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $array = $this->extractAndFill($request);

        return new JsonModel($array);
    }
}
