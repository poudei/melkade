<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\Estate;

class EstateSimilarsController extends AbstractRestfulController
{
    public function getList()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start    = $this->params()->fromQuery('start', 0);
        $limit    = $this->params()->fromQuery('limit', 10);
        $orderBy  = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy  = $this->params()->fromQuery('orderDy', ' DESC');
        $estateId = $this->params()->fromRoute("estate_id");
        $estate   = $em->find('RealEstate\Entity\Estate', $estateId);

        if ($estate == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $filter   = array();
        $filter[] = 'id|neq|' . $estateId;

        $type = $estate->getType();
        if (!empty($type)) {
            $filter[] = 'type|eq|' . $type;
        }

        $request = $estate->getRequest();
        if (!empty($request)) {
            $filter[] = 'request|eq|' . $request;
        }

        $price = $estate->getPrice();
        if (!empty($price)) {
            $threshold = (int) $price / 5;
            $min       = $price - $threshold;
            $max       = $price + $threshold;
            $filter[]  = ' price|between|' . implode('-', array($min, $max));
        }

        $exPrice = $estate->getExPrice();
        if (!empty($exPrice)) {
            $threshold = (int) $exPrice / 5;
            $min       = $exPrice - $threshold;
            $max       = $exPrice + $threshold;
            $filter[]  = 'exPrice|between|' . implode('-', array($min, $max));
        }

        $qb = $em->createQueryBuilder();
        $qb->from('RealEstate\Entity\Estate', 's');

        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('s.deleted', "false"));

        $validNames = array(
            'id',
            'estateId',
            'metrage',
            'price',
            'exPrice',
            'type',
            'request'
        );
        
        $hasWhere = false;
        foreach ($filter as $row) {
            $data = explode('|', $row);
            if (in_array($data[0], $validNames)) {
                switch ($data[1]) {
                    case 'eq':
                    case 'lt':
                    case 'gt':
                    case 'gte':
                    case 'lte':
                    case 'neq':
                        $op = $data[1];
                        $and->add($qb->expr()->$op('s.' . $data[0], $data[2]));
                        $hasWhere = true;
                        break;
        
                    case 'between':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode('-', $val);
                        $and->add($qb->expr()->between('s.' . $col, $val[0], $val[1]));
                        $hasWhere = true;
                        break;

                    case 'ora':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->orArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;

                    case 'anda':
                        $col = $data[0];
                        $val = strtolower($data[2]);
                        $val = explode(',', $val);
                        $and->add($qb->expr()->andArray('s.' . $col, $val));
                        $hasWhere = true;
                        break;
                }
            }
        }
        $qb->where($and);

        $cqb = clone $qb;
        $cqb->select("count(s.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $qb->select('s')
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->orderBy('s.' . $orderBy, $orderDy);

        $query   = $qb->getQuery();
        $estates = $query->getResult();
        $return  = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );
        
        foreach ($estates as $estate) {
            $estateArray       = $this->extractAndFill($estate);
            $return['items'][] = $estateArray;
        }
        return new JsonModel($return);
    }
}
