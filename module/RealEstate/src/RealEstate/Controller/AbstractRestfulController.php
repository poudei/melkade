<?php

namespace RealEstate\Controller;

use Application\Controller\AbstractRestfulController as BaseAbstractRestfulController;
use Zend\View\Model\JsonModel;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\Query\ResultSetMapping;
use Application\Service\DateService;

class AbstractRestfulController extends BaseAbstractRestfulController
{
    protected function fill($data, $object)
    {
         parent::fill($data, $object);
         
         $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        if ($object instanceof \RealEstate\Entity\User) {
            unset($data['password']);
        }

        if ($object instanceof \RealEstate\Entity\Estate) {

            if (isset($data['agencyId']) && !empty ($data['agencyId'])) {
                $agency = $em->getRepository('RealEstate\Entity\Agency')
                        ->find($data['agencyId']);
                if ($agency) {
                    $data['agency'] = array(
                        'id'    => $agency->getId(),
                        'name'  => $agency->getName(),
                        'phone' => $agency->getPhones()
                    );
                }
            }

            if (isset($data['agentUserId']) && !empty($data['agentUserId'])) {
                $agentUser = $em->getRepository('RealEstate\Entity\User')
                        ->find($data['agentUserId']);
                if ($agentUser) {
                    $data['agentUser'] = array(
                        'id'          => $agentUser->getId(),
                        'lastname'    => $agentUser->getLastname(),
                        'cellphone'   => $agentUser->getCellphone(),
                        'agentPhones' => $agentUser->getPhones()
                    );
                }
            }
            
            if (isset($data['agentUserId']) && isset($data['agencyId'])) {
                $agent = $em->getRepository('RealEstate\Entity\Agent')
                  ->findOneBy(
                         array(
                             'userId'   => $data['agentUserId'],
                             'agencyId' => $data['agencyId']
                       )
                );

                if ($agent) {
                    $data['agent'] = $this->extract($agent);
                }
            }

             if (isset($data['ownerUserId']) && !empty($data['ownerUserId'])) {
                $ownerUser = $em->getRepository('RealEstate\Entity\User')
                        ->find($data['ownerUserId']);

                if ($ownerUser) {
                    $data['ownerUser'] = array(
                        'id'        => $ownerUser->getId(),
                        'lastname'  => $ownerUser->getLastname(),
                        'cellphone' => $ownerUser->getCellphone(),
                        'phone'     => $ownerUser->getPhones()
                    );
                    
                }
            }
            
            if (isset($data['neighborId']) && !empty($data['neighborId'])) {
                $neighbor = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->find($data['neighborId']);

                if ($neighbor) {
                    $data['neighbor'] = array(
                        'id'      => $neighbor->getId(),
                        'title'   => $neighbor->getTitle(),
                        'path'    => $neighbor->getPath(),
                        'parentId'=> $neighbor->getParentId()
                    );
                }
            }
        }
        if ($object instanceof \RealEstate\Entity\AdvEstate) {
            
        if (isset($data['neighborId']) && !empty($data['neighborId'])) {
                $neighbor = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->find($data['neighborId']);

                if ($neighbor) {
                    $data['neighbor'] = array(
                        'id'      => $neighbor->getId(),
                        'title'   => $neighbor->getTitle(),
                        'path'    => $neighbor->getPath(),
                        'parentId'=> $neighbor->getParentId()
                    );
                }
            }
            
            if (isset($data['userId']) && !empty($data['userId'])) {
                $user = $em->getRepository('RealEstate\Entity\User')
                        ->find($data['userId']);
                if ($user) {
                    $data['user'] = array(
                        'id'          => $user->getId(),
                        'firstname'   => $user->getFirstname(),
                        'lastname'    => $user->getLastname(),
                        'cellphone'   => $user->getCellphone(),
                        'type'        => $user->getType()
                    );
                }
            }
        } 

            
        if ($object instanceof \RealEstate\Entity\Bookmark) {
            $estateId = $object->getEstateId();
            if (!empty($estateId)) {
                $estate = $em->getRepository('RealEstate\Entity\Estate')
                        ->find($estateId);

                if ($estate) {
                    $data['estate'] = $this->extract($estate);
                }
                
                 $neighbor = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->find($estate->getNeighborId());

                if ($neighbor) {
                    $data['neighbor'] = array(
                        'id'      => $neighbor->getId(),
                        'title'   => $neighbor->getTitle(),
                        'path'    => $neighbor->getPath(),
                        'parentId'=> $neighbor->getParentId()
                    );
                }
            }
        }

        if ($object instanceof \RealEstate\Entity\Agent) {
            $userId = $object->getUserId();
            if (!empty($userId)) {
                $user = $em->getRepository('RealEstate\Entity\User')
                        ->find($userId);

                if ($user) {
                    $data['user'] = $this->extract($user);
                }  
            }
        }
        
        if ($object instanceof \RealEstate\Entity\Agency) {
            if (isset($data['neighborId']) && !empty($data['neighborId'])) {
                $neighbor = $em->getRepository('RealEstate\Entity\Neighbor')
                        ->find($data['neighborId']);

                if ($neighbor) {
                    $data['neighbor'] = array(
                        'id'      => $neighbor->getId(),
                        'title'   => $neighbor->getTitle(),
                        'path'    => $neighbor->getPath(),
                        'parentId'=> $neighbor->getParentId()
                    );
                }
            }
        }   
       
        return $data;
    }
    
    protected function extract($object)
    {
        $data = parent::extract($object);

        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        if ($object instanceof \RealEstate\Entity\Estate) {
                  
          /*  $user = $this->identity();
            $unsetValues = true;
            $planStatus  = 0;

            if ($user == null) {
                $unsetValues = true;
            } else {
                if ($user->getType() == 2503) {
                    $unsetValues = false;
                    $planStatus  = 1;
                } else {
                    $unlimitedPlans = $em->getRepository('\RealEstate\Entity\Plan')
                            ->getUnlimitedPlans($user->getId());
                    
                    $hasUnlimitedPlan = false;
                    foreach ($unlimitedPlans as $plan) {
                        if (in_array($object->getNeighborId(), $plan->getRegions())) {
                            $unsetValues = false;
                            $planStatus  = 2;
                            $hasUnlimitedPlan = true;
                            break;
                        }
                    }

                    if ($hasUnlimitedPlan == false) {
                        $userId = $user->getId();
                        $agent  = $em->getRepository('\RealEstate\Entity\Agent')
                                ->getUserAgent($user->getId());

                        $totalCredit = $em->getRepository('\RealEstate\Entity\Plan')
                                ->getTotalCredit($user->getId());
                        if ($agent) {
                            $totalClick = $em->getRepository('\RealEstate\Entity\UserClick')
                                    ->sumClickAmount(null, $agent->getAgencyId());
                        } else {
                            $totalClick = $em->getRepository('\RealEstate\Entity\UserClick')
                                    ->sumClickAmount($userId, null);
                        }

                        if ($totalCredit > $totalClick) {
                            $unsetValues = false;
                            $planStatus  = 3;
                        } else {
                            $unsetValues = true;
                            $planStatus  = 4;
                        }
                    }
                }
            }
            $data['planStatus'] = $planStatus;
            if ($unsetValues) {
                unset($data['visitDescription']);
                unset($data['advertisedBy']);
                unset($data['ownerUserId']);
                unset($data['agencyId']);
                unset($data['agentUserId']);
            }*/
        }
        
        return $data;
    }
}

