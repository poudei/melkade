<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use RealEstate\Entity\UserClick;
use RealEstate\Entity\Estate;
use RealEstate\Entity\User;
use RealEstate\Entity\Agency;
// estefade nashode
class UserClickController extends AbstractRestfulController
{
    public function getList()
    {
        $em      = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $start   = $this->params()->fromQuery('start', 0);
        $limit   = $this->params()->fromQuery('limit', 10);
        $orderBy = $this->params()->fromQuery('orderBy', 'modifiedDate');
        $orderDy = $this->params()->fromQuery('orderDy', 'DESC');
        $order   = array($orderBy => $orderDy);
        $userId  = $this->params()->fromRoute("user_id");
        $user    = $em->find('RealEstate\Entity\User', $userId);

        if ($user == null) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not found'));
        }
        
        $userClicks = $em->getRepository('RealEstate\Entity\UserClick')
             ->findBy(
                 array(
                      "userId" => $user->getId(), 
                      "deleted" => false
                    ), 
                    $order,
                    $limit, 
                    $start
               );

        $query = $em->createQuery("SELECT count(s) FROM RealEstate\Entity\UserClick s
                       WHERE s.userId = :user_id AND  s.deleted = false");

        $query->setParameters(
                array(
                    'user_id' => $user->getId()
             )
        );

        $count = $query->getSingleScalarResult();
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($userClicks as $userClick) {
            $userClickArray = $this->extractAndFill($userClick);

            $estate = $em->find('RealEstate\Entity\Estate', $userClick->getEstateId());
            $userClickArray['estate'] = $this->extract($estate);

            $return['items'][] = $userClickArray;
        }
        return new JsonModel($return);
    }
}
