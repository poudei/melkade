<?php

namespace RealEstate\Controller;

use RealEstate\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use RealEstate\Entity\Subscription;
use Application\Service\DateService;
use RealEstate\Entity\User;

class UserSubscriptionController extends AbstractActionController
{
    public function indexAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user = $this->identity();

        if ($user == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "کاربر وجود ندارد "));
            return $view;
        }
        
        $userId   = $user->getId();
        $cellphone = $user->getCellphone();
        
        $subscriptions = $em->getRepository('RealEstate\Entity\Subscription')
                ->findBy(
                array(
                    'userId' => $userId
             )
        );
        
        if ($subscriptions == null) {
            $view = new ViewModel(array());
            $view->setTemplate('real-estate/subscription/error');
            $view->setVariables(array("error" => "در حال حاضر پلنی برای کاربر $cellphone ثبت  نشده است. "));
            return $view;
        }
        
        $billDate  = array();
        $startDate = array();
        $endDate   = array();
        
        foreach ($subscriptions as $subscription) {
            //ba extract tabdil be array , key value :
            $billDate[$subscription->getId()]  = DateService::convertGregorianToJalali($subscription->getBillDate()->format('Y-m-d'));
            $startDate[$subscription->getId()] = DateService::convertGregorianToJalali($subscription->getStartDate()->format('Y-m-d'));
            $endDate[$subscription->getId()]   = DateService::convertGregorianToJalali($subscription->getEndDate()->format('Y-m-d'));
        }
        
        $return = array(
            'items' => $subscriptions
        );
        
        return new ViewModel(
                array(
                    'subscription'=> $return,
                    'user'        => $user,
                    'billDate'    => $billDate,
                    'startDate'   => $startDate,
                    'endDate'     => $endDate
             )
        );
        
    }
}