<?php

namespace RealEstate\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;
use RealEstate\Entity\Bill;
use RealEstate\Entity\Plan;
use RealEstate\Entity\PlanUser;
use RealEstate\Entity\User;
// estedafe nashode
class PlanBuyController extends AbstractActionController
{
    public function buyAction()
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $query     = $em->createQuery("SELECT a FROM RealEstate\Entity\Neighbor a");
        $neighbors = $query->getResult();

        return new ViewModel(
                array(
                    'neighbors' => $neighbors
             )
        );
    }

    public function checkoutAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();

        if ($data['type'] == 2802) {
            $plan = new Plan();
            $plan->setMonths((int) $data['months']);
            $plan->setUsers($data['usersCount']);
            $plan->setAmount($data['amount']);
            $plan->setAgencyId($data['agencyid']);
            $plan->setType(2801);
            $plan->setCreationDate(new \DateTime('now'));
            $plan->setDeleted(false);
        }

        if ($data['type'] == 2801) {
            $plan = new Plan();
            $plan->setMonths((int) $data['months']);
            $plan->setUsers($data['usersCount']);
            $plan->setStartDate(new \DateTime('now'));
            $plan->setEndDate(new \DateTime((int)$data['months'] . ' months'));
            $plan->setRegions($data['regions']);
            $plan->setAmount($data['amount']);
            $plan->setAgencyId($data['agencyId']);
            $plan->setType(2802);
            $plan->setCreationDate(new \DateTime('now'));
            $plan->setDeleted(false);

        }

        $em->persist($plan);
        $em->flush();

        $regions = array();

        if (is_array($data['regions'])) {
            foreach ($data['regions'] as $regionId) {
                $regions[] = $em->find('RealEstate\Entity\Neighbor', $regionId);
            }
        }
        
        return new ViewModel(
                array(
                    'regions' => $regions,
                    'plan'    => $plan
            )
        );
    }

    public function paymentAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();
        $plan = $em->find('RealEstate\Entity\Plan', $data['plan_id']);

        if ($plan == null) {
            $view = new ViewModel(
                   array(
                 )
            );

            $view->setTemplate('real-estate/plan-buy/error');
            return $view;
        }

        return new ViewModel(
               array(
                    'plan' => $plan
             )
        );
    }

    public function callbackAction()
    {
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $data = $this->getRequest()->getPost();
        $plan = $em->find('RealEstate\Entity\Plan', $data['ResNum']);
        $user = $this->identity();
        
        if (!$plan instanceof Plan) {
             $view = new ViewModel(
                     array(
                         
                )
            );

            $view->setTemplate('real-estate/plan-buy/error');
            return $view;
        }

        if (($data['Status'] == 'OK') && !empty($data['RefNum'])) {

            $client = new SoapClient("https://modern.enbank.net/ref-payment/ws/ReferencePayment?WSDL");

            $result = $client->VerifyTransaction("RefNum", "MID");

            if ($result <= $plan->getAmount()) {

                $soapclient = new soapclient('https://modern.enbank.net/ref-payment/ws/ReferencePayment?WSDL', 'wsdl');

                $res = $soapclient->ReverseTransaction('RefNum', 'MID', 'MPassword', 'Amount');

                if ($res == 1) {
                    echo 'reversed successfully' ;
                    $viewModel = new ViewModel( 
                            array(                           
                        )
                    );

                    $viewModel->setTemplate('real-estate/plan-buy/success');
                    return $viewModel;
                } else {
                    echo 'reversed failed' ;
                    $view = new ViewModel(
                            array(        
                        )
                    );

                    $view->setTemplate('real-estate/plan-buy/error');
                    return $view;
                }
                
            } else {

                $bill = new Bill();
                $bill->setAmount($result);
                $bill->setPayerId($user->getId());
                $bill->setTransactionNo($data['RefNum']);
                $bill->setBillDate(new \DateTime('now'));
                $bill->setCreationDate(new \DateTime('now'));
                $bill->setDeleted(false);

                $em->persist($bill);
                $plan->setBillId($bill->getId());
                
                $planUser = new PlanUser();
                $planUser->setUserId($user->getId());
                $planUser->setPlanId($plan->getId());
                $planUser->setCreationDate(new \DateTime('now'));
                $planUser->setDeleted(false);
                
                $em->persist($planUser);

                $em->flush();

                $viewModel = new ViewModel(
                        array(
                   )
                );

                $viewModel->setTemplate('real-estate/plan-buy/success');
                return $viewModel;
                
                return new ViewModel(
                         array(
                              'bill' => $bill,
                              'plan' => $plan,
                              'planUser' => $planUser
                    )
                );
            }
        } else {
            $view = new ViewModel(
                    array(
                )
            );

            $view->setTemplate('real-estate/plan-buy/error');
            return $view;
        }
    }
}
