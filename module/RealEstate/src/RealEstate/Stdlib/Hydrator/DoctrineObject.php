<?php

namespace RealEstate\Stdlib\Hydrator;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as BaseHydrator;
use Application\Entity\Feed;

class DoctrineObject extends BaseHydrator
{

    /**
     * Extract values from an object
     *
     * @param  object $object
     * @return array
     */
    public function extract($object)
    {
        $data = parent::extract($object);

        return $data;
    }

}
