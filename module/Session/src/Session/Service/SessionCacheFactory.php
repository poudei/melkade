<?php
namespace Session\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SessionCacheFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sl)
    {
        $zendCache = new \Zend\Cache\Storage\Adapter\Memory();
        $cache = new \DoctrineModule\Cache\ZendStorageCache($zendCache);
        /* I've added the additional line below that could be used as an
         * example to the other cache handlers that are available.
         * By default it's set to use ZendStorageCache (Enabled above)
         */
        //$cache = new \Doctrine\Common\Cache\FilesystemCache("data/cache");
        return $cache;
    }
}
