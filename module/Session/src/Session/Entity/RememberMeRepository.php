<?php
namespace Session\Entity;

use Session\Entity\Remember,
    GoalioRememberMeDoctrineORM\Repository\RememberMe as BaseRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class RememberMeRepository extends BaseRepository
{
    public function deleteByUid($uid)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete($this->_entityName,'r')
            ->where('r.user_id= :uid')
            ->setParameter('uid', $uid);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function deleteByUidAndSid($uid,$sid)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete($this->_entityName,'r')
            ->where('r.user_id = :uid')
            ->andWhere('r.sid = :sid')
            ->setParameter('uid', $uid)
            ->setParameter('sid', $sid);

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return parent::findOneBy($criteria, $orderBy);
    }
}
