<?php
namespace Session\SaveHandler;

use Zend\Session\SaveHandler\SaveHandlerInterface;
use Doctrine\ORM\EntityManager;

class DoctrineGateway implements SaveHandlerInterface
{

    /**
     * Session Save Path
     *
     * @var string
     */
    protected $sessionSavePath;

    /**
     * Session Name
     *
     * @var string
     */
    protected $sessionName;

    /**
     * Lifetime
     * @var int
     */
    protected $lifetime;

    /** @var \Zend\ServiceManager\ServiceLocatorInterface */
    protected $sm;
    protected $em;

    /**
     *
     * @param \Zend\ServiceManager\ServiceLocatorInterface $serviceManager
     */
    public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->sm = $serviceManager;
    }

    /**
     * Open Session
     *
     * @param  string $savePath
     * @param  string $name
     * @return bool
     */
    public function open($savePath, $name)
    {
        $this->sessionSavePath = $savePath;
        $this->sessionName     = $name;
        $this->lifetime        = ini_get('session.gc_maxlifetime');

        return true;
    }

    /**
     * Close session
     *
     * @return bool
     */
    public function close()
    {
        return true;
    }

    /**
     * Read session data
     *
     * @param string $id
     * @return string
     */
    public function read($id)
    {
        $row = $this->getEntityManager()->find('Session\Entity\Session', $id);
        if ($row) {
            if (($row->getModified() + $row->getLifetime()) > time()) {
                return $row->getData();
            }
            $this->destroy($id);
        }
        return '';
    }

    /**
     * Write session data
     *
     * @param string $id
     * @param string $data
     * @return bool
     */
    public function write($id, $data)
    {
        $repo = $this->getEntityManager()->getRepository('Session\Entity\Session');

        if (!$entity = $repo->findOneBy(array('id' => $id))) {
            $entity = new \Session\Entity\Session();
        }

        $authService = $this->sm->get('Zend\Authentication\AuthenticationService');
        if ($authService->hasIdentity()) {
            $entity->setUserId($authService->getIdentity()->getId());
        }

        $entity->setModified(time());
        $entity->setData((string) $data);
        $entity->setId($id);
        $entity->setName($this->sessionName);
        $entity->setLifetime($this->lifetime);
        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush($entity);
        return \TRUE;
    }

    /**
     * Destroy session
     *
     * @param  string $id
     * @return bool
     */
    public function destroy($id)
    {
        $entity = $this->getEntityManager()->find('Session\Entity\Session', $id);

        if (!$entity) {
            return false;
        }

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
        return \TRUE;
    }

    /**
     * Garbage Collection
     *
     * @param int $maxlifetime
     * @return true
     */
    public function gc($maxlifetime)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->delete("Session\Entity\Session", 's');
        $qb->andWhere($qb->expr()->gt('(' . time() . ' - s.modified)', 's.lifetime'));
        $query = $qb->getQuery();
        $query->execute();

        return \TRUE;
    }

    /**
     * Sets the EntityManager
     *
     * @param EntityManager $em
     * @access protected
     * @return PostController
     */
    protected function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * Returns the EntityManager
     *
     * Fetches the EntityManager from ServiceLocator if it has not been initiated
     * and then returns it
     *
     * @access protected
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if (null === $this->em) {
            $this->setEntityManager($this->sm->get('Doctrine\ORM\EntityManager'));
        }
        return $this->em;
    }
}
