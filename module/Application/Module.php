<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener,
    Zend\Mvc\MvcEvent,
    Zend\Form\Annotation\AnnotationBuilder,
    Application\Mvc\View\JsonExceptionStrategy;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $application         = $e->getApplication();
        $eventManager        = $e->getApplication()->getEventManager();
        $serviceManager      = $e->getApplication()->getServiceManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        /*
          $application->getResponse()->getHeaders()->addHeaders(
          array("Access-Control-Allow-Origin" => "*")
          );
         */
        
        try {
            \Doctrine\DBAL\Types\Type::addType('set', 'Application\DBAL\Types\SetType');
            \Doctrine\DBAL\Types\Type::addType('intarray', 'Application\DBAL\Types\IntArrayType');
        } catch (\Exception  $exp) { }

        $em = $serviceManager->get('Doctrine\ORM\EntityManager');
        $em->getConnection()
                ->getDatabasePlatform()->registerDoctrineTypeMapping('IntARRAY', 'intarray');
        $em->getConnection()
                ->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        $em->getConnection()
                ->getDatabasePlatform()->registerDoctrineTypeMapping('Set', 'set');

        $em->getConnection()
                ->getDatabasePlatform()->registerDoctrineTypeMapping('_int4', 'integer');

        $application
            ->getEventManager()
            ->attach('route', array('Application\Listener\OAuth', 'onRoute'), 200000);
        $application
                ->getEventManager()
                ->attach('route', array('Application\Listener\TrustIPAuth', 'onRoute'), 200001);

        $doctrineEventManager = $serviceManager->get('doctrine.eventmanager.orm_default');

        $cacheable = new \Application\ORM\Cacheable\CacheableListener();
        $doctrineEventManager->addEventSubscriber($cacheable);

        $shardable = new \Application\ORM\Shardable\ShardableListener();
        $doctrineEventManager->addEventSubscriber($shardable);

        $versionable = new \Application\ORM\Versionable\VersionableListener();
        $versionable->setAuthenticationService($serviceManager->get('Zend\Authentication\AuthenticationService'));
        $doctrineEventManager->addEventSubscriber($versionable);

        $this->bootstrapPhpSettings($e);
        //$this->bootstrapExceptionStrategy($e);

        $events = $e->getApplication()->getEventManager()->getSharedManager();
        $events->attach('ZfcUser\Form\Register', 'init', function ($e) {
                    $form = $e->getTarget();
                    // Do what you please with the form instance ($form)
                    $form->add(array(
                        'name' => 'firstname',
                        'options' => array(
                            'label' => 'نام',
                        ),
                        'attributes' => array(
                            'type' => 'text'
                        ),
                    ));

                    $form->add(array(
                        'name' => 'lastname',
                        'options' => array(
                            'label' => 'نام خانوادگی',
                        ),
                        'attributes' => array(
                            'type' => 'text'
                        ),
                    ));
                });

        $zfcServiceEvents = $e->getApplication()->getServiceManager()->get('zfcuser_user_service')->getEventManager();
        $zfcServiceEvents->attach('register', function ($e) {
                    $form = $e->getParam('form');
                    $user = $e->getParam('user');

                    $user->setStatus(1);
                    $user->setDeleted(0);
                    $user->setCreationDate(new \DateTime('now'));
                    $user->setModifiedDate(new \DateTime('now'));
                });

        if ($serviceManager->has('ScnSocialAuth\Authentication\Adapter\HybridAuth')) {
            $sclServiceEvents = $serviceManager->get('ScnSocialAuth\Authentication\Adapter\HybridAuth')->getEventManager();
            $sclServiceEvents->attach('registerViaProvider', function ($e) {
                        $user = $e->getParam('user');

                        $user->setStatus(1);
                        $user->setDeleted(0);
                        $user->setCreatedOn(new \DateTime('now'));
                        $user->setModifiedOn(new \DateTime('now'));
                    });
        }
    }

    public function bootstrapExceptionStrategy($e)
    {
        $application    = $e->getTarget();
        $serviceManager = $application->getServiceManager();
        $config         = $serviceManager->get('Config');


        // Config json enabled exceptionStrategy
        $exceptionStrategy = new JsonExceptionStrategy();
        $displayExceptions = false;

        if (isset($config['view_manager']['display_exceptions'])) {
            $displayExceptions = $config['view_manager']['display_exceptions'];
        }

        $exceptionStrategy->setDisplayExceptions($displayExceptions);
        $exceptionStrategy->attach($application->getEventManager());
    }

    public function bootstrapPhpSettings($e)
    {
        $application = $e->getTarget();
        $serviceManager = $application->getServiceManager();
        $config = $serviceManager->get('Config');

        if (isset($config['phpSettings'])) {
            $phpSettings = $config['phpSettings'];
            if ($phpSettings) {
                foreach ($phpSettings as $key => $value) {
                    ini_set($key, $value);
                }
            }
        }
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'memcache' => function (ServiceManager $sm) {
                    $memcache = new \Memcache(); // .. do some configuration and connect
                    return $memcache;
                },
                'ZfcUser\Form\Register' => function ($sm) {
                    
                },
                'Application\Service\UidGenerator' => function ($sm) {
                    return new \Application\Service\UidGenerator();
                },
            )
        );
    }

}
