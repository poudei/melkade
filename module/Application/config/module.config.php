<?php

return array(
    'router' => array(
        'routes' => array(
            'api' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/api',
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'user_login' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/login',
                            'defaults' => array(
                                'controller' => 'Application\Controller\User',
                                'action' => 'login'
                            )
                        )
                    ),
                    
                    'user_logout' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/logout',
                            'defaults' => array(
                                'controller' => 'Application\Controller\User',
                                'action' => 'logout'
                            )
                        )
                    ),
                    
                    'users' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users[/[:id]]',
                            'constraints' => array(
                                'id' => '[0-9]+',
                            ),
                            'defaults' => array(
                                'controller' => 'Application\Controller\User'
                            ),
                        ),
                    ),
                    'user_image' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/users/image',
                            'defaults' => array(
                                'controller' => 'Application\Controller\UserImage',
                            ),
                        ),
                    ),
                    'user_register' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/user/signup',
                            'defaults' => array(
                                'controller' => 'Application\Controller\User',
                                'action' => 'register'
                            )
                        )
                    ),
                    'user_password' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/change-password',
                            'defaults' => array(
                                'controller' => 'Application\Controller\User',
                                'action' => 'changepassword'
                            ),
                        ),
                    ), 
                     'device' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/device-register',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Device',
                            ),
                        ),
                    ),
                    
                      'device-count' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/device',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Device',
                                'action' => 'statistics'
                            ),
                        ),
                    ),
                    
                    'authenticate' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route'    => '/oauth/token',
                            'defaults' => array(
                                'controller' => 'Application\Controller\Authenticate',
                                'action'     => 'getToken',
                                ),
                            ),
                        ),
                )
            ),
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'RealEstate\Controller\Estate',
                        'action' => 'index',
                    ),
                ),
            ),
            'frontend_regex' => array(
                'type' => 'Zend\Mvc\Router\Http\Regex',
                'options' => array(
                    'regex' => '^((\/login)|(\/estates)|(\/addnew/property\/([a-zA-Z0-9_-]+))|(\/addnew/property)|(\/signup)|(\/addnew/office\/([a-zA-Z0-9_-]+))|(\/addnew/office)|(\/addnew/shop\/([a-zA-Z0-9_-]+))|(\/addnew/shop)|(\/addnew/apartment\/([a-zA-Z0-9_-]+))|(\/addnew/apartment)|(\/addnew/land\/([a-zA-Z0-9_-]+))|(\/addnew/land)|(\/addnew/villa\/([a-zA-Z0-9_-]+))|(\/addnew/villa)|(\/estate/edit\/([a-zA-Z0-9_-]+))|(\/estate/view\/([a-zA-Z0-9_-]+))|(\/profile)|(\/agent/edit\/([a-zA-Z0-9_-]+))|(\/agency/edit\/([a-zA-Z0-9_-]+))|(\/owner/edit\/([a-zA-Z0-9_-]+))|(\/personals)|(\/bookmarks)|(\/agents)|(\/agencies)|(\/estates\/([a-zA-Z0-9_]+)=([0-9]+))|(\/estate/view\/([a-zA-Z0-9_-]+)\/newAgent)|(\/estate/view\/([a-zA-Z0-9_-]+)\/newAgency)|(\/estate/view\/([a-zA-Z0-9_-]+)\/newOwner)|(\/estate/view\/([a-zA-Z0-9_-]+)\/newAgent\/newAgency)|(\/password)|(\/estate/compare\/([0-9,]+))|(\/plans)|(\/addnewOthers)|(\/operators)|(\/melk)|(\/adv/view\/([a-zA-Z0-9_-]+)))',
                    'defaults' => array( 
                        'controller' => 'Application\Controller\Index',
                        'action' => 'index',
                    ),
                    'spec' => '%',
                ),
            ),  
            
            'template' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/api/template',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action' => 'template',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            
            'help' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/help',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Help',
                        'action' => 'index'
                    )
                )
            )
        )
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
        'factories' => array(
            'OAuthProvider'              => 'Application\Service\OAuthProviderFactory',
            'Doctrine\ORM\EntityManager' => 'Application\Service\EntityManagerFactory',
            'Application\ORM\Versionable\VersionManager' => function ($sm) {
                $vm = new \Application\ORM\Versionable\VersionManager($sm->get('Doctrine\ORM\EntityManager'));
                return $vm;
            },
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index'        => 'Application\Controller\IndexController',
            'Application\Controller\User'         => 'Application\Controller\UserController',
            'Application\Controller\UserImage'    => 'Application\Controller\UserImageController',
            'Application\Controller\Device'       => 'Application\Controller\DeviceController',
            'RealEstate\Controller\Estate'        => 'RealEstate\Controller\EstateController',
            'Application\Controller\Authenticate' => 'Application\Controller\AuthenticateController',
            'Application\Controller\Help'         => 'Application\Controller\HelpController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout'                => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index'      => __DIR__ . '/../view/application/index/index.phtml',
            'application/index/guest'      => __DIR__ . '/../view/application/index/guest_index.phtml',
            'zfc-user/user/login'          => __DIR__ . '/../view/application/user/login.phtml',
            'error/404'                    => __DIR__ . '/../view/error/404.phtml',
            'error/index'                  => __DIR__ . '/../view/error/index.phtml',
            'realestate/email/register'    =>__DIR__ . '/../view/application/email/register.phtml',
            'realestate/email/changepass'  =>__DIR__ . '/../view/application/email/changepass.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'params' => array(
                    'shardChoser' => 'Application\Service\ShardChoser'
                )
            )
        ),
        'driver' => array(
            'application_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Application/Entity')
            ),
            'versionable_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/Application/ORM/Versionable/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'Application\Entity' => 'application_entities',
                    'Application\ORM\Versionable\Entity' => 'versionable_entities'
                )
            ),
        ),
    ),
    'slm_queue' => array(
        'job_manager' => array(
            'factories' => array(
                'Application\Job\SendEmail' => function ($locator) {
                    return new Application\Job\SendEmail($locator->getServiceLocator());
                },
                'Application\Job\CreateThumb' => function ($locator) {
                    return new Application\Job\CreateThumb($locator->getServiceLocator()->get('Thumbnailer'));
                },
                'Application\Job\PublishActivity' => function ($locator) {
                    return new Application\Job\PublishActivity($locator->getServiceLocator());
                },
                'Application\Job\SendSms' => function ($locator) {
                    return new Application\Job\SendSms($locator->getServiceLocator());
                }
            )
        ),
        'queue_manager' => array(
            'factories'  => array(
                'email'      => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'social'     => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'thumbnail'  => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'sms'        => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
                'estate-sms' => 'SlmQueueDoctrine\Factory\DoctrineQueueFactory',
            )
        )
    ),
);
