<?php

namespace Application\Listener;

use Zend\Mvc\MvcEvent;
use Zend\Http\Request as HttpRequest;
use Zend\Authentication\Result;

class TrustIPAuth
{
    /**
     * @param MvcEvent $e
     */
    public static function onRoute(MvcEvent $e)
    {
        $app    = $e->getTarget();
        $sm     = $app->getServiceManager();
        $config = $sm->get('Config');

        if (!$e->getRequest() instanceof HttpRequest) {
            return;
        }

        if ($sm->get('Zend\Authentication\AuthenticationService')->hasIdentity()) {
            return;
        }

        $trustAuth = $e->getRequest()->getHeader('Trust-IP-Authorization', null);
        if (!$trustAuth) {
            return;
        }

        $sm->get('Zend\Authentication\AuthenticationService')
            ->setStorage(new \Zend\Authentication\Storage\NonPersistent());

        if (!isset($config['trust-ip-auth'])) {
            return;
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        foreach ($config['trust-ip-auth']['ips'] as $network => $uid) {
            $matched = self::match($network, $ip);

            if ($matched) {
                $user = $sm->get('zfcuser_user_mapper')->findById($uid);

                if ($user) {
                    $sm->get('Zend\Authentication\AuthenticationService')->getStorage()->write($user);
                    $sm->get('ZfcRbac\Service\Rbac')->setIdentity($user);

                    return true;
                }
            }
        }
    }

    private static function match($network, $ip)
    {
        $network      = trim($network);
        $orig_network = $network;
        $ip           = trim($ip);

        if ($ip == $network) {
            return true;
        }

        $network = str_replace(' ', '', $network);

        if (strpos($network, '*') !== false) {
            if (strpos($network, '/') !== false) {
                $asParts = explode('/', $network);
                $network = @ $asParts[0];
            }

            $nCount  = substr_count($network, '*');
            $network = str_replace('*', '0', $network);

            if ($nCount == 1) {
                $network .= '/24';
            } else if ($nCount == 2) {
                $network .= '/16';
            } else if ($nCount == 3) {
                $network .= '/8';
            } else if ($nCount > 3) {
                return true; // if *.*.*.*, then all, so matched
            }
        }

        $d = strpos($network, '-');

        if ($d === false) {
            $ip_arr = explode('/', $network);
            if (!preg_match("@\d*\.\d*\.\d*\.\d*@", $ip_arr[0], $matches)) {
                $ip_arr[0].=".0";    // Alternate form 194.1.4/24
            }

            $network_long = ip2long($ip_arr[0]);
            $x            = ip2long($ip_arr[1]);
            $mask         = long2ip($x) == $ip_arr[1] ? $x : (0xffffffff << (32 - $ip_arr[1]));
            $ip_long      = ip2long($ip);
            return ($ip_long & $mask) == ($network_long & $mask);

        } else {
            $from = trim(ip2long(substr($network, 0, $d)));
            $to   = trim(ip2long(substr($network, $d+1)));
            $ip   = ip2long($ip);

            return ($ip>=$from and $ip<=$to);
        }
    }
}
