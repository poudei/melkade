<?php
namespace Application\Filter;

use Zend\Filter\FilterInterface;

class ConvertFarsiNum implements FilterInterface
{
    public function filter($value)
    {
        if (!is_string($value)) {
            return $value;
        }
        $farsi = array(
           '۱',
            '۲',
            '۳',
            '۴',
            '۵',
            '۶',
            '۷',
            '۸',
            '۹',
            '۰'
        );
        $english = array(
           '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '0'
        );
        
        $value = str_replace($farsi, $english, $value);
        
        return $value;
    }
}