<?php
namespace Application\Filter;

class ArrayFilter extends \Zend\Filter\AbstractFilter
{
    public function filter($value)
    {
        if (!is_array($value)) {
            return array();
        }
        
        return $value;
    }
}