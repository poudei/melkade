<?php
namespace Application\Filter;

class Int extends \Zend\Filter\AbstractFilter
{
    public function filter($value)
    {
        if (empty($value)) {
            return null;
        }

        if (!is_scalar($value)) {
            trigger_error(
                sprintf(
                    '%s expects parameter to be scalar, "%s" given; cannot filter',
                    __METHOD__,
                    (is_object($value) ? get_class($value) : gettype($value))
                ),
                E_USER_WARNING
            );
            return $value;
        }

        return (int) ((string) $value);
    }
}