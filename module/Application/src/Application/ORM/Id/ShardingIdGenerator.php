<?php
namespace Application\ORM\Id;

use Doctrine\ORM\EntityManager;

class ShardingIdGenerator extends \Doctrine\ORM\Id\AbstractIdGenerator
{
    /**
     * Generates an identifier for an entity.
     *
     * @param \Doctrine\ORM\Entity $entity
     * @return mixed
     */
    public function generate(EntityManager $em, $entity)
    {
        $currentShard = 1;
        $uid          = 1;

        return $this->generateId($currentShard, $uid);
    }

    private function generateId($shard, $uid)
    {
        list($msec, $sec) = explode(' ', microtime());

        $msec = intval($msec * 1000);
        $uid  = intval($uid % 1024);
        $sec  = time();
        $sec  = $sec - strtotime('2000-01-01 00:00:00');

        $sid   = str_pad(base_convert($sec, 10, 2), 31, '0', STR_PAD_LEFT);
        $mid   = str_pad(base_convert($msec, 10, 2), 10, '0', STR_PAD_LEFT);
        $shard = str_pad(base_convert($shard, 10, 2), 13, '0', STR_PAD_LEFT);
        $uid   = str_pad(base_convert($uid, 10, 2), 10, '0', STR_PAD_LEFT);
        $id    = $sid . $mid . $shard .  $uid;

        $a = gmp_init($id, 2);

        return gmp_strval($a, 10);
    }
}
