<?php
namespace Application\ORM\Versionable;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

class VersionManager
{
    private $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }

    public function getVersions($resource)
    {
        if (!is_object($resource)) {
            return null;
        }

        $versionableClassName = get_class($resource);
        $versionableClass     = $this->_em->getClassMetadata($versionableClassName);
        $resourceId           = current($versionableClass->getIdentifierValues($resource));

        $query = $this->_em->createQuery(
            //'SELECT v FROM Application\ORM\Versionable\Entity\ResourceVersion v INDEX BY v.version '. //TODO
            'SELECT v FROM Application\ORM\Versionable\Entity\ResourceVersion v '.
            'WHERE v.resourceName = ?1 AND v.resourceId = ?2 ORDER BY v.version DESC'
        );
        $query->setParameter(1, $versionableClassName);
        $query->setParameter(2, $resourceId);

        $versions = array();

        foreach ($query->getResult() AS $version) {
            $versions[] = $version;
        }

        return $versions;
    }
}
