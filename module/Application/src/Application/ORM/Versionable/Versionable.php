<?php

namespace Application\ORM\Versionable;


/**
 * Versionable
 *
 * @Annotation
 * @Target("CLASS")
 */
class Versionable
{
    /**
     * versionField
     *
     * @var string
     * @access public
     */
    public $versionField = "version";
}
