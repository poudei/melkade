<?php

namespace Application\ORM\Versionable\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ResourceVersions
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="resourceId")
 *
 * @ORM\Table(name="resource_versions")
 * @ORM\Entity
 */
class ResourceVersion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $version;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="resource_name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $resourceName;

    /**
     * @var integer
     *
     * @ORM\Column(name="resource_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $resourceId;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $action;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $creationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="versioned_data", type="array")
     */
    private $versionedData;


    /**
     * Set id
     *
     * @param integer $id
     * @return ResourceVersions
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set version
     *
     * @param integer $version
     * @return ResourceVersions
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return ResourceVersions
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set resourceName
     *
     * @param string $resourceName
     * @return ResourceVersions
     */
    public function setResourceName($resourceName)
    {
        $this->resourceName = $resourceName;

        return $this;
    }

    /**
     * Get resourceName
     *
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * Set resourceId
     *
     * @param integer $resourceId
     * @return ResourceVersions
     */
    public function setResourceId($resourceId)
    {
        $this->resourceId = $resourceId;

        return $this;
    }

    /**
     * Get resourceId
     *
     * @return integer
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return ResourceVersions
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return ResourceVersions
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set versionedData
     *
     * @param string $versionedData
     * @return ResourceVersions
     */
    public function setVersionedData($versionedData)
    {
        $this->versionedData = $versionedData;

        return $this;
    }

    /**
     * Get versionedData
     *
     * @return string
     */
    public function getVersionedData()
    {
        return $this->versionedData;
    }
}
