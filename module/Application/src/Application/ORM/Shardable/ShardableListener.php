<?php
namespace Application\ORM\Shardable;

use Doctrine\ORM\Events,
    Doctrine\Common\EventSubscriber,
    Doctrine\ORM\Event\LifecycleEventArgs,
    Doctrine\Common\Annotations\SimpleAnnotationReader,
    Application\Service\UidGenerator,
    Zend\ServiceManager\ServiceManager;

class ShardableListener implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
        );
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity  = $args->getEntity();
        $em      = $args->getEntityManager();

        $reader      = new SimpleAnnotationReader();
        $reflClass   = $em->getClassMetadata(get_class($entity))->getReflectionClass();
        $annotations = $reader->getClassAnnotations($reflClass);
        $annotation  = null;

        foreach ($annotations as $annot) {
            if ($annot instanceof \Application\ORM\Shardable\Shardable) {
                $annotation = $annot;
            }
        }

        if ($annotation == null) {
            return;
        }

        $shard = 0;

        if ($annotation->type == "independent") {
            $shard = 1; //Current Active Shard
        } else {
            $dependId = call_user_func(array($entity, 'get' . ucfirst($annotation->dependColumn)));
            $shard    = UidGenerator::fetchData($dependId, 'shard');
        }

        $dependColumn = null;

        if (!empty($annotation->column)) {
            $id = UidGenerator::generate($shard, mt_rand(0, 1024));
            call_user_func(array($entity, 'set' . ucfirst($annotation->column)), $id);

            $dependColumn = $annotation->column;
        }

        if (!empty($annotation->dependColumn)) {
            $dependColumn = $annotation->dependColumn;
        }

        $distributionValue = call_user_func(array($entity, 'get' . ucfirst($dependColumn)));

        $args->getEntityManager()
            ->getConnection()
            ->getShardManager()
            ->selectShard($distributionValue);
    }

}
