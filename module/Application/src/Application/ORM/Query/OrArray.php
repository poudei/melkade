<?php
namespace Application\ORM\Query;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class OrArray extends FunctionNode
{
    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {

        $field = $sqlWalker->walkSimpleArithmeticExpression($this->field);
        $vals  = array();

        foreach ($this->values as $value) {
            if (is_integer($value->type)) {
                $vals[] = $value->value;
            } else {
                $vals[] = "'" . $value->value . "'";
            }
        }
        return 'ARRAY[' . implode(',', $vals) . '] && ' . $field;
    }

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->field = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $parser->match(Lexer::T_OPEN_CURLY_BRACE);

        do {
            $this->values[] = $parser->Literal();
            if ($parser->getLexer()->isNextToken(Lexer::T_COMMA)) {
                $parser->match(Lexer::T_COMMA);
            }
        } while (!$parser->getLexer()->isNextToken(Lexer::T_CLOSE_CURLY_BRACE));

        $parser->match(Lexer::T_CLOSE_CURLY_BRACE);
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
