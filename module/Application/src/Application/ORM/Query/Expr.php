<?php
namespace Application\ORM\Query;

use Doctrine\ORM\Query\Expr as BaseExpr;

class Expr extends BaseExpr
{
    public function orArray($x, $y)
    {
        if (is_array($y)) {
            foreach ($y as &$t) {
                $t = str_replace('"', '\\"', $t);
                if (!is_numeric($t)) {
                    $t = "'" . $t . "'";
                }
            }
        }

        return new BaseExpr\Func('ORARRAY' , $x . ',{' . implode(',', $y) . '}');
    }

    public function andArray($x, $y)
    {
        if (is_array($y)) {
            foreach ($y as &$t) {
                $t = str_replace('"', '\\"', $t);
                if (!is_numeric($t)) {
                    $t = "'" . $t . "'";
                }
            }
        }

        return new BaseExpr\Func('ANDARRAY' , $x . ',{' . implode(',', $y) . '}');
    }
}
