<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Zend\Form\Annotation as Form;

/**
 * Device
 *
 * @ORM\Table(name="devices")
 * @ORM\Entity
 * 
 */
class Device
{

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=225, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":225}})
     * 
     */
      private $deviceId;

      /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * @Form\Filter({"name":"Application\Filter\Int"})
     * @Form\Validator({"name":"Digits"})
     * @Form\Required(false)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=225, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":225}})
     * @Form\Required(false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="brand", type="string", length=225, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":225}})
     * @Form\Required(false)
     */
    private $brand;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=225, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":225}})
     * @Form\Required(false)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=225, precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"StringTrim"})
     * @Form\Validator({"name":"StringLength", "options":{"min":1, "max":225}})
     * @Form\Required(false)
     */
    private $manufacturer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="register_date", type="datetime", precision=0, scale=0, nullable=true, unique=false)
     * 
     * @Form\Filter({"name":"Application\Filter\ConvertFarsiNum"})
     * 
     * @Form\Exclude()
     */
    private $registerDate;

    /**
     * Set deviceId
     *
     * @param integer $deviceId
     * @return Device
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId
     *
     * @return integer 
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Set version
     *
     * @param integer $version
     * @return Device
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Device
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * @return Device
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Device
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     * @return Device
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     * @return Device
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime 
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

}
