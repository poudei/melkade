<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Feed
 *
 * @ORM\Table(name="feeds")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Application\Entity\BaseRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", column="id", dependColumn="userId")
 */
class Feed
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $activityId;

    /**
     * @var string
     *
     * @ORM\Column(name="verb", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $verb;

    /**
     * @var integer
     *
     * @ORM\Column(name="object_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(name="object_type", type="string", length=40, precision=0, scale=0, nullable=true, unique=false)
     */
    private $objectType;

    /**
     * @var string
     *
     * @ORM\Column(name="context_type", type="string", length=40, precision=0, scale=0, nullable=true, unique=false)
     */
    private $contextType;

    /**
     * @var integer
     *
     * @ORM\Column(name="context_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $contextId;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $privacy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $creationDate;

    /**
     * set id
     *
     * @param integer $id
     * @access public
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Feed
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set activityId
     *
     * @param integer $activityId
     * @return Feed
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;

        return $this;
    }

    /**
     * Get activityId
     *
     * @return integer
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * Set verb
     *
     * @param string $verb
     * @return Feed
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * Get verb
     *
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     * @return Feed
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return integer
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set objectType
     *
     * @param string $objectType
     * @return Feed
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     *
     * Set contextType
     *
     * @param string $contextType
     * @return Activity
     */
    public function setContextType($contextType)
    {
        $this->contextType = $contextType;

        return $this;
    }

    /**
     * Get contextType
     *
     * @return string
     */
    public function getContextType()
    {
        return $this->contextType;
    }

    /**
     * Set contextId
     *
     * @param integer $contextId
     * @return Activity
     */
    public function setContextId($contextId)
    {
        $this->contextId = $contextId;

        return $this;
    }

    /**
     * Get contextId
     *
     * @return integer
     */
    public function getContextId()
    {
        return $this->contextId;
    }

    /**
     * Set privacy
     *
     * @param integer $privacy
     * @return Feed
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Feed
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
}
