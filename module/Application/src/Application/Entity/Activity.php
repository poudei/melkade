<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="activities")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="Application\Entity\BaseRepository")
 *
 * @Application\ORM\Shardable\Shardable(type="independent", column="id")
 */
class Activity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="actor_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     */
    private $actorId;

    /**
     * @var string
     *
     * @ORM\Column(name="actor_type", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $actorType;

    /**
     * @var string
     *
     * @ORM\Column(name="verb", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     */
    private $verb;

    /**
     * @var integer
     *
     * @ORM\Column(name="object_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $objectId;

    /**
     * @var string
     *
     * @ORM\Column(name="object_type", type="string", length=40, precision=0, scale=0, nullable=true, unique=false)
     */
    private $objectType;

    /**
     * @var string
     *
     * @ORM\Column(name="target_type", type="string", length=40, precision=0, scale=0, nullable=true, unique=false)
     */
    private $targetType;

    /**
     * @var integer
     *
     * @ORM\Column(name="target_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $targetId;

    /**
     * @var string
     *
     * @ORM\Column(name="context_type", type="string", length=40, precision=0, scale=0, nullable=true, unique=false)
     */
    private $contextType;

    /**
     * @var integer
     *
     * @ORM\Column(name="context_id", type="bigint", precision=0, scale=0, nullable=true, unique=false)
     */
    private $contextId;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_count", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $commentCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="privacy", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $privacy;

    /**
     * @var string
     *
     * @ORM\Column(name="metadata", type="json_array", nullable=true)
     */
    private $metadata;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", precision=0, scale=0, nullable=false, unique=false)
     */
    private $updateDate;

    /**
     * set id
     *
     * @param integer $id
     * @access public
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actorId
     *
     * @param integer $actorId
     * @return Activity
     */
    public function setActorId($actorId)
    {
        $this->actorId = $actorId;

        return $this;
    }

    /**
     * Get actorId
     *
     * @return integer
     */
    public function getActorId()
    {
        return $this->actorId;
    }

    /**
     * Set actorType
     *
     * @param string $actorType
     * @return Activity
     */
    public function setActorType($actorType)
    {
        $this->actorType = $actorType;

        return $this;
    }

    /**
     * Get actorType
     *
     * @return string
     */
    public function getActorType()
    {
        return $this->actorType;
    }

    /**
     * Set verb
     *
     * @param string $verb
     * @return Activity
     */
    public function setVerb($verb)
    {
        $this->verb = $verb;

        return $this;
    }

    /**
     * Get verb
     *
     * @return string
     */
    public function getVerb()
    {
        return $this->verb;
    }

    /**
     * Set objectId
     *
     * @param integer $objectId
     * @return Activity
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * Get objectId
     *
     * @return integer
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * Set objectType
     *
     * @param string $objectType
     * @return Activity
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * Get objectType
     *
     * @return string
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * Set targetType
     *
     * @param string $targetType
     * @return Activity
     */
    public function setTargetType($targetType)
    {
        $this->targetType = $targetType;

        return $this;
    }

    /**
     * Get targetType
     *
     * @return string
     */
    public function getTargetType()
    {
        return $this->targetType;
    }

    /**
     * Set targetId
     *
     * @param integer $targetId
     * @return Activity
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;

        return $this;
    }

    /**
     * Get targetId
     *
     * @return integer
     */
    public function getTargetId()
    {
        return $this->targetId;
    }

    /**
     * Set contextType
     *
     * @param string $contextType
     * @return Activity
     */
    public function setContextType($contextType)
    {
        $this->contextType = $contextType;

        return $this;
    }

    /**
     * Get contextType
     *
     * @return string
     */
    public function getContextType()
    {
        return $this->contextType;
    }

    /**
     * Set contextId
     *
     * @param integer $contextId
     * @return Activity
     */
    public function setContextId($contextId)
    {
        $this->contextId = $contextId;

        return $this;
    }

    /**
     * Get contextId
     *
     * @return integer
     */
    public function getContextId()
    {
        return $this->contextId;
    }

    /**
     * Set commentCount
     *
     * @param integer $commentCount
     * @return Activity
     */
    public function setCommentCount($commentCount)
    {
        $this->commentCount = $commentCount;

        return $this;
    }

    /**
     * Get commentCount
     *
     * @return integer
     */
    public function getCommentCount()
    {
        return $this->commentCount;
    }

    /**
     * Set privacy
     *
     * @param integer $privacy
     * @return Activity
     */
    public function setPrivacy($privacy)
    {
        $this->privacy = $privacy;

        return $this;
    }

    /**
     * Get privacy
     *
     * @return integer
     */
    public function getPrivacy()
    {
        return $this->privacy;
    }

    /**
     * Set metadata
     *
     * @param string $metadata
     * @return Activity
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Activity
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Activity
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
}
