<?php
namespace Application\Entity;

use ShareTo\Entity\Share,
    ShareTo\Entity\ShareUser,
    ShareTo\Entity\FolderItem,
    Application\Entity\BaseRepository,
    DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

class MessageRepository extends BaseRepository
{
    public function getContextMessages($type, $id, $start=0, $limit=10)
    {
        $sql = 'SELECT m FROM Application\Entity\Message m '.
            'WHERE m.contextType = :type AND m.contextId = :id';

        $params = array('type' => $type, 'id' => $id);
        $query  = $this->getEntityManager()->createQuery($sql);
        $query->setParameters($params);
        $query->setMaxResults($limit);
        $query->setFirstResult($start);
        //$query->useResultCache(true);

        return $query->getResult();
    }
}
