<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserProvider
 *
 * @ORM\Table(name="user_providers")
 * @ORM\Entity
 *
 * @Application\ORM\Shardable\Shardable(type="dependent", dependColumn="userId")
 */
class UserProvider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_id", type="string", length=50, precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $providerId;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $provider;


    /**
     * Set userId
     *
     * @param integer $userId
     * @return UserProvider
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set providerId
     *
     * @param string $providerId
     * @return UserProvider
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Get providerId
     *
     * @return string
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set provider
     *
     * @param string $provider
     * @return UserProvider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
