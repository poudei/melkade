<?php

namespace Application\Controller;

use Application\Controller\AbstractRestfulController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\Entity\Device;

class DeviceController extends AbstractRestfulController
{

    public function create($data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();
        $device   = new Device();
        $form     = $builder->createForm($device);

        $form->setHydrator($hydrator);
        $form->bind($device);
        $form->setData($data);
        
       $devicee  = $em->find('Application\Entity\Device', $data['deviceId']);
        
         if ($devicee) {
            return new JsonModel(array('OK'));
        }

        if ($form->isValid()) {
            $device = $form->getData();

            $device->setRegisterDate(new \DateTime('now'));
            $device->setDeviceId($data['deviceId']);

            $em->persist($device);
            $em->flush();

            return new JsonModel($this->extractAndFill($device));
        } else {
              return new JsonModel(array('OK'));
        }
    }
    
    public function statisticsAction()
    {
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $query = $em->createQuery("SELECT count(s) FROM Application\Entity\Device s");

        $query->setParameters(array());

        $result = $query->getSingleScalarResult();

        return new JsonModel(array('count' => $result));
    }

}