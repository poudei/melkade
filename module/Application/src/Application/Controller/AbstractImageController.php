<?php

namespace Application\Controller;

use Application\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

abstract class AbstractImageController extends AbstractRestfulController
{
    public function getList()
    {
        $this->getResponse()->setStatusCode(404);
        return new JsonModel(array('error' => 'Not Found'));
    }

    public function get($id)
    {
        $this->getResponse()->setStatusCode(404);
        return new JsonModel(array('error' => 'Not Found'));
    }

    protected function removeImage($image)
    {
        if (!empty($image)) {
            $baseDir = realpath(ROOT_PATH . '/public/');
            $path    = realpath($baseDir . DIRECTORY_SEPARATOR . $image);

            if (file_exists($path)) {
                @unlink($path);
            }
        }

        return true;
    }

    protected function saveImage($file)
    {
        $user       = $this->identity();
        $imageName  = substr(md5($user->getId()), 0, 10);
        $imageName .= '-';
        $imageName .= md5(microtime() . $file['name']);
        $imageName .= '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
        $baseDir    = realpath(ROOT_PATH . '/public/');
        $directory  = '/files/images/'
            . substr($imageName, 0, 2) . '/' . substr($imageName, 2, 2);

        if (!is_dir($baseDir . DIRECTORY_SEPARATOR . $directory)) {
            mkdir($baseDir . DIRECTORY_SEPARATOR . $directory, 0777, true);
        }

        $path = $baseDir . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $imageName;
        move_uploaded_file($file['tmp_name'], $path);

        $path = $directory . '/' . $imageName;

        $job = $this->getServiceLocator()
            ->get('SlmQueue\Job\JobPluginManager')
            ->get('Application\Job\CreateThumb');

        $job->setContent(
            array(
                'image'  => realpath($baseDir . '/' . $path),
                'width'  => 100,
                'height' => 100
            )
        );

        $this->getServiceLocator()
            ->get('SlmQueue\Queue\QueuePluginManager')
            ->get('thumbnail')
            ->push($job);
        return $path;
    }
}
