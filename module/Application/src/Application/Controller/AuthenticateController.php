<?php
namespace Application\Controller;

use Application\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel,
    Zend\Crypt\Password\Bcrypt,
    Zend\Form\Annotation\AnnotationBuilder,
   // Application\Entity\User,
    Application\Entity\OauthClient,
    Application\Entity\OauthAccessToken,
    Application\Entity\Error;
use RealEstate\Entity\User;  

class AuthenticateController extends AbstractActionController
{
    public function getTokenAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $objectManager  = $serviceLocator->get('Doctrine\ORM\EntityManager');
        $hydrator       = $this->getServiceLocator()->get('Hydrator');
 
        $data = $this->getRequest()->getContent();    
        $data = \Zend\Json\Json::decode($data, \Zend\Json\Json::TYPE_ARRAY);
        
        $clientId  = $data['client_id'];
        $clientSec = $data['client_secret'];
        $identity  = $data['identity'];
        $password  = $data['password'];
        
        if (is_string($identity)) {
            $identity = strtolower(trim($identity));
        }

        $client = $objectManager
            ->getRepository('Application\Entity\OauthClient')
            ->findOneBy(array('clientId' => $clientId));
        
        if (!$client instanceof OauthClient) {
             $this->getResponse()->setStatusCode(404);
             return new JsonModel(array(
                 'error' => array(
                     'code'    => 4041,
                     'message' => 'Client Not Found'
                 )
             ));
        }

        $user = $objectManager
            ->getRepository('RealEstate\Entity\User')
            ->findOneBy(array('username' => $identity));
        
            if (!$user instanceof User) {
                 $this->getResponse()->setStatusCode(404);
                 return new JsonModel(array('error' => array(
                     'code'    => 4042,
                     'message' => 'User Not Found'
                 )));
            }
   
        $passCost = $serviceLocator
            ->get('ZfcUser\Authentication\Adapter\Db')
            ->getOptions()
            ->getPasswordCost();

        $bcrypt = new Bcrypt();
        $bcrypt->setCost($passCost);

        if (!$bcrypt->verify($password,$user->getPassword())) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => array(
                 'code' => 4043,
                 'message'  => 'Supplied Credential is Invalid.'
             )));
        }

        $provider = $serviceLocator->get('OAuthProvider');
        $token    = bin2hex($provider->generateToken(16));
        $secret   = bin2hex($provider->generateToken(16));

        $accessToken = new OauthAccessToken();
        $accessToken->setToken($token);
        $accessToken->setTokenSecret($secret);
        $accessToken->setClientId($client->getClientId());
        $accessToken->setUserId($user->getId());
        $accessToken->setExpires(new \DateTime('+1 month'));
        $accessToken->setScope('');

        $objectManager->persist($accessToken);

        $objectManager->flush();

        return new JsonModel(
            array(
                'oauth_token'        => $token,
                'oauth_token_secret' => $secret,
                'user_id'            => $user->getId()
            )
        );
    }
}
