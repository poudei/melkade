<?php

namespace Application\Controller;

use Application\Controller\AbstractImageController,
    Zend\View\Model\JsonModel;

class UserImageController extends AbstractImageController
{
    public function create($data)
    {
        //TODO Check Permission
        $user     = $this->identity();
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $request  = $this->getRequest();
        $files    = $request->getFiles()->toArray();

        if (!$user) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        if (!isset($files['files']) || !isset($files['files'][0])) {
            return new JsonModel(array('error' => 'Error'));//TODO error
        }

        $file = $files['files'][0];
        if ($file['error']) {
            return new JsonModel(array('error' => 'Error'));//TODO error
        }

        $path = $this->saveImage($file);

        $this->removeImage($user->getImagePath());

        $user->setImagePath($path);

        $em->flush();

        $data = $hydrator->extract($user);

        return new JsonModel(array('data' => $data));
    }

    public function deleteList()
    {
        return $this->delete(0);
    }

    public function delete($id)
    {
        //TODO Check Permission
        $user     = $this->identity();
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        $this->removeImage($user->getImagePath());

        $user->setImagePath(null);

        $em->flush();

        $data = $hydrator->extract($user);

        return new JsonModel(array('status' => 'ok', 'data' => $data));
    }
}
