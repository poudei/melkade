<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController as ZendAbstractActionController;
use Doctrine\Common\Annotations\AnnotationReader;

class AbstractActionController extends ZendAbstractActionController
{

    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName(); 
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        return false;
    }

    private function _explodeId($id)
    {
        if (strpos($id, ',') === false) {
            return $id;
        }

        return explode(',', $id);
    }

    protected function extract($object)
    {
        if (!is_object($object)) {
            return array();
        }

        $hydrator = $this->getServiceLocator()->get('Hydrator');

        return $hydrator->extract($object);
    }

    protected function extractAndFill($object)
    {
        $array = $this->extract($object);
        $array = $this->fill($array, $object);

        return $array;
    }

    protected function fill($data, $object)
    {
        if (!is_object($object)) {
            return $data;
        }

        return $data;
    }

}