<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use RealEstate\Entity\User;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        
        $arrUser = ($identity = $this->identity()) ? $hydrator->extract($identity) : false;

        if (!$arrUser) {
            $viewModel = new ViewModel();
            $viewModel->setTemplate('application/index/guest');
            return $viewModel;
        }

        $user = $this->identity();
        $em   = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        return new ViewModel(array('user' => $arrUser));
    }

    public function docsAction() {
    }

    public function templateAction()
    {
        $templates = explode(',', str_replace('.', '/', $this->getRequest()->getQuery('templates', '')));
        $tplPath   = realpath(ROOT_PATH . '/public/static/templates/') . DIRECTORY_SEPARATOR;
        $content   = '';

        foreach ($templates as $template) {
            $template = trim($template);

            $content .= '<script id="' . str_replace('/', '_', $template) . '">';

            if (file_exists($tplPath . $template . '.html')) {
                $content .= file_get_contents($tplPath . $template . '.html');
            } else {
                $content .= 'Missing template "' . $template . '"';
            }

            $content .= "</script>\n";
        }

        $response = $this->getResponse();
        $response->setContent($content);

        return $response;
    }
}
