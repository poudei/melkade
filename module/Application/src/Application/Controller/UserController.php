<?php

namespace Application\Controller;

use Application\Controller\AbstractRestfulController,
    ZfcUserDoctrineORM\Options\ModuleOptions,
    Zend\Form\Annotation\AnnotationBuilder,
    RealEstate\Entity\User,
    Zend\Crypt\Password\Bcrypt,
    Zend\View\Model\ViewModel,
    Zend\View\Model\JsonModel;
use DoctrineModule\Validator\NoObjectExists as NoObjectExistsValidator;

class UserController extends AbstractRestfulController
{
    public function getList()
    {
        $start = $this->getRequest()->getQuery('start', 0);
        $limit = $this->getRequest()->getQuery('limit', 50);
        $query = $this->getRequest()->getQuery('name', '');
        $type  = $this->getRequest()->getQuery('type', null);
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');

        $qb  = $em->getRepository("RealEstate\Entity\User")->createQueryBuilder('u');
        $and = $qb->expr()->andX();
        $and->add($qb->expr()->eq('u.deleted', "false"));

        $addWhere = false;

        if (!empty($query)) {
            $orx = $qb->expr()->orX();
            $orx->add($qb->expr()->like('u.firstname', $qb->expr()->literal('%' . $query . '%')));
            $orx->add($qb->expr()->like('u.lastname', $qb->expr()->literal('%' . $query . '%')));

            $and->add($orx);

            $addWhere = true;
        }

        if (!empty($type)) {
            $and->add($qb->expr()->eq('u.type', $type));

            $addWhere = true;
        }

        if ($addWhere) {
            $qb->where($and);
        }

        $cqb = clone $qb;
        $cqb->select("count(u.id)");
        $cQuery = $cqb->getQuery();
        $count  = $cQuery->getSingleScalarResult();

        $result = $qb->getQuery()
                ->setFirstResult($start)
                ->setMaxResults($limit)
                ->getResult();
        
        $return = array(
            'start' => $start,
            'limit' => $limit,
            'count' => $count,
            'items' => array(),
        );

        foreach ($result as $user) {
            $return['items'][] = $this->extractAndFill($user);
        }

        return new JsonModel($return);
    }

    public function get($id)
    {
        $class = $this->getServiceLocator()->get('zfcuser_module_options')->getUserEntityClass();
        $em    = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user  = $em->find($class, $id);

        if (!$user instanceof $class) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
         if ($user->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

        $data = $this->extractAndFill($user);

        return new JsonModel(array('data' => $data));
    }

    public function loginAction()
    {
        if ($this->zfcUserAuthentication()->getAuthService()->hasIdentity()) {
            return new JsonModel(array('status' => 'ok'));
        }
        
        $request = $this->getRequest();       
        $form    = $this->getServiceLocator()->get('zfcuser_login_form');
        
        if (!$request->isPost()) {
            //return new JsonModel(array('status' => 'error'));
        }

        $data = $request->getPost();
        $form->setData($data);    
        
       //username or password user 
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        
        $username = $em->getRepository('RealEstate\Entity\User')
              ->findOneBy(
                   array(
                        'username' => $data['identity'],
             )
        );

        if (!isset($username)) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Username'));
        }

        $password = $data['credential'];

        $passCost = $this->getServiceLocator()->get('ZfcUser\Authentication\Adapter\Db')
                ->getOptions()
                ->getPasswordCost();

        $bcrypt = new Bcrypt();
        $bcrypt->setCost($passCost);

        if (!$bcrypt->verify($password, $username->getPassword())) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Password'));
        }


        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => 'Authentication Failed'));
        }
        

        // clear adapters
        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();

        $adapter = $this->zfcUserAuthentication()->getAuthAdapter();
        $result  = $adapter->prepareForAuthentication($this->getRequest());
        $auth    = $this->zfcUserAuthentication()->getAuthService()->authenticate($adapter);

        if (!$auth->isValid()) {
            $adapter->resetAdapters();
            return new JsonModel(array('error' => 'Authentication failed. Please try again.'));
        }

        return new JsonModel(array('status' => 'ok'));
    }
    
    public function logoutAction()
    {
        $this->zfcUserAuthentication()->getAuthAdapter()->resetAdapters();
        $this->zfcUserAuthentication()->getAuthAdapter()->logoutAdapters();
        $this->zfcUserAuthentication()->getAuthService()->clearIdentity();
        
        return $this->redirect()->toUrl('/');
    }

    public function registerAction()
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        $builder  = new AnnotationBuilder();
        $user     = new User();
        $form     = $builder->createForm($user);

        $noObjectExistsValidator = new NoObjectExistsValidator(
            array(
                'object_repository' => $em->getRepository('RealEstate\Entity\User'),
                'fields' => 'cellphone'
           )
        );
        $noObjectExistsValidator->setMessage(
                'The cellphone already exists'
        );
        
        $cellInput = $form->getInputFilter()->get('cellphone');
        $cellInput->getValidatorChain()->attach($noObjectExistsValidator);

        $form->setHydrator($hydrator);
        $form->bind($user);
        $data = $this->getRequest()->getPost();
        $form->setData($data);

        if ($form->isValid()) {
            $user = $form->getData();
            //$str      = "abcdefghijkmnpqrstuvwxyz23456789";
            //$str      = str_shuffle($str);
            //$pass     = substr($str, 0, 8);

            $pass        = $user->getCellphone();
            $usernameSms = $user->getCellphone();
    
            $passs   = "+98" . substr($pass, -10);    
            $message = "وقت به خیر،\n به ملکده (melkade.com) خوش آمدید،\n نام کاربری:$usernameSms\nکلمه عبور:$pass\n";
            $phone   = $passs;
            $this->sendSms($phone, $message);
            
            $bcrypt   = new \Zend\Crypt\Password\Bcrypt();
            $password = $bcrypt->create($pass);

            $user->setCreationDate(new \DateTime('now'));
            $user->setModifiedDate(new \DateTime('now'));
            $user->setDeleted(false);
            $user->setUsername($user->getCellphone());
            $user->setPassword($password);

            $em->persist($user);
            
            $user->setCreatedById($user->getId());
            
            if (isset($data['type']) && ($data['type'] == '2502')) {

                $agency = $em->getRepository('RealEstate\Entity\Agency')
                        ->findOneBy(
                            array(
                                'phones' => $data['agencyPhones'],
                            )
                        );
                /*
                  if (!$agency) {
                  $agency = new \RealEstate\Entity\Agency();
                  $agency->setPhones($data['agencyPhones']);
                  // $agency->setName($data['agencyName']);
                  $agency->setCreationDate(new \DateTime('now'));
                  $agency->setModifiedDate(new \DateTime('now'));
                  $agency->setDeleted(false);

                  $em->persist($agency);

                  } */

                $user->setType(2502);

                if ($agency) {
                    $agent = new \RealEstate\Entity\Agent();
                    $agent->setAgencyId($agency->getId());
                    $agent->setUserId($user->getId());
                    $agent->setCreationDate(new \DateTime('now'));
                    $agent->setModifiedDate(new \DateTime('now'));
                    $agent->setDeleted(false);

                    $em->persist($agent);
                    
                }
            }

            if ($user->getEmail() != null) {
                
                $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
                $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
                $queue        = $queueManager->get('email');

                $job = $jobManager->get('Application\Job\SendEmail');
                $job->setContent(
                        array(
                            'template' => 'realestate/email/register',
                            'subject'  => 'ملکـــده',
                            'to_email' => $user->getEmail(),
                            'to_name'  => $user->getFirstName() . ' ' . $user->getLastName(),
                            'data'     => array(
                                             'password' => $pass,
                                             'username' => $user->getCellphone()
                      )
                ));

                $queue->push($job);
            }

            $em->flush();

            return new JsonModel($this->extractAndFill($user));
        } else {
            $this->getResponse()->setStatusCode(400);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function update($id, $data)
    {
        $em       = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $user     = $em->find('RealEstate\Entity\User', $id);
        $hydrator = $this->getServiceLocator()->get('Hydrator');

        if (!is_object($user)) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }

         if ($user->getDeleted()) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel(array('error' => 'Not Found'));
        }
        
        $preDate = $this->extract($user);
        $data    = array_merge($preDate, $data);      
        $builder = new AnnotationBuilder();
        $form    = $builder->createForm($user);

        if ($user->getCellphone() == $data['cellphone']) {
            $form->remove('cellphone');
            // unset ($data['cellphone']);
            // $cellInput = $form->getInputFilter()->get('cellphone');
            // $cellInput->setRequired(false);
        } else {
            $noObjectExistsValidator = new NoObjectExistsValidator(
                array(
                    'object_repository' => $em->getRepository('RealEstate\Entity\User'),
                    'fields' => 'cellphone'
                )
            );
            
            $noObjectExistsValidator->setMessage(
                    'The cellphone already exists'
            );
            $cellInput = $form->getInputFilter()->get('cellphone');
            $cellInput->getValidatorChain()->attach($noObjectExistsValidator);
        }

        $form->setHydrator($hydrator);
        $form->bind($user);
        $form->setData($data);

        if ($form->isValid()) {
            $user = $form->getData();

            $user->setModifiedDate(new \DateTime('now'));
            $user->setUsername($user->getCellphone());
            $user->setLastModifiedBy($this->identity()->getId());

            $em->flush();
            $data = $this->extractAndFill($user);

            $authService = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
            $authService->getStorage()->write($user);

            return new JsonModel($data);
        } else {
            $this->getResponse()->setStatusCode(401);
            return new JsonModel(array('error' => $form->getMessages()));
        }
    }

    public function changepasswordAction()
    {
        if (!$this->zfcUserAuthentication()->hasIdentity()) {
            throw new \Exception('You are not logged in.', 403);
        }

        $user = $this->identity();
        $form = $this->getServiceLocator()->get('zfcuser_change_password_form');
        $body = $this->getRequest()->getContent();
        $data = $this->getRequest()->getPost();

        if (count($data) == 0 && !empty($body)) {
            $data = \Zend\Json\Json::decode($body, \Zend\Json\Json::TYPE_ARRAY);
        }

        $data['identity'] = $user->getUsername();
        $form->setData($data);

        if (!$form->isValid()) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(array('error' => $form->getMessages()));
        }

        if (!$this->getServiceLocator()->get('zfcuser_user_service')->changePassword($form->getData())) {
            $this->getResponse()->setStatusCode(401);

            return new JsonModel(array('error' => array(
                              'credential' =>  'We couldn\'t recognize your current password. Please try again.'              
                   )
                 )
               );
        }
        
         if ($user->getEmail() != null) {
             
                $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
                $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
                $queue        = $queueManager->get('email');

                $job = $jobManager->get('Application\Job\SendEmail');
                $job->setContent(
                        array(
                            'template' => 'realestate/email/changepass',
                            'subject'  => 'ملکــده',
                            'to_email' => $user->getEmail(),
                            'to_name'  => $user->getFirstName() . ' ' . $user->getLastName(),
                            'data'     => array(
                                             'password' => $data['newCredential'],
                                             'username' => $user->getUsername()
                      )
                ));
                
                $queue->push($job);
            }

        return new JsonModel(array('success' => true));
    }
}
