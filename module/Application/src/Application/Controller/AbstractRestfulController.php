<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController as ZendAbstractRestfulController,
    Zend\View\Model\JsonModel;

class AbstractRestfulController extends ZendAbstractRestfulController
{
    protected function getIdentifier($routeMatch, $request)
    {
        $identifier = $this->getIdentifierName();
        $id = $routeMatch->getParam($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        $id = $request->getQuery()->get($identifier, false);
        if ($id !== false) {
            return $this->_explodeId($id);
        }

        return false;
    }

    private function _explodeId($id)
    {
        if (strpos($id, ',') === false) {
            return $id;
        }

        return explode(',', $id);
    }

    public function getList()
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function get($id)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function create($data)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function update($id, $data)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }

    public function delete($id)
    {
        $this->getResponse()
            ->setStatusCode(501);

        return new JsonModel(array('error' => 'Not Implemented'));
    }
    
    protected function extract($object)
    {
        if (!is_object($object)) {
            return array();
        }
        
        $hydrator = $this->getServiceLocator()->get('Hydrator');
        
        return $hydrator->extract($object);
    }
    
    protected function extractAndFill($object)
    {
        $array = $this->extract($object);
        $array = $this->fill($array, $object);
        
        return $array;
    }
    
    protected function fill($data, $object)
    {
        if (!is_object($object)) {
            return $data;
        }
        
        return $data;
    }
    
    protected function sendSms($phone, $message)
    {
        $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
        $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('sms');

        $job = $jobManager->get('Application\Job\SendSms');
        $job->setContent(
                array(
                    'phone'   => $phone,
                    'message' => $message
             )
        );

        $queue->push($job); 
    }
    
    protected function sendEstateSms($phone, $message)
    {
        $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
        $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
        $queue        = $queueManager->get('estate-sms');

        $job = $jobManager->get('Application\Job\SendSms');
        $job->setContent(
                array(
                    'phone'   => $phone,
                    'message' => $message
             )
        );

        $queue->push($job); 
    }
}
