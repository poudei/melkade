<?php

namespace Application\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform,
    Doctrine\DBAL\Types\Type;

class StringArrayType extends Type
{
    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getDoctrineTypeMapping('STRINGARRAY');
    }

    public function convertToPHPValue($values, AbstractPlatform $platform)
    {
        $values = trim(trim($values, '{""}'));
        if (empty($values)) {
            return array();
        }
        
        return explode(',', $values);
    }

    public function convertToDatabaseValue($values, AbstractPlatform $platform)
    {
        //This is executed when the value is written to the database.
        //Make your conversions here, optionally using the $platform.

        settype($values, 'array'); // can be called with a scalar or array
        $result = array();

        foreach ($values  as $t) {
            $t = str_replace('"', '\\"', $t);
            $t = '"' . $t . '"';

            $result[] = $t;
        }
        return '{' . implode(",", $result) . '}'; // format
    }

    public function getName()
    {
        return "STRINGARRAY";
    }
}
