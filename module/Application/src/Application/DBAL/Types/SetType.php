<?php

namespace Application\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform,
    Doctrine\DBAL\Types\Type;

class SetType extends Type
{
    protected $name;
    protected $values = array();

    public function getSqlDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        $values = array_map(function($val) { return "'".$val."'"; }, $this->values);

        return " SET(".implode(", ", $values).") COMMENT '(DC2Type:".$this->name.")' ";
    }

    public function convertToPHPValue($values, AbstractPlatform $platform)
    {
        return explode(',', $values);
    }

    public function convertToDatabaseValue($values, AbstractPlatform $platform)
    {
        if (is_array($values)) {
            return join(',', $values);
        }

        return '';
    }

    public function getName()
    {
        return $this->name;
    }
}
