<?php
namespace Application\Job;

use SlmQueue\Job\AbstractJob,
    Zend\ServiceManager\ServiceManager,
    Zend\View\Model\ViewModel,
    Zend\Mail,
    Zend\Mail\Transport\Smtp,
    Zend\Mail\Transport\SmtpOptions,
    Zend\View\Renderer\PhpRenderer,
    Zend\Mime\Message as MimeMessage,
    Zend\Mime\Part as MimePart;

class SendEmail extends AbstractJob
{
    protected $serviceLocator;

    public function __construct(ServiceManager $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function execute()
    {
        $config   = $this->serviceLocator->get('Config');
        $default  = $config['email']['default'];
        $content  = $this->getContent();
        $content  = array_merge($default, $content);
        $template = $content['template'];

        $renderer = new PhpRenderer();
        $renderer->plugin('url')->setRouter($this->serviceLocator->get('HttpRouter'));

        $model    = new ViewModel();
        $model->setVariables($content['data']);
        $model->setTemplate($template);

        $renderer->setResolver($this->serviceLocator->get('ViewTemplateMapResolver'));
        $body     = $renderer->render($model);

        $mail = new Mail\Message();

        $html = new MimePart($body);
        $html->type = "text/html";

        $body = new MimeMessage();
        $body->addPart($html);

        $mail->setBody($body);
        $mail->setFrom($content['from_email'], $content['from_name']);
        $mail->addTo($content['to_email'], $content['to_name']);
        $mail->setSubject($content['subject']);

        $transport = new Smtp();
        $transport->setOptions(new SmtpOptions($config['email']['transport']['options']));
        $transport->send($mail);
    }
}
