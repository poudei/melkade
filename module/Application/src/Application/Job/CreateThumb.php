<?php
namespace Application\Job;

use SlmQueue\Job\AbstractJob;

class CreateThumb extends AbstractJob
{
    protected $thumbnailer;

    public function __construct($thumbnailer)
    {
        $this->thumbnailer = $thumbnailer;
    }

    public function execute()
    {
        $data  = $this->getContent();
        $thumb = $this->thumbnailer->create($data['image'], array());

        $thumb->resize($data['width'], $data['height']);

        $newPath = pathinfo($data['image'], PATHINFO_DIRNAME)
            . DIRECTORY_SEPARATOR . pathinfo($data['image'], PATHINFO_FILENAME)
            . '-' . $data['width'] . 'x' . $data['height'] . '.'
            . pathinfo($data['image'], PATHINFO_EXTENSION);

        $thumb->save($newPath);
    }
}
