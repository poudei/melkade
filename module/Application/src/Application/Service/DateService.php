<?php

namespace Application\Service;

use CybersExperts\Bundle\JalaliDateBundle\lib\JalaliDateConverter as JalaliDateConverter,
    CybersExperts\Bundle\JalaliDateBundle\Service\DateConverter as DateConverter;

class DateService
{
    public static function convertJalaliToGregorian($date)
    {
        $dateArray = explode(' ', $date);
        $_date  = $dateArray[0];
        $_time  = isset($dateArray[1]) ? $dateArray[1] : '00:00:00';

        $dateArray = explode('-', $_date);
        $_year     = $dateArray[0];
        $_month    = isset($dateArray[1]) ? $dateArray[1] : '1';
        $_day      = isset($dateArray[2]) ? $dateArray[2] : '1';
        
        $jalaliDate    = new JalaliDateConverter();
        $dateConverter = new DateConverter($jalaliDate);
        $greorian = $dateConverter->jalaliToGregorian($_year, $_month, $_day);
        
      //  return join('-', $greorian) . ' ' . $_time;
        return join('-', $greorian);
    }
    
    public static function convertGregorianToJalali($date)
    {
        $dateArray = explode(' ', $date);
        $_date  = $dateArray[0];
        $_time  = isset($dateArray[1]) ? $dateArray[1] : '00:00:00';
     
        $dateArray = explode('-', $date);

        $_year     = $dateArray[0];
        $_month    = isset($dateArray[1]) ? $dateArray[1] : '1';
        $_day      = isset($dateArray[2]) ? $dateArray[2] : '2';

        $jalaliDate    = new JalaliDateConverter();
        $dateConverter = new DateConverter($jalaliDate);
        $jalali = $dateConverter->gregorianToJalali($_year, $_month, $_day);
        
        $jalali[1] = (strlen($jalali[1]) == 1) ? '0' . $jalali[1] : $jalali[1];
        $jalali[2] = (strlen($jalali[2]) == 1) ? '0' . $jalali[2] : $jalali[2];
        
        $date = join('/', $jalali);
            return $date;
    }
}

