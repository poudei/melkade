define([
  'jquery',
  'underscore',
  'map/markerclusterer',
  'vendor/async!https://maps.googleapis.com/maps/api/js?key=AIzaSyB93UdGcJbblwTxeQ9it5nPDKgL1_dKncY&sensor=false&language=fa&libraries=places'
  ], function ($, _) {

    var map;
    var markers = [];

    var type = {
      '201' : 'apartment',
      '202' : 'villa',
      '203' : 'land',
      '204' : 'office',
      '205' : 'shop',
      '206' : 'property'
    };

    var type_fa = {
      '201' : 'آپارتمان',
      '202' : 'ویلا',
      '203' : 'زمین',
      '204' : 'اداری',
      '205' : 'مغازه',
      '206' : 'مستغلات'
    };

    var request_fa = {
      '301' : 'فروش',
      '302' : 'پیش فروش',
      '303' : 'رهن و اجاره'
    };
    
    var mapObj = {

      markerClusterer : null,

      initialize : function () {

        var mapOptions = {
          zoom: 11,
          center: new google.maps.LatLng(35.7014396,51.3498186)
        };
        map = new google.maps.Map(document.getElementById('map'),
          mapOptions);

        var legend = document.getElementById('legend');

        if (legend) {

        var div = document.createElement('div');
        div.innerHTML = '<img src="/img/mapLegend.png">';
        legend.appendChild(div);
         map.controls[google.maps.ControlPosition.BOTTOM].push(legend);
      }


       

        var styles = [[{
          url: '../images/people35.png',
          height: 35,
          width: 35,
          opt_anchor: [16, 0],
          opt_textColor: '#ff00ff',
          opt_textSize: 10
        }, {
          url: '../images/people45.png',
          height: 45,
          width: 45,
          opt_anchor: [24, 0],
          opt_textColor: '#ff0000',
          opt_textSize: 11
        }, {
          url: '../images/people55.png',
          height: 55,
          width: 55,
          opt_anchor: [32, 0],
          opt_textSize: 12
        }], [{
          url: '../images/conv30.png',
          height: 27,
          width: 30,
          anchor: [3, 0],
          textColor: '#ff00ff',
          opt_textSize: 10
        }, {
          url: '../images/conv40.png',
          height: 36,
          width: 40,
          opt_anchor: [6, 0],
          opt_textColor: '#ff0000',
          opt_textSize: 11
        }, {
          url: '../images/conv50.png',
          width: 50,
          height: 45,
          opt_anchor: [8, 0],
          opt_textSize: 12
        }], [{
          url: '../images/heart30.png',
          height: 26,
          width: 30,
          opt_anchor: [4, 0],
          opt_textColor: '#ff00ff',
          opt_textSize: 10
        }, {
          url: '../images/heart40.png',
          height: 35,
          width: 40,
          opt_anchor: [8, 0],
          opt_textColor: '#ff0000',
          opt_textSize: 11
        }, {
          url: '../images/heart50.png',
          width: 50,
          height: 44,
          opt_anchor: [12, 0],
          opt_textSize: 12
        }]];


        var zoom = 13;
        var size = 20;
        var style = 10;

        this.markerClusterer = new MarkerClusterer(map, markers, {
          maxZoom: zoom,
          gridSize: size,
          styles: styles[style]
        });

        if($('#pac-input').length > 0) {
          this.searchBar();
        }

      },

      searchBar : function() {
        var input = /** @type {HTMLInputElement} */(
          document.getElementById('pac-input'));

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var options = {
          componentRestrictions: {country: 'ir'},
          bounds : new google.maps.LatLngBounds (new google.maps.LatLng(35.445128, 50.818284), new google.maps.LatLng(35.544879, 51.798198)) //sw, ne
        };
        var autocomplete = new google.maps.places.Autocomplete(input, options );
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            return;
          }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
    marker.setIcon(/** @type {google.maps.Icon} */({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
      (place.address_components[0] && place.address_components[0].short_name || ''),
      (place.address_components[1] && place.address_components[1].short_name || ''),
      (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

},

addPoint: function(long, lat, data){
  var point = new google.maps.LatLng(lat, long);
  var marker = new google.maps.Marker({
    map: map,
    position: point,
    icon: '/img/pins/'+type[data['type']]+'_' + data['request'] +'.png'
  });
  marker.id = data['id'];
  var infowindow = new google.maps.InfoWindow();

  var price = "";
  if(data['price'] == undefined || data['price'] == 0 || data['price'] == "") {
    price = data['exPrice'];
  } else {
    price = data['price'];
  }

  price = parseInt(price/1000000);

  var content = '<div class="google-map-popup-content">' + type_fa[data['type']] + ' - ' + request_fa[data['request']]
  + '<br>'
  + price.toString().replace('.00', '').replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",") + ' م ت ' 
  + "<br>"
  + "" + (data['metrage'] ? data['metrage'].replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",").replace('.00', '')  : data['area'].replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",").replace('.00', '')) + ' م'
  + '<br>'
  + '' + (data['beds'] ? (data['beds'] + ' خ') : '') + '</div>';

  infowindow.setContent(content);
  google.maps.event.addListener(marker, 'mouseover', function() {
    infowindow.open(map, marker);
  });
  google.maps.event.addListener(marker, 'mouseout', function() {
    infowindow.close(map, marker);
  });


  google.maps.event.addListener(marker, 'click', function() {
    var url = '/estates/' + data['id'] + '/view';
    window.open(url, '_blank');
  });

  if (this.markerClusterer != undefined) {
    this.markerClusterer.addMarker(marker);
  }
},

addPointNoData: function(long, lat){
  var point = new google.maps.LatLng(lat, long);
  var marker = new google.maps.Marker({
    map: map,
    position: point
  });
},

addClickListener : function() {
  google.maps.event.addListener(map, 'click', function(event) {
    if(markers.length > 0) {
      markers[markers.length-1].setMap(null);
      markers.length = 0;
    }

    var marker = new google.maps.Marker({
      position: event.latLng,
      map: map
    });

    markers.push(marker);

    $('#latitude-input').val(event.latLng.lat());
    $('#longitude-input').val(event.latLng.lng());

  });
},

clearMarkers : function () {
  this.markerClusterer.clearMarkers();
},

findMarker : function(id) {
  var allMarkers = this.markerClusterer.getMarkers();

  for (var i = 0; i < allMarkers.length; i++) {
    if(allMarkers[i].id == id) {
      return allMarkers[i];
      break;
    }
  }

  return false;
},

setMarkerIcon : function(marker, iconPath) {
  marker.setIcon(iconPath);
},

setCenter : function (longitude, latitude) {
  map.setCenter(new google.maps.LatLng(latitude, longitude));
  map.setZoom(15);
},

findMarkersCluster : function (marker) {
  return this.markerClusterer.getMarkersCluster(marker);
}

}

return mapObj;

});

// use this.markerClusterer.getMarkersCluster(marker);