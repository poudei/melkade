define([
    'jquery',
    'underscore',
    'backbone',
    'views/new-estate',
    'views/new-adv',
    'models/estate',
    'models/neighbor',
    'models/adv',
    'collections/estate',
    'collections/bookmarks',
    'views/estatesView',
    'views/homeView',    
    'views/estateView',
    'views/advView',
    'views/editEstate',
    'views/compareEstates',
    'views/estateUploadPhoto',
    'views/login',
    'views/signup',  
    'views/editProfile',
    'views/editPassword',
    'views/editAgency',
    'views/editOwner',
    'views/editAgent',
    'views/agenciesList',
    'views/agentsList',
    'views/plansList',
    'views/new-estate-others',
    'views/operatorsList',
    'views/modifyEstateView',
    'views/modifyCharts'
    ], function($, _, Backbone, addNewEstateView, addNewAdvView, EstateModel, NeighborModel, AdvModel, EstateCollection, BookmarksCollection,
        EstatesView, HomeView, EstateView, AdvView, EditEstate, CompareEstate, EstateAddPhotos, Login, Signup, EditProfile, EditPassword,
        EditAgency, EditOwner, EditAgent, AgenciesListView, AgentsListView, PlansListView, NewEstateOthersView, OperatorsListView,
        ModifyEstateView, ModifyCharts) {

        var type = {
            '201' : 'apartment',
            '202' : 'villa',
            '203' : 'land',
            '204' : 'office',
            '205' : 'shop',
            '206' : 'property'
        };

        var AppRouter = Backbone.Router.extend({
            
            routes : {
                // ''                                  : 'home',
                'addnew/:type'                      : 'showNewForm',
                'addnew/:type/:advId'               : 'showNewForm',
                'melk'                              : 'newAdv',
                'addnewOthers'                      : 'showNewFormOthers',
                'estate/view/:id'                   : 'showEstate',
                'adv/view/:id'                      : 'showAdv',
                'estates/:id/view'                  : 'modifyEstateView',
                'charts'                            : 'modifyCharts',
                'estates/:id/view/:par1'            : 'modifyEstateView',
                'estate/view/:id/:par1/:par2'       : 'showEstate',
                'estate/view/:id/:par1'             : 'showEstate',
                'estate/compare/:ids'               : 'compareEstate',
                'estate/edit/:id'                   : 'editEstate',
                'agency/:id/estates'                : 'showAgencyEstates',
                'agents/:id/estates'                : 'showAgentEstates',
                'agent/edit/:id'                    : 'editAgent',
                'agency/edit/:id'                   : 'editAgency',
                'owner/edit/:id'                    : 'editOwner',
                'estate/upload-photos/:id'          : 'estateAddPhotos',
                ''                                  : 'showEstates',
                'estates/:filter'                   : 'showEstates',
                'personals'                         : 'personals',
                'bookmarks'                         : 'bookmarks',
                'login'                             : 'login',
                'signup'                            : 'signup',
                'profile'                           : 'profile',
                'password'                          : 'password',
                'agencies'                          : 'agencies',
                'agents'                            : 'agents',
                'plans'                             : 'plans',
                'operators'                         : 'operators',
                '*fragment'                         : 'notFound'
            },
            
            before: function(id, route) {
                var page = route.split('/')[0];
                switch (page) {
                    // case '':
                    case 'signup':
                    case 'profile':
                    case 'password':
                    case 'owner':
                    $('body').addClass("home-background");
                    break;

                    default :
                    $('body').removeClass("home-background");
                    break;
                }

                if(id && id[0] == 'user/login') {
                    $('body').addClass("home-background");
                }
            },

            after : function(id, route) {
                var $t = $('.wrapper-all');
                $(".loading").fadeOut();
            },

            home: function(){
                var homeView = new HomeView();               
                $('.overwrite-container').html(homeView.render().$el);
                homeView.afterRender();
            },
            
            showNewForm : function(_type, advId){ 
                if (advId) {
                    var model = new AdvModel({
                        id:advId
                    });
                    model.fetch({
                        success : function(model, result) {
                            
                            var estate = new EstateModel();
                            $.each(result, function(index, value) {
                                estate.set(index, value);
                            });
                            estate.unset("id");
                            estate.set("advId", model.get("id"));
                            if (model.get("user")["type"] == "2501") {
                                estate.set("ownerLastname", model.get("user")["lastname"]);
                                estate.set("ownerCellphone", model.get("user")["cellphone"]);
                                estate.set("advertisedBy", "101");
                            } else {
                                estate.set("agentLastname", model.get("user")["lastname"]);
                                estate.set("agentCellphone", model.get("user")["cellphone"]);
                                estate.set("advertisedBy", "102");
                            }
                                
                            require(['text!views/templates/New-Estate-Form/' + _type + '-form.html'],
                                function(NewFormTpl){
                                    var NewEstateView = new addNewEstateView({
                                        model: estate,
                                        tpl  : NewFormTpl
                                    });

                                    $('.overwrite-container').html(NewEstateView.render().$el);
                                    NewEstateView.afterRender();
                                });
                        }
                    });
                }
                else {
                    require(['text!views/templates/New-Estate-Form/' + _type + '-form.html'],
                        function(NewFormTpl){
                            var NewEstateView = new addNewEstateView({
                                model: new EstateModel(),
                                tpl : NewFormTpl
                            });

                            $('.overwrite-container').html(NewEstateView.render().$el);
                            NewEstateView.afterRender();
                        });
                }
            },
            
            newAdv : function(){  
                require(['text!views/templates/new-adv-form.html'],
                    function(NewFormTpl){
                        var NewAdvView = new addNewAdvView({
                            model: new AdvModel(),
                            tpl : NewFormTpl
                        });
                        $('.overwrite-container').html(NewAdvView.render().$el);
                        NewAdvView.afterRender();
                    });
            },

            showNewFormOthers: function(){
                var newEstateOthersView = new NewEstateOthersView();               
                $('.overwrite-container').html(newEstateOthersView.render().$el);
            },

            showEstate: function(id, par1, par2){                
                var model = new EstateModel({
                    id:id
                });
                model.fetch({
                    success : function(model, result) {
                        require(['text!views/templates/View-Estate/'+type[model.get('type')]+'-view.html'],
                            function(EstateTpl){
                                var estateView = new EstateView({
                                    model   : model,
                                    tpl     : EstateTpl,
                                    par1    : par1,
                                    par2    : par2
                                });
                                $('.overwrite-container').html(estateView.render().$el);
                                estateView.afterRender();
                            });
                    }
                });
            },
            
            showAdv: function(id, par1, par2){                
                
                var model = new AdvModel({
                    id:id
                });
                model.fetch({
                    success : function(model, result) {
                        require(['text!views/templates/adv-view.html'],
                            function(AdvTpl){
                                var advView = new AdvView({
                                    model   : model,
                                    tpl     : AdvTpl,
                                    par1    : par1,
                                    par2    : par2
                                });
                                $('.overwrite-container').html(advView.render().$el);
                                advView.afterRender();
                            });
                    }
                });
            },

            modifyEstateView: function(id, par1, par2){                
                $(".loading").fadeOut();
                new ModifyEstateView();

            },
            
            modifyCharts: function(){                
                $(".loading").fadeOut();
                new ModifyCharts();

            },

            editEstate : function (id) {            
                var editEstate = new EditEstate({
                    id:id
                });
            },

            compareEstate : function (ids) {
                var estateCollection = new EstateCollection();
                estateCollection.filters.set('ids', ids);

                var compareEstate = new CompareEstate({
                    collection : estateCollection
                });

                $('.overwrite-container').html(compareEstate.render().$el);
                
            },

            showEstates : function(filter){
                var map;
                require(['map/map'], function(Map){
                    map = Map;

                    if (filter != undefined) {
                        var type = filter.split('=')[0];
                        var id = filter.split('=')[1];

                        switch(type)
                        {
                            case "neighborId":
                            
                            var neighborModel = new NeighborModel({
                                id: id
                            }),
                            estateCollection = new EstateCollection();
                            estateCollection.filters.set('neighborId', id);

                            var estatesView = new EstatesView({
                                collection : estateCollection
                            });
                            $('.overwrite-container').html(estatesView.render().$el);
                            estatesView.afterRender();

                            break;

                            case "agentUserId":
                            case "agencyId":
                            var estateCollection = new EstateCollection();
                            estateCollection.filters.set(type, id);
                            var estatesView = new EstatesView({
                                collection : estateCollection
                            });
                            $('.overwrite-container').html(estatesView.render().$el);
                            estatesView.afterRender();
                            break;

                            default:
                            return;
                        }
                    } else {
                        var estatesView = new EstatesView({
                            collection : new EstateCollection()
                        });
                        $('.overwrite-container').html(estatesView.render().$el);
                        estatesView.afterRender();
                    }
                    map.initialize();
                });
},

showAgencyEstates : function(id) {
    var map;
    require(['map/map'], function(Map){
        map = Map;
        var estateCollection = new EstateCollection();
        estateCollection.filters.set('agencyId', id);
        var estatesView = new EstatesView({
            collection : estateCollection
        });
        $('.overwrite-container').html(estatesView.render().$el);
        estatesView.afterRender();
        
        map.initialize();
    });
},

showAgentEstates : function(id) {
    var map;
    require(['map/map'], function(Map){
        map = Map;
        var estateCollection = new EstateCollection();
        estateCollection.filters.set('agentUserId', id);
        var estatesView = new EstatesView({
            collection : estateCollection
        });
        $('.overwrite-container').html(estatesView.render().$el);
        estatesView.afterRender();
        
        map.initialize();
    });
},

notFound: function(fragment){
    // console.log('notFound' + fragment);
},  

estateAddPhotos : function(id) {            
    var estateAddPhotos = new EstateAddPhotos({
        form: true
    });
    var model = new EstateModel({
        id:id
    });
    model.fetch({
        success : function(model, result) {
            $('.overwrite-container').html(estateAddPhotos.render(model).$el);
            estateAddPhotos.afterRender();
        }
    });

},

personals : function() {
    var map;
    require(['map/map'], function(Map){
        map = Map;
        if(CURRENT_USER != null) {
            var type = "",
            id = ""
            if (CURRENT_USER.agentId != undefined && 
                CURRENT_USER.agentId != null) {
                type = "agentUserId";
            id = CURRENT_USER.agentId;
        } else {
            type = "ownerUserId";
            id = CURRENT_USER.id;
        }
        var estateCollection = new EstateCollection();
        estateCollection.filters.set(type, id);
        var estatesView = new EstatesView({
            collection : estateCollection
        });
        $('.overwrite-container').html(estatesView.render().$el);
        estatesView.afterRender();

        map.initialize();

    }
});
},

bookmarks : function() {
    var map;
    require(['map/map'], function(Map){
        map = Map;
        var filters = new Backbone.Model();
        if(CURRENT_USER != null) {
            var estatesView = new EstatesView({
                collection : new BookmarksCollection()
            });
            $('.overwrite-container').html(estatesView.render().$el);
            estatesView.afterRender();

            map.initialize();
        }
    });
},

login : function() {
    new Login({
        $el : $('.overwrite-container')
    });
},

signup : function() {
    new Signup({
        $el : $('.overwrite-container')
    });
},

profile : function(){
    var editProfileView = new EditProfile();
    $('.overwrite-container').html(editProfileView.$el);
},

password : function(){
    var editPasswordView = new EditPassword();
    $('.overwrite-container').html(editPasswordView.$el);
},

editAgent : function(id){
    var editAgentView = new EditAgent({
        agentId : id
    });
    $('.overwrite-container').html(editAgentView.$el);
},

editAgency : function(id){
    var editAgencyView = new EditAgency({
        agencyId : id
    });
    $('.overwrite-container').html(editAgencyView.$el);
},

editOwner : function(id){
    var editOwnerView = new EditOwner({
        ownerId : id
    });
    $('.overwrite-container').html(editOwnerView.$el);
},

agencies : function(id){
    $(".loading").fadeOut();
},

agents : function(id){
    $(".loading").fadeOut();
},

plans : function(id){
    var plansView = new PlansListView();
    $('.overwrite-container').html(plansView.$el);
},

operators : function(){
    var operatorsView = new OperatorsListView();
    $('.overwrite-container').html(operatorsView.$el);
}

});

return AppRouter;

});
