define([
    'jquery',
    'underscore',
    'backbone',
    ], function($, _, Backbone) {
        var AdvModel = Backbone.Model.extend({
            defaults : {
                request             : "",
                state               : 'تهران',
                city                : 'تهران',
                street              : "",
                longitude           : "",
                latitude            : "",
                phones              : "",
                cellphone           : "",
                status              : ""
            },

            validation : {
                type : {
                    required: true
                },
        
                request : {
                    required: true
                },
       
                beds : {
                    type :{
                        201 : { 
                            range : [0 , 20]
                        }, 
                        202 : {
                            range : [0 , 20]
                        }, 
                        204 : {
                            range : [0 , 20]
                        }
                    }
                },
                
                dueMonths : {
                    date : true
                },
     
                phones : {
                    range : [0 , 20]
                }
            },


            url: function() {
                if (this.get('id')  != undefined) {
                    return '/api/adv-estate/' + this.get('id');
                } else {
                    return '/api/adv-estate';
                }
            },

            parse : function(model) {
                
                if (model.beds == 0) {
                    model.beds = "0";
                }
                if (model.yearBuilt == 0) {
                    model.yearBuilt = "0";
                }
                
                return model;
            }  
      
        });
        return AdvModel;
    });