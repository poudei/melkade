define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var PlanModel = Backbone.Model.extend({
        defaults: {
            remainingValue : ''
        },

        url: function() {
            if(this.get('id')){
                return '/api/plans/' + this.get('id');
            }else{
                return '/api/plans';
            }            
        },    
        
        parse : function(resp){
            return resp;
        }
        
    });
    return PlanModel;
});