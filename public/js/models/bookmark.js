define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var BookmarkModel = Backbone.Model.extend({
    	defaults : {
            estateId : '',
            userId: ''
    	},

        url: function() {
            if(this.get('id')  != undefined) {
                return '/api/bookmarks/' + this.get('id');
            } else {
                return '/api/bookmarks/';
            }
        }
    });

    return BookmarkModel;
});