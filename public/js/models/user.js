define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var UserModel = Backbone.Model.extend({
    	defaults : {            
            username    : '',
            firstname   : '',
            lastname    : '',
            email       : ''
    	},

        url: function() {
            if(this.get('id')){
                return '/api/users/' + this.get('id');
            }else{
                return '/api/users';
            }            
        },    
        
        parse : function(resp){
            return resp.data
        }
        
    });
    return UserModel;
});