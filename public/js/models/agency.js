define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var AgencyModel = Backbone.Model.extend({
    	defaults : {            
            username    : '',
            firstname   : '',
            lastname    : '',
            email       : ''
    	},

        url: function() {
            if(this.get('id')){
                return '/api/agencies/' + this.get('id');
            }else{
                return '/api/agencies';
            }            
        },    
        
        parse : function(resp){
            return resp;
        }
        
    });
    return AgencyModel;
});