define([
  'jquery',
  'underscore',
  'backbone',
  ], function($, _, Backbone) {
    var EstateModel = Backbone.Model.extend({
      defaults : {
        state               : 'تهران',
        city                : 'تهران',
        hasBookmark         : -1,
        compare             : false,
        ownerName           : "",
        ownerCellphone      : "",
        ownerPhones         : "",
        agencyPhone         : "",
        agencyName          : "",
        agentPhone          : "",
        agent               : "",
        agentName           : "",
        agentCellphone      : "",
        agency              : "",
        editAgency          : "",
        agentUser           : "",
        editAgent           : "",
        ownerUserId         : "",
        editOwner           : ""
      },

      validation : {
        neighborText : {
          required: true
        },

        neighborId : {
          required: true
        },

        ownerPhones:{
          owner: true
        },

        ownerName: {
          owner: true
        },

        agentCellphone : {             
          cellphone : true,
          agent: true
        },

        ownerCellphone : {
         cellphone : true
       },

       agencyPhone: {
        agency: true
      },

      agencyName: {
        agency: true
      },

      agentPhone: {
        agency: true
      },

      agentName: {
        agency: true
      },

      type :{
       required : true 
     },
     
     advertisedBy :{
      required : true
    },
    
    street : {
      required : true
    },
    
    state : {
      required : true
    },
    
    city : {
      required : true
    },
    
    addressVisable : {
      required : true
    },
    
    locationVisible : {
      required : true
    },
    
    request : {
     required : true
   },
   
   building : {
    type : {
      206 : {
        required : true
      }
    }       
  },
  
  yearBuilt : {
    type:{
      201 : {
        required : true, 
        length: 2,
        range : [10 , 93]
      }, 
      202 : {
        required : true,
        length: 2,
        range : [10 , 93]
      }, 
      204 : {
        required : true,
        length: 2,
        range : [10 , 93]
      },
      206 : {
        required : true,
        length: 2,
        range : [10 , 93]
      },  
      205 : {
        required : true,
        length: 2,
        range : [10 , 93]
      }
    }
  },
  
  stories : {
    type:{
      201 : {
        required : true, 
        range : [1 , 150]
      }, 
      202 : {
        required : true,
        range : [1 , 150]
      }, 
      204 : {
        required : true,
        range : [1 , 150]
      },
      206 : {
        required : true,
        range : [1 , 150]
      },  
      205 : {
        range : [1,150]
      }
    }
  },
  
  storey : {
    type :{
      201 : {
        required : true, 
        range : [-5, 150]
      }, 
      
      204 : {
        required : true,
        range : [-5, 150]
      },
      206 : {
        range : [-5, 150]
      },  
      205 : {
        range : [-5, 150]
      }
    }
  },
  
  unit : {
    type:{
      201 : {
        range : [1 , 1500]
      }, 
      
      204 : {
        range : [1 , 1500]
      },
      206 : {
        range : [1 , 1500]
      },  
      205 : {
        range : [1,1500]
      }
    }
  },
  
  units : {
    type: {
      201 : {
        required : true, 
        range : [1 , 1500]
      }, 
      202 : {
        required : true,
        range : [1 , 1500]
      }, 
      204 : {
        required : true,
        range : [1 , 1500]
      },
      206 : {
        range : [1 , 1500]
      },  
      205 : {
        range : [1,1500]
      }
    }
  },
  
  metrage : {
    required : true,
    type:{
      201 : { 
        range : [20 , 10000]
      }, 
      202 : {
        range : [20 , 10000]
      }, 
      203 : {
        range : [20 , 10000]
      }, 
      204 : {
        range : [5 , 10000]
      },
      206 : {
        range : [20 , 10000]
      },  
      205 : {
        range : [2 , 1000]
      }
    }                
  },
  
  beds : {
    type :{
      201 : {
        required : true, 
        range : [0 , 20]
      }, 
      202 : {
        required : true,
        range : [0 , 20]
      }, 
      204 : {
        required : true,
        range : [0 , 20]
      }
    }
  },
  
  parking : {
    type :{
      201 : {
        range : [0 , 20]
      }, 
      202 : {
        range : [0 , 20]
      }, 
      204 : {
        range : [0 , 20]
      },
      205 : {
        range : [0 , 20]
      }
    }
  },
  
  price : {
    range : [ 100000 , 900000000000]
  },
  
  area : {
    type:{
      201 : {
        range : [2 , 10000]
      }, 
      202 : {
        range : [2 , 10000]
      }, 
      203 : {
        required : true,
        range : [2 , 10000]
      },
      204 : {
        range : [2 , 10000]
      },
      206 : {
        
        range : [2 , 10000]
      },  
      205 : {
        range : [2 , 10000]
      }
    }
  },
  requestDate : {
    required : true,
    date : true
  },

  expireDate : {
    required : true,
    date : true
  },
  
  addressVisible : {
   required : true
 },
 
 dueMonths : {
  date : true
},

direction : {
  required : true
},

deal : {
  type :{
    204 : {
      required : true
    }, 
    205 : {
      required : true
    }
  }
},

postalCode : {
  length: 10
},
phones : {
 range : [0 , 20]
},
           //  loan :{
           //      range : [100000 , 1000000000]
           //  },
           //  exPrice : {
           //      range : [100000 , 900000000000]
           // },
           length : {
            range : [1, 999]
          },
          width : {
            range : [1, 999]
          },
          refinement : {
            range : [1, 999]
          },
          toilets : {
            range : [0, 20]
          },
          irToilets : {
           range : [0, 20]
         },
         bath : {
          range : [0, 20]
        },
        
        balcony : {
          range : [0, 1000]
        },
        
        storage : {
          range : [0, 1000]
        }
      },


      url: function() {
        if (this.get('id')  != undefined) {
          return '/api/estates/' + this.get('id');
        } else {
          return '/api/estates';
        }
      },

      parse : function(model) {
        if(model.agentUser != undefined && model.agency != undefined) {
          this.set('agentName', model.agentUser.lastname || model.agentUser.name);
          this.set('agentCellphone', model.agentUser.cellphone);
          
          this.set('agencyName', model.agency.name);
          this.set('agencyPhone', model.agency.phone);
        } else if (model.ownerUser)  {
          this.set('ownerName', model.ownerUser.lastname);
          this.set('ownerCellphone', model.ownerUser.cellphone);
          this.set('ownerPhones', model.ownerUser.phone);
        }

        return model;
      }  
      
    });
return EstateModel;
});