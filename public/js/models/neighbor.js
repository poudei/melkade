define([
    'jquery',
    'underscore',
    'backbone',
], function($, _, Backbone) {
    var NeighborModel = Backbone.Model.extend({
    	defaults : {
            
            address : '',
            street : '',
            bystreet : ''
    	},

        url: function() {
            if(this.get('id')  != undefined) {
                return '/api/neighbors/' + this.get('id');
            }
            

        },     
        
    });
    return NeighborModel;
});