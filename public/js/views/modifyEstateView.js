define([
    'jquery',
    'underscore',
    'backbone',
    'map/map',
    'models/estate',
    'collections/similarEstates',
    'models/neighbor',
    'base/view/view',    
    'views/estatesListView',    
    'vendor/bootstrap-select',     
    'base/labels/labels', 
    ], function ($, _, Backbone, Map, EstateModel, SimilarEstatesCollection, NeighborModel, BaseView, EstatesListView ) {

        var EstatesView = BaseView.extend({		
            events: { },

            initialize: function(options) {
                this.model = new EstateModel();
                this.model.attributes = _CURRENT_ESTATE;

                this.render();
            },

            render: function() {
                var self = this;
                if (this.model.get('photos') == undefined) {
                    this.model.set('photos', undefined);
                }

                var requestDate = new Date(this.model.get('requestDate')['date']);
                var requestDateString = requestDate.getFullYear() + '/' + requestDate.getMonth() + '/' + requestDate.getDate()

                this.model.set('requestDateString', requestDateString);

                $.each($('.show-field'), function(index, el){                    
                    var id          = $(el).attr('id'),
                    val         = '',
                    constant    = self.model.get(id);
                    
                    if($(el).data('multiselect')){
                      for (var i = 0; constant && i < constant.length - 1; i++) {
                          val = val + '<span>' + Labels[id][constant[i]] + '</span>' + '، ';
                      }
                      if(Labels[id] && constant)
                        val = val + Labels[id][constant[constant.length - 1]];
                }else{
                  if($(el).data("numbermap")){ 
                      val = _.numberVal(constant);
                  }else{
                      if(Labels[id]){
                        val = Labels[id][constant];
                        if(constant == undefined || constant == "undefined"){
                            val = Labels[id][0];
                        }                            
                    }                         
                }
            }
            
            if(val == '' && constant && constant.date){
             val = _.dateFormat(constant.date);
         }

         if(constant && constant != null && val != undefined && val != "undefined" && val != ''){
            if(typeof(constant) == 'object' && constant.length < 1) {
                $('#' + id).html("-");
            } else {
                $('#' + id).html(val);
            }

        }else{
         if(constant && constant != null && val != "undefined"){
            if(typeof(constant) == 'object' && constant.length == 0) {
                $('#' + id).html("-");   
            } else {
                $('#' + id).html(constant);
            }
        } else {
            if(constant != null && typeof(constant) == 'object' && constant.length < 1) {
                $('#' + id).html("-");
            }
        }
    }
});

var advertisedBy = this.model.get('advertisedBy'),
ownerEl = $('.owner-td'),
agencyEl = $('.agency-td');

if(advertisedBy == '101') {
    agencyEl.hide();
    ownerEl.show();
} else {
    agencyEl.show();
    ownerEl.hide();
}

$.each($('.one-line'), function(item, value){
    if ($(value).find('span').length < 2 ) {
        $(value).remove();
    }   
});

$.each($('td'), function(item, value) {
    var el1 = $(".rf-content", value), el2 = $(".lf-content", value), el3 = $("span", value);

    $(".cnt-table-content", value).width(el1.width() + el2.width() + el3.width()+16);
})

$('.views').html($('.views').html().replace(/\.00\b/g,''));

this.commaSeperation();
this.removeExtraLabels();
this.convertToPersian();

this.afterRender();

return this;
},

commaSeperation : function() {
    $.each($('.views span.comma'), function(item, value) {
        $(value).html($(value).html().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    });

    $.each($('.views span.number-inp'), function(item, value) {
        $(value).html($(value).html().replace(/^0$/, "-"));
    });
},

removeExtraLabels : function () {
    $.each($('.views span.number-inp'), function(item, value) {
        if (/^-$/.test($(value).html())) {
            $(value).parents('td').find('.lf-content').html('');
        }
    });
},

convertToPersian : function() {
    $.each($('.views span'), function(item, value) {
        $(value).html($(value).html().replace(/0/g,'۰'));
        $(value).html($(value).html().replace(/1/g,'۱'));
        $(value).html($(value).html().replace(/2/g,'۲'));
        $(value).html($(value).html().replace(/3/g,'۳'));
        $(value).html($(value).html().replace(/4/g,'۴'));
        $(value).html($(value).html().replace(/5/g,'۵'));
        $(value).html($(value).html().replace(/6/g,'۶'));
        $(value).html($(value).html().replace(/7/g,'۷'));
        $(value).html($(value).html().replace(/8/g,'۸'));
        $(value).html($(value).html().replace(/9/g,'۹'));
    });
},

afterRender: function(){            
    var self = this;

    $('.carousel').carousel();

    Map.initialize();
    Map.addPoint(this.model.get('longitude'), this.model.get('latitude'), this.model.toJSON());
    Map.setCenter(this.model.get('longitude'), this.model.get('latitude'));
}
});
return EstatesView;

});