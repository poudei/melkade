define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'models/estate',
    'models/neighbor',
    'base/view/form',
    'map/map',
    'vendor/bootstrap-select',
    'vendor/jquery.icheck'

    ], function(bootstrap, $, _, Backbone, EstateModel, NeighborModel, FormView, Map) {

        var EstateEdit = FormView.extend({
            events: {
                'keyup input'           : 'inputCheck',
            },

            initialize: function(options) {
                _.bindAll(this, 'render', 'afterRender');
                var self = this;
                this.events = _.extend(this.events, this.formEvents);

                var type = {
                    201: "apartment",
                    202: "villa",
                    203: "land",
                    204: "office",
                    205: "shop",
                    206: "property"

                };

                this.model = new EstateModel({id: options.id});
                this.model.fetch({
                    success: function(model, result) {
                        self.model = model;
                        require(['text!views/templates/New-Estate-Form/' + type[model.get('type')] + '-form.html'],
                            function(FormTpl) {
                                self.template = FormTpl;
                                if (self.model.get('price') && self.model.get('price').split('.00')) {
                                  self.model.set('price', self.model.get('price').split('.00')[0]);
                                }
                                if (self.model.get('exPrice') && self.model.get('exPrice').split('.00')) {
                                  self.model.set('exPrice', self.model.get('exPrice').split('.00')[0]);
                                }

                                if (self.model.get('yearBuilt') && self.model.get('yearBuilt').toString().split('13')) {
                                    self.model.set('yearBuilt',self.model.get('yearBuilt').toString().split('13')[1]);
                                }

                                $('.overwrite-container').append(self.render().$el);
                                self.afterRender();
                            });
                    }
                });
            },

            render: function() {            
                this.$el.html(this.template);           
                return this;
            },

            inputCheck : function(event){
                var el = $(event.currentTarget);
                if(el.hasClass('comma')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9۰-۹]/g, '').replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",")
                    });
                }
                if(el.hasClass('digi')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9۰-۹]/g, '');
                    });
                }

                if(el.hasClass('digi-')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9-۰-۹]/g, '');
                    });
                }
            },

            afterRender: function() {

                Map.initialize();
                Map.addClickListener();

                this.parent('afterRender');

                $('.cnt-select .bootstrap-select', this.$el).each(function(index, el){
                   $(el).width( ($(el).closest('.cnt-select')).width() - ($(el).siblings('.rf-content-select')).width() -6 );
               });
                this.populate();
                var advertisedBy = this.model.get('advertisedBy'),
                ownerEl = this.$el.find('.owner-td'),
                agencyEl = this.$el.find('.agency-td');

                if(advertisedBy == '101') {
                    agencyEl.hide();
                    ownerEl.show();
                } else {
                    agencyEl.show();
                    ownerEl.hide();
                }

                Map.addPoint(this.model.get('longitude'), this.model.get('latitude'), this.model.toJSON());
                // Map.setCenter(this.model.get('longitude') +' ' +this.model.get('latitude'));
                var neighborModel = new NeighborModel({
                    id: this.model.get('neighborId')
                });
                neighborModel.fetch({
                    success: function(model) {
                        // map.self.drawNeighbor(model.get('boundaryText'));
                        $('input[name="neighborText"]').val(model.get('title'));
                    }
                });
            },

            submitSuccess : function(){                
                var url = 'estate/view/' + this.model.get('id');
                
                Backbone.history.navigate(url, true);
            },

            submitFailure : function(model, err) {
                if(err != undefined && err.responseText != "") {
                    self.$('.msg-container').html(err.responseText);
                }
            }
        });

return EstateEdit;

});