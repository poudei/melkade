define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'base/view/view',
    'views/estatesListView',
    'models/neighbor',
    'map/map',
    'text!views/templates/estatesView.html',
    'jquery-ui',
    'vendor/jquery.nanoscroller.min',
    'vendor/bootstrap-select',
    'vendor/jquery.icheck'
    ], function (bootstrap ,$, _, Backbone, BaseView,
        EstatesListView, NeighborModel, Map, EstatesTpl ) {
        
        var map;

        var EstatesView = BaseView.extend({
            tagName: 'div',
            className: 'estates',
            events: {
                'keyup input'                           : 'inputCheck',
                'ifToggled .navbar-filter'              : 'applyFilter',
                'click .navbar-filter'                  : 'applyFilter',
                'click .navbar-bound'                   : 'applyBound',
                'click .neighbor-search-btn'            : 'neighborSearch',
                'click .dropdown-menu input[type=text]' : 'clickForSetFilter',
                'click .left-slider-bar'                : 'leftSlider'
            },

            initialize: function(options) {
                this.template = _.template(EstatesTpl);
                this.$el.html(this.template());
                this.collection = this.options.collection;
            },

            render: function() {
                var self = this;

                if (this.options.geo != undefined) {
                    $( ".estates-input-search", self.$el ).val(this.options.geo.neighborTitle);
                }

                if(this.options.filters != undefined ){
                    this.collection.filters.set(this.options.filters.toJSON());
                    
                    for (var filter in self.options.filters.toJSON()) {
                        if (self.options.filters.toJSON()[filter].split != undefined) {
                            var fi = self.options.filters.toJSON()[filter].split(',');
                            for (var f in fi) {
                                var el = self.$('.navbar-filter[name="' + filter + '"][value="' + fi[f] + '"]');
                                if (el.length != 0) {
                                    el.attr('checked', 'checked');
                                }
                            };
                        }
                    };
                };

                this.collection.reset();
                this.collection.filters.set('start', 0);
                $(".loading").fadeIn();

                this.collection.fetch({
                    success : function(){
                        var estatesListView = new EstatesListView({
                            collection : self.collection
                        });
                        $('.sidebar-left', self.$el).html(estatesListView.render().$el);
                        

                        var heightWindow = $(window).height();
                        var navHeight =  $('.mk-docs-nav').height() + 1 +10;
                        var navStatic = $('.mk-docs-nav-static').parent().height() + 10;
                        var mapHeight = heightWindow - navHeight - navStatic - 4  ;
                        $('.map').css('height' , mapHeight);
                        $('.estates .sidebar-left').css('height' , mapHeight);
                        var estateTop = $('.estate-top-container').height() + 31;
                        var estateListView = mapHeight - estateTop;
                        $('.sidebar-left .estate-list-view-container').css('height' , estateListView);

                        $(".nano").nanoScroller();
                        $(".loading").fadeOut();
                    }
                });

    return this;
},

inputCheck : function(event){
    var el = $(event.currentTarget);
    if(el.hasClass('comma')){
        el.val(function(index, value) {
            return value.replace(/[^0-9۰-۹]/g, '').replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",")
        });
    }
},

afterRender: function(){
    var self = this;

    if(this.options.geo != undefined) {
        map.self.setCenter(this.options.geo.neighborCenter);
        map.self.drawNeighbor(this.options.geo.neighborBoundary);
    }

    $(".mk-docs-nav-static .navbar-toggle").click(function () {
        var h = $(this).attr('data-target');
        $(h).toggleClass('in');
        $(h).css('height' , 'auto');
    });

    $( "#neighborId" ,self.$el).autocomplete({
        appendTo: ".cnt-search",
        messages: {
            noResults: '',
            results: function() {}
        },

        source: function(request, response) {
            $.ajax({
                url : "/api/neighbors?filter[]=title|like|" + request.term,
                dataType : "json",
                success : function(data) {
                    response($.map( data.items, function( item ) {
                        return {
                            label: item.title ,
                            value: item.title,
                            center : item.centerText,
                            boundary: item.boundaryText,
                            id : item.id,
                            title : item.title
                        }
                    }));
                }
            });
        },

        select: function( event, ui){

            $('#neighborId', self.$el).val(ui.item.id);
            var data = {
                id: ui.item.id,
                title : ui.item.value,
                center: ui.item.center,
                boundary: ui.item.boundary
            };
            self.data = data;

            self.neighborSearch(data);
        },
        focus : function(event, ui){
            self.data = ui.item;
        }
    });

$('input[type=checkbox]', this.$el).iCheck({
    checkboxClass: 'icheckbox_minimal-purple navbar-filter'
});
$('select', this.$el).selectpicker();
$('.dropdown-toggle', this.$el).dropdown();

var heightWindow = $(window).height();
var navHeight =  $('.mk-docs-nav').height() + 1 +10;
var navStatic = $('.mk-docs-nav-static').parent().height() + 10;
var mapHeight = heightWindow - navHeight - navStatic - 4  ;
$('.map').css('height' , mapHeight);

$( window ).resize(function() {
    var heightWindow = $(window).height();
    var navHeight =  $('.mk-docs-nav').height() + 1 +10;
    var navStatic = $('.mk-docs-nav-static').parent().height() + 10;
    var mapHeight = heightWindow - navHeight - navStatic - 4 ;
    $('.map').css('height' , mapHeight);
    $('.estates .sidebar-left').css('height' , mapHeight);
    var estateTop = $('.estate-top-container').height() + 31;
    var estateListView = mapHeight - estateTop;
    $('.sidebar-left .estate-list-view-container').css('height' , estateListView);
    $(".nano").nanoScroller();
});

},
clickForSetFilter : function(e){
    e.stopPropagation();
    return false;
},

applyFilter : function (e) {
    var self = this;

    e.stopPropagation();

    if($(e.target) == undefined) {
        return;
    }

    var filterName = $(e.target).attr('name');
    var filterValue = $(e.target).attr('value');

    if($(e.target).data('type') != undefined && $(e.target).data('type') == 'array') {
        if ($(e.target).is(':checked')) {
            this.collection.filters.set(filterName, filterValue);
        } else {
            this.collection.filters.unset(filterName);
        }
    } else {
        if ($(e.target).is(':checked')) {
            if (this.collection.filters.get(filterName) == undefined) {
                this.collection.filters.set(filterName, filterValue);
            } else {
                this.collection.filters.set(filterName, this.collection.filters.get(filterName) + ',' + filterValue);
            }
        } else {
            var filtersArray = this.collection.filters.get(filterName).split(',');
            var index = filtersArray.indexOf(filterValue);
            if (index > -1) {
                filtersArray.splice(index, 1);
                this.collection.filters.set(filterName, filtersArray.join(','));
                if(filtersArray.length == 0) {
                    this.collection.filters.unset(filterName);
                }

            }
        }
    }

    Map.markerClusterer.clearMarkers();

    this.render();

    return;
},

applyBound : function (e) {
    var self = this;

    e.stopPropagation();

    if($(e.target) == undefined) {
        return;
    }

    var parent = $(e.target).closest('.dropdown-menu');

    var lowerName = parent.find('.lowerBound').attr('name'),
    upperName = parent.find('.upperBound').attr('name'),
    lowerValue = parent.find('.lowerBound').val().replace(/,/g , ""),
    upperValue = parent.find('.upperBound').val().replace(/,/g , "");

    if(lowerValue == "") {
        parent.find('.lowerBound').val("0");
        lowerValue = "0";
    }
    if(upperValue == "") {
        parent.find('.upperBound').val("999,999,999,999");
        upperValue = "999999999999";
    }

    this.collection.filters.set(lowerName, lowerValue);
    this.collection.filters.set(upperName, upperValue);

    Map.clearMarkers();

    this.render();

    var h = $(this).attr('data-target');
    $(h).toggleClass('in');
    $(h).css('height' , 'auto');

    return;
},

neighborSearch: function(data){
    var self = this;

    if(data.center == undefined && this.data != undefined) {
        data = this.data;
    }

    this.collection.filters.set('neighborId', data.id);

    Map.clearMarkers();

    this.render();

    if(data != undefined) {
        map.self.drawNeighbor(data.boundary);
        map.self.setCenter(data.center);
    }
},

leftSlider : function(e) {
    if(! $('.left-slider-bar span', this.$el).hasClass('left-caret')) {
        $('.left-slider-bar span', this.$el).addClass('left-caret');
        $('.map-container', this.$el).removeClass("col-md-9");
        $('.map-container', this.$el).addClass("col-md-12");
        $('.sidebar-left', this.$el).animate({
            width: 'toggle'
        });
    } else {
        $('.left-slider-bar span', this.$el).removeClass('left-caret');
        $('.map-container', this.$el).removeClass("col-md-12");
        $('.map-container', this.$el).addClass("col-md-9");
        $('.sidebar-left', this.$el).animate({
            width: 'toggle'
        });
    }

    map.updateSize();
}

});

return EstatesView;

});