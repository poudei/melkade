define([
    'jquery',
    'underscore',
    'backbone',    
    'base/view/listView',
    'collections/operators',    
    'views/operatorItemView',    
    'text!views/templates/operators.html',    
], function($, _, Backbone, BaseListView, OperatorsCollection, OperatorItemView, OperatorsTpl) {
    var OperatorsListView = BaseListView.extend({
        events      : {
            
        },                
                
        initialize: function() {
            this.collection = new OperatorsCollection([], {
                mode : 'all'
            });            
            
            this.render();        
            this.collection.on('reset', function(){
                $('.agencies-list').empty();
            });   
            OperatorsListView.__super__.initialize.apply(this, []);
        },       
        
        render : function(){
            var template = _.template(OperatorsTpl);
            this.$el.html(template());
        },
                
        renderItem: function(model) {                        
            var item = new OperatorItemView({
                model       : model                    
            });
            $('ul', this.$el).append(item.render().$el);            
        },
       
       afterRenderItems : function(){
            if(this.collection.length == 0){
                $('.agencies-list', this.$el).html('جستجو نتیجه ای در بر نداشت.')
            }
       }              
        
    });

    return OperatorsListView;
});
