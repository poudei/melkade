define([
    'jquery',
    'underscore',
    'backbone',    
    'base/view/listView',
    'collections/plans',    
    'views/planItemView',    
    'text!views/templates/plans.html',    
], function($, _, Backbone, BaseListView, PlansCollection, PlanItemView, PlansTpl) {
    var PlansListView = BaseListView.extend({
        events      : {
             
        },                
                
        initialize: function() {
            this.collection = new PlansCollection([], {
                mode : 'all',
                limit: 20
            });            
            
            this.render();            
            
            PlansListView.__super__.initialize.apply(this, []);
        },       
        
        render : function(){
            var template = _.template(PlansTpl);
            this.$el.html(template());
        },
                
        renderItem: function(model) {                        
            var item = new PlanItemView({
                model       : model                    
            });
            $('table', this.$el).append(item.render().$el);            
        },
                
       afterRenderItems : function(){
            if(this.collection.length == 0){
                // $('.plans-list', this.$el).html('جستجو نتیجه ای در بر نداشت.')
            }
       }
        
    });

    return PlansListView;
});
