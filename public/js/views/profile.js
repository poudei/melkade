define([
    'jquery',
    'underscore',
    'backbone',
    'base/view/view',
    'text!views/templates/profile.html'

    ], function ($, _, Backbone, BaseView, Tpl) {
        
        var ProfileView = BaseView.extend({
		
            events: {
			
            },

            initialize: function(options) {

                _.bindAll(this, 'render', 'afterRender');
                this.$el = options.$el;
                this.template = _.template(Tpl);
                this.render();
            },

            render: function() {
                var self = this;

                this.$el.html(this.template());

                return this;
            },

            afterRender: function(){
            
                var self = this;
            }
        });
        return ProfileView;

    });