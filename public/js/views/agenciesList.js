define([
    'jquery',
    'underscore',
    'backbone',    
    'base/view/listView',
    'collections/agencies',    
    'views/agencyItemView',    
    'text!views/templates/agencies.html',    
], function($, _, Backbone, BaseListView, AgenciesCollection, AgencyItemView, AgenciesTpl) {
    var AgenciesListView = BaseListView.extend({
        events      : {
            'keyup .agencies-search-wrapper input' : 'searchAgencies',
            'click .agencies-search-btn' : 'searchAgencies'
        },                
                
        initialize: function() {
            this.collection = new AgenciesCollection([], {
                mode : 'all'
            });            
            
            this.render();        
            this.collection.on('reset', function(){
                $('.agencies-list').empty();
            });   
            AgenciesListView.__super__.initialize.apply(this, []);
        },       
        
        render : function(){
            var template = _.template(AgenciesTpl);
            this.$el.html(template());
        },
                
        renderItem: function(model) {                        
            var item = new AgencyItemView({
                model       : model                    
            });
            $('ul', this.$el).append(item.render().$el);            
        },
       
       afterRenderItems : function(){
            if(this.collection.length == 0){
                $('.agencies-list', this.$el).html('جستجو نتیجه ای در بر نداشت.')
            }
       },

        searchAgencies : function(e){
            if (e.type == "keyup" && e.keyCode == 13) {
                var el = $(e.target);
               this.collection.filters.set({
                    'filter'  : ['name|like|' + el.val()],
                    'start'   : 0
                });
                this.collection.reset();
                this.collection.fetch({
                    add     : true,
                    silent  : true,
                    update  : true
                });
            } else if(e.type == "click") {
                var el = $(".agency-input", this.$el);
                this.collection.filters.set({
                    'filter'  : ['name|like|' + el.val()],
                    'start'   : 0
                });
                this.collection.reset();
                this.collection.fetch({
                    add     : true,
                    silent  : true,
                    update  : true
                });
            }

        }                
        
    });

    return AgenciesListView;
});
