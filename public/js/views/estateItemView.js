define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'models/estate',
    'models/bookmark',
    'base/view/view',
    'views/estateView',
    'map/map',
    'text!views/templates/estateItem.html'
    ], function ($, _, Backbone, App, EstateModel, BookmarkModel, BaseView, EstateView, Map, EstateItemTpl) {

        var addNewEstateView = BaseView.extend({

            tagName : 'li',
            className : 'single-estate',
            events : {
                "mouseenter" : 'highlightPoint',
                "mouseleave" : 'removeHighlightPoint',
                'click .edit-estate-btn' : 'edit',
                'click .add-compare' : 'compare',
                'click .add-bookmark' : 'bookmark'
            },

            type : {
                '201' : 'apartment',
                '202' : 'villa',
                '203' : 'land',
                '204' : 'office',
                '205' : 'shop',
                '206' : 'property'
            },

            initialize : function(options) {
                this.options = options;
                this.template = _.template(EstateItemTpl);
            },
            
            render : function() {

                var typeObj = {
                    '201' : 'آپارتمان',
                    '202' : 'ویلا',
                    '203' : 'زمین',
                    '204' : 'دفتر',
                    '205' : 'مغازه',
                    '206' : 'مستغلات'
                };

                var requestObj = {
                    '301' : 'فروش',
                    '302' : 'پیش فروش',
                    '303' : 'اجاره'
                };

                if(this.model.get('requestDate')) {
                    var requestDate = new Date(this.model.get('requestDate')['date']);
                    var requestDateString = requestDate.getFullYear() + '/' + requestDate.getMonth() + '/' + requestDate.getDate()

                    this.model.set('typeObj', typeObj);
                    this.model.set('requestObj', requestObj);
                    this.model.set('requestDateString', requestDateString);
                } else {
                    this.model.set('requestDateString', "");
                }
                
                if(this.model.get('operatorName') == undefined) {
                    this.model.set('operatorName', '');
                }

                $(this.el).html(this.template(this.model.toJSON()));

                if (this.options.fromSort == null) {
                    Map.addPoint(this.model.get('longitude'), this.model.get('latitude'), this.model.toJSON());
                }

                if (this.options.model.get('photos') && this.options.model.get('photos').length > 0) {
                    $('.info-more-text a', this.$el).append('<img title="با تصویر" src="img/hasPhoto.png" style="padding:8px">');
                }

                this.commaSeperation();
                this.convertToPersian();

                return this;
            }, 

            commaSeperation : function() {
                $.each($('span.comma', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                });

                $.each($('span.number-inp', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/^0$/, "-"));
                });
            },


            convertToPersian : function() {
                $.each($('span', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/0/g,'۰'));
                    $(value).html($(value).html().replace(/1/g,'۱'));
                    $(value).html($(value).html().replace(/2/g,'۲'));
                    $(value).html($(value).html().replace(/3/g,'۳'));
                    $(value).html($(value).html().replace(/4/g,'۴'));
                    $(value).html($(value).html().replace(/5/g,'۵'));
                    $(value).html($(value).html().replace(/6/g,'۶'));
                    $(value).html($(value).html().replace(/7/g,'۷'));
                    $(value).html($(value).html().replace(/8/g,'۸'));
                    $(value).html($(value).html().replace(/9/g,'۹'));

                });
            },

            highlightPoint : function(e) {
                var marker = Map.findMarker(this.model.get('id'));
                if (marker) {
                    var cluster = Map.findMarkersCluster(marker);
                    if (cluster) {
                        $(cluster.clusterIcon_.div_).css('opacity', 0.4);
                    }
                    var iconPath = '/img/pins/'+this.type[this.model.get('type')]+'_hover'+'.png';
                    Map.setMarkerIcon(marker, iconPath);
                }
            },

            removeHighlightPoint : function(e) {
                var marker = Map.findMarker(this.model.get('id'));
                if (marker) {
                    var cluster = Map.findMarkersCluster(marker);
                    if (cluster) {
                        $(cluster.clusterIcon_.div_).css('opacity', 1);
                    }
                    var iconPath = '/img/pins/'+this.type[this.model.get('type')]+ '_'+ this.model.get('request') +'.png';
                    Map.setMarkerIcon(marker, iconPath);
                }
            },

            edit : function (e) {
                Backbone.history.navigate('estate/edit/' + this.model.get('id'),  {
                    trigger: true
                });
            },

            bookmark : function(e) {
                if(CURRENT_USER != undefined) {
                    var self = this;

                    var bookmarkModel = new BookmarkModel({
                        estateId: this.model.get('id'),
                        userId: CURRENT_USER.id
                    });

                    if(this.model.get('bookmarkId') < 0) {
                        bookmarkModel.save({}, {
                            success: function(model) {
                                self.$el.find('.add-bookmark i').removeClass('star-e-icon');
                                self.$el.find('.add-bookmark i').addClass('star-icon');
                                self.model.set('bookmarkId', model.get('id'));
                            }
                        });
                    } else {
                        bookmarkModel.set('id', this.model.get('bookmarkId'));
                        bookmarkModel.destroy({
                            success: function(model){
                                self.$el.find('.add-bookmark i').removeClass('star-icon');
                                self.$el.find('.add-bookmark i').addClass('star-e-icon');
                                self.model.set('bookmarkId', -1);
                                if(self.model.get("fromBookmark") != undefined) {
                                    self.$el.remove();
                                }
                            }
                        });
                    }
                } else {

                }
            },

            compare : function(e){
                if( this.model.collection.compare == undefined ){
                    this.model.collection.compare = [];
                }
                var compareArray = this.model.collection.compare;
                if(compareArray.length < 3 && !this.model.get('compare')) {
                    compareArray.push(this.model.get('id'));
                    $('.compare-count').html(compareArray.length);
                    this.$el.find('.add-compare i').removeClass('plus-icon');
                    this.$el.find('.add-compare i').addClass('plus-e-icon');
                    this.model.set('compare', true);
                } else {
                    var index = compareArray.indexOf(this.model.get('id'));
                    if (index > -1) {
                        compareArray.splice(index, 1);
                        $('.compare-count').html(compareArray.length);
                        this.$el.find('.add-compare i').removeClass('plus-e-icon');
                        this.$el.find('.add-compare i').addClass('plus-icon');
                        this.model.set('compare', false);
                    }
                }

            }
        });

return addNewEstateView;
});
