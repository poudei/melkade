define([
    'jquery',
    'underscore',
    'backbone',
    'base/view/view',
    'text!views/templates/login.html'

], function($, _, Backbone, BaseView, Tpl) {

    var LoginView = BaseView.extend({
        events: {
            'click .login-melkade'      : 'login',
            'keyup input'               : 'removeErrors' 
        },
                
        initialize: function(options) {            
            this.$el = options.$el;
            this.template = _.template(Tpl);
            this.render();
        },
                
        render: function() {
            this.$el.html(this.template());
            return this;
        },
                
       removeErrors : function(e){
           $('.msg-container', this.$el).html('');
           $('.msg-container', this.$el).css('visibility', 'hidden');
           if(e.keyCode == 13){
               this.login();
           }
       },
        
        validation : function(){
            var valid = true;
            if($('[name=identity]', this.$el).val() == ''){
                $('.msg-container', this.$el).css('visibility', 'visible');
                $('.msg-container', this.$el).html('لطفا تلفن همراه خود را وارد کنید.');
                return false;
            }
            
            if($('[name=credential]', this.$el).val() == ''){
                $('.msg-container', this.$el).css('visibility', 'visible');
                $('.msg-container', this.$el).html('لطفا رمز خود را وارد کنید.');
                return false;
            }			
            return true;
        },
                
        login: function(e) {
            if(this.validation()){
               $('form', this.$el).submit();
            }
            e.stopPropagation();
            return false;
        }
    });
    return LoginView;

});