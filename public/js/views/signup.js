define([
    'jquery',
    'underscore',
    'backbone',
    'base/view/view',
    'text!views/templates/signup.html',
    'vendor/jquery.icheck',   
    'helpers/base'
    ], function ($, _, Backbone, BaseView, Tpl) {
        
        var SignupView = BaseView.extend({
		
            events: {
                'ifToggled   .agent-chck'       : 'agent',
                'click       .signup-btn'     	: 'saveUser'
            },

            initialize: function(options) {
                _.bindAll(this, 'render', 'afterRender');
                this.$el = options.$el;
                this.template = _.template(Tpl);
                this.render();
            },

            render: function() {
                var self = this;

                this.$el.html(this.template());
                this.afterRender();

                return this;
            },

            afterRender: function(){                            
                $('input').iCheck({
                    checkboxClass: 'icheckbox_minimal-purple',
                    increaseArea: '20%'
                });

                this.$el.find('.agencyPhones').parents('li').hide();
            },

            agent : function(e) {
                this.$el.find('.agencyPhones').parents('li').toggle();
            },
			
            validation : function(){
				
                if($('[name=lastname]').val() == ''){
                    $('.msg-container', this.$el).html('نام خانوادگی اجباری میباشد.');
                    return false;
                }

                if($('[name=cellphone]').val() == ''){
                    $('.msg-container', this.$el).html('شماره تلفن همراه اجباری می باشد.');
                    return false;
                }

                if(!/^09(\d{9}$)/.test($('[name=cellphone]').val())){
                    $('.msg-container', this.$el).html('شماره وارد شده صحیح نمیباشد.');
                    return false;
                }				
				
                if( $('[name=email]').val() != "" && !/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($('[name=email]').val())){
                    $('.msg-container', this.$el).html('ایمیل صحیح نمی باشد.');
                    return false;
                }

                if($('.agent-chck', this.$el).is(':checked')) {
                    if($('.agencyPhones', this.$el).val() == '') {
                        $('.msg-container', this.$el).html('اگر مشاور هستید نام مشاور املاک را پر کنید.');
                        return false;
                    }
                }

                return true;
            },

            saveUser : function(){
                var formObj = $('form', this.$el).serializeObject();  
                if(this.validation()){
                    $(".loading").fadeIn();
                    $.ajax({
                            url: '/api/user/signup',
                            data: formObj,
                            type: "POST",
                            dataType: 'json',
                            success: function(resp){
                                    $('.msg-container', this.$el).css('visibility', 'none');
                                    $(".loading").fadeOut();
                                    window.location.href='/user/login';
                            },
                            error: function(resp) {
                                $('.msg-container', this.$el).html(resp.responseText);
                                $('.msg-container', this.$el).css('visibility', 'visible');
                                $(".loading").fadeOut();
                            }
                    }, this);
                }else{
                    $('.msg-container', this.$el).css('visibility', 'visible');
		}                
            }

        });
        return SignupView;

    });