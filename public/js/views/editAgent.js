define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',    
    'models/user',
    'base/view/form',
    'text!views/templates/agent-form.html',
], function(bootstrap, $, _, Backbone, UserModel, FormView, AgentFormTpl) {

    var EditAgentView = FormView.extend({
        events: { 
            'click .edit-agent-btn'   : 'saveAgent'
        },
                
        initialize: function(options) {
            _.bindAll(this, 'render', 'afterRender');
            var self = this;
            
            this.events = _.extend(this.events, this.formEvents);
            this.model = new UserModel({id: options.agentId});
            this.model.fetch({
                success: function(model, result) {            
                    self.model = model;
                    self.render();
                }
            });
        },
        
        render: function() {    
            var template = _.template(AgentFormTpl);
            this.$el.html(template(this.model.toJSON()));           
            return this;
        },

        validation : function(){            
            self.$('.msg-container').removeClass('hidden');
            if($('[name=email]', this.$el).val() != "" && !/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($('[name=email]', this.$el).val())){
                    $('.msg-container', this.$el).html('ایمیل صحیح نمی باشد.');
                    self.$('.msg-container').addClass('alert alert-danger');
                    $('.msg-container', this.$el).css('visibility', 'visible');
                    return false;
            }   

            if(!/^09(\d{9}$)/.test($('[name=cellphone]').val())){
                self.$('.msg-container').addClass('alert alert-danger');
                $('.msg-container', this.$el).css('visibility', 'visible');
                $('.msg-container', this.$el).html('شماره وارد شده صحیح نمیباشد.');
                return false;
            }   

            return true;
        },
                
        saveAgent: function() {
            var user =  this.$('form').serializeObject(),
                self = this;  

            if(!this.validation()){
                return false;
            }    
            
            this.model.save(user, {
                success : function(){
                    self.$('.msg-container').removeClass('hidden');
                    self.$('.msg-container').removeClass('alert alert-danger');
                    self.$('.msg-container').addClass('alert alert-success');
                    self.$('.msg-container').html('اطلاعات با موفقیت تغییر کرده است.');
                    self.$('.msg-container').css('visibility', 'visible');
                },
                error : function(model, resp){
                    self.$('.msg-container').html(resp.responseText);
                    self.$('.msg-container').css('visibility', 'visible');
                    self.$('.msg-container').removeClass('alert alert-success');
                    self.$('.msg-container').addClass('alert alert-danger');
                    self.$('.msg-container').removeClass('hidden');
                     //self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید.');
                }        
            })
        }
    });

    return EditAgentView;

});