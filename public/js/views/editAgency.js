define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',    
    'models/agency',
    'base/view/form',
    'map/map',
    'text!views/templates/agency-form.html',
    'vendor/bootstrap-select'
    ], function(bootstrap, $, _, Backbone, AgencyModel, FormView, Map, AgencyFormTpl ) {

        var EditAgencyView = FormView.extend({
            events: { 
                'click .edit-agency-btn'   : 'saveAgency'
            },
                
            initialize: function(options) {
                _.bindAll(this, 'render', 'afterRender');
                var self = this;
                this.events = _.extend(this.events, this.formEvents);
                this.model = new AgencyModel({
                    id: options.agencyId
                });
                this.model.fetch({
                    success: function(model, result) {            
                        self.model = model;
                        self.render();
                    }
                });
            },
        
            render: function() {    
                var template = _.template(AgencyFormTpl);
                this.$el.html(template(this.model.toJSON()));           
            
                Map.initialize();
                Map.addClickListener();
                Map.addPointNoData(this.model.get('longitude'), this.model.get('latitude'));
                
                $('input[type=checkbox]', this.$el).iCheck({
                    checkboxClass: 'icheckbox_minimal-purple navbar-filter'
                });
                $('select', this.$el).selectpicker();
                $('.dropdown-toggle', this.$el).dropdown();
                
                $('.neighborId', this.$el).selectpicker('val', this.model.get('neighbor').id);
                
                return this;
            },

            validation : function(){
                var valid = true;
                if($('[name=postalCode]', this.$el).val() != "" && $('[name=postalCode]', this.$el).val().length < 10){
                    self.$('.msg-container').css('visibility', 'visible');
                    self.$('.msg-container').removeClass('hidden');
                    $('.msg-container', this.$el).html('کد پستی اشتباه وارد شده است.');
                    return false;
                }  
                
                if($('[name=neighborId]', this.$el).val() == ""){
                    self.$('.msg-container').css('visibility', 'visible');
                    self.$('.msg-container').removeClass('hidden');
                    $('.msg-container', this.$el).html('منطقه را وارد کنید.');
                    return false;
                } 
                
                 if(($('[name=latitude]', this.$el).val() == "") || ($('[name=longitude]', this.$el).val() == "") ){
                    self.$('.msg-container').css('visibility', 'visible');
                    self.$('.msg-container').removeClass('hidden');
                    $('.msg-container', this.$el).html('نقشه را کلیک کنید.');
                    return false;
                } 

                return true;
            },
                
            saveAgency: function() {
                var user =  this.$('form').serializeObject(),
                self = this;    
                if(!this.validation()){
                    return false;
                }   

                this.model.set({
                    'city': 'نهران',
                    'state': 'نهران'
                });

                this.model.save(user, {
                    success : function(){
                        self.$('.msg-container').removeClass('hidden');
                        self.$('.msg-container').removeClass('alert alert-danger');
                        self.$('.msg-container').addClass('alert alert-success');
                        self.$('.msg-container').html('اطلاعات با موفقیت تغییر کرده است.');
                        self.$('.msg-container').css('visibility', 'visible');
                    },
                    error : function(model, resp){
                        $('.msg-container').html(resp.responseText);
                        self.$('.msg-container').css('visibility', 'visible');
                        self.$('.msg-container').removeClass('alert alert-success');
                        self.$('.msg-container').addClass('alert alert-danger');
                        self.$('.msg-container').removeClass('hidden');
                    //self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید.');
                    }        
                })
            }
        });

        return EditAgencyView;

    });