define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'models/adv',
    'models/estate',
    'map/map',
    'base/view/adv-form',
    'models/neighbor',
    'views/plupload',
    'pwt-date',
    'pwt-datepicker',
    'vendor/bootstrap-select',
    'vendor/jquery.icheck',
    
    ], function ( bootstrap, $, _, Backbone, App, AdvModel, EstateModel, Map, BaseForm, NeighborModel, Plupload, PwtDate, PwtDatepicker, bootstrapselect , icheck) {
        var map;

        var addNewAdvView = BaseForm.extend({
            
            className : 'add-new-adv',

            events:{
                'keyup input'           : 'inputCheck',
                'change .request'       : 'changeRequestLabels'
            },

            init : function(options) {
                this.template = _.template(options.tpl);
                _.bindAll(this, 'addPhotos');
                // this.model.bind('change', this.addPhotos);
                this.bind('beforeSubmit', this.beforeSubmit);
            },

            inputCheck : function(event){
                var el = $(event.currentTarget);
                if(el.hasClass('comma')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9۰-۹]/g, '').replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",")
                    });
                }
                if(el.hasClass('digi')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9۰-۹]/g, '');
                    });
                }

                if(el.hasClass('digi-')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9-۰-۹]/g, '');
                    });
                }
            },

            addPhotos : function() {                
                var uploader = $('.photo-uploader', this.$el).pluploadQueue();

                // Files in queue upload them first
                if (uploader.files.length > 0) {
                    // When all files are uploaded submit form
                    uploader.bind('StateChanged', function() {
                        if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {

                        }
                    });

                    uploader.settings.url = '/api/adv/' + this.model.get('id') + '/photos'
                    uploader.start();
                } else {

                }

                return false;                
            },
        

            submitSuccess : function(){ 
                var url = 'adv/view/' + this.model.get('id');
                this.addPhotos();
                
                Backbone.history.navigate(url, true);
            },

            submitFailure : function(model, err) {
                if(err != undefined && err.responseText != "") {
                    self.$('.msg-container').html(err.responseText);
                }
            },

            beforeSubmit : function() {
            },
            
            render : function() {  
                this.parent('render');
                return this;
            }, 
           
            afterRender : function() {
                
                Map.initialize();
                Map.addClickListener();

                this.parent('afterRender');
                
                new Plupload({
                    id: this.model.get('id')
                });
                $('.plupload_button.plupload_start', this.$el).hide();

                var ownerEl = this.$el.find('.owner-td');
                ownerEl.hide();

                var dueMonthsEl = this.$el.find('input[name="dueMonths"]');
                dueMonthsEl.parents('td').hide();

                var dueMonth = new Date().setMonth(new Date().getMonth() + 2);
                dueMonthsEl.val(new pDate(dueMonth).format('YYYY/MM/DD'));

                $('.cnt-select .bootstrap-select').each(function(index, el){
                    $(el).width( ($(el).closest('.cnt-select')).width() - ($(el).siblings('.rf-content-select')).width() -6 );
                });


                $('.select-neibor .bootstrap-select').each(function(index, el){
                    $(el).width( ($(el).closest('.select-neibor')).width() - ($(el).siblings('.addres-1')).width() - ($(el).siblings('.addres-2')).width() - ($(el).siblings('.addres-3')).width()- ($(el).siblings('.addres-4')).width() - ($(el).siblings('.addres-5')).width() - ($(el).siblings('.addres-6')).width() - ($(el).siblings('.addres-7')).width() );
                });

                $( window ).resize(function() {
                    $('.cnt-select .bootstrap-select').each(function(index, el){
                        $(el).width( ($(el).closest('.cnt-select')).width() - ($(el).siblings('.rf-content-select')).width() -6 );
                    });
                });
                var ownerResidanceEl = this.$el.find('select[name="ownerResidance"]');
                ownerResidanceEl.parents('td').hide();
            },
            
            validation : function(){
                if(($('[name=latitude]', this.$el).val() == "") || ($('[name=longitude]', this.$el).val() == "") ){
                    self.$('.msg-container').css('visibility', 'visible');
                    self.$('.msg-container').removeClass('hidden');
                    $('.msg-container', this.$el).html('نقشه را کلیک کنید.');
                    return false;
                } 

                return true;
            },

            changeRequestLabels : function(e) {
                var type = $(e.currentTarget).selectpicker('val'),
                priceEl = this.$el.find('input[name="price"]'),
                exPriceEl = this.$el.find('input[name="exPrice"]'),
                ownerResidanceEl = this.$el.find('select[name="ownerResidance"]'),
                loanEl = this.$el.find('input[name="loan"]'),
                dueMonthsEl = this.$el.find('input[name="dueMonths"]');

                if(type != '303') {
                    priceEl.closest('div.cnt-table-content').find('.rf-content').html('قیمت'); 
                    exPriceEl.closest('div.cnt-table-content').find('.rf-content').html('متری'); 
                    ownerResidanceEl.parents('td').hide();
                    loanEl.parents('td').showV();
                } else {
                    priceEl.closest('div.cnt-table-content').find('.rf-content').html('رهن'); 
                    exPriceEl.closest('div.cnt-table-content').find('.rf-content').html('اجاره'); 
                    ownerResidanceEl.parents('td').show();
                    loanEl.parents('td').hideV();
                }

                if(type == "302") {
                    dueMonthsEl.parents('td').show();
                } else {
                    dueMonthsEl.parents('td').hide();
                }
            }
        });

        return addNewAdvView;
    });