define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',    
    'models/user',
    'base/view/form',
    'text!views/templates/editPassword.html',
], function(bootstrap, $, _, Backbone, UserModel, FormView, EditPasswordTpl) {

    var EditPasswordView = FormView.extend({
        events: {
            'click .change-pass-btn'    : 'changePassword'        
        },
                
        initialize: function(options) {
            _.bindAll(this, 'render', 'afterRender');
            var self = this;
            if (CURRENT_USER == null) {
                console.log('user not found!');
                return;
            }

            this.events = _.extend(this.events, this.formEvents);
            this.model = new UserModel({id: CURRENT_USER.id});
            this.model.fetch({
                success: function(model, result) {            
                    self.model = model;
                    self.render();
                }
            });
        },
        
        render: function() {    
            var template = _.template(EditPasswordTpl);
            this.$el.html(template(this.model.toJSON()));           
            return this;
        },
                
        changePassword : function(){
            var self = this;
            $.ajax({
                url: '/api/change-password',
                type: 'POST',
                data: {
                    'credential'            : $('#credential', this.$el).val(),
                    'newCredential'         : $('#newCredential').val(),
                    'newCredentialVerify'   : $('#newCredentialVerify').val()
                },
                success: function(resp) {
                    self.$('.msg-container').removeClass('alert alert-danger');
                    self.$('.msg-container').addClass('alert alert-success');
                    self.$('.msg-container').html('پسورد با موفقیت تغییر کرده است.');
                    $('.msg-container', this.$el).css('visibility', 'visible');
                },
                error: function() {                   
                    self.$('.msg-container').removeClass('alert alert-success');
                    self.$('.msg-container').addClass('alert alert-danger');
                    self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید.');                   
                    $('.msg-container', this.$el).css('visibility', 'visible');
                }
            });
        }
    });

    return EditPasswordView;

});
