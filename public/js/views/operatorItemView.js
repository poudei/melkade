define([
    'jquery',
    'underscore',
    'backbone',            
    'text!views/templates/operator.html',           
    ], function ($ , _, Backbone, OperatorTpl) {
        var OperatorItemView = Backbone.View.extend({
            tagName   : 'li',
            className : 'agency-item',
            events : {  },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },                  

            render    : function() {     
                this.template = _.template(OperatorTpl);          
                this.$el.html(this.template(this.model.toJSON()));                   
                
                return this;
            }
                    
            
        });

        return OperatorItemView;
    });
