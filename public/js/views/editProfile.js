define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',    
    'models/user',
    'base/view/form',
    'text!views/templates/editProfile.html',
], function(bootstrap, $, _, Backbone, UserModel, FormView, EditProfileTpl) {

    var EditProfileView = FormView.extend({
        events: {
            'click .edit-profile-btn'   : 'saveProfile'
        },
                
        initialize: function(options) {
            _.bindAll(this, 'render', 'afterRender');
            var self = this;
            if (CURRENT_USER == null) {
                console.log('user not found!');
                return;
            }

            this.events = _.extend(this.events, this.formEvents);
            this.model = new UserModel({id: CURRENT_USER.id});
            this.model.fetch({
                success: function(model, result) {            
                    self.model = model;
                    self.render();
                }
            });
        },
        
        render: function() {    
            var template = _.template(EditProfileTpl);
            this.$el.html(template(this.model.toJSON()));           
            return this;
        },

        validation : function(){
            
            if($('[name=lastname]').val() == ''){
                $('.msg-container', this.$el).html('نام خانوادگی اجباری میباشد.');
                return false;
            }

            if($('[name=cellphone]').val() == ''){
                $('.msg-container', this.$el).html('شماره تلفن همراه اجباری می باشد.');
                return false;
            }

            if(!/^09(\d{9}$)/.test($('[name=cellphone]').val())){
                $('.msg-container', this.$el).html('شماره وارد شده صحیح نمیباشد.');
                return false;
            }               
            
            if(!/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test($('[name=email]').val())){
                $('.msg-container', this.$el).html('ایمیل صحیح نمی باشد.');
                return false;
            }       

            return true;
        },
                
        saveProfile: function() {
            var user =  this.$('form').serializeObject(),
                self = this;    
            if(!this.validation()){
                self.$('.msg-container').addClass('alert alert-danger');
                $('.msg-container', this.$el).css('visibility', 'visible');
                return;
            }
            
            this.model.save(user, {
                success: function(model, resp) {     
                    self.$('.msg-container').removeClass('hidden');
                    self.$('.msg-container').removeClass('alert alert-danger');
                    self.$('.msg-container').addClass('alert alert-success');
                    $('.msg-container', self.$el).css('visibility', 'visible');
                    self.$('.msg-container').html('اطلاعات با موفقیت تغییر کرده است.');
                },
                error : function(){
                     self.$('.msg-container').removeClass('hidden');
                     self.$('.msg-container').addClass('alert alert-danger');
                     $('.msg-container', self.$el).css('visibility', 'visible');
                     self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید.');
                }        
            })
        }
    });

    return EditProfileView;

});