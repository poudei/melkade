define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',    
    'models/user',
    'base/view/form',
    'text!views/templates/owner-form.html',
], function(bootstrap, $, _, Backbone, UserModel, FormView, OwnerFormTpl) {

    var EditOwnerView = FormView.extend({
        events: { 
            'click .edit-owner-btn'   : 'saveOwner'
        },
                
        initialize: function(options) {
            _.bindAll(this, 'render', 'afterRender');
            var self = this;
            
            this.events = _.extend(this.events, this.formEvents);
            this.model = new UserModel({id: options.ownerId});
            this.model.fetch({
                success: function(model, result) {            
                    self.model = model;
                    self.render();
                }
            });
        },
        
        render: function() {    
            var template = _.template(OwnerFormTpl);
            this.$el.html(template(this.model.toJSON()));           
            return this;
        },
                
        saveOwner: function() {
            var user =  this.$('form').serializeObject(),
                self = this;    
            
            this.model.save(user, {
                success : function(){
                    self.$('.msg-container').removeClass('hidden');
                    self.$('.msg-container').removeClass('alert alert-danger');
                    self.$('.msg-container').addClass('alert alert-success');
                    self.$('.msg-container').html('اطلاعات با موفقیت تغییر کرده است.');
                },
                error : function(){
                     self.$('.msg-container').removeClass('hidden');
                     self.$('.msg-container').addClass('alert alert-danger');
                     self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید.');
                }        
            })
        }
    });

    return EditOwnerView;

});