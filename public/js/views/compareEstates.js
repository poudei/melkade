define([
	'jquery',
	'underscore',
	'backbone',
	'base/view/view',
	'views/estateView',
	'base/labels/labels'

], function ($, _, Backbone, BaseView , EstateView) {

	var CompareEstate = BaseView.extend({
		
		events: {
			
		},

		type : {
			201 : "apartment",
			202 : "villa",
			203 : "land",
			204 : "office",
			205 : "shop",
			206 : "property"
		},

		initialize: function(options) {
			this.collection = this.options.collection;
		},

		render: function() {
			var self = this;
			
			this.collection.fetch({
				success : function() {
					self.displayResutls();
				}
			});

			return this;
		},

		displayResutls : function () {
			var self = this;

			for (var i in self.collection.models)
			{
				for (var j in self.collection.models[i].toJSON()) {
					var model = self.collection.models[i].toJSON();
					if (model[j] == null || model[j] == undefined) {
						self.collection.models[i].set(j, "-")
					}
				}
			}

			require(['text!views/templates/Compare/' + self.type[this.collection.models[0].get('type')] + '-compare.html'],
	            function(CompareTpl){
	                $('.overwrite-container').html(_.template(CompareTpl, {models :self.collection.models, labels: Labels}));

	                self.afterRender();
	            });
		},

		afterRender: function(){
            
		}
   	});


	return CompareEstate;

});