define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'collections/estate',
    'base/view/view',
    'views/compareEstates',
    'views/estateItemView',
    'map/map',
    'text!views/templates/estatesListView.html' ,
    'vendor/bootstrap-select',
    'vendor/jquery.icheck',
    'vendor/jquery.nanoscroller.min',
    ], function (bootstrap, $, _, Backbone, EstateCollection,  BaseView, CompareEstates,
        EstateItemView, Map, EstateListTpl, bootstrapselect, icheck , scroll) {
        var estatesListView = BaseView.extend({

            events: {
                'click .estate-list-sort-btn' : 'sortListView',
                'click .compare-btn' : 'compare'
            },
		
            initialize: function() {
                _.bindAll(this, 'render', 'onScroll', 'appendNewItems');
                this.template = _.template(EstateListTpl);
                this.$el.html(this.template());

                $('.content', this.$el).bind('scroll', this.onScroll);
                this.collection.bind('nextPageComplete', this.appendNewItems);
			
            },
            render: function(fromSort) {
                var self = this;

                _.each(self.collection.models, function(model){
                    var estatesItemView = new EstateItemView({
                        model : model,
                        fromSort: fromSort
                    });
                    $('.estate-list', self.$el).append(estatesItemView.render().$el);     
                });

                self.afterRender();
                
                return this;
            },

            afterRender:function(){
                $('.bookmark-message', this.$el).hide();
                $('.compare-message', this.$el).hide();
                $('.dropdown-toggle', this.$el).dropdown();
            },

            sortListView : function (e) {
			    
                e.preventDefault();
                e.stopPropagation();

                $(e.currentTarget).closest('.dropdown').toggleClass('open');

                var self = this,
                type = $(e.currentTarget).data('type');
                $('.sort-filters a', self.$el).removeClass('active')
                $(e.target).addClass('active');

                var sortedModels = self.collection.sortBy(function(estate) {
                    return estate.get(type);
                });

                self.collection.reset();
                self.collection.add(sortedModels);
                $('.estate-list', self.$el).html('');

                this.render(true);

                $('.btn-head', self.$el).html('مرتب سازی:' + $('.sort-filters .active', self.$el).html());
            },

            compare : function () {
                if(this.collection.compare != undefined && this.collection.compare.length > 1) {
                    var models = "";
                    for (var i in this.collection.compare) {
                        i = parseInt(i);
                        if (this.collection.compare[i+1] != undefined) {
                            if ( (this.collection.get(this.collection.compare[i]).get('type') != this.collection.get(this.collection.compare[i+1]).get('type')) ||
                                    this.collection.get(this.collection.compare[i]).get('request') != this.collection.get(this.collection.compare[i+1]).get('request'))
                                 {
                                    alert("برای مقایسه نوع ملک و نوع معامله باید یکسان باشند.");
                                    return;
                            }
                        }

                        models += this.collection.get(this.collection.compare[i]).get('id');
                        models += ",";
                    }
                    models = models.replace(/,$/,'');
                    var url = '/estate/compare/'+ models;
                    window.open(url, '_blank');
                }
            },

            onScroll : function(e) {
                var el = $(e.currentTarget);
                if(el.scrollTop() + el.innerHeight() >= el[0].scrollHeight) {
                    if (this.collection.models.length < this.collection.filters.get('totalCount')
                        && !this.collection.filters.get('loading')) {
                        $(".estate-list li:last").append('<li class="load-more"><div class="loader"><span></span><span></span><span></span></div></li>');
                        this.collection.nextPage();
                    }
                }
            },

            appendNewItems : function() {
                
                var self = this;
                $(".estate-list li[class='load-more']", this.$el).remove();

                for (var i = this.collection.filters.get('start'); i < this.collection.length; i++) {
                    var estatesItemView = new EstateItemView({
                        model : this.collection.models[i]
                    });
                    $('.estate-list', self.$el).append(estatesItemView.render().$el);
                }
                $(".nano").nanoScroller();
            }

        });

        return estatesListView;

    });