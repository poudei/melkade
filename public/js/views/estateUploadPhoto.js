define([
	'jquery',
	'underscore',
	'backbone',
	'base/view/view',
	'text!views/templates/photo-upload.html',
	'views/plupload'

], function ($, _, Backbone, BaseView, EstateTpl, Plupload ) {

	var EstatesView = BaseView.extend({		
		events: {},

		initialize: function() {
                    _.bindAll(this, 'render', 'afterRender');			
		},

		render: function(model) {
			this.model = model;
			this.model.on('save', function(model) {
				console.log(model);
			});
			
			var self = this;
			this.template = _.template(EstateTpl);
			this.$el.html(this.template(this.model.toJSON()));

			return this;
		},

		afterRender: function(){
			new Plupload({id: this.model.get('id')});
		}
	});

	return EstatesView;

});