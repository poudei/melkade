define([
    'jquery',
    'underscore',
    'backbone',
    'base/view/view',    
    'vendor/bootstrap-select',     
    'base/labels/labels', 
    ], function ($, _, Backbone, BaseView) {

        var EstatesView = BaseView.extend({		
            events: { },

            initialize: function(options) {

                this.render();
            },

            render: function() {
                var self = this;
                
                   $('select').selectpicker({'width': '150px'});
            }
});

return EstatesView;

});