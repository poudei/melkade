define([
    'jquery',
    'underscore',
    'backbone',            
    'text!views/templates/agency.html',           
    ], function ($ , _, Backbone, AgencyTpl) {
        var AgencyItemView = Backbone.View.extend({
            tagName   : 'li',
            className : 'agency-item',
            events : {  },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },                  

            render    : function() {     
                this.template = _.template(AgencyTpl);          
                this.$el.html(this.template(this.model.toJSON()));                   
                
                return this;
            }
                    
            
        });

        return AgencyItemView;
    });
