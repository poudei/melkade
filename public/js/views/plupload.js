define([
    'jquery',
    'vendor/plupload/plupload.full.min',
    'vendor/plupload/jquery.plupload.queue/jquery.plupload.queue.min'

    ], function ($) {

    
        var Uploader = function (options) {
            $(".photo-uploader").pluploadQueue({
                // General settings
                runtimes : 'html5',
                url : 'api/estates/' + options.id + '/photos',
                max_file_size : '10mb',
                chunk_size : '1mb',
                unique_names : true,

                // Resize images on clientside if we can
                resize : {width : 320, height : 240, quality : 90},

                // Specify what files to browse for
                filters : [
                    {title : "Image files", extensions : "jpg,gif,png"}
                ]
            });

            
            if(options != undefined & options.form) {
                // Client side form validation
                $('form').submit(function(e) {
                    var uploader = $('.photo-uploader').pluploadQueue();

                    // Files in queue upload them first
                    if (uploader.files.length > 0) {
                        // When all files are uploaded submit form
                        uploader.bind('StateChanged', function() {
                            if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                                $('form')[0].submit();
                            }
                        });
                            
                        uploader.start();
                    } else {
                        alert('فایلی وجود ندارد');
                    }

                    return false;
                });
            }
        }

        return Uploader;
});