define([
    'jquery',
    'underscore',
    'backbone',    
    'base/view/listView',
    'collections/agents',    
    'views/agentItemView',    
    'text!views/templates/agents.html',    
], function($, _, Backbone, BaseListView, AgentsCollection, AgentItemView, AgenciesTpl) {
    var AgentsListView = BaseListView.extend({
        events      : {
             'keyup .agents-search-wrapper input' : 'searchAgents',
             'click .agents-search-btn' : 'searchAgents'
        },                
                
        initialize: function() {
            this.collection = new AgentsCollection([], {
                mode : 'all'
            });            
            
            this.render();            
            this.collection.on('reset', function(){
                $('.agents-list').empty();
            });   
            AgentsListView.__super__.initialize.apply(this, []);
        },       
        
        render : function(){
            var template = _.template(AgenciesTpl);
            this.$el.html(template());
        },
                
        renderItem: function(model) {                        
            var item = new AgentItemView({
                model       : model                    
            });
            $('ul', this.$el).append(item.render().$el);            
        },
                
       afterRenderItems : function(){
            if(this.collection.length == 0){
                $('.agents-list', this.$el).html('جستجو نتیجه ای در بر نداشت.')
            }
       },

       searchAgents : function(e){
            if (e.type == "keyup" && e.keyCode == 13) {
                var el = $(e.target);
                
                this.collection.filters.set({
                    'name'    : el.val(),
                    'start'   : 0
                });
                this.collection.reset();
                this.collection.fetch({
                    add     : true,
                    silent  : true,
                    update  : true
                });
            } else if (e.type == "click") {
                var el = $(".agent-input", this.$el);
                
                this.collection.filters.set({
                    'name'    : el.val(),
                    'start'   : 0
                });
                this.collection.reset();
                this.collection.fetch({
                    add     : true,
                    silent  : true,
                    update  : true
                });
            }

        }               

        
    });

    return AgentsListView;
});
