define([
    'jquery',
    'underscore',
    'backbone',            
    'text!views/templates/plan.html',           
    ], function ($ , _, Backbone, PlanTpl) {
        var PlanItemView = Backbone.View.extend({
            tagName   : 'tr',
            className : 'plan-item',
            events : {  },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },                  

            render    : function() {     
                this.template = _.template(PlanTpl);          
                this.$el.html(this.template(this.model.toJSON()));                   
                
                return this;
            }
                    
            
        });

        return PlanItemView;
    });
