define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'models/estate',
    'map/map',
    'base/view/form',
    'models/neighbor',
    'views/plupload',
    'pwt-date',
    'pwt-datepicker',
    'vendor/bootstrap-select',
    'vendor/jquery.icheck',
    
    ], function ( bootstrap, $, _, Backbone, App, EstateModel, Map, BaseForm, NeighborModel, Plupload, PwtDate, PwtDatepicker, bootstrapselect , icheck) {
        var map;

        var addNewEstateView = BaseForm.extend({
            
            className : 'add-new-estate',

            events:{
                'keyup input'           : 'inputCheck',
                'change .request'       : 'changeRequestLabels',
                'change .advertisedBy'  : 'changeAdvertisedByLabels'
            },

            init : function(options) {
                this.model    = options.model;
                this.template = _.template(options.tpl);
                _.bindAll(this, 'addPhotos');
                // this.model.bind('change', this.addPhotos);
                this.bind('beforeSubmit', this.beforeSubmit);
            },

            inputCheck : function(event){
                var el = $(event.currentTarget);
                if(el.hasClass('comma')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9۰-۹]/g, '').replace(/\B(?=([0-9۰-۹]{3})+(?![0-9۰-۹]))/g, ",")
                    });
                }
                if(el.hasClass('digi')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9۰-۹]/g, '');
                    });
                }

                if(el.hasClass('digi-')){
                    el.val(function(index, value) {
                        return value.replace(/[^0-9-۰-۹]/g, '');
                    });
                }
            },

            addPhotos : function() {                
                var uploader = $('.photo-uploader', this.$el).pluploadQueue();

                // Files in queue upload them first
                if (uploader.files.length > 0) {
                    // When all files are uploaded submit form
                    uploader.bind('StateChanged', function() {
                        if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {

                        }
                    });

                    uploader.settings.url = '/api/estates/' + this.model.get('estate').id + '/photos'
                    uploader.start();
                } else {

                }

                return false;                
            },
        

            submitSuccess : function(){                
                var url = 'estate/view/' + this.model.get('estate').id;
                this.addPhotos();
                
                url = this.model.get('newOwner') ? (url + '/newOwner') : url;
                url = this.model.get('newAgent') ? (url + '/newAgent') : url;
                url = this.model.get('newAgency') ? (url + '/newAgency') : url;
                
                Backbone.history.navigate(url, true);
            },

            submitFailure : function(model, err) {
               
            },

            beforeSubmit : function() {
            },
            
            render : function() {  
                var self = this;
                $('select', this.$el).selectpicker();
                
                this.parent("render");
                
                
                return this;
            },
           
            afterRender : function() {
                
                Map.initialize();
                Map.addClickListener();

                this.parent('afterRender');
                
                new Plupload({
                    id: this.model.get('id')
                });
                $('.plupload_button.plupload_start', this.$el).hide();

                var ownerEl = this.$el.find('.owner-td');
                ownerEl.hide();

                var dueMonthsEl = this.$el.find('input[name="dueMonths"]');
                dueMonthsEl.parents('td').hide();

                var dueMonth = new Date().setMonth(new Date().getMonth() + 2);
                dueMonthsEl.val(new pDate(dueMonth).format('YYYY/MM/DD'));

                $('.cnt-select .bootstrap-select').each(function(index, el){
                    $(el).width( ($(el).closest('.cnt-select')).width() - ($(el).siblings('.rf-content-select')).width() -6 );
                });


                $('.select-neibor .bootstrap-select').each(function(index, el){
                    $(el).width( ($(el).closest('.select-neibor')).width() - ($(el).siblings('.addres-1')).width() - ($(el).siblings('.addres-2')).width() - ($(el).siblings('.addres-3')).width()- ($(el).siblings('.addres-4')).width() - ($(el).siblings('.addres-5')).width() - ($(el).siblings('.addres-6')).width() - ($(el).siblings('.addres-7')).width() );
                });

                $( window ).resize(function() {
                    $('.cnt-select .bootstrap-select').each(function(index, el){
                        $(el).width( ($(el).closest('.cnt-select')).width() - ($(el).siblings('.rf-content-select')).width() -6 );
                    });
                });
                var ownerResidanceEl = this.$el.find('select[name="ownerResidance"]');
                ownerResidanceEl.parents('td').hide();
                
                $('.carousel', this.$el).carousel();
                
                
                
                if (this.model.get("advId")) {
                    
                    if (this.model.get('price') && this.model.get('price').split('.00')) {
                        this.model.set('price', this.model.get('price').split('.00')[0]);
                    }
                    if (this.model.get('exPrice') && this.model.get('exPrice').split('.00')) {
                        this.model.set('exPrice', this.model.get('exPrice').split('.00')[0]);
                    }
                    if (this.model.get('yearBuilt') && this.model.get('yearBuilt').toString().split('13')) {
                        this.model.set('yearBuilt',this.model.get('yearBuilt').toString().split('13')[1]);
                    }
                    if (this.model.get('area') && this.model.get('area').split('.00')) {
                        this.model.set('area', this.model.get('area').split('.00')[0]);
                    }
                    if (this.model.get('metrage') && this.model.get('metrage').split('.00')) {
                        this.model.set('metrage', this.model.get('metrage').split('.00')[0]);
                    }
                    
                    $('#advId').val(this.model.get("advId"));
                    
                    this.populate();
                    
                    $('form [name="advertisedBy"]', self.$el).attr('disabled', true);
                    $('form [name="ownerLastname"]', self.$el).attr('readonly', true);
                    $('form [name="ownerCellphone"]', self.$el).attr('readonly', true);
                    $('form [name="agentLastname"]', self.$el).attr('readonly', true);
                    $('form [name="agentCellphone"]', self.$el).attr('readonly', true);
                    
                    Map.addPoint(
                        this.model.get('longitude'), 
                        this.model.get('latitude'), 
                        this.model.toJSON()
                    );
                }
            },

            changeRequestLabels : function(e) {
                var type = $(e.currentTarget).selectpicker('val'),
                priceEl = this.$el.find('input[name="price"]'),
                exPriceEl = this.$el.find('input[name="exPrice"]'),
                ownerResidanceEl = this.$el.find('select[name="ownerResidance"]'),
                loanEl = this.$el.find('input[name="loan"]'),
                dueMonthsEl = this.$el.find('input[name="dueMonths"]');

                if(type != '303') {
                    priceEl.closest('div.cnt-table-content').find('.rf-content').html('قیمت'); 
                    exPriceEl.closest('div.cnt-table-content').find('.rf-content').html('متری'); 
                    ownerResidanceEl.parents('td').hide();
                    loanEl.parents('td').showV();
                } else {
                    priceEl.closest('div.cnt-table-content').find('.rf-content').html('رهن'); 
                    exPriceEl.closest('div.cnt-table-content').find('.rf-content').html('اجاره'); 
                    ownerResidanceEl.parents('td').show();
                    loanEl.parents('td').hideV();
                }

                if(type == "302") {
                    dueMonthsEl.parents('td').show();
                } else {
                    dueMonthsEl.parents('td').hide();
                }
            },

            changeAdvertisedByLabels : function(e) {
                var advertisedBy = $(e.currentTarget).selectpicker('val'),
                ownerEl = this.$el.find('.owner-td'),
                agencyEl = this.$el.find('.agency-td');

                if(advertisedBy == '101') {
                    agencyEl.hide();
                    ownerEl.show();
                } else {
                    agencyEl.show();
                    ownerEl.hide();
                }
            },
            
            commaSeperation : function() {
                $.each($('.estate span.comma', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                });

                $.each($('.estate span.number-inp', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/^0$/, "-"));
                });
            },

            removeExtraLabels : function () {
                $.each($('.estate span.number-inp', this.$el), function(item, value) {
                    if (/^-$/.test($(value).html())) {
                        $(value).parents('td').find('.lf-content').html('');
                    }
                });
            }
        });

        return addNewEstateView;
    });