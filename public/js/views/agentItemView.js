define([
    'jquery',
    'underscore',
    'backbone',            
    'text!views/templates/agent.html',           
    ], function ($ , _, Backbone, AgentTpl) {
        var AgentItemView = Backbone.View.extend({
            tagName   : 'li',
            className : 'agent-item',
            events : {  },

            initialize : function() {
                _.bindAll(this, 'render');                
                this.model.on('change', this.render);                    
            },                  

            render    : function() {     
                this.template = _.template(AgentTpl);          
                this.$el.html(this.template(this.model.toJSON()));                   
                
                return this;
            }
                    
            
        });

        return AgentItemView;
    });
