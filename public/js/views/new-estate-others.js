define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/view', 
    'text!views/templates/new-estate-others.html' 
    ], function (bootstrap ,$, _, Backbone, App, BaseView, newEstateOthersTpl) {

        var newEstateOthersView = BaseView.extend({
            events:{
            },

            initialize : function(opt) {
                opt = opt || {};
                this.template = _.template(newEstateOthersTpl);
            },
                        

            render : function() {                  
                this.$el.html(this.template());                
              
                return this;
            }
        });

        return newEstateOthersView;
    });
