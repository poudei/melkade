define([
    'bootstrap',
    'jquery',
    'underscore',
    'backbone',
    'app',
    'base/view/view', 
    'collections/estate', 
    'models/neighbor',
    'views/estatesView',
    'text!views/templates/home.html',
    'jquery-ui',
    'bootstrap',
    'map/map',
    'vendor/bootstrap-select',
    'vendor/jquery.icheck',
    'vendor/jquery.cookie'
    ], function (bootstrap ,$, _, Backbone, App, BaseView, EstateCollection, NeighborModel, EstatesView, HomeTmp) {

        var homeView = BaseView.extend({
            className : 'home-page',
            events:{
                'click .home-neighbor'         : 'search'
            },

            initialize : function(opt) {
                opt = opt || {};
                this.template = _.template(HomeTmp);
            },
                        

            render : function() {                  
                this.$el.html(this.template());                
              
                return this;
            }, 

            afterRender : function(){
                var self = this;
            },

            search: function(event){
                $(".loading").fadeIn();
                var self = this,
                geo = {},
                filters = new Backbone.Model(),
                neighborId = $(event.target).data('neighborid');
                Backbone.history.navigate('/estates/neighborId=' + neighborId,  {trigger: true});

                /* var neighborModel = new NeighborModel({id:neighborId});
                 neighborModel.fetch({
                     success : function(model) {

                        filters.set('neighborId', model.get('id'));
                        geo.neighborTitle = model.get('title');
                        geo.neighborCenter =  model.get('centerText');
                        geo.neighborBoundary = model.get('boundaryText');

                        var estateCollection = new EstateCollection();
                        estateCollection.filters.set('neighborId', model.get('id'))

                        var estatesView = new EstatesView({
                            collection : estateCollection,
                            filters : filters,
                            geo : geo
                        });
                        $('body').removeClass('home-background');
                        $('.overwrite-container').html(estatesView.render().$el);
                        estatesView.afterRender();
                        
                        $(".loading").fadeOut();
                    }
                 }); */
            } 
        });

        return homeView;
    });
