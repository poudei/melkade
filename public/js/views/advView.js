define([
    'jquery',
    'underscore',
    'backbone',
    'models/neighbor',
    'base/view/view',     
    'vendor/bootstrap-select',     
    'base/labels/labels', 
    ], function ($, _, Backbone, NeighborModel, BaseView) {
        
        var AdvsView = BaseView.extend({		
            events: { },

            initialize: function(options, param1, params2) {
                _.bindAll(this, 'render', 'afterRender');
                this.model = options.model;
                this.template = _.template(options.tpl);
            },

            render: function() {
                
                var self = this;
                
                if (this.model.get('photos') == undefined) {
                    this.model.set('photos', undefined);
                }
                
                var creationDate = {date: this.model.get('creationDate')};
                this.model.set('creationDate', creationDate);
                
                this.$el.html(this.template(this.model.toJSON()));
                
                $.each($('.show-field', this.$el), function(index, el){                    
                    var id      = $(el).attr('id'),
                    val         = '',
                    constant    = self.model.get(id);
                    
                    if($(el).data('multiselect')){
                        for (var i = 0; constant && i < constant.length - 1; i++) {
                            val = val + '<span>' + Labels[id][constant[i]] + '</span>' + '، ';
                        }
                        if(Labels[id] && constant)
                            val = val + Labels[id][constant[constant.length - 1]];
                    }else{
                        if($(el).data("numbermap")){ 
                            val = _.numberVal(constant);
                        }else{
                            if(Labels[id]){
                                val = Labels[id][constant];
                                if(constant == undefined || constant == "undefined"){
                                    val = Labels[id][0];
                                }                            
                            }                         
                        }
                    }
            
                    if(val == '' && constant && constant.date){
                        val = _.dateFormat(constant.date);
                    }

                    if(constant && constant != null && val != undefined && val != "undefined" && val != ''){
                        if(typeof(constant) == 'object' && constant.length < 1) {
                            $('#' + id, self.$el).html("-");
                        } else {
                            $('#' + id, self.$el).html(val);
                        }

                    }else{
                        if(constant && constant != null && val != "undefined"){
                            if(typeof(constant) == 'object' && constant.length == 0) {
                                $('#' + id, self.$el).html("-");   
                            } else {
                                $('#' + id, self.$el).html(constant);
                            }
                        } else {
                            if(constant != null && typeof(constant) == 'object' && constant.length < 1) {
                                $('#' + id, self.$el).html("-");
                            }
                        }
                    }
                });

                $.each($('.one-line', this.$el), function(item, value){
                    if ($(value).find('span').length < 2 ) {
                        $(value).remove();
                    }   
                });

                $('.views', this.$el).html($('.views', this.$el).html().replace(/\.00\b/g,''));

                this.commaSeperation();
                this.removeExtraLabels();
                // this.convertToPersian();

                return this;
            },

            commaSeperation : function() {
                $.each($('.views span.comma', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                });

                $.each($('.views span.number-inp', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/^0$/, "-"));
                });
            },

            removeExtraLabels : function () {
                $.each($('.views span.number-inp', this.$el), function(item, value) {
                    if (/^-$/.test($(value).html())) {
                        $(value).parents('td').find('.lf-content').html('');
                    }
                });
            },

            convertToPersian : function() {
                $.each($('.views span', this.$el), function(item, value) {
                    $(value).html($(value).html().replace(/0/g,'۰'));
                    $(value).html($(value).html().replace(/1/g,'۱'));
                    $(value).html($(value).html().replace(/2/g,'۲'));
                    $(value).html($(value).html().replace(/3/g,'۳'));
                    $(value).html($(value).html().replace(/4/g,'۴'));
                    $(value).html($(value).html().replace(/5/g,'۵'));
                    $(value).html($(value).html().replace(/6/g,'۶'));
                    $(value).html($(value).html().replace(/7/g,'۷'));
                    $(value).html($(value).html().replace(/8/g,'۸'));
                    $(value).html($(value).html().replace(/9/g,'۹'));

                });
            },

            afterRender: function(){            
                var self = this;

                $('.carousel', this.$el).carousel();
            }
        });
        
        return AdvsView;
    });