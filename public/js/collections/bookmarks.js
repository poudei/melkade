define([
    'underscore',
    'backbone',
    'models/bookmark',
    'base/collection/collection',
], function(_, Backbone, BookmarkModel, BaseCollection) {
    'use strict';

    var BookmarksCollection = BaseCollection.extend({
        
        model: BookmarkModel,
        
        url: function() {
            return 'api/bookmarks';
        },
        
        parse: function(resp) {
            var newItems = [];
            for (var i in resp.items) {
                resp.items[i].estate.fromBookmark = true;
                resp.items[i].estate.bookmarkId = resp.items[i].id;
                newItems.push(resp.items[i].estate);
            }
            resp.items = [];
            resp.items = newItems;

            return resp.items;
        }

    });

    return BookmarksCollection;
});