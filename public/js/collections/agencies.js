define([
    'underscore',
    'backbone',
    'models/agency',
    'base/collection/collection',
], function(_, Backbone, AgencyModel, BaseCollection) {
    'use strict';

    var AgenciesCollection = BaseCollection.extend({
        model: AgencyModel,
        url: function() {
            return '/api/agencies';
        },
                
        parse: function(resp) {
            this.filters.count = resp.count;
            return resp.items;
        }

    });

    return AgenciesCollection;
});