define([
	'underscore',
	'backbone',
	'models/adv',
    'base/collection/collection',
], function (_, Backbone, AdvModel, BaseCollection) {
	'use strict';

	var AdvCollection = BaseCollection.extend({

		initialize: function() {
			this.parent('initialize');
            this.filters.set('limit', 100);
        },

		model : AdvModel,

		url: function() {
			return ('/api/advs/' +
			(this.filters.get('ids')  ? this.filters.get('ids')  : '') + 
			'?limit=' + this.filters.get('limit') + '&start=' + this.filters.get('start') + 
			(this.filters.get('userId')  ? '&filter[]=userId|eq|'   + this.filters.get('userId')  : '') + 
			(this.filters.get('neighborId')  ? '&filter[]=neighborId|eq|'   + this.filters.get('neighborId')  : '') + 
			(this.filters.get('request') ? '&filter[]=request|eq|'  + this.filters.get('request') : '') +  
			(this.filters.get('type')  ? '&filter[]=type|in|'   + this.filters.get('type')  : '')).replace(/ /g,'') ; 
		},
                
                fetch: function(options) {
                    options || (options = {});

                    // this extention done for new updates of backbone

                    $.extend(options, {
                        update: true,
                        remove: false
                    });
                    
                    var collection = this;
                    var success = options.success;
                    options.success = function(collection, resp, xhr) {
                        collection.trigger('fetchSuccess', resp, collection);
                        if (success)
                            success(collection, resp);

                    };
                    return Backbone.Collection.prototype.fetch.call(this, options);
                }

	});

	return AdvCollection;
});