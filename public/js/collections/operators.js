define([
    'underscore',
    'backbone',
    'models/operator',
    'base/collection/collection',
], function(_, Backbone, OperatorModel, BaseCollection) {
    'use strict';

    var OperatorsCollection = BaseCollection.extend({
        model: OperatorModel,
        url: function() {
            return '/api/operators-statistic';
        },
                
        parse: function(resp) {
            this.filters.count = resp.operatorsCount;
            return resp.items;
        }

    });

    return OperatorsCollection;
});