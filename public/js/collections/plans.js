define([
    'underscore',
    'backbone',
    'models/plan',
    'base/collection/collection',
], function(_, Backbone, PlanModel, BaseCollection) {
    'use strict';

    var PlansCollection = BaseCollection.extend({
        model: PlanModel,
        url: function() {
            return '/api/plans';
        },
                
        parse: function(resp) {
            this.filters.count = resp.count;
            return resp.items;
        }

    });

    return PlansCollection;
});