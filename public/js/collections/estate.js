define([
	'underscore',
	'backbone',
	'models/estate',
    'base/collection/collection',
], function (_, Backbone, EstateModel, BaseCollection) {
	'use strict';

	var EstateCollection = BaseCollection.extend({

		initialize: function() {
			this.parent('initialize');
            this.filters.set('limit', 100);
        },

		model : EstateModel,

		url: function() {
			return ('/api/estates/' +
			(this.filters.get('ids')  ? this.filters.get('ids')  : '') + 
			'?limit=' + this.filters.get('limit') + '&start=' + this.filters.get('start') + 
			(this.filters.get('ownerUserId')  ? '&filter[]=ownerUserId|eq|'   + this.filters.get('ownerUserId')  : '') + 
			(this.filters.get('agencyId')  ? '&filter[]=agencyId|eq|'   + this.filters.get('agencyId')  : '') + 
			(this.filters.get('agentUserId')  ? '&filter[]=agentUserId|eq|'   + this.filters.get('agentUserId')  : '') + 
			(this.filters.get('bookmarkId')  ? '&filter[]=bookmarkId|gt|'   + this.filters.get('bookmarkId')  : '') + 
			(this.filters.get('request') ? '&filter[]=request|eq|'  + this.filters.get('request') : '') +  
			(this.filters.get('sportFacilities') ? '&filter[]=sportFacilities|ora|'  + this.filters.get('sportFacilities') : '') +  
			(this.filters.get('securities') ? '&filter[]=securities|ora|'  + this.filters.get('securities') : '') +  
			(this.filters.get('equipments') ? '&filter[]=equipments|ora|'  + this.filters.get('equipments') : '') +  
			(this.filters.get('facilities') ? '&filter[]=facilities|ora|'  + this.filters.get('facilities') : '') +  
			(this.filters.get('priceLowerBound') ? ('&filter[]=price|between|'  + this.filters.get('priceLowerBound') +','+ this.filters.get('priceUpperBound')): '') +  
			(this.filters.get('metrageLowerBound') ? ('&filter[]=metrage|between|'  + this.filters.get('metrageLowerBound') +','+ this.filters.get('metrageUpperBound')): '') +  
			(this.filters.get('bedsLowerBound') ? ('&filter[]=beds|between|'  + this.filters.get('bedsLowerBound') +','+ this.filters.get('bedsUpperBound')): '') +  
			(this.filters.get('exPriceLowerBound') ? ('&filter[]=exPrice|between|'  + this.filters.get('exPriceLowerBound') +','+ this.filters.get('exPriceUpperBound')): '') +  
			(this.filters.get('type')  ? '&filter[]=type|in|'   + this.filters.get('type')  : '') +
			(this.filters.get('neighborId')  ? '&filter[]=neighborId|in|'   + this.filters.get('neighborId')  : '')).replace(/ /g,'');
		},
                
                fetch: function(options) {
                    options || (options = {});

                    // this extention done for new updates of backbone

                    $.extend(options, {
                        update: true,
                        remove: false
                    });
                    
                    var collection = this;
                    var success = options.success;
                    options.success = function(collection, resp, xhr) {
                        collection.trigger('fetchSuccess', resp, collection);
                        if (success)
                            success(collection, resp);

                    };
                    return Backbone.Collection.prototype.fetch.call(this, options);
                }

	});

	return EstateCollection;
});