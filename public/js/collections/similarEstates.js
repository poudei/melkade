define([
	'underscore',
	'backbone',
	'models/estate',
    'base/collection/collection',
], function (_, Backbone, EstateModel, BaseCollection) {
	'use strict';

	var SimilarEstatesCollection = BaseCollection.extend({

		model : EstateModel,

		url: function() {

			this.filters.set('limit' , 5);
            this.filters.set('start' , 0);

			return ('/api/estates/' + this.filters.get('estateId') + '/similars?limit=' + this.filters.get('limit') + '&start=' + this.filters.get('start') ) ; 

		}

	});

	return SimilarEstatesCollection;
});