define([
    'underscore',
    'backbone',
    'models/agency',
    'base/collection/collection',
], function(_, Backbone, AgencyModel, BaseCollection) {
    'use strict';

    var AgentsCollection = BaseCollection.extend({
        
        model: AgencyModel,

        initialize: function() {
            this.parent('initialize');
            this.filters.set('type', 2502);
        },

        url: function() {
            return '/api/users';
        },
        
        parse: function(resp) {            
            this.filters.count = resp.count;
            return resp.items;
        }

    });

    return AgentsCollection;
});