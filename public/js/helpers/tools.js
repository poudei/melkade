define(['app',
    'jquery', 
    'underscore', 
    'backbone',
//    'text!templates/general/signin.html',
    'bootbox',
], function(App, $, _, Backbone , bootbox){
    var tools = {};
    tools.showLogin = function() {       
        bootbox.dialog({
            title: 'Please pick a social network',
            message: ''//_.template(signinTemplate)()
        });
       
    };

    tools.checkLogin = function() {
        if (!App.User.get('id'))
        {
            tools.showLogin();
            return false;
        }
        return true;
    };

    tools.generateSlug = function(str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();

            // remove accents, swap ñ for n, etc
            var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
            var to   = "aaaaaeeeeeiiiiooooouuuunc------";
            for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }

            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

            return str;
    };

    return tools;
});