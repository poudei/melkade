require.config({
    baseURL : "/js",
    waitSeconds : 60,
    paths : {
        'jquery'                    : 'vendor/jquery/jquery',
        'jquery-ui'                 : 'vendor/jquery-ui/ui/jquery-ui',
        'jquery-values'             : 'vendor/jquery.values/jquery.values',
        'underscore'                : 'vendor/underscore-amd/underscore',
        'backbone'                  : 'vendor/backbone-amd/backbone',
        'text'                      : 'vendor/requirejs-text/text',   
        'backbone.routefilter'      : 'vendor/backbone.routefilter/dist/backbone.routefilter.min',        
        'bootstrap'                 : 'vendor/bootstrap/js/bootstrap',
        'pwt-date'                  : 'vendor/pwt-datepicker/pwt-date',
        'pwt-datepicker'            : 'vendor/pwt-datepicker/pwt-datepicker',
        'bootstrap-select'          : 'vendor/bootstrap-select'
    },
    locale: 'en-us',
    shim: {
        'jquery-ui': {
            deps : [ 'jquery' ]
        },

        'bootstrap' : {
            deps : ['jquery']
        },

        'backbone.routefilter' : {
            deps : ['backbone', 'underscore']
        },

        'pwt-date' : {
            deps :['jquery', 'jquery-ui']
        },
        
        'pwt-datepicker' : {
            deps :['pwt-date']
        }
       
    }
});

require(['bootstrap', 'jquery', 'underscore', 'backbone', 'app', 'backbone.routefilter', 'helpers/base'], function(bootstrap, $, _, Backbone, App, Router){

    App.initialize();
    App.MixinFn();
    App.events;
    Backbone.history.start({pushState: true, root: '/'});      
} );