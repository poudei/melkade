define([
    'bootstrap',
    'jquery',
    'backbone',
    'underscore',
    'router',
    ], function(bootstrap, $, Backbone, _, Router){
        
        var events = _.extend({}, Backbone.Events);

        var initialize = function() {
            var router = new Router();
            $('#modal-give').on('click', function(e){
                if($(e.target).closest('.modal-dialog').length == 0){
                    $('#modal-give').modal('hide');
                }
            });
           
            $(".mk-docs-nav .navbar-toggle").click(function () {
                var h = $(this).attr('data-target');
                $(h).toggleClass('in');
                $(h).css('height' , 'auto');
            });

             $('.plan-buy select').selectpicker();
        };



        var MixinFn = function(){
            _.mixin({
                numberVal : function(num){
                    if(num == 0){
                        return('ندارد');
                    }else{
                        if(num == 9){
                            return('۹ عدد یا بیشتر');
                        }else{
                            return(num + 'عدد');
                        }
                    }
                },
                
                dateFormat : function(date){
                    var d = date.split(' ')[0];
                    if(d){
                        d = date.split(' ')[0].replace(/-/g,'\/');
                    }
                    
                    return d;
                }
            });
        };
    
        return {
            initialize      : initialize,
            MixinFn         : MixinFn,
            events          : events
        };

    });