define([
    'underscore',
    'backbone'
], function(_, Backbone) {
    'use strict';

    var BaseCollection = Backbone.Collection.extend({
        initialize: function() {
            this.filters = new Backbone.Model();
            this.filters.set('limit', 20);
            this.filters.set('start', 0);
        },
        parse: function(result) {
            this.filters.set('totalPages', Math.ceil(result.count / result.limit));
            this.filters.set('totalCount', result.count);

            return result.items || result;
        },
                
        fetch: function(options) {
            options || (options = {});

            // this extention done for new updates of backbone

            $.extend(options, {
                update: true,
                remove: false
            });

            if (this.filters) {
                var filters = this.filters.toJSON();
                if (options.data) {
                    for (var i in filters) {
                        options.data[i] = filters[i];
                    }
                }
                else {
                    options.data = filters;
                }
            }
            else {
                //            console.log('filters is undefined');
            }
            var collection = this;
            var success = options.success;
            options.success = function(collection, resp, xhr) {
                collection.trigger('fetchSuccess', resp, collection);
                if (success)
                    success(collection, resp);

            }
            return Backbone.Collection.prototype.fetch.call(this, options);
        },
                
        nextPage: function() {
            var self = this;
            if (this.models.length < this.filters.get('totalCount')) {
                this.filters.set('start', this.filters.get('start') + this.filters.get('limit'));
                this.filters.set('loading', true);
                this.fetch({
                    remove: false,
                    success: function() {
                        self.trigger('nextPageComplete');
                        self.filters.set('loading', false);
                    }
                });
            }
        },
        
        increase: function( count ){
                this.filters.set('start', this.filters.get('start') + (count ? count : this.filters.get('limit')));                 
                return this;
        }
    });

    return BaseCollection;
});