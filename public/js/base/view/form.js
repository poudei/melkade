define([
    'jquery',
    'underscore',
    'backbone',
    'base/view/view',
    'pwt-date',
    'pwt-datepicker',
    'map/map',
    'models/neighbor'

    ], function ($, _, Backbone, BaseView, PwtDate, PwtDatepicker, Map, NeighborModel) {
        'use strict';

        var BaseForm = BaseView.extend({
            events: {},
            formEvents: {
                'click .btn-submit' : 'submit',
                'keydown .select-on-press' : 'selectOnPrss'
            },
            
            initialize: function(options){
                this.events   = _.extend(this.events, this.formEvents);
                this.options  = options;
                this.init(options);
            },

            render: function(){
                var self = this;
                var template = this.template || _.template(this.options.tpl);
                self.$el.html(template);            
                return this;            
            },

            afterRender : function() {
                var self = this;
                $(".date-picker", this.$el).pDatepicker({
                    format : 'YYYY/MM/DD',
                    autoClose : true
                });

                $('input', this.$el).iCheck({
                    checkboxClass: 'icheckbox_minimal-purple',
                    increaseArea: '20%' 
                });           
                
                $('select', this.$el).selectpicker();

                this.$('input').on('blur' , function(){
                    var el = $(this);
                    var elVal = $(this).val();
                    if(el.hasClass('comma')){
                        var elVal = elVal.replace(/,/g , "");
                    }                  
                });

                $( ".neighborText" , this.$el).autocomplete({
                    messages: {
                        noResults: '',
                        results: function() {}
                    },
                    source: function(request, response) {
                        
                        $.ajax({
                            url : "/api/neighbors?limit=20&filter[]=title|like|" + request.term,
                            dataType : "json",
                            success : function(data) {
                                response($.map( data.items, function( item ) {
                                    return {
                                        label: item.title ,
                                        value: item.title,
                                        id : item.id
                                    }
                                }));
                            }
                        });
                    },
                    select: function( e, ui){
                        e.preventDefault();
                        e.stopPropagation();
                        
                        $( ".neighborId", self.$el ).val(ui.item.id);
                        if (ui.item.id != undefined){
                            var neighborModel = new NeighborModel({
                                id: ui.item.id
                            });
                            neighborModel.fetch({
                                success: function(model) {
                                    map.self.drawNeighbor(model.get('boundaryText'));
                                    map.self.setCenter(model.get('centerText'));
                                }
                            });
                        }
                    }
                });

            },

            addValidationErrors : function(type, formInput, msg){          
                if(type == 'tooltip'){
                    formInput.tooltip('destroy');
                    formInput.tooltip({
                        title   : msg,
                        trigger : 'manual'
                    });        
                }
    
                formInput.addClass("err-field");
            },

            removeValidationErrors : function(){
                $('form .err-field', this.$el).removeClass('err-field');
                $.each(this.$el.find('form input'), function(index, value) {                  
                    $(value).tooltip('destroy');
                });
            },

            checkValidation : function(serializedForm){
                var errorAll = false;
                this.removeValidationErrors();
    
                for (var item in serializedForm) {
                    if(typeof(serializedForm[item]) != 'object'){
                        serializedForm[item] = serializedForm[item].replace(/,/g , "");
                    }
                }
    
                if(this.model.validation){
                    for( var fieldName in this.model.validation){          
            
                        // If there is this field in form
                        if($('[name="' + fieldName + '"]').length > 0){
                            var result = this.checkElementValidation(fieldName , serializedForm[fieldName.replace('[]','')]);                    
                            errorAll = (!errorAll) ? result : errorAll;
                        }                        
                    }
                }

                if ( $('[name="price"]', this.$el).val() == (undefined || "")  && $('[name="exPrice"]', this.$el).val() == (undefined || "") ) {
                    errorAll = true;
                    this.addValidationErrors('tooltip', $('[name="price"]', this.$el), 'قیمت یا قیمت متری باید پر شوند');
                    this.addValidationErrors('tooltip', $('[name="exPrice"]', this.$el), 'قیمت یا قیمت متری باید پر شوند');
                }

                $.each(this.$el.find('form input'), function(index, value) {                  
                    $(value).tooltip('show');
                });

                if(errorAll){
                    self.$('.msg-container').removeClass('hidden');
                    self.$('.msg-container').addClass('alert alert-danger');
                    self.$('.msg-container').html('فیلد های اجباری را پر کنید.');
                }

                return errorAll;
            },    

            checkElementValidation : function(fieldName , fieldVal){
                var error = false;
                var fieldModelValidation = this.model.validation[fieldName];
                if(fieldModelValidation){
                    if(fieldModelValidation.type){
                        var typeName = $('input[name="type"]').val(), 
                        moldelType  = fieldModelValidation.type[typeName];
                        if(moldelType){
                            var result = this.checking(moldelType , fieldName, fieldVal );
                            if(error == false)
                                error = result;
                        }
                    }
                    result = this.checking(fieldModelValidation , fieldName, fieldVal);
                    if(error == false)
                        error = result;
                }
                return error;
            },

            checking : function (modelIndex , fieldName, fieldVal) {
                var error = false
                var formInput = $('input[name="' + fieldName + '"]') ;
                var self = this;
                if (formInput){
                    if(modelIndex.range){
                        if(fieldVal){
                            if((parseInt(fieldVal) < modelIndex.range[0]) 
                                || parseInt(fieldVal) > modelIndex.range[1] || isNaN(parseInt(fieldVal))){
                                error = true;
                                this.addValidationErrors('tooltip', formInput, 'مقدار مورد نظر باید بین ' + modelIndex.range[0] + ' و ' + modelIndex.range[1] + ' باشد.');                                                       
                            }
                        }                
                    }

                    if(modelIndex.required){
                        if(modelIndex.required == true && fieldVal == ""){
                            error = true;                            
                            this.addValidationErrors('required', formInput);
                        }
                    }

                    if(modelIndex.length){
                        if(fieldVal && fieldVal.length != modelIndex.length){
                            error = true;                           
                            this.addValidationErrors('tooltip', formInput, 'تعداد کاراکترها باید ' + modelIndex.length + ' باشد.');                                                       
                        }
                    }

                    if(modelIndex.cellphone){ 
                        if(fieldVal != "" && !/^09(\d{9}$)/.test(fieldVal)){
                            error = true;                           
                            this.addValidationErrors('tooltip', formInput, 'شماره وارد شده صحیح نمی باشد.');                                                       
                        }
                    }

                    if(modelIndex.date) {
                        if (!(/^\d{4}\/\d{2}\/\d{2}$/).test(fieldVal) && !(/^[۰-۹\\d]{4}\/[۰-۹\\d]{2}\/[۰-۹\\d]{2}$/).test(fieldVal)) {
                            error = true;
                            this.addValidationErrors('tooltip', formInput, 'مقدار نامعتبر است');                                                                                                          
                        }
                    }

                    if(modelIndex.agency) {
                        if ($('select[name="advertisedBy"]', this.$el).val() == 102) {
                            $.each($('.agency-td input', this.$el), function(item, value) {
                                if ($(value).val() == "") {
                                    error = true;                            
                                    self.addValidationErrors('required', $(value));
                                }
                            });
                        }
                    }

                    if(modelIndex.owner) {
                        if ($('select[name="advertisedBy"]', this.$el).val() == 101) {
                            $.each($('.owner-td input', this.$el), function(item, value) {
                                if ($(value).val() == "") {
                                    error = true;                            
                                    self.addValidationErrors('required', $(value));
                                }
                            });
                        }
                    }

                    if ( $('.neighborId', this.$el).val() == undefined || $('.neighborId', this.$el).val() == "") {
                        error = true;
                        self.addValidationErrors('required', $('.neighborText', this.$el));
                    }

                    if ( $('#latitude-input', this.$el).val() == undefined || $('#latitude-input', this.$el).val() == "") {
                        error = true;
                        self.addValidationErrors('required', $('.neighborText', this.$el));
                    }
                }
            
                return error;
            },
        
            submitSuccess : function(){},
            submitFailure : function(){},
        
            submit: function(e) {            
                var self   = this;
                e.preventDefault();
                e.stopPropagation();

                this.$('.btn-submit').addClass('disabled');

                this.trigger('beforeSubmit');
                this.$('.msg-container').addClass('hidden');
            
                var serializedForm = this.$('form').serializeObject();
                if(this.checkValidation(serializedForm)){
                    this.$('.btn-submit').removeClass('disabled');
                    return ;
                }

                if (serializedForm['requestDate']) {
                    serializedForm['requestDate'] = serializedForm['requestDate'].replace(/\//g, '-');
                }
                if (serializedForm['dueDate']) {
                    serializedForm['dueDate'] = serializedForm['dueDate'].replace(/\//g, '-');
                }
                if (serializedForm['expireDate']) {
                    serializedForm['expireDate'] = serializedForm['expireDate'].replace(/\//g, '-');
                }

                if (serializedForm['yearBuilt']) {
                    serializedForm['yearBuilt'] = "13" + serializedForm['yearBuilt'];   
                }

                self.model.save(serializedForm, {
                    success: function(model, resp) {     
                        self.model.set(resp,{
                            silent : true
                        });
                        self.$('.msg-container').removeClass('hidden');
                        self.$('.msg-container').removeClass('alert alert-danger');
                        self.$('.msg-container').addClass('alert alert-success');
                        self.$('.msg-container').html('ملک شما در سامانه ثبت شد. لطفا صبر کنید.');
                        self.$('.msg-container').css('visibility', 'visible');
                        self.submitSuccess();
                    /* to reset photo uploader use this
                                    var fileQueueEl = self.$('.photo-uploader');
                                    if(fileQueueEl.length > 0) {
                                        var fileQueue = fileQueueEl.pluploadQueue();
                                        var files = fileQueue.files;
                                        for (file in files) {
                                            fileQueue.removeFile(file.id);
                                        }
                                    }*/
                    },
                   error: function(model, err){
                        self.$('.msg-container').removeClass('hidden');
                        self.$('.msg-container').addClass('alert alert-danger');
                        if (err && err.statusText != undefined && err.responseJSON != undefined) {
                            self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید. ' + err.responseJSON.error == undefined ? err.statusText : err.responseJSON.error);
                        } else {
                            self.$('.msg-container').html('خطایی در سامانه رخ داده است. لطفا مجددا تلاش کنید.');
                        }
                        self.submitFailure(model, err);
                        self.$('.btn-submit').removeClass('disabled');
                        self.$('.msg-container').css('visibility', 'visible');
                    }

                });
            },

            populate : function() {
                var self = this;
                $.each(this.model.attributes, function(key, value) {
                    if(typeof(value) === 'object') {
                        var items = $('form [name="' + key +'"]', self.$el);
                        items.selectpicker('val', value);
                    } else {
                        var item = $('form [name="' + key + '"]', self.$el);
                        if(item.length > 0 && item[0].nodeName.toLowerCase() === 'select'){
                            item.selectpicker('val', value);
                        }else{
                            if(item.length == 1){
                                item.val(value);
                            }
                        }
                    }
                });

                var ownerEl = this.$el.find('.owner-td'),
                agencyEl = this.$el.find('.agency-td');

                if(this.model.get('advertisedBy') == '101') {
                    agencyEl.hide();
                    ownerEl.show();
                } else {
                    agencyEl.show();
                    ownerEl.hide();
                }

                if (this.model.get('agentUserId')) {
                    $('form [name="agentLastname"]', self.$el).val(this.model.get('agentUser').lastname);
                    $('form [name="agencyPhones"]', self.$el).val(this.model.get('agency').phone);
                } else {
                    if (this.model.get('ownerUser')) {
                        $('form [name="ownerLastname"]', self.$el).val(this.model.get('ownerUser').lastname);
                        
                        if (this.model.get('ownerUser').phone) {
                            $('form [name="ownerPhones[]"]', self.$el).val(this.model.get('ownerUser').phone[0]);
                        }
                    }
                }

            },

            selectOnPrss : function(evt) {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                if(key == 9) { //tab 
                    return;
                }
                key = String.fromCharCode( key );

                var regex = /[0-9]|\./;
                if( !regex.test(key) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                } else {
                    var el = $(evt.currentTarget).siblings()[1];
                    $(el).selectpicker('val', key);
                }
            }

        });

        return BaseForm;
    });