<?php
date_default_timezone_set('Asia/Tehran');
ini_set('display_errors', true);
error_reporting(E_ALL);

// Define path to application directory
defined('ROOT_PATH')
    || define('ROOT_PATH', realpath(dirname(__FILE__) . '/..'));

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

function d($var) {
	echo '<pre class="prettyprint">';
	var_dump($var);
	die('</pre><script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?skin=sunburst"></script>');
}

// Setup autoloading
require 'init_autoloader.php';

try { 
\Doctrine\DBAL\Types\Type::addType('set', 'Application\DBAL\Types\SetType');
\Doctrine\DBAL\Types\Type::addType('intarray', 'Application\DBAL\Types\IntArrayType');
} catch (\Exception $e) { }


// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
