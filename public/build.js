({
    appDir: './',
    baseUrl: "./js",
    mainConfigFile: './js/main.js',
    dir:'./release',
    logLevel:0,
    preserveLicenseComments:!1,
    // optimize:"uglify2",
     optimize:"uglify2",
    removeCombined: true,
    findNestedDependencies: true,
    optimizeCss: 'standard',
    waitSeconds: 60,
    // 3rd party script alias names
    paths: {        
        'jquery'                    : 'vendor/jquery/jquery',
        'jquery-ui'                 : 'vendor/jquery-ui/ui/jquery-ui',
        'jquery-values'             : 'vendor/jquery.values/jquery.values',
        'underscore'                : 'vendor/underscore/underscore',
        'backbone'                  : 'vendor/backbone/backbone',
        'text'                      : 'vendor/requirejs-text/text',   
        'backbone.routefilter'      : 'vendor/backbone.routefilter/dist/backbone.routefilter.min',        
        'bootstrap'                 : 'vendor/bootstrap/js/bootstrap',
        'bootbox'                   : 'vendor/bootbox/bootbox',
        'pwt-date'                  : 'vendor/pwt-datepicker/pwt-date',
        'pwt-datepicker'            : 'vendor/pwt-datepicker/pwt-datepicker',
        'bootstrap-select'          : 'vendor/bootstrap-select'
    },

// Sets the use.js configuration for your application
    use: {

         'jquery-ui': {
            deps : [ 'jquery' ]
        },

        'bootstrap' : {
            deps : ['jquery']
        },

        'backbone.routefilter' : {
            deps : ['backbone', 'underscore']
        },

        'pwt-date' : {
            deps :['jquery', 'jquery-ui']
        },
        
        'pwt-datepicker' : {
            deps :['pwt-date']
        },
        
        'underscore': {
            attach: "_" //attaches "_" to the window object
        },
        'backbone': {
            deps : ['underscore', 'jquery'],
            attach: "Backbone"  //attaches "Backbone" to the window object
        }


    }, // end Use.js Configuration
    // Modules to be optimized:
   
   
           name: "main"
   

})
