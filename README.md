Documentation
===============

Send Job to Queue
-----------------

```php
    $jobManager   = $this->getServiceLocator()->get('SlmQueue\Job\JobPluginManager');
    $emailJob = $jobManager->get('Application\Job\SendEmailJob');
    $queueManager = $this->getServiceLocator()->get('SlmQueue\Queue\QueuePluginManager');
    $queue        = $queueManager->get('sendEmailQueue');
    $queue->push($emailJob);
```

Running the Daemon
------------------

php public/index.php queue doctrine stats --start