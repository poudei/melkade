#!/bin/bash

echo "Hello dear, "$USER".  This script will minify your js & css";
echo -n "Enter your version number (e.g. 1.0) and press [ENTER]: "
read version;

cd public;

node r.js -o build.js;

cd release/js;

# on mac you need a backup file for sed, so use ''
sed -i '' 's/define("backbone",function(){})/define("backbone",function(){return window.Backbone;})/g' main.js
sed -i '' 's/define("underscore",function(){})/define("underscore",function(){return window._;})/g' main.js

mv main.js main.$version.js

cd ..
cd css
mv styles.css styles.$version.css

echo "Done!"
echo "Do not forget to change file names in your main index file, js filename is:" main.$version.js "& css:" styles.$version.css
echo ""